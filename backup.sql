-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: db_project
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Detail_Information`
--

DROP TABLE IF EXISTS `Detail_Information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Detail_Information` (
  `ID` varchar(22) NOT NULL,
  `Overview` mediumtext,
  `Purpose` mediumtext,
  `Evaluation_Rate` varchar(50) DEFAULT NULL,
  `TextBook` varchar(100) DEFAULT NULL,
  `Assistant` varchar(10) DEFAULT NULL,
  `As_Email` varchar(50) DEFAULT NULL,
  `Num_Students` int(11) DEFAULT NULL,
  KEY `ID` (`ID`),
  CONSTRAINT `Detail_Information_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `Info_Table` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Detail_Information`
--

LOCK TABLES `Detail_Information` WRITE;
/*!40000 ALTER TABLE `Detail_Information` DISABLE KEYS */;
INSERT INTO `Detail_Information` VALUES ('2014/2-0000-1-0019-01','프로그래밍 언어에 대한 기본적인 이해와 공학적 문제를 컴퓨터 프로그래밍화하는 과정을 익히며, 컴퓨터 프로그래밍을 위한 제어문, 배열, 포인터, 함수, 논리제어, 파일 입출력 등을 이용하여 프로그래밍을 실습한다. 본 교과목에서 사용하는 컴퓨터 언어는 C 프로그래밍언어이다.','1. 모든 프로그램의 기본이 되는 C프로그램을 이해하고 응용할 수 있는 능력을 배양한다. \n2. C 문법과 C 알고리즘을 이해하고 요구된 필요조건에 맞추어 자료구조 및 프로그램구조를 설계하고 프로그래밍 할 수 있는 능력을 배양한다.\n3. C 프로그래밍을 직접 수행해 봄으로써 보다 복잡하고 융합적인 문제를 C프로그래밍을 통하여 해결할 수 있는 능력을 배양한다.','출석,20,수업태도,10,중간고사,20,기말고사,20,발표,0,토의,0,과제,20,Quiz','C언어 기초(개정판),우재남,한빛미디어,2014','','',20),('2014/2-0000-1-0387-01','- 농구의 이론적 지식(탄생배경, 경기규칙및 방식)과 과학적인 원리(슛, 드리블, 패스 등)를 이해하여 실제 수행에 적용할 수 있도록 한다.\n- 농구의 기본기술을 반복 학습을 통하여 실제 수행에 적용할 수 있도록 한다.','- 농구의 이론(탄생배경, 경기규칙및 방식)과 과학적 원리(슛, 드리블, 패스 등)를 이해한다.\n- 농구의 기본기술(슛, 드리블, 패스 등)을 이해한다.\n- 농구의 대인 전술, 팀 전술(수비, 공격)을 이해한다.\n- 농구로 하여금 팀 정신과 sportsmanship을 배운다.\n- 프로농구경기 현장 관람을 통하여 농구의 이해도를 높인다.','출석,30,중간고사,10,기말고사,30,과제보고서,10,수업태도,20,Quiz,0,기타,0','농구바이블,방열,대경북스,2006','','',18),('2014/2-0000-1-0387-02','- 농구의 이론적 지식(탄생배경, 경기규칙및 방식)과 과학적인 원리(슛, 드리블, 패스 등)를 이해하여 실제 수행에 적용할 수 있도록 한다.\n- 농구의 기본기술을 반복 학습을 통하여 실제 수행에 적용할 수 있도록 한다.','- 농구의 이론(탄생배경, 경기규칙및 방식)과 과학적 원리(슛, 드리블, 패스 등)를 이해한다.\n- 농구의 기본기술(슛, 드리블, 패스 등)을 이해한다.\n- 농구의 대인 전술, 팀 전술(수비, 공격)을 이해한다.\n- 농구로 하여금 팀 정신과 sportsmanship을 배운다.','출석,40,중간고사,0,기말고사,30,과제보고서,20,수업태도,10,Quiz,0,기타,0','농구바이블,방열,대경북스,2006','','',19),('2014/2-0000-1-0670-01','인권존중과 법치주의를 기본이념으로 하는 현대국가에서 시민들의 생활은 법률에 의하여 규율된다. 법률에 관한 지식을 갖추는 것은 현대사회에 살고 있는 민주시민의 필수요건이다. 우리생활에서 자주 발생하는 법률문제를 중심으로 재산법, 친족상속법, 주택, 소비, 노동법, 형법 및 헌법의 기초이론, 사례 및 판례 등을 설명하고 이에 대해 학생들이 직접 발표하고 토론함으로써 법률문제에 대한 수강생들의 충분한 이해를 돕고 타인과 사고를 공유하여 보다 보편적이고 타당한 법률적 지식을 얻도록 한다.','1. 법의 개념, 법과 기타 사회규범과의 관계, 법의 목적, 법의 해석 등 법의 일반이론과 개별법 분야의 주요한 개념들을 이해한다.\n2. 법과 관련된 기본적인 지식을 익히고 이를 실생활에서 적용할 수 있는 능력을 배양한다.\n3. 법률문제를 회피하지 않고, 적극적으로 해결하고 간단한 법률문제 등을 상담할 수 있는 능력을 배양시키며, 법과 관련된 문제에 대하여 흥미를 갖도록 한다.\n4. 일상에서 발생하는 법과 연관된 복잡하고 융합적인 문제를 회피하지 않고 직면하는 태도와 그 문제를 해결하는 능력의 기틀을 마련한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','강의자료(ppt 자료-강의자료실에 탑재)','김홍','cjsgkdos@naver.com',95),('2014/2-0000-1-0670-02','인권존중과 법치주의를 기본이념으로 하는 현대국가에서 시민들의 생활은 법률에 의하여 규율된다. 법률에 관한 지식을 갖추는 것은 현대사회에 살고 있는 민주시민의 필수요건이다. 우리생활에서 자주 발생하는 법률문제를 중심으로 재산법, 친족상속법, 주택, 소비, 노동법, 형법 및 헌법의 기초이론, 사례 및 판례 등을 설명하고 이에 대해 학생들이 직접 발표하고 토론함으로써 법률문제에 대한 수강생들의 충분한 이해를 돕고 타인과 사고를 공유하여 보다 보편적이고 타당한 법률적 지식을 얻도록 한다.','1. 법의 개념, 법과 기타 사회규범과의 관계, 법의 목적, 법의 해석 등 법의 일반이론과 개별법 분야의 주요한 개념들을 이해한다.\n2. 법과 관련된 기본적인 지식을 익히고 이를 실생활에서 적용할 수 있는 능력을 배양한다.\n3. 법률문제를 회피하지 않고, 적극적으로 해결하고 간단한 법률문제 등을 상담할 수 있는 능력을 배양시키며, 법과 관련된 문제에 대하여 흥미를 갖도록 한다.\n4. 일상에서 발생하는 법과 연관된 복잡하고 융합적인 문제를 회피하지 않고 직면하는 태도와 그 문제를 해결하는 능력의 기틀을 마련한다.','출석,10,수업태도,20,중간고사,10,기말고사,40,발표,0,토의,0,과제,20,Quiz','강의안,전정화,2014','박세윤','heycover@nate.com',98),('2014/2-0000-1-0670-03','인권존중과 법치주의를 기본이념으로 하는 현대국가에서 시민들의 생활은 법률에 의하여 규율된다. 법률에 관한 지식을 갖추는 것은 현대사회에 살고 있는 민주시민의 필수요건이다. 우리생활에서 자주 발생하는 법률문제를 중심으로 재산법, 친족상속법, 주택, 소비, 노동법, 형법 및 헌법의 기초이론, 사례 및 판례 등을 설명하고 이에 대해 학생들이 직접 발표하고 토론함으로써 법률문제에 대한 수강생들의 충분한 이해를 돕고 타인과 사고를 공유하여 보다 보편적이고 타당한 법률적 지식을 얻도록 한다.','1. 법의 개념, 법과 기타 사회규범과의 관계, 법의 목적, 법의 해석 등 법의 일반이론과 개별법 분야의 주요한 개념들을 이해한다.\n2. 법과 관련된 기본적인 지식을 익히고 이를 실생활에서 적용할 수 있는 능력을 배양한다.\n3. 법률문제를 회피하지 않고, 적극적으로 해결하고 간단한 법률문제 등을 상담할 수 있는 능력을 배양시키며, 법과 관련된 문제에 대하여 흥미를 갖도록 한다.\n4. 일상에서 발생하는 법과 연관된 복잡하고 융합적인 문제를 회피하지 않고 직면하는 태도와 그 문제를 해결하는 능력의 기틀을 마련한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','강의자료(ppt 자료-강의자료실에 탑재)','강영은','bieu@naver.com',99),('2014/2-0000-1-0670-04','인권존중과 법치주의를 기본이념으로 하는 현대국가에서 시민들의 생활은 법률에 의하여 규율된다. 법률에 관한 지식을 갖추는 것은 현대사회에 살고 있는 민주시민의 필수요건이다. 우리생활에서 자주 발생하는 법률문제를 중심으로 재산법, 친족상속법, 주택, 소비, 노동법, 형법 및 헌법의 기초이론, 사례 및 판례 등을 설명하고 이에 대해 학생들이 직접 발표하고 토론함으로써 법률문제에 대한 수강생들의 충분한 이해를 돕고 타인과 사고를 공유하여 보다 보편적이고 타당한 법률적 지식을 얻도록 한다.','1. 법의 개념, 법과 기타 사회규범과의 관계, 법의 목적, 법의 해석 등 법의 일반이론과 개별법 분야의 주요한 개념들을 이해한다.\n2. 법과 관련된 기본적인 지식을 익히고 이를 실생활에서 적용할 수 있는 능력을 배양한다.\n3. 법률문제를 회피하지 않고, 적극적으로 해결하고 간단한 법률문제 등을 상담할 수 있는 능력을 배양시키며, 법과 관련된 문제에 대하여 흥미를 갖도록 한다.\n4. 일상에서 발생하는 법과 연관된 복잡하고 융합적인 문제를 회피하지 않고 직면하는 태도와 그 문제를 해결하는 능력의 기틀을 마련한다.','출석,20,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,0,Quiz,','현대인의 법과 생활,이보영,동방문화사,2013','','',100),('2014/2-0000-1-0670-05','인권존중과 법치주의를 기본이념으로 하는 현대국가에서 시민들의 생활은 법률에 의하여 규율된다. 법률에 관한 지식을 갖추는 것은 현대사회에 살고 있는 민주시민의 필수요건이다. 우리생활에서 자주 발생하는 법률문제를 중심으로 재산법, 친족상속법, 주택, 소비, 노동법, 형법 및 헌법의 기초이론, 사례 및 판례 등을 설명하고 이에 대해 학생들이 직접 발표하고 토론함으로써 법률문제에 대한 수강생들의 충분한 이해를 돕고 타인과 사고를 공유하여 보다 보편적이고 타당한 법률적 지식을 얻도록 한다.','1. 법의 개념, 법과 기타 사회규범과의 관계, 법의 목적, 법의 해석 등 법의 일반이론과 개별법 분야의 주요한 개념들을 이해한다.\n2. 법과 관련된 기본적인 지식을 익히고 이를 실생활에서 적용할 수 있는 능력을 배양한다.\n3. 법률문제를 회피하지 않고, 적극적으로 해결하고 간단한 법률문제 등을 상담할 수 있는 능력을 배양시키며, 법과 관련된 문제에 대하여 흥미를 갖도록 한다.\n4. 일상에서 발생하는 법과 연관된 복잡하고 융합적인 문제를 회피하지 않고 직면하는 태도와 그 문제를 해결하는 능력의 기틀을 마련한다.','출석,26,수업태도,10,중간고사,30,기말고사,30,발표,4,토의,0,과제,0,Quiz,','생활과 법률,이환경,형설,2013','최명순','adare@hanmail.net',97),('2014/2-0000-1-0709-01','볼링은 현대스포츠의 한 종목으로 남녀노소 즐길수 있다. 볼링은 규정된 길이와 너비를 가진 레인에 세워진 10개의 핀을 비금속성 공을 굴려서 쓰러뜨리는 실내경기를 말한다.','볼링을 처음 시작하는 광운대학교 학생들에게 볼링에 대한 예절, 기본 장비, 점수 계산법, 경기 방식에 대한 정보를 제공하고 기술 동작을 반복하여 연습함으로써 심신 건강 유지 및 증진을 도모할뿐만아니라 여가생활에 적용할 수 있는 능력을 배양하고자 한다.','출석,40,중간고사,30,기말고사,30,과제보고서,0,수업태도,0,Quiz,0,기타,0','볼링(제대로 배우기),김일곤, 이규승, 이종호, 김영미 공저,대경북스','','',19),('2014/2-0000-1-0709-02','볼링은 현대스포츠의 한 종목으로 남녀노소 즐길수 있다. 볼링은 규정된 길이와 너비를 가진 레인에 세워진 10개의 핀을 비금속성 공을 굴려서 쓰러뜨리는 실내경기를 말한다.','볼링을 처음 시작하는 광운대학교 학생들에게 볼링에 대한 예절, 기본 장비, 점수 계산법, 경기 방식에 대한 정보를 제공하고 기술 동작을 반복하여 연습함으로써 심신 건강 유지 및 증진을 도모할뿐만아니라 여가생활에 적용할 수 있는 능력을 배양하고자 한다.','출석,40,중간고사,30,기말고사,30,과제보고서,0,수업태도,0,Quiz,0,기타,0','볼링(제대로 배우기),김일곤, 이규승, 이종호, 김영미 공저,대경북스','','',19),('2014/2-0000-1-0806-01','본 강좌는 경제학 전공자 또는 비전공자를 대상으로 경제현상을 이해하는데 필요한 이론적 개념을 설명하고, 세계화, 정보화시대의 복잡한 경제 환경 속에서 바람직한 경제생활을 영위하기 위하여 다양한 경제문제를 실제 경제생활과 연결시켜 쉽게 이해할 수 있도록 한다. 본 교과목이 다루는 분야는 미시경제, 기업과 경영, 거시경제, 국제경제, 환경경제학 등의 분야로 나눠지는데, 미시경제와 거시경제를 주로 다루고 이어서 국제경제 분야에 대한 개관이 이루어진다. 수업은 강의 뿐만 아니라 학생들이 직접 경제문제에 대해 발표하고 토론하는 수업진행방식을 포함한다.','1. 복잡한 현실 경제의 여러 가지 현상들을 이해하고 분석할 수 있는 역량을 배양한다. \n2. 경제신문을 이해하고 분석하여 실생활에 적용할 수 있는 수준까지 수강자의 경제지식을 향상시킨다.\n3. 학생들이 직접 발표하고 토론함으로써 자신의 의견에 대한 타인의 생각을 공유하고 이를 바탕으로 경제 관련 복합적이고 융합적인 문제들을 보다 합리적으로 해결할 수 있는 능력의 기틀을 마련한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','경제와 생활,김기영외4인,두남출판사,2006','임종훈','jonghun92@naver.com',68),('2014/2-0000-1-0806-02','본 강좌는 경제학 전공자 또는 비전공자를 대상으로 경제현상을 이해하는데 필요한 이론적 개념을 설명하고, 세계화, 정보화시대의 복잡한 경제 환경 속에서 바람직한 경제생활을 영위하기 위하여 다양한 경제문제를 실제 경제생활과 연결시켜 쉽게 이해할 수 있도록 한다. 본 교과목이 다루는 분야는 미시경제, 기업과 경영, 거시경제, 국제경제, 환경경제학 등의 분야로 나눠지는데, 미시경제와 거시경제를 주로 다루고 이어서 국제경제 분야에 대한 개관이 이루어진다. 수업은 강의 뿐만 아니라 학생들이 직접 경제문제에 대해 발표하고 토론하는 수업진행방식을 포함한다.','1. 복잡한 현실 경제의 여러 가지 현상들을 이해하고 분석할 수 있는 역량을 배양한다. \n2. 경제신문을 이해하고 분석하여 실생활에 적용할 수 있는 수준까지 수강자의 경제지식을 향상시킨다.\n3. 학생들이 직접 발표하고 토론함으로써 자신의 의견에 대한 타인의 생각을 공유하고 이를 바탕으로 경제 관련 복합적이고 융합적인 문제들을 보다 합리적으로 해결할 수 있는 능력의 기틀을 마련한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,10,Quiz','경제의 이해,김기영 외 2인 공저,두남,2014','','',37),('2014/2-0000-1-0806-03','본 강좌는 경제학 전공자 또는 비전공자를 대상으로 경제현상을 이해하는데 필요한 이론적 개념을 설명하고, 세계화, 정보화시대의 복잡한 경제 환경 속에서 바람직한 경제생활을 영위하기 위하여 다양한 경제문제를 실제 경제생활과 연결시켜 쉽게 이해할 수 있도록 한다. 본 교과목이 다루는 분야는 미시경제, 기업과 경영, 거시경제, 국제경제, 환경경제학 등의 분야로 나눠지는데, 미시경제와 거시경제를 주로 다루고 이어서 국제경제 분야에 대한 개관이 이루어진다. 수업은 강의 뿐만 아니라 학생들이 직접 경제문제에 대해 발표하고 토론하는 수업진행방식을 포함한다.','1. 복잡한 현실 경제의 여러 가지 현상들을 이해하고 분석할 수 있는 역량을 배양한다. \n2. 경제신문을 이해하고 분석하여 실생활에 적용할 수 있는 수준까지 수강자의 경제지식을 향상시킨다.\n3. 학생들이 직접 발표하고 토론함으로써 자신의 의견에 대한 타인의 생각을 공유하고 이를 바탕으로 경제 관련 복합적이고 융합적인 문제들을 보다 합리적으로 해결할 수 있는 능력의 기틀을 마련한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','경제와 생활,김기영외 5인,두남출판사,2007','임종훈','jonghun92@naver.com',64),('2014/2-0000-1-0806-04','본 강좌는 경제학 전공자 또는 비전공자를 대상으로 경제현상을 이해하는데 필요한 이론적 개념을 설명하고, 세계화, 정보화시대의 복잡한 경제 환경 속에서 바람직한 경제생활을 영위하기 위하여 다양한 경제문제를 실제 경제생활과 연결시켜 쉽게 이해할 수 있도록 한다. 본 교과목이 다루는 분야는 미시경제, 기업과 경영, 거시경제, 국제경제, 환경경제학 등의 분야로 나눠지는데, 미시경제와 거시경제를 주로 다루고 이어서 국제경제 분야에 대한 개관이 이루어진다. 수업은 강의 뿐만 아니라 학생들이 직접 경제문제에 대해 발표하고 토론하는 수업진행방식을 포함한다.','1. 복잡한 현실 경제의 여러 가지 현상들을 이해하고 분석할 수 있는 역량을 배양한다. \n2. 경제신문을 이해하고 분석하여 실생활에 적용할 수 있는 수준까지 수강자의 경제지식을 향상시킨다.\n3. 학생들이 직접 발표하고 토론함으로써 자신의 의견에 대한 타인의 생각을 공유하고 이를 바탕으로 경제 관련 복합적이고 융합적인 문제들을 보다 합리적으로 해결할 수 있는 능력의 기틀을 마련한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,5,토의,5,과제,10,Quiz','경제학 낙서,이호섭,청람,2012','','',53),('2014/2-0000-1-0858-01','수영을 통해 물에대한 두려움을 없애고 체력을 향상시킨다.\n\n수업장소: 성북종합레포츠타운(서울특별시 성북구 화랑로18자길 13, 전화:02-962-2082)\n\n수업시간: 9월11일~10월30일 매주-화&목요일19:00~20:00시\n9월-11일, 16일,18일, 23일, 25일, 30일\n10월-2일,7일, 14일,16일, 21일,23일, 28일, 30일(시험)\n휴강-9월9일(추석), 10월9일(한글날)\n\n비용: 일 입장료5000원 개인부담, 매 수업마다 수영장에서 구입하여 입장\n\n준비물: 수영복, 수모, 수경, 샤워도구, 수건\n\n출석30%\n실기시험 40% \n태도 30% \n\n출석: 1번 결석 시3점 감점, 지각1번 1점 감점, 4번 이상 결석 F학점 \n\n실기시험: 수준에 맞는 시험(2분반)\n태도: 적극적으로 참여정도','수영에 대한 흥미유발과 영법에 대한 교정과 실력을 향상시킨다','출석,30,중간고사,0,기말고사,40,과제보고서,0,수업태도,30,Quiz,0,기타,0','경제학 낙서,이호섭,청람,2012','','',15),('2014/2-0000-1-0858-02','수영을 통해 물에대한 두려움을 없애고 체력을 향상시킨다.\n\n수업장소: 성북종합레포츠타운(서울특별시 성북구 화랑로18자길 13, 전화:02-962-2082)\n\n수업시간: 9월11일~10월30일 매주-화&목요일19:00~20:00시\n9월-11일, 16일,18일, 23일, 25일, 30일\n10월-2일,7일, 14일,16일, 21일,23일, 28일, 30일(시험)\n휴강-9월9일(추석), 10월9일(한글날)\n\n비용: 일 입장료5000원 개인부담, 매 수업마다 수영장에서 구입하여 입장\n\n준비물: 수영복, 수모, 수경, 샤워도구, 수건\n\n출석30%\n실기시험 40% \n태도 30% \n\n출석: 1번 결석 시3점 감점, 지각1번 1점 감점, 4번 이상 결석 F학점 \n\n실기시험: 수준에 맞는 시험(2분반)\n태도: 적극적으로 참여정도','수영에 대한 흥미유발과 영법에 대한 교정과 실력을 향상시킨다','출석,30,중간고사,0,기말고사,40,과제보고서,0,수업태도,30,Quiz,0,기타,0','경제학 낙서,이호섭,청람,2012','','',14),('2014/2-0000-1-1077-01','This is a conversation course designed to develop and improve speaking and listening skills through role-playing, listening tasks and pair and group activities.The primary goals will be to: 1. Increase your basic English vocabulary, 2. Improve your ability in everyday English conversation, 3. Develop your listening skills, 4. Learn correct English pronunciation, and 5. Enhance your boldness in speaking English.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes, and to help them become more confident in speaking and using English in everyday life. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend every class and to actively participate in class speaking activities.','출석,10,중간고사,30,기말고사,30,과제보고서,15,수업태도,0,Quiz,0,기타,15','American English File 2,Clive Oxenden, Christina Latham-Koenig,Oxford,2008','','',19),('2014/2-0000-1-1077-02','This is a conversation course designed to develop and improve speaking and listening skills through role-playing, listening tasks and pair and group activities.The primary goals will be to: 1. Increase your basic English vocabulary, 2. Improve your ability in everyday English conversation, 3. Develop your listening skills, 4. Learn correct English pronunciation, and 5. Enhance your boldness in speaking English.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes, and to help them become more confident in speaking and using English in everyday life. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend every class and to actively participate in class speaking activities.','출석,10,중간고사,30,기말고사,30,과제보고서,15,수업태도,0,Quiz,0,기타,15','American English File 2,Clive Oxenden, Christina Latham-Koenig,Oxford,2008','','',20),('2014/2-0000-1-1077-03','This is a conversation course designed to develop and improve speaking and listening skills through role-playing, listening tasks and pair and group activities.The primary goals will be to: 1. Increase your basic English vocabulary, 2. Improve your ability in everyday English conversation, 3. Develop your listening skills, 4. Learn correct English pronunciation, and 5. Enhance your boldness in speaking English.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes, and to help them become more confident in speaking and using English in everyday life. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend every class and to actively participate in class speaking activities.','출석,10,중간고사,30,기말고사,30,과제보고서,15,수업태도,0,Quiz,0,기타,15','American English File 2,Clive Oxenden, Christina Latham-Koenig,Oxford,2008','','',20),('2014/2-0000-1-1077-04','This is a conversation class in which students will be expected to speak with each other in English using model dialogues, simple question and answer exercises, and other communicative activities. The students will also watch videos and interact with the professor in order to better understand contextualized, practical English conversation.','The primary goal of this class is to help students develop their speaking ability and confidence in using English as a communicative tool. The secondary goals include expanding the students’ knowledge in English vocabulary and grammar, alongside improving reading, analytical, and discussion skills.','출석,27,중간고사,0,기말고사,33,과제보고서,0,수업태도,20,Quiz,0,기타,20','American English File 2,Oxford University Press.,Oxenden, Clive; Latham-Koenig, Christina.,2008','','',20),('2014/2-0000-1-1077-05','The purpose of the class is not only to improve the students English knowledge but also to help them use that knowledge to express themselves effectively in various situations. It is hoped they will gain confidence in using English to communicate their ideas and emotions naturally and clearly.','','출석,5,중간고사,20,기말고사,25,과제보고서,25,수업태도,0,Quiz,25,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',19),('2014/2-0000-1-1077-06','This is a conversation-oriented class in which students will be expected and encouraged to speak with each other in English. Students will participate in communicative activities, such as model dialogues, question-and-answer exercises, and brief presentations. They will also have to do a group project.','The purpose of the class is not only to improve the students English knowledge but also to help them use that knowledge to express themselves effectively in various situations. It is hoped they will gain confidence in using English to communicate their ideas and emotions naturally and clearly.','출석,5,중간고사,20,기말고사,25,과제보고서,25,수업태도,0,Quiz,25,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',20),('2014/2-0000-1-1077-07','This is a conversation class in which students will be expected and encouraged to speak with each other in English using model dialogs, simple question and answer exercises, and other communicative activities. Most of the lectures and activities in the class will be based on the main textbook. There will be some listening activities, and homework assignments.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend class every day and to actively participate in class speaking activities.','출석,0,중간고사,30,기말고사,30,과제보고서,30,수업태도,0,Quiz,10,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',18),('2014/2-0000-1-1077-08','This is a conversation class in which students will be expected and encouraged to speak with each other in English using model dialogs, simple question and answer exercises, and other communicative activities. Most of the lectures and activities in the class will be based on the main textbook. There will be some listening activities, and homework assignments.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend class every day and to actively participate in class speaking activities.','출석,0,중간고사,30,기말고사,30,과제보고서,30,수업태도,0,Quiz,10,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',18),('2014/2-0000-1-1077-09','This is a conversation class in which students will be expected and encouraged to speak with each other in English using model dialogs, simple question and answer exercises, and other communicative activities. Most of the lectures and activities in the class will be based on the main textbook. There will be some listening activities, and homework assignments.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend class every day and to actively participate in class speaking activities.','출석,0,중간고사,30,기말고사,30,과제보고서,30,수업태도,0,Quiz,10,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',20),('2014/2-0000-1-1077-10','This is a conversation class in which students will be expected to speak with each other in English using model dialogues, simple question and answer exercises, and other communicative activities. The students will also watch videos and interact with the professor in order to better understand contextualized, practical English conversation.','The primary goal of this class is to help students develop their speaking ability and confidence in using English as a communicative tool. The secondary goals include expanding the students’ knowledge in English vocabulary and grammar, alongside improving reading, analytical, and discussion skills.','출석,27,중간고사,0,기말고사,33,과제보고서,0,수업태도,20,Quiz,0,기타,20','American English File 2,Oxford University Press.,Oxenden, Clive; Latham-Koenig, Christina.,2008','','',18),('2014/2-0000-1-1077-11','This is a conversation class in which students will be expected to speak with each other in English using model dialogues, simple question and answer exercises, and other communicative activities. The students will also watch videos and interact with the professor in order to better understand contextualized, practical English conversation.','The primary goal of this class is to help students develop their speaking ability and confidence in using English as a communicative tool. The secondary goals include expanding the students’ knowledge in English vocabulary and grammar, alongside improving reading, analytical, and discussion skills.','출석,27,중간고사,0,기말고사,33,과제보고서,0,수업태도,20,Quiz,0,기타,20','American English File 2,Oxford University Press,Oxenden, Clive; Latham-Koenig, Christina.,2008','','',20),('2014/2-0000-1-1077-12','This is a conversation class in which students will be expected and encouraged to speak with each other in English using model dialogs, simple question and answer exercises, and other communicative activities. Most of the lectures and activities in the class will be based on the main textbook. There will be some listening activities, and homework assignments.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend class every day and to actively participate in class speaking activities.','출석,15,중간고사,25,기말고사,30,과제보고서,15,수업태도,15,Quiz,0,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',20),('2014/2-0000-1-1077-13','This is a conversation class in which students will be expected and encouraged to speak with each other in English using model dialogs, simple question and answer exercises, and other communicative activities. Most of the lectures and activities in the class will be based on the main textbook. There will be some listening activities, and homework assignments.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend class every day and to actively participate in class speaking activities.','출석,0,중간고사,30,기말고사,30,과제보고서,30,수업태도,0,Quiz,10,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',15),('2014/2-0000-1-1077-14','This is a conversation class in which students will be expected and encouraged to speak with each other in English using model dialogs, simple question and answer exercises, and other communicative activities. Most of the lectures and activities in the class will be based on the main textbook. There will be some listening activities, and homework assignments.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend class every day and to actively participate in class speaking activities.','출석,15,중간고사,25,기말고사,30,과제보고서,15,수업태도,15,Quiz,0,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',17),('2014/2-0000-1-1077-15','This is a conversation class in which students will be expected and encouraged to speak with each other in English using model dialogs, simple question and answer exercises, and other communicative activities. Most of the lectures and activities in the class will be based on the main textbook. There will be some listening activities and homework assignments.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend class every day and to actively participate in class speaking activities.','출석,0,중간고사,30,기말고사,30,과제보고서,30,수업태도,0,Quiz,10,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',20),('2014/2-0000-1-1077-16','This is a conversation course designed to develop and improve speaking and listening skills through role-playing, listening tasks and pair and group activities.The primary goals will be to: 1. Increase your basic English vocabulary, 2. Improve your ability in everyday English conversation, 3. Develop your listening skills, 4. Learn correct English pronunciation, and 5. Enhance your boldness in speaking English.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes, and to help them become more confident in speaking and using English in everyday life. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend every class and to actively participate in class speaking activities.','출석,10,중간고사,30,기말고사,30,과제보고서,15,수업태도,0,Quiz,0,기타,15','American English File 2,Clive Oxenden, Christina Latham-Koenig,Oxford,2008','','',20),('2014/2-0000-1-1077-17','This is a conversation-oriented class in which students will be expected and encouraged to speak with each other in English. Students will participate in communicative activities, such as model dialogues, question-and-answer exercises, and brief presentations. They will also have to do a group project.','The purpose of the class is not only to improve the students English knowledge but also to help them use that knowledge to express themselves effectively in various situations. It is hoped they will gain confidence in using English to communicate their ideas and emotions naturally and clearly.','출석,5,중간고사,20,기말고사,25,과제보고서,25,수업태도,0,Quiz,25,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',20),('2014/2-0000-1-1077-18','This is a conversation class in which students will be expected and encouraged to speak with each other in English using model dialogs, simple question and answer exercises, and other communicative activities. Most of the lectures and activities in the class will be based on the main textbook. There will be some listening activities, and homework assignments.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend class every day and to actively participate in class speaking activities.','출석,0,중간고사,30,기말고사,30,과제보고서,30,수업태도,0,Quiz,10,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',17),('2014/2-0000-1-1077-19','This is a conversation class in which students will be expected and encouraged to speak with each other in English using model dialogs, simple question and answer exercises, and other communicative activities. Most of the lectures and activities in the class will be based on the main textbook. There will be some listening activities, and homework assignments.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend class every day and to actively participate in class speaking activities.','출석,15,중간고사,25,기말고사,30,과제보고서,15,수업태도,15,Quiz,0,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',12),('2014/2-0000-1-1077-20','This is a conversation class in which students will be expected and encouraged to speak with each other in English using model dialogs, simple question and answer exercises, and other communicative activities. Most of the lectures and activities in the class will be based on the main textbook. There will be some listening activities, and homework assignments.','The purpose of the course is to better enhance students’ general English competence for taking more specified upper-level classes. For that, students will be expected to do their homework on their own and the homework should be done before coming to class. Students will also be expected to attend class every day and to actively participate in class speaking activities.','출석,0,중간고사,30,기말고사,30,과제보고서,30,수업태도,0,Quiz,10,기타,0','American English File – Student Book 2,Clive Oxenden, Christina Latham-Koenig, and Paul Seligson,Oxf','','',17),('2014/2-0000-1-1077-21','This is a conversation-oriented class in which students will be expected and encouraged to speak with each other in English. Students will participate in communicative activities, such as model dialogues, question-and-answer exercises, and brief presentations.','The purpose of the class is not only to improve the students English knowledge but also to help them use that knowledge to express themselves effectively in various situations. It is hoped they will gain confidence in using English to communicate their ideas and emotions naturally and clearly.','출석,5,중간고사,20,기말고사,40,과제보고서,20,수업태도,15,Quiz,0,기타,0','To be announced in first class','','',20),('2014/2-0000-1-1618-01','축구는 특별한 기술이나 까다로운 규칙이 없고, 시간적이나 공간적으로 제약이 덜하며 공 하나만으로 남녀노소가 손쉽게 즐길 수 있는 운동이다. 전세계적으로 가장 큰 인기를 얻고 있는 스포츠이기도 하다. 특히 축구는 단체운동이기 때문에 동료와의 협동에 의해 상대방과 경쟁하는 스포츠이기도 하다. 따라서 공에 익숙해지기 위해 기본기술을 습득한 후 경기에 필요한 전술을 익혀 모두가 함께 할 수 있는 경기력을 배양하고 이를 토대로 건전한 신체발달을 도모할 수 있다.','미래의 일꾼인 대학생들의 체력향상과 기술습득을 통해 팀 구성원간의 협동심을 배양하고, 이를 통해 사회성을 기르는데 교육목표를 둔다.','출석,25,중간고사,20,기말고사,25,과제보고서,20,수업태도,10,Quiz,0,기타,0','축구 100% 즐기기,송강영 외 옮김,대한미디어,2001','','',18),('2014/2-0000-1-1618-02','축구는 특별한 기술이나 까다로운 규칙이 없고, 시간적이나 공간적으로 제약이 덜하며 공 하나만으로 남녀노소가 손쉽게 즐길 수 있는 운동이다. 전세계적으로 가장 큰 인기를 얻고 있는 스포츠이기도 하다. 특히 축구는 단체운동이기 때문에 동료와의 협동에 의해 상대방과 경쟁하는 스포츠이기도 하다. 따라서 공에 익숙해지기 위해 기본기술을 습득한 후 경기에 필요한 전술을 익혀 모두가 함께 할 수 있는 경기력을 배양하고 이를 토대로 건전한 신체발달을 도모할 수 있다.','미래의 일꾼인 대학생들의 체력향상과 기술습득을 통해 팀 구성원간의 협동심을 배양하고, 이를 통해 사회성을 기르는데 교육목표를 둔다.','출석,25,중간고사,20,기말고사,25,과제보고서,20,수업태도,10,Quiz,0,기타,0','축구 100% 즐기기,송강영 외 옮김,대한미디어,2001','','',17),('2014/2-0000-1-1659-01','사용자가 컴퓨터와 소통하는 매체가 컴퓨터 언어이다. 본 교과목에서는 이 컴퓨터 언어의 기본 개념과 이를 사용하는 방법을 익힌다. 기본 자료형과 제어구문, 함수와 모듈을 통해서 컴퓨터 언어의 기초를 익히고, 클래스와 객체지향 프로그래밍에 대한 내용도 함께 익힌다. 또한 비 전공자가 일상생활에서 접하는 문제를 간단히 프로그래밍 언어로 해결하는 방법을 배운다. 이를 위해 특정 컴퓨터 언어를 선택하여 그 언어가 갖고 있는 논리성을 기반으로 교과목이 운영된다.\n\nThis lecture deals with the concept of a computer programming language and how to program with it. The lecture includes data types, control structures, functions and modules. It also covers the concept of class and object oriented programming. Students can learn how to solve problems and how to process information with computer programming languages.','1. 컴퓨터 언어의 특수성을 이해하고 그 언어가 가지고 있는 논리성을 통해 컴퓨터에 명령을 내리는 논리적 체계를 습득한다.\n2. 특정 컴퓨터 언어의 프로그래밍 방법을 습득하고 직접 프로그래밍을 수행해 봄으로써 비전공자라도 일상생활에서 발생하는 문제를 컴퓨터 언어를 사용하여 컴퓨터로 간략히 해결하는 능력을 배양한다.\n3. 객체 지향의 개념을 이해하여 이를 활용하는 능력을 키운다.\n4. 컴퓨터 언어를 사용하여 복잡하고 융합적인 문제를 해결하는 기반을 다진다. \n\n1. Students are to understand the characteristics of computer programming languages and to learn how to use them.\n2. Students, no matter if they are majoring in computer science or not, learn how to solve problems by themselves, not using special software tools.\n3. Students are to understand the concept of Object Oriented Programming.\n4. This class will provide students the basic ideas and the ability for solving complex problems using programming languages.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','파이썬 바이블,이강성,프리렉,2013','유지향','yujihyang18@naver.com',19),('2014/2-0000-1-1659-02','사용자가 컴퓨터와 소통하는 매체가 컴퓨터 언어이다. 본 교과목에서는 이 컴퓨터 언어의 기본 개념과 이를 사용하는 방법을 익힌다. 기본 자료형과 제어구문, 함수와 모듈을 통해서 컴퓨터 언어의 기초를 익히고, 클래스와 객체지향 프로그래밍에 대한 내용도 함께 익힌다. 또한 비 전공자가 일상생활에서 접하는 문제를 간단히 프로그래밍 언어로 해결하는 방법을 배운다. 이를 위해 특정 컴퓨터 언어를 선택하여 그 언어가 갖고 있는 논리성을 기반으로 교과목이 운영된다.\n\nThis lecture deals with the concept of a computer programming language and how to program with it. The lecture includes data types, control structures, functions and modules. It also covers the concept of class and object oriented programming. Students can learn how to solve problems and how to process information with computer programming languages.','1. 컴퓨터 언어의 특수성을 이해하고 그 언어가 가지고 있는 논리성을 통해 컴퓨터에 명령을 내리는 논리적 체계를 습득한다.\n2. 특정 컴퓨터 언어의 프로그래밍 방법을 습득하고 직접 프로그래밍을 수행해 봄으로써 비전공자라도 일상생활에서 발생하는 문제를 컴퓨터 언어를 사용하여 컴퓨터로 간략히 해결하는 능력을 배양한다.\n3. 객체 지향의 개념을 이해하여 이를 활용하는 능력을 키운다.\n4. 컴퓨터 언어를 사용하여 복잡하고 융합적인 문제를 해결하는 기반을 다진다. \n\n1. Students are to understand the characteristics of computer programming languages and to learn how to use them.\n2. Students, no matter if they are majoring in computer science or not, learn how to solve problems by themselves, not using special software tools.\n3. Students are to understand the concept of Object Oriented Programming.\n4. This class will provide students the basic ideas and the ability for solving complex problems using programming languages.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','파이썬 바이블,이강성,프리렉,2013','유지향','yujihyang18@naver.com',20),('2014/2-0000-1-1679-01','탁구는 좁은 공간에서도 쉽게 건강과 체력을 유지할 수 있고 남녀 노소 구분없이 각자의 수준에 따라 즐길 수 있는 여가 스포츠이다. 강의내용은 탁구 경기의 유래와 특성,경기개요 등 탁구에 관한 전반적인 지식에서부터 단식과 복식경기를 할 수 있는 경기 기능의 습득까지 포함된다. 나아가 평생 스포츠로 즐길 수 있는 기회를 제공한다.','탁구의 경기개요 및 전반적인 지식을 습득한다.\n기본 기술을 구사할 수 있다. \n단식 및 복식경기를 즐길 수 있다.','출석,20,중간고사,25,기말고사,30,과제보고서,15,수업태도,10,Quiz,0,기타,0','현정화의 퍼팩트 탁구,현정화,삼호미디어,2012','','',18),('2014/2-0000-1-1679-02','탁구는 좁은 공간에서도 쉽게 건강과 체력을 유지할 수 있고 남녀 노소 구분없이 각자의 수준에 따라 즐길 수 있는 여가 스포츠이다. 강의내용은 탁구 경기의 유래와 특성,경기개요 등 탁구에 관한 전반적인 지식에서부터 단식과 복식경기를 할 수 있는 경기 기능의 습득까지 포함된다. 나아가 평생 스포츠로 즐길 수 있는 기회를 제공한다.','탁구의 경기개요 및 전반적인 지식을 습득한다.\n기본 기술을 구사할 수 있다. \n단식 및 복식경기를 즐길 수 있다.','출석,20,중간고사,25,기말고사,30,과제보고서,15,수업태도,10,Quiz,0,기타,0','현정화의 퍼팩트 탁구,현정화,삼호미디어,2012','','',18),('2014/2-0000-1-1794-01','한국문화사는 한국의 역사적 문화적 정체성을 확인하는 강좌이다. \n한국사회를 구성하는 역사적 힘들이 어떻게 형성되었으며 어떤 과정을 거쳐 오늘날과 같은 모습을 띠게 되었는지를 고대로부터 고려시대, 조선시대를 거쳐 근현대에 이르기까지 개괄적으로 살펴본다.\n고구려, 백제, 신라의 형성, 삼국통일의 문화적 의미, 고려와 발해, 조선의 건국과 문화발달, 개항기와 일제강점기의 문화변동 등을 포함하는 한국사에 대한 통사적 접근을 통해 한국문화의 원류를 파악한다.','1. 한국사의 각 국면에서 문화적 지속 또는 변화를 야기한 주된 원인이 무엇이었는지 파악한다. \n2. 한국사의 독특성과 한국문화의 역사적 기원에 대해 이해하고 세계화·다문화시대에 그것이 어떤 의미를 갖는지 이해한다. \n3. 우리나라의 문화 정체성을 확인하고 나아가 우리의 문화가 세계화시대에 글로벌화 될 가능성을 조망한다.\n4. 문화에 대한 역사적 배경과 원인을 파악하여 문화를 창조하는 문화창조인으로서의 자질을 함양한다.','출석,20,수업태도,0,중간고사,20,기말고사,25,발표,25,토의,10,과제,0,Quiz','수업중 강의노트 및 동영상,사진','','',94),('2014/2-0000-1-1794-02','한국문화사는 한국의 역사적 문화적 정체성을 확인하는 강좌이다. \n한국사회를 구성하는 역사적 힘들이 어떻게 형성되었으며 어떤 과정을 거쳐 오늘날과 같은 모습을 띠게 되었는지를 고대로부터 고려시대, 조선시대를 거쳐 근현대에 이르기까지 개괄적으로 살펴본다.\n고구려, 백제, 신라의 형성, 삼국통일의 문화적 의미, 고려와 발해, 조선의 건국과 문화발달, 개항기와 일제강점기의 문화변동 등을 포함하는 한국사에 대한 통사적 접근을 통해 한국문화의 원류를 파악한다.','1. 한국사의 각 국면에서 문화적 지속 또는 변화를 야기한 주된 원인이 무엇이었는지 파악한다. \n2. 한국사의 독특성과 한국문화의 역사적 기원에 대해 이해하고 세계화·다문화시대에 그것이 어떤 의미를 갖는지 이해한다. \n3. 우리나라의 문화 정체성을 확인하고 나아가 우리의 문화가 세계화시대에 글로벌화 될 가능성을 조망한다.\n4. 문화에 대한 역사적 배경과 원인을 파악하여 문화를 창조하는 문화창조인으로서의 자질을 함양한다.','출석,20,수업태도,0,중간고사,20,기말고사,25,발표,25,토의,10,과제,0,Quiz','수업중 강의노트 및 동영상,사진','','',72),('2014/2-0000-1-2238-01','스케이팅의 기본자세와 직선활주 및 코너활주에 대하여 집중적으로 배운다.','스케이트는 유산소 운동으로 근력증가 등을 통한 신체적, 정신적, 사회적 건강을 도모하는 동계올림픽의 종목으로써 스케이팅의 기술을 습득하여 평생스포츠로 빙상을 선택할 수 있는 동기를 제공한다.','출석,40,중간고사,25,기말고사,25,과제보고서,0,수업태도,10,Quiz,0,기타,0','수업중 강의노트 및 동영상,사진','','',17),('2014/2-0000-1-2238-02','스케이팅의 기본자세와 직선활주 및 코너활주에 대하여 집중적으로 배운다.','스케이트는 유산소 운동으로 근력증가 등을 통한 신체적, 정신적, 사회적 건강을 도모하는 동계올림픽의 종목으로써 스케이팅의 기술을 습득하여 평생스포츠로 빙상을 선택할 수 있는 동기를 제공한다.','출석,40,중간고사,25,기말고사,25,과제보고서,0,수업태도,10,Quiz,0,기타,0','수업중 강의노트 및 동영상,사진','','',19),('2014/2-0000-1-2337-01','우리는 언어가 물, 공기, 불 등과 같이 우리 주변에서 흔히 접할 수 있어 언어를 잘 알고 있다고 생각한다. 그러나 정작 언어가 무엇이냐고 물으면 혹은 자기가 알고 있는 언어를 남에게 가르치려고 하면 언어에 대해 알고 있는 것이 구체적으로 무엇인지 얘기하기 어렵다는 것을 느낀다. \n이 강좌에서는 언어에 대한 과학적 연구를 통해 확립된 언어에 대한 기초지식을 가)언어의 일반적 특징, (나) 소리, (다) 소리 체계, (라) 단어, (마) 문장, (바) 의미 및 화용 등의 분야로 나누어 소개하고 이어 언어와 관련된 연구 분야로 (사) 언어와 사회, (자) 언어와 뇌/언어습득, (차) 언어의 변화 및 세계의 언어, (카) 인류가 만든 문자들, (타) 언어와 컴퓨터 등에서 논의된 기본 개념들을 소개한다.','1. 언어를 올바로 바라볼 수 있는 시각을 키운다.\n2. 언어와 관련된 여러 현상들에 대해서도 보다 객관적인 이해를 할 수 있는 능력을 배양한다. \n3. 언어 현상에 대한 분석을 시도하여 학생들이 논리적으로 사고하는 능력을 키울 수 있도록 돕는다.','출석,10,수업태도,0,중간고사,20,기말고사,30,발표,10,토의,0,과제,30,Quiz','언어: 풀어 쓴 언어학 개론 (개정 3판),강범모,한국문화사,2010','','',48),('2014/2-0000-1-2437-01','일반적으로 연극관람은 영화 한 편을 보는 것만큼 수월하게 느껴지지 않는다. 본 강좌는 연극에 대한 이론적, 실천적 접근을 통해 연극 관람에 수반되는 망설임과 두려움을 없애주는 수업이다. 구체적 작품을 중심으로 그리스 비극에서 현대극까지 연극의 역사를 개괄하고, 이와 더불어 각 나라의 연극이 가진 차이를 이해한다. 학생들은 희곡읽기를 통해 작품을 실천적 형태로 체험하며, 극의 한 부분을 상연해봄으로써 직접 공연의 실제를 경험하게 된다.','1. 연극이라는 예술영역을 보다 친근하게 느낀다.\n2. 그리스 비극 작가들의 작품들과 더불어 연극사의 중요한 작품들을 알고 각각의 차이를 비교분석할 수 있다.\n3. 연기를 직접 해봄으로써 미학적 감각을 배양하고 예술적 잠재력을 개발한다.\n4. 작품들이 다루는 인물의 특성 및 그들의 삶에 관련된 서사를 숙지함으로써 보다 성숙한 인성을 함양한다.','출석,10,수업태도,10,중간고사,25,기말고사,25,발표,10,토의,0,과제,20,Qui','교재 없음','','',96),('2014/2-0000-1-2665-01','본 과목은 중국어를 처음으로 접하는 학생들에게 기초 중국어회화를 중심으로 진행하는 강의이며, 이와 함께 중국어와 관련된 기본 지식에 대한 이해 및 중국문화도 함께 소개할 것이다. 요쯤　변화하고　있는　중국과　함께　중국인들이　사용하고　있는　새　어휘에　대해서도　전달하고자　하는　과목이다．','1. 중국어란　무엇인가에　대한　기본　소양과 자질을 갖도록 한다.\n2. 기초중국어회화를 할 수 있게 한다．\n3. 중국어　학습을　통해　중국　문화에　대해서도　이해하도록　한다，　\n4. 중국과　중국어에　대한　흥미를　느끼게　한다．\n5. 중국　전문가　되고자　하는　학생들이　가장　기본적인　기초　지식을　습득하게　된다．','출석,20,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,0,Quiz,','친절한 중국어,곡효여, 곡효운,도서출판 그린,2013','','',20),('2014/2-0000-1-2665-02','본 과목은 중국어를 처음으로 접하는 학생들에게 기초 중국어회화를 중심으로 진행하는 강의이며, 이와 함께 중국어와 관련된 기본 지식에 대한 이해 및 중국문화도 함께 소개할 것이다. 요쯤　변화하고　있는　중국과　함께　중국인들이　사용하고　있는　새　어휘에　대해서도　전달하고자　하는　과목이다．','1. 중국어란　무엇인가에　대한　기본　소양과 자질을 갖도록 한다.\n2. 기초중국어회화를 할 수 있게 한다．\n3. 중국어　학습을　통해　중국　문화에　대해서도　이해하도록　한다，　\n4. 중국과　중국어에　대한　흥미를　느끼게　한다．\n5. 중국　전문가　되고자　하는　학생들이　가장　기본적인　기초　지식을　습득하게　된다．','출석,20,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,0,Quiz,','친절한 중국어,곡효여, 곡효운,도서출판 그린,2013','','',20),('2014/2-0000-1-2948-01','본 강좌는 대학 생활과 학문 연구에 반드시 필요한 글쓰기에 관한 기술과 능력을 습득하는 데 역점을 둔다. 이를 위해 글쓰기에 꼭 필요한 전반적인 사항들을 학습하고, 다양한 실습들을 통해 그 능력을 기르도록 한다.','<교과목의 개요> 참조','출석,20,중간고사,0,기말고사,30,과제보고서,30,수업태도,0,Quiz,0,기타,20','글쓰기, 이젠 문제없다,광운대 교양국어 교재편찬위원회,보고사','','',18),('2014/2-0000-1-2959-01','본 강좌는 유한한 존재자로서의 인간이 가지는 여러 특성 중에서 Eros, Pathos, Ethos 적인 측면들에 대한 철학적 물음을 던짐으로써 존재의 의미를 이해하고 있는 유일한 존재자라 할 수 있는 ‘인간’의 의미에 한걸음 다가가려 한다. 이를 위해 내용적으로는 동양과 서양, 과거와 현재에서 논의되고 있는 인간에 대한 다양한 이론과 그 주장들을 검토할 것이며 나아가 이러한 이해를 토대로 하여 자신의 인생이 이전보다 다른 방식으로 변화하도록 하여 궁극적으로는 자신과 자신을 둘러싼 세계에 대한 보다 성숙한 이해를 하도록 수강생을 이끌 것이다.','1. 인간 존재의 심연에 대한 사상사적 이해를 통해 \n자신의 삶을 바라보고 진지하게 바라보도록 함\n 2. 이를 통해 보다 성숙한 방식으로 삶, 세계와 \n 대면하도록 하도록 이끔','출석,10,수업태도,0,중간고사,30,기말고사,30,발표,30,토의,0,과제,0,Quiz,','별도제작(한울관 1층 복사실)','','',31),('2014/2-0000-1-2959-02','본 강좌는 유한한 존재자로서의 인간이 가지는 여러 특성 중에서 Eros, Pathos, Ethos 적인 측면들에 대한 철학적 물음을 던짐으로써 존재의 의미를 이해하고 있는 유일한 존재자라 할 수 있는 ‘인간’의 의미에 한걸음 다가가려 한다. 이를 위해 내용적으로는 동양과 서양, 과거와 현재에서 논의되고 있는 인간에 대한 다양한 이론과 그 주장들을 검토할 것이며 나아가 이러한 이해를 토대로 하여 자신의 인생이 이전보다 다른 방식으로 변화하도록 하여 궁극적으로는 자신과 자신을 둘러싼 세계에 대한 보다 성숙한 이해를 하도록 수강생을 이끌 것이다.','1. 인간 존재의 심연에 대한 사상사적 이해를 통해 \n자신의 삶을 바라보고 진지하게 바라보도록 함\n 2. 이를 통해 보다 성숙한 방식으로 삶, 세계와 \n 대면하도록 하도록 이끔','출석,10,수업태도,0,중간고사,40,기말고사,40,발표,0,토의,0,과제,10,Quiz,','포스트휴먼과 탈근대적 주제,마정미,커뮤니케이션 북스,2014','','',43),('2014/2-0000-1-2982-01','본 교과목은 인간의 행동양식과 정신과정에 대하여 탐구하는 학문인 심리학을 전반적으로 소개하고 이를 바탕으로 심리학적 지식을 실생활에 적용함으로써, 인간 행동에 대한 심층적인 이해를 돕는다. 발달과 심리, 사회와 심리, 성격과 심리, 인지와 심리 등 인간심리의 다각적 측면을 중심으로 감각과 지각, 이상행동, 상담과 심리치료 등이 폭넓게 다루어질 것이다. 발표와 토론을 통하여 인간행동의 원리들을 발견하고, 자신과 타인의 행동과 사고를 이해함으로써 급변하는 현대사회에 건강하게 적응할 수 있는 능력을 함양한다.','1. 심리학에 대한 기초적 이해를 통해 인간 심리와 행동의 복합성에 대한 이해력을 증진시킨다.\n2. 심리학적 지식을 실제 생활에 적용하여 자기자신과 타인의 행위와 내면세계에 대한 해석력을 향상시킨다. \n3. 심리학적 분석을 통해 타인과 인간관계에서 발생한 문제를 원만하게 해결하고 설득력 있는 대화를 할 수 있는 능력을 배양한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','현대심리학의 이해,유태용 외 공저,학지사,2009','','',52),('2014/2-0000-1-2982-02','본 교과목은 인간의 행동양식과 정신과정에 대하여 탐구하는 학문인 심리학을 전반적으로 소개하고 이를 바탕으로 심리학적 지식을 실생활에 적용함으로써, 인간 행동에 대한 심층적인 이해를 돕는다. 발달과 심리, 사회와 심리, 성격과 심리, 인지와 심리 등 인간심리의 다각적 측면을 중심으로 감각과 지각, 이상행동, 상담과 심리치료 등이 폭넓게 다루어질 것이다. 발표와 토론을 통하여 인간행동의 원리들을 발견하고, 자신과 타인의 행동과 사고를 이해함으로써 급변하는 현대사회에 건강하게 적응할 수 있는 능력을 함양한다.','1. 심리학에 대한 기초적 이해를 통해 인간 심리와 행동의 복합성에 대한 이해력을 증진시킨다.\n2. 심리학적 지식을 실제 생활에 적용하여 자기자신과 타인의 행위와 내면세계에 대한 해석력을 향상시킨다. \n3. 심리학적 분석을 통해 타인과 인간관계에서 발생한 문제를 원만하게 해결하고 설득력 있는 대화를 할 수 있는 능력을 배양한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','현대심리학의 이해,현성용,학지사','','',66),('2014/2-0000-1-2982-03','본 교과목은 인간의 행동양식과 정신과정에 대하여 탐구하는 학문인 심리학을 전반적으로 소개하고 이를 바탕으로 심리학적 지식을 실생활에 적용함으로써, 인간 행동에 대한 심층적인 이해를 돕는다. 발달과 심리, 사회와 심리, 성격과 심리, 인지와 심리 등 인간심리의 다각적 측면을 중심으로 감각과 지각, 이상행동, 상담과 심리치료 등이 폭넓게 다루어질 것이다. 발표와 토론을 통하여 인간행동의 원리들을 발견하고, 자신과 타인의 행동과 사고를 이해함으로써 급변하는 현대사회에 건강하게 적응할 수 있는 능력을 함양한다.','1. 심리학에 대한 기초적 이해를 통해 인간 심리와 행동의 복합성에 대한 이해력을 증진시킨다.\n2. 심리학적 지식을 실제 생활에 적용하여 자기자신과 타인의 행위와 내면세계에 대한 해석력을 향상시킨다. \n3. 심리학적 분석을 통해 타인과 인간관계에서 발생한 문제를 원만하게 해결하고 설득력 있는 대화를 할 수 있는 능력을 배양한다.','출석,10,수업태도,10,중간고사,25,기말고사,30,발표,15,토의,0,과제,0,Quiz','마이어스의 심리학(8판),데이비드 마이어스,시그마프레스,2008','김거도','kimgudo@naver.com',48),('2014/2-0000-1-3008-01','현대사회의 조직과 리더십에 관한 다양한 이론을 살펴보고 변화된 리더십의 의미와 역할을 고찰하며 차세대 리더로서의 바람직한 행동과 강령들을 교육한다. 각 리더십 이론이 설명하는 리더십의 특징과 효용, 최근 리더십 패러다임의 흐름과 주요 이슈, 그리고 리더십 이론이 실제 조직상황에 서 활용되는 구체적인 사례 등에 대해 논의한다.','1. 현대사회의 조직과 리더십에 대한 기본지식을 학습하고 그 속성과 지향점을 인식한다.\n2. 21세기가 요구하는 리더십의 새로운 유형을 습득하여 바람직한 리더상을 확립한다.\n3. 조직과 리더십과의 역학관계를 이해하고 효과적인 행동 변화를 도모한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','조직관리론,민진,대영문화사','','',40),('2014/2-0000-1-3008-02','현대사회의 조직과 리더십에 관한 다양한 이론을 살펴보고 변화된 리더십의 의미와 역할을 고찰하며 차세대 리더로서의 바람직한 행동과 강령들을 교육한다. 각 리더십 이론이 설명하는 리더십의 특징과 효용, 최근 리더십 패러다임의 흐름과 주요 이슈, 그리고 리더십 이론이 실제 조직상황에 서 활용되는 구체적인 사례 등에 대해 논의한다.','1. 현대사회의 조직과 리더십에 대한 기본지식을 학습하고 그 속성과 지향점을 인식한다.\n2. 21세기가 요구하는 리더십의 새로운 유형을 습득하여 바람직한 리더상을 확립한다.\n3. 조직과 리더십과의 역학관계를 이해하고 효과적인 행동 변화를 도모한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','조직관리론,민진,대영문화사','김소담','thekfody@hanmail.net',62),('2014/2-0000-1-3029-01','컴퓨터와 인터넷은 21세기 정보사회를 이끌어 가고 있는 가장 중요한 요소이고, 특히 인터넷은 각종 정보를 공유함은 물론 인터넷 홈뱅킹, 인터넷교육을 통한 교육, 광고 등과 같은 다양한 분야에서 사용되고 있다. 본 강좌는 웹 이용자들이 단순히 웹에 떠있는 정보를 이용하는 데에 그치지 않고 직접 정보를 가공하고 가공된 정보를 웹 상에 올려 다른 이용자들이 이용할 수 있도록 하기위한 기본 언어인 HTML 언어와 자바스크립트 언어를 논리적이고 체계적으로 배운다.','인터넷 활용은 인터넷의 기본 기해를 바탕으로, 홈 페이지 작성에 필요한 기본적인 HTML 언어를 심화하는 자바스크립트 언어를 숙지하여 웹브라우저에서 사용할 수 있는 언어를 활용하여 홈 페이지 작성에 목적을 둔다.','출석,20,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,10,Quiz','인터넷웹프로그래밍,최명복,이상훈','김지훈','kimjihoon@kw.ac.kr',38),('2014/2-0000-1-3029-02','컴퓨터와 인터넷은 21세기 정보사회를 이끌어 가고 있는 가장 중요한 요소이고, 특히 인터넷은 각종 정보를 공유함은 물론 인터넷 홈뱅킹, 인터넷교육을 통한 교육, 광고 등과 같은 다양한 분야에서 사용되고 있다. 본 강좌는 웹 이용자들이 단순히 웹에 떠있는 정보를 이용하는 데에 그치지 않고 직접 정보를 가공하고 가공된 정보를 웹 상에 올려 다른 이용자들이 이용할 수 있도록 하기위한 기본 언어인 HTML 언어와 자바스크립트 언어를 논리적이고 체계적으로 배운다.','인터넷 활용은 인터넷의 기본 기해를 바탕으로, 홈 페이지 작성에 필요한 기본적인 HTML 언어를 심화하는 자바스크립트 언어를 숙지하여 웹브라우저에서 사용할 수 있는 언어를 활용하여 홈 페이지 작성에 목적을 둔다.','출석,20,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,10,Quiz','인터넷웹프로그래밍,최명복,이상훈','김가온','gaon108@naver.com',17),('2014/2-0000-1-3029-03','컴퓨터와 인터넷은 21세기 정보사회를 이끌어 가고 있는 가장 중요한 요소이고, 특히 인터넷은 각종 정보를 공유함은 물론 인터넷 홈뱅킹, 인터넷교육을 통한 교육, 광고 등과 같은 다양한 분야에서 사용되고 있다. 본 강좌는 웹 이용자들이 단순히 웹에 떠있는 정보를 이용하는 데에 그치지 않고 직접 정보를 가공하고 가공된 정보를 웹 상에 올려 다른 이용자들이 이용할 수 있도록 하기위한 기본 언어인 HTML 언어와 자바스크립트 언어를 논리적이고 체계적으로 배운다.','인터넷 활용은 인터넷의 기본 기해를 바탕으로, 홈 페이지 작성에 필요한 기본적인 HTML 언어를 심화하는 자바스크립트 언어를 숙지하여 웹브라우저에서 사용할 수 있는 언어를 활용하여 홈 페이지 작성에 목적을 둔다.','출석,20,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,10,Quiz','인터넷웹프로그래밍,최명복, 이상훈,글로벌,2008','김가온','gaon108@naver.com',35),('2014/2-0000-1-3029-04','컴퓨터와 인터넷은 21세기 정보사회를 이끌어 가고 있는 가장 중요한 요소이고, 특히 인터넷은 각종 정보를 공유함은 물론 인터넷 홈뱅킹, 인터넷교육을 통한 교육, 광고 등과 같은 다양한 분야에서 사용되고 있다. 본 강좌는 웹 이용자들이 단순히 웹에 떠있는 정보를 이용하는 데에 그치지 않고 직접 정보를 가공하고 가공된 정보를 웹 상에 올려 다른 이용자들이 이용할 수 있도록 하기위한 기본 언어인 HTML 언어와 자바스크립트 언어를 논리적이고 체계적으로 배운다.','인터넷 활용은 인터넷의 기본 기해를 바탕으로, 홈 페이지 작성에 필요한 기본적인 HTML 언어를 심화하는 자바스크립트 언어를 숙지하여 웹브라우저에서 사용할 수 있는 언어를 활용하여 홈 페이지 작성에 목적을 둔다.','출석,10,수업태도,10,중간고사,25,기말고사,25,발표,10,토의,0,과제,10,Qui','홈페이지 만들기,이근형,생능출판사,2013','이우석','leewoosuk@me.com',19),('2014/2-0000-1-3029-05','컴퓨터와 인터넷은 21세기 정보사회를 이끌어 가고 있는 가장 중요한 요소이고, 특히 인터넷은 각종 정보를 공유함은 물론 인터넷 홈뱅킹, 인터넷교육을 통한 교육, 광고 등과 같은 다양한 분야에서 사용되고 있다. 본 강좌는 웹 이용자들이 단순히 웹에 떠있는 정보를 이용하는 데에 그치지 않고 직접 정보를 가공하고 가공된 정보를 웹 상에 올려 다른 이용자들이 이용할 수 있도록 하기위한 기본 언어인 HTML 언어와 자바스크립트 언어를 논리적이고 체계적으로 배운다.','인터넷 활용은 인터넷의 기본 기해를 바탕으로, 홈 페이지 작성에 필요한 기본적인 HTML 언어를 심화하는 자바스크립트 언어를 숙지하여 웹브라우저에서 사용할 수 있는 언어를 활용하여 홈 페이지 작성에 목적을 둔다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','IT CookBook for Beginner, 인터넷 프로그래밍 기초,고민정,한빛미디어,2008','정연우','blueshark2011@gmail.com',30),('2014/2-0000-1-3029-06','컴퓨터와 인터넷은 21세기 정보사회를 이끌어 가고 있는 가장 중요한 요소이고, 특히 인터넷은 각종 정보를 공유함은 물론 인터넷 홈뱅킹, 인터넷교육을 통한 교육, 광고 등과 같은 다양한 분야에서 사용되고 있다. 본 강좌는 웹 이용자들이 단순히 웹에 떠있는 정보를 이용하는 데에 그치지 않고 직접 정보를 가공하고 가공된 정보를 웹 상에 올려 다른 이용자들이 이용할 수 있도록 하기위한 기본 언어인 HTML 언어와 자바스크립트 언어를 논리적이고 체계적으로 배운다.','인터넷 활용은 인터넷의 기본 기해를 바탕으로, 홈 페이지 작성에 필요한 기본적인 HTML 언어를 심화하는 자바스크립트 언어를 숙지하여 웹브라우저에서 사용할 수 있는 언어를 활용하여 홈 페이지 작성에 목적을 둔다.','출석,10,수업태도,10,중간고사,25,기말고사,25,발표,10,토의,0,과제,10,Qui','홈페이지 만들기,이근형,생능출판사,2013','송영일','just1025@naver.com',33),('2014/2-0000-1-3223-01','','','출석,0,중간고사,0,기말고사,0,과제보고서,0,수업태도,0,Quiz,0,기타,0','홈페이지 만들기,이근형,생능출판사,2013','','',13),('2014/2-0000-1-3223-02','','','출석,0,중간고사,0,기말고사,0,과제보고서,0,수업태도,0,Quiz,0,기타,0','홈페이지 만들기,이근형,생능출판사,2013','','',14),('2014/2-0000-1-3293-01','본 교과목에서는 컴퓨터를 사용한 다양한 데이터 처리방법과 자료정리방법에 대해 다룬다. 가장 범용적으로 사용되는 스프레드시트인 엑셀을 이용하여 데이터의 처리와 구축, 함수를 통한 고급기능 사용 등 엑셀의 다양한 기능을 익히며, 프레젠테이션을 위한 도구로서 파워포인트를 이용하여 다양한 형태의 발표문 작성과 고급사용법을 습득함으로써 실생활과 사회생활에서 원활한 자료정리 및 발표자료 작성방법을 익힌다.','1. 엑셀의 기본기능과 데이터의 개념을 이해하고, 데이터를 처리하고 응용하여 여러 가지 형태로 자료를 활용하는 능력을 배양한다.\n2. 엑셀에서 제공하는 각종 함수를 익히므로 자료처리와 관련된 업무능력을 향상시키고, 데이터베이스 기능을 익힘으로써 필요한 자료를 가공하는 능력을 배양한다. \n3. 파워포인트가 갖고 있는 다양한 기능을 자유롭게 사용할 수 있는 능력을 배양한다.\n4. 파워포인트의 다양한 기능을 활용하여 적절하고 적합한 프레젠테이션자료를 제작하는 능력을 키운다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,10,Quiz','컴퓨터 활용,이종용 정계동 서영호,생릉출판사,2012','이광철','sheepy55@naver.com',20),('2014/2-0000-1-3293-02','본 교과목에서는 컴퓨터를 사용한 다양한 데이터 처리방법과 자료정리방법에 대해 다룬다. 가장 범용적으로 사용되는 스프레드시트인 엑셀을 이용하여 데이터의 처리와 구축, 함수를 통한 고급기능 사용 등 엑셀의 다양한 기능을 익히며, 프레젠테이션을 위한 도구로서 파워포인트를 이용하여 다양한 형태의 발표문 작성과 고급사용법을 습득함으로써 실생활과 사회생활에서 원활한 자료정리 및 발표자료 작성방법을 익힌다.','1. 엑셀의 기본기능과 데이터의 개념을 이해하고, 데이터를 처리하고 응용하여 여러 가지 형태로 자료를 활용하는 능력을 배양한다.\n2. 엑셀에서 제공하는 각종 함수를 익히므로 자료처리와 관련된 업무능력을 향상시키고, 데이터베이스 기능을 익힘으로써 필요한 자료를 가공하는 능력을 배양한다. \n3. 파워포인트가 갖고 있는 다양한 기능을 자유롭게 사용할 수 있는 능력을 배양한다.\n4. 파워포인트의 다양한 기능을 활용하여 적절하고 적합한 프레젠테이션자료를 제작하는 능력을 키운다.','출석,10,수업태도,10,중간고사,25,기말고사,25,발표,10,토의,0,과제,10,Qui','컴퓨터 활용,이종용외 2명,생능출판사,2012','김용민','dggjelly@naver.com',20),('2014/2-0000-1-3293-03','본 교과목에서는 컴퓨터를 사용한 다양한 데이터 처리방법과 자료정리방법에 대해 다룬다. 가장 범용적으로 사용되는 스프레드시트인 엑셀을 이용하여 데이터의 처리와 구축, 함수를 통한 고급기능 사용 등 엑셀의 다양한 기능을 익히며, 프레젠테이션을 위한 도구로서 파워포인트를 이용하여 다양한 형태의 발표문 작성과 고급사용법을 습득함으로써 실생활과 사회생활에서 원활한 자료정리 및 발표자료 작성방법을 익힌다.','1. 엑셀의 기본기능과 데이터의 개념을 이해하고, 데이터를 처리하고 응용하여 여러 가지 형태로 자료를 활용하는 능력을 배양한다.\n2. 엑셀에서 제공하는 각종 함수를 익히므로 자료처리와 관련된 업무능력을 향상시키고, 데이터베이스 기능을 익힘으로써 필요한 자료를 가공하는 능력을 배양한다. \n3. 파워포인트가 갖고 있는 다양한 기능을 자유롭게 사용할 수 있는 능력을 배양한다.\n4. 파워포인트의 다양한 기능을 활용하여 적절하고 적합한 프레젠테이션자료를 제작하는 능력을 키운다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','자체교재','박성빈','Sungbin30116@naver.com',18),('2014/2-0000-1-3589-01','유럽연합(EU)의 심장부에 위치한 경제, 정치 중심국인 독일의 문화이해는 언어습득에서부터 시작할 수 있다. 언어 습득은 단순히 문법과 어휘를 배우는 것에 국한되지 않고, 언어 속에 담겨있는 한 민족의 삶을 배우는 것이다. 이 수업은 독일어 초급자를 대상으로 하며, 일상회화를 중심으로 기초 회화 능력 배양과 국제화 시대에 걸맞는 상호문화적 경제협력 능력을 키우는데 목적이 있다.','독일어는 세계 주요 3대 언어 중의 하나이며, 독일어 인구 사용비율은 유럽내에서 당연 1위다. 영어 몰입식의 한국적 특수 상황 속에서 다문화, 다양한 언어 세계를 고려한다면 유럽의 경제 정치 중심국인 독일 언어를 배우는 데에도 나름의 의의를 찾을 수 있겠다. 독일어 습득을 통해 국제 사회에서 꼭 갖추어야 할 에티켓을 영국식, 프랑스식, 이태리식, 스페인식과 비교하면서 배우도록 하겠다.','출석,10,중간고사,30,기말고사,30,과제보고서,10,수업태도,20,Quiz,0,기타,0','입에서 톡 일일어 시즌 2. 독일어회화,박현선,문예림,2011','','',19),('2014/2-0000-1-3593-01','일본어를 처음 접하는 학생들에게 일본어를 읽기,쓰기,발음의 기본적인 원리를 습득케 한다. 또한 일본어의 기본적인 문법 사항 및 기초적인 말하기 연습을 통하여, 일본어의 기초를 익힌다. 수업은 일본어 문자 및 발음, 기본문형 익히기 등을 통하여 일상생활에 필요한 기초표현을 학습함으로써 일본어와 친숙해지고, 나아가 일본에서의 다양한 일상생활에 대한 관심을 갖도록 한다.','1.일본어의 문자(히라가나, 가타카나) 및 발음, 어휘(한자포함)를 익히도록 한다.\n2.일본어를 말하고 이해하는데 필요한 기초표현을 익히도록 한다.\n3.일본어의 기초표현 등을 익히고 활용함으로써 간단한 일상회화를 할 수 있도록 한다.\n4.상기 성과를 바탕으로 일본인 및 일본 사회와 문화에 대한 지식을 전달하여, 일본의 문화와 사회에 대한 관심과 흥미를 고취시킨다. \n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','도끼도끼일본어첫걸음,이승영,최정아,책사랑,2012','양현우','hyunwoo242@naver.com',19),('2014/2-0000-1-3593-02','일본어를 처음 접하는 학생들에게 일본어를 읽기,쓰기,발음의 기본적인 원리를 습득케 한다. 또한 일본어의 기본적인 문법 사항 및 기초적인 말하기 연습을 통하여, 일본어의 기초를 익힌다. 수업은 일본어 문자 및 발음, 기본문형 익히기 등을 통하여 일상생활에 필요한 기초표현을 학습함으로써 일본어와 친숙해지고, 나아가 일본에서의 다양한 일상생활에 대한 관심을 갖도록 한다.','1.일본어의 문자(히라가나, 가타카나) 및 발음, 어휘(한자포함)를 익히도록 한다.\n2.일본어를 말하고 이해하는데 필요한 기초표현을 익히도록 한다.\n3.일본어의 기초표현 등을 익히고 활용함으로써 간단한 일상회화를 할 수 있도록 한다.\n4.상기 성과를 바탕으로 일본인 및 일본 사회와 문화에 대한 지식을 전달하여, 일본의 문화와 사회에 대한 관심과 흥미를 고취시킨다. \n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','도끼도끼 일본어 1,이승영, 최정아,책사랑,2012','','',50),('2014/2-0000-1-3593-03','일본어를 처음 접하는 학생들에게 일본어를 읽기,쓰기,발음의 기본적인 원리를 습득케 한다. 또한 일본어의 기본적인 문법 사항 및 기초적인 말하기 연습을 통하여, 일본어의 기초를 익힌다. 수업은 일본어 문자 및 발음, 기본문형 익히기 등을 통하여 일상생활에 필요한 기초표현을 학습함으로써 일본어와 친숙해지고, 나아가 일본에서의 다양한 일상생활에 대한 관심을 갖도록 한다.','1.일본어의 문자(히라가나, 가타카나) 및 발음, 어휘(한자포함)를 익히도록 한다.\n2.일본어를 말하고 이해하는데 필요한 기초표현을 익히도록 한다.\n3.일본어의 기초표현 등을 익히고 활용함으로써 간단한 일상회화를 할 수 있도록 한다.\n4.상기 성과를 바탕으로 일본인 및 일본 사회와 문화에 대한 지식을 전달하여, 일본의 문화와 사회에 대한 관심과 흥미를 고취시킨다. \n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','도끼도끼일본어첫걸음,이승영.최정아,책사랑,2012','박하윤','superhayoon@naver.com',46),('2014/2-0000-1-3593-04','일본어를 처음 접하는 학생들에게 일본어를 읽기,쓰기,발음의 기본적인 원리를 습득케 한다. 또한 일본어의 기본적인 문법 사항 및 기초적인 말하기 연습을 통하여, 일본어의 기초를 익힌다. 수업은 일본어 문자 및 발음, 기본문형 익히기 등을 통하여 일상생활에 필요한 기초표현을 학습함으로써 일본어와 친숙해지고, 나아가 일본에서의 다양한 일상생활에 대한 관심을 갖도록 한다.','1.일본어의 문자(히라가나, 가타카나) 및 발음, 어휘(한자포함)를 익히도록 한다.\n2.일본어를 말하고 이해하는데 필요한 기초표현을 익히도록 한다.\n3.일본어의 기초표현 등을 익히고 활용함으로써 간단한 일상회화를 할 수 있도록 한다.\n4.상기 성과를 바탕으로 일본인 및 일본 사회와 문화에 대한 지식을 전달하여, 일본의 문화와 사회에 대한 관심과 흥미를 고취시킨다. \n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','도끼도끼 일본어첫걸음,최정아, 이승영,책사랑,2012','','',18),('2014/2-0000-1-3593-05','일본어를 처음 접하는 학생들에게 일본어를 읽기,쓰기,발음의 기본적인 원리를 습득케 한다. 또한 일본어의 기본적인 문법 사항 및 기초적인 말하기 연습을 통하여, 일본어의 기초를 익힌다. 수업은 일본어 문자 및 발음, 기본문형 익히기 등을 통하여 일상생활에 필요한 기초표현을 학습함으로써 일본어와 친숙해지고, 나아가 일본에서의 다양한 일상생활에 대한 관심을 갖도록 한다.','1.일본어의 문자(히라가나, 가타카나) 및 발음, 어휘(한자포함)를 익히도록 한다.\n2.일본어를 말하고 이해하는데 필요한 기초표현을 익히도록 한다.\n3.일본어의 기초표현 등을 익히고 활용함으로써 간단한 일상회화를 할 수 있도록 한다.\n4.상기 성과를 바탕으로 일본인 및 일본 사회와 문화에 대한 지식을 전달하여, 일본의 문화와 사회에 대한 관심과 흥미를 고취시킨다. \n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','도키도키일본어첫걸음,최정아 이승영,책사랑,2010','유재혁','honorjh@naver.com',20),('2014/2-0000-1-3593-06','일본어를 처음 접하는 학생들에게 일본어를 읽기,쓰기,발음의 기본적인 원리를 습득케 한다. 또한 일본어의 기본적인 문법 사항 및 기초적인 말하기 연습을 통하여, 일본어의 기초를 익힌다. 수업은 일본어 문자 및 발음, 기본문형 익히기 등을 통하여 일상생활에 필요한 기초표현을 학습함으로써 일본어와 친숙해지고, 나아가 일본에서의 다양한 일상생활에 대한 관심을 갖도록 한다.','1.일본어의 문자(히라가나, 가타카나) 및 발음, 어휘(한자포함)를 익히도록 한다.\n2.일본어를 말하고 이해하는데 필요한 기초표현을 익히도록 한다.\n3.일본어의 기초표현 등을 익히고 활용함으로써 간단한 일상회화를 할 수 있도록 한다.\n4.상기 성과를 바탕으로 일본인 및 일본 사회와 문화에 대한 지식을 전달하여, 일본의 문화와 사회에 대한 관심과 흥미를 고취시킨다. \n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','도키도키일본어첫걸음1,이승영, 최정아,책사랑,2012','','',49),('2014/2-0000-1-3593-07','일본어를 처음 접하는 학생들에게 일본어를 읽기,쓰기,발음의 기본적인 원리를 습득케 한다. 또한 일본어의 기본적인 문법 사항 및 기초적인 말하기 연습을 통하여, 일본어의 기초를 익힌다. 수업은 일본어 문자 및 발음, 기본문형 익히기 등을 통하여 일상생활에 필요한 기초표현을 학습함으로써 일본어와 친숙해지고, 나아가 일본에서의 다양한 일상생활에 대한 관심을 갖도록 한다.','1.일본어의 문자(히라가나, 가타카나) 및 발음, 어휘(한자포함)를 익히도록 한다.\n2.일본어를 말하고 이해하는데 필요한 기초표현을 익히도록 한다.\n3.일본어의 기초표현 등을 익히고 활용함으로써 간단한 일상회화를 할 수 있도록 한다.\n4.상기 성과를 바탕으로 일본인 및 일본 사회와 문화에 대한 지식을 전달하여, 일본의 문화와 사회에 대한 관심과 흥미를 고취시킨다. \n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','도끼도끼 일본어첫걸음1,이승영,최정아,책사랑,2012','','',50),('2014/2-0000-1-3693-01','이 수업은 교과수업과 테마수업으로 구분된다. 교과수업에서는 구어체를 통한 정확한 발음과 독일어의 기초구문을 습득한다. 테마수업이란 독일과 독일인, 더나아가 유럽에 대한 테마를 매시간 정하여 동영상을 통한 수업을 말한다. 1학기 테마수업은 주로 독일과 독일인을 이해하는 범주에서 설정되었고, 2학기 테마수업은 게르만 민족과 유럽이라는 범주에서 독일과의 관계를 조면하였다. 또한 학기마다 3-4편의 독일영화를 감상함으로써 독일문화를 간접적으로 체험한다.','\"독일어 배우기 어렵다\"는 고정관념은 회화보다는 문법에 지나치게 치중한 수업방식에서 연유한다. 이 수업에서는 문법보다는 정확한 발음을 익히며, 일상생활에서 쓰이는 구어체를 중심으로 독일어의 구문을 이해하는데 중점을 두고 있다. 또한 독일과 유럽에 대한 소개를 통하여 세계화 시대가 요구하는 정보를 습득함으로써 이에 유연하게 대처할 수 있는 감각을 습득한다. 효율적인 수업을 위하여 매시간 동영상을 통한 시청각 수업을 병행한다.','출석,20,중간고사,20,기말고사,30,과제보고서,20,수업태도,10,Quiz,0,기타,0','독일어 테마산책,문윤덕,호서대학교,2013','','',17),('2014/2-0000-1-3696-01','일본어 학습단계의 초급 후반에 해당되므로, 초급일본어1(또는 일본어듣기와 쓰기, 문자, 기초문법 등)을 한 학기 이상 이수한 학생을 대상으로 일본어 읽기,듣기,쓰기,말하기 능력을 보다 강화하는 수업이다. 일본어를 보다 심층적으로 학습하고자 하는 욕구를 충족시켜줌과 동시에 기초적인 일상회화의 습득과 일본문화 등을 소개함으로써 글로벌화의 파트너로서 일본을 올바르게 이해하도록 한다.','1. 동사를 주로 한 서술표현을 중심으로 다양한 일본어 문형을 학습한다. \n2. 자연스러운 일본어를 익히기 위해 필수적인 기본 문형, 어휘 습득, 문법을 학습하여 기본적인 문장을 말하고 만드는 능력을 양성한다. \n3. 일본어의 일상적 생활회화가 가능하도록 생활회화를 중심으로 한 일본어의 기초문법지식과 어휘를 학습한다.\n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,10,Quiz','이키이키 일본어첫걸음2,이승영,최정아,책사랑,2011','김영주','sweetshsh@naver.com',43),('2014/2-0000-1-3696-02','일본어 학습단계의 초급 후반에 해당되므로, 초급일본어1(또는 일본어듣기와 쓰기, 문자, 기초문법 등)을 한 학기 이상 이수한 학생을 대상으로 일본어 읽기,듣기,쓰기,말하기 능력을 보다 강화하는 수업이다. 일본어를 보다 심층적으로 학습하고자 하는 욕구를 충족시켜줌과 동시에 기초적인 일상회화의 습득과 일본문화 등을 소개함으로써 글로벌화의 파트너로서 일본을 올바르게 이해하도록 한다.','1. 동사를 주로 한 서술표현을 중심으로 다양한 일본어 문형을 학습한다. \n2. 자연스러운 일본어를 익히기 위해 필수적인 기본 문형, 어휘 습득, 문법을 학습하여 기본적인 문장을 말하고 만드는 능력을 양성한다. \n3. 일본어의 일상적 생활회화가 가능하도록 생활회화를 중심으로 한 일본어의 기초문법지식과 어휘를 학습한다.\n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','이키이키 일본어첫걸음2,이승영,최정아,책사랑,2011','박하윤','superhayoon@naver.com',16),('2014/2-0000-1-3696-03','일본어 학습단계의 초급 후반에 해당되므로, 초급일본어1(또는 일본어듣기와 쓰기, 문자, 기초문법 등)을 한 학기 이상 이수한 학생을 대상으로 일본어 읽기,듣기,쓰기,말하기 능력을 보다 강화하는 수업이다. 일본어를 보다 심층적으로 학습하고자 하는 욕구를 충족시켜줌과 동시에 기초적인 일상회화의 습득과 일본문화 등을 소개함으로써 글로벌화의 파트너로서 일본을 올바르게 이해하도록 한다.','1. 동사를 주로 한 서술표현을 중심으로 다양한 일본어 문형을 학습한다. \n2. 자연스러운 일본어를 익히기 위해 필수적인 기본 문형, 어휘 습득, 문법을 학습하여 기본적인 문장을 말하고 만드는 능력을 양성한다. \n3. 일본어의 일상적 생활회화가 가능하도록 생활회화를 중심으로 한 일본어의 기초문법지식과 어휘를 학습한다.\n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,35,기말고사,35,발표,0,토의,0,과제,0,Quiz,','이키이키 일본어첫걸음2,이승영, 최정아,책사랑,2011','','',20),('2014/2-0000-1-3696-04','일본어 학습단계의 초급 후반에 해당되므로, 초급일본어1(또는 일본어듣기와 쓰기, 문자, 기초문법 등)을 한 학기 이상 이수한 학생을 대상으로 일본어 읽기,듣기,쓰기,말하기 능력을 보다 강화하는 수업이다. 일본어를 보다 심층적으로 학습하고자 하는 욕구를 충족시켜줌과 동시에 기초적인 일상회화의 습득과 일본문화 등을 소개함으로써 글로벌화의 파트너로서 일본을 올바르게 이해하도록 한다.','1. 동사를 주로 한 서술표현을 중심으로 다양한 일본어 문형을 학습한다. \n2. 자연스러운 일본어를 익히기 위해 필수적인 기본 문형, 어휘 습득, 문법을 학습하여 기본적인 문장을 말하고 만드는 능력을 양성한다. \n3. 일본어의 일상적 생활회화가 가능하도록 생활회화를 중심으로 한 일본어의 기초문법지식과 어휘를 학습한다.\n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,30,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,0,Quiz,','도끼도끼 일본어 첫걸음,이승영, 최정아,도서출판 책사랑,2012','','',36),('2014/2-0000-1-3696-05','일본어 학습단계의 초급 후반에 해당되므로, 초급일본어1(또는 일본어듣기와 쓰기, 문자, 기초문법 등)을 한 학기 이상 이수한 학생을 대상으로 일본어 읽기,듣기,쓰기,말하기 능력을 보다 강화하는 수업이다. 일본어를 보다 심층적으로 학습하고자 하는 욕구를 충족시켜줌과 동시에 기초적인 일상회화의 습득과 일본문화 등을 소개함으로써 글로벌화의 파트너로서 일본을 올바르게 이해하도록 한다.','1. 동사를 주로 한 서술표현을 중심으로 다양한 일본어 문형을 학습한다. \n2. 자연스러운 일본어를 익히기 위해 필수적인 기본 문형, 어휘 습득, 문법을 학습하여 기본적인 문장을 말하고 만드는 능력을 양성한다. \n3. 일본어의 일상적 생활회화가 가능하도록 생활회화를 중심으로 한 일본어의 기초문법지식과 어휘를 학습한다.\n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','이키이키 일본어첫걸음2,이승영,최정아,책사랑,2011','유재혁','honorjh@naver.com',49),('2014/2-0000-1-3696-06','일본어 학습단계의 초급 후반에 해당되므로, 초급일본어1(또는 일본어듣기와 쓰기, 문자, 기초문법 등)을 한 학기 이상 이수한 학생을 대상으로 일본어 읽기,듣기,쓰기,말하기 능력을 보다 강화하는 수업이다. 일본어를 보다 심층적으로 학습하고자 하는 욕구를 충족시켜줌과 동시에 기초적인 일상회화의 습득과 일본문화 등을 소개함으로써 글로벌화의 파트너로서 일본을 올바르게 이해하도록 한다.','1. 동사를 주로 한 서술표현을 중심으로 다양한 일본어 문형을 학습한다. \n2. 자연스러운 일본어를 익히기 위해 필수적인 기본 문형, 어휘 습득, 문법을 학습하여 기본적인 문장을 말하고 만드는 능력을 양성한다. \n3. 일본어의 일상적 생활회화가 가능하도록 생활회화를 중심으로 한 일본어의 기초문법지식과 어휘를 학습한다.\n4. 일본어뿐만 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.\n5. 기초일본어를 다짐으로써 중급일본어의 토대를 마련함과 더불어 일본에 대해 전반적으로 이해하게 하여 일본어능력시험의 3,4급 정도의 일본어실력을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','이키이키 일본어첫걸음2,이승영,최정아,책사랑,2011','','',17),('2014/2-0000-1-3697-01','이 과목은 <프랑스어1>을 이수한 학생들이 더 깊이 있는 불어 지식을 습득할 수 있도록 개설된 과목이다.','- <프랑스어2>에서는 <프랑스어1>보다 더 심화된 불어 문법과 불어 표현을 다룬다. 또한 시간이 허락하는 한 다양한 시청각 교재를 통해 불어에 대한 이해력과 표현력을 기르고자 한다.\n- 언어가 문화와 불가분의 관계를 맺고 있고, 언어를 배우는 한 목적이 실제로 의사소통을 하기 위한 것이니만큼 교과서 진도를 중심으로 강의를 진행하되, 프랑스 문화 습득과 실제 회화에 도움이 될 수 있는 간단한 표현들을 배울 수 있는 수업이 되도록 할 것이다.\n\n* 유의사항: 1. 이 수업은 프랑스어에 대한 기본 지식이 있는 학생을 대상으로 하는 수업입니다. 불어 알파벳도 모르고 들어오는 학생도 물론 수강신청을 할 수 있지만 이미 기초가 있는 학생들에 비해 수업을 따라오는 데 있어 어려움을 겪을 수 있습니다. 그렇지만 초반에 복습을 할 것이고, 불어초보자 학생들이 많은 경우 수업의 난이도를 조절할 것입니다.\n2. 반복학습이 수행되어야 하는 어학수업임에도 불구하고 일주일에 단 한 번만 수업이 있는 점을 감안하여 매시간 배운 것을 써 보고 문제를 풀어보는 연습문제가 과제로 있습니다. 이 연습문제가 레포트 대신이므로 매번 과제를 성실하게 내 주길 바랍니다.\n3. 두 학기 불어 수업을 듣고도 불어문장을 제대로 읽지 못하는 학생이 많은데 이는 안타까운 일이 아닐 수 없습니다. 선생님은 수업 중 따라 읽는 것을 많이 시키니 그때그때 크게 따라 읽어주길 바랍니다. \n4. 15주 수업이므로 첫 시간부터 강의합니다.','출석,10,중간고사,35,기말고사,35,과제보고서,10,수업태도,10,Quiz,0,기타,0','Amusez-vous bien,길해옥,누보출판사,2012','','',18),('2014/2-0000-1-3812-01','스페인어가 무엇인가를 알아보는 것\n딱딱한 수업을 최대한 즐겁고 유쾌하게 진행하고자 함\n수업대상: \"스페인어를 처음배우고자 하는 학생\" \n주의사항: 1학기에 스페인어1과 스페인어를 조금이라도 배운학생은 스페인어2를 신청하기바람.','본 수업은 무엇보다도 발음에 많은 투자를 하는 것\n두번째, 비디오를 통해 듣기 훈련을 병행\n셋째, 영문법과 비교한 아주 간편 간단한 문법 지식 전달.','출석,20,중간고사,40,기말고사,40,과제보고서,0,수업태도,0,Quiz,0,기타,0','누구라도 말할 수 있는 스페인어,조혜진,월인,2003','','',20),('2014/2-0000-1-3814-01','영화를 감상하는 사람의 지식에 따라 감상의 깊이가 결정된다. 본 강좌에서 학생들은 영화를 보다 깊이 있게 감상할 수 있는 이론적/실천적 기반을 마련하게 된다. 영화의 기본용어, 영화의 역사, 영화사조, 감독론, 작품론 등을 개괄적으로 이해하고 직접 단편 시나리오를 작성해본다. 학생들은 영화감상에 자연스럽게 적용될 수 있는 실천적인 방식으로 이론을 학습하고, 이를 바탕으로 작품에 대해 토론한다.','1. 영화를 보다 깊이 있게 보고 더욱 다채롭게 즐길 수 있다.\n2. 영화의 분석 및 비평 능력을 배양하고, 이를 조리 있게 글로 구성하는 능력을 기른다.\n3. 자신이 관람한 영화를 영화사 속에 배치하고 이를 토대로 작품의 독창성 및 고유한 가치를 평가할 수 있다.\n4. 간단한 단편 시나리오를 쓸 수 있는 능력을 기른다.\n5. 다양한 영화들을 통해 다채로운 삶의 방식들을 간접경험 함으로써 인성함양을 도모한다.','출석,20,수업태도,0,중간고사,25,기말고사,25,발표,10,토의,0,과제,20,Quiz','제본','정철원','hoyyoh@naver.com',94),('2014/2-0000-1-3814-02','영화를 감상하는 사람의 지식에 따라 감상의 깊이가 결정된다. 본 강좌에서 학생들은 영화를 보다 깊이 있게 감상할 수 있는 이론적/실천적 기반을 마련하게 된다. 영화의 기본용어, 영화의 역사, 영화사조, 감독론, 작품론 등을 개괄적으로 이해하고 직접 단편 시나리오를 작성해본다. 학생들은 영화감상에 자연스럽게 적용될 수 있는 실천적인 방식으로 이론을 학습하고, 이를 바탕으로 작품에 대해 토론한다.','1. 영화를 보다 깊이 있게 보고 더욱 다채롭게 즐길 수 있다.\n2. 영화의 분석 및 비평 능력을 배양하고, 이를 조리 있게 글로 구성하는 능력을 기른다.\n3. 자신이 관람한 영화를 영화사 속에 배치하고 이를 토대로 작품의 독창성 및 고유한 가치를 평가할 수 있다.\n4. 간단한 단편 시나리오를 쓸 수 있는 능력을 기른다.\n5. 다양한 영화들을 통해 다채로운 삶의 방식들을 간접경험 함으로써 인성함양을 도모한다.','출석,20,수업태도,0,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Quiz','『영화의 이해 : 이론과 실제』,루이스 쟈네티 저,,케이북스,2012','','',99),('2014/2-0000-1-3814-03','영화를 감상하는 사람의 지식에 따라 감상의 깊이가 결정된다. 본 강좌에서 학생들은 영화를 보다 깊이 있게 감상할 수 있는 이론적/실천적 기반을 마련하게 된다. 영화의 기본용어, 영화의 역사, 영화사조, 감독론, 작품론 등을 개괄적으로 이해하고 직접 단편 시나리오를 작성해본다. 학생들은 영화감상에 자연스럽게 적용될 수 있는 실천적인 방식으로 이론을 학습하고, 이를 바탕으로 작품에 대해 토론한다.','1. 영화를 보다 깊이 있게 보고 더욱 다채롭게 즐길 수 있다.\n2. 영화의 분석 및 비평 능력을 배양하고, 이를 조리 있게 글로 구성하는 능력을 기른다.\n3. 자신이 관람한 영화를 영화사 속에 배치하고 이를 토대로 작품의 독창성 및 고유한 가치를 평가할 수 있다.\n4. 간단한 단편 시나리오를 쓸 수 있는 능력을 기른다.\n5. 다양한 영화들을 통해 다채로운 삶의 방식들을 간접경험 함으로써 인성함양을 도모한다.','출석,30,수업태도,0,중간고사,20,기말고사,30,발표,10,토의,0,과제,10,Quiz','초보자를 위한 시네 클래스,문관규 외,커뮤니케이션북스,2012','이정원','milk_way@naver.com',99),('2014/2-0000-1-3919-01','서양과 구분되는 동양의 문화적, 역사적 변화과정을 고찰하여 동양의 정체성이 형성되어 온 역사적 과정을 파악한다. \n한·중·일 동북아 삼국을 중심으로 한 동북아시아, 몽고를 포함하는 중앙아시아의 유목민족, 동남아시아와 서남아시아의 주요한 역사적 흐름, 각 지역별 문화적 전통과 그 현대적 변용 양상에 대해 살펴본다.','1. 동양문명이 어떻게 정체성을 띠게 되었는지 이해하고, 그 속에서 한반도의 문화사적 특성을 자리매김한다.\n2. 동양 각국의 역사적 상호작용이 어떻게 이루어졌으며, 그것이 어떠한 문화적 교류와 접촉을 낳았는지 이해한다. \n3. 글로벌 시대 동양에 대한 지식과 균형적이고 비판적인 역사인식을 형성한다.','출석,10,수업태도,10,중간고사,20,기말고사,20,발표,20,토의,10,과제,10,Qu','초보자를 위한 시네 클래스,문관규 외,커뮤니케이션북스,2012','','',94),('2014/2-0000-1-3925-01','나는 어떠한 존재이며 앞으로 내 삶의 좌표를 어떻게 설정할 것인가? 동양과 서양, 전통과 현대, 과거와 미래의 다양한 역사적 상황 속에 자신의 주체적 실천을 대입시켜봄으로써 나의 현존을 둘러싸고 있는 시공간의 역사적 특수성을 파악한다. 다양한 역사적 상황을 입체적으로 이해할 수 있는 볼거리와 읽을거리들을 소개하고 수강생들은 이를 재해석하여 발표, 토론하는 과정을 통해 능동적이고 적극적으로 역사적 상상력과 역사적 존재로서의 자아에 대한 성찰력을 기른다.','1. 나의 정체성이 어떤 사회적·역사적 조건 속에서 형성되어 왔는지 역사적 상상력과 통찰력을 기른다.\n2. 개인의 실천과 사회의 변화의 상호관계를 이해함으로써 역사의식과 책임감을 겸비한 자의식적 주체를 형성한다. \n3. 자아에 대한 긍정과 확신을 통해 21세기 세계사적 존재로서 자신의 삶에 대한 역사의식과 주체의식을 확립한다.','출석,10,수업태도,10,중간고사,0,기말고사,30,발표,20,토의,20,과제,10,Qui','주요교재는 강의계획서에 있음','','',17),('2014/2-0000-1-3928-01','본 강좌에서는 개인 속에 자리 잡고 있는 남성으로서의 혹은 여성으로서의 성정체성이 어떻게 형성되어 왔는지를 분석하고 토론하게 된다. 더 나아가 사랑과 성에 관한 담론이 다양해지면서 그 다양성속에 표류하는 현대인의 성을 짚어본다. “동거가 이혼을 줄일 수 있다던데... 매춘이 강간을 방지할 수도 있다던데... 성형수술은 일종의 화장 같은 것이 아닐까... 아무리 그래도 호모 섹슈얼리티는 인간의 본질을 거스르는 게 아닐까...” 등과 같은 성에 관한 이슈, 담론, 사회문화적 통념 등을 토론하면서, 성의 사회·정치적, 문화·심리적 함의에 관하여 생각해보고 성에 관한 자율적이고 성숙한 관점을 갖도록 한다.','1. 사랑과 연애, 직업과 결혼, 섹스와 심리 등의 의미를 살펴보고, 이러한 주제를 자신의 삶에 적용하여 비판적으로 해석하는 능력을 배양한다.\n2. 젠더라는 렌즈를 통해 기존의 사회, 정치, 문화 현실을 분석하고, 한 개체로서 자신 안에 내재된 여성성과 남성성을 균형 있게 발전시키는 성숙함을 갖게 한다.\n3. 남녀 사이의 갈등을 지혜롭게 풀어가기 위한 다양한 방법을 토론하며 자기성찰의 중요성을 인식한다.','출석,20,수업태도,10,중간고사,35,기말고사,35,발표,0,토의,0,과제,0,Quiz,','성차이의 심리,송관재,선학사','정의영','galooval@naver.com',98),('2014/2-0000-1-3928-02','본 강좌에서는 개인 속에 자리 잡고 있는 남성으로서의 혹은 여성으로서의 성정체성이 어떻게 형성되어 왔는지를 분석하고 토론하게 된다. 더 나아가 사랑과 성에 관한 담론이 다양해지면서 그 다양성속에 표류하는 현대인의 성을 짚어본다. “동거가 이혼을 줄일 수 있다던데... 매춘이 강간을 방지할 수도 있다던데... 성형수술은 일종의 화장 같은 것이 아닐까... 아무리 그래도 호모 섹슈얼리티는 인간의 본질을 거스르는 게 아닐까...” 등과 같은 성에 관한 이슈, 담론, 사회문화적 통념 등을 토론하면서, 성의 사회·정치적, 문화·심리적 함의에 관하여 생각해보고 성에 관한 자율적이고 성숙한 관점을 갖도록 한다.','1. 사랑과 연애, 직업과 결혼, 섹스와 심리 등의 의미를 살펴보고, 이러한 주제를 자신의 삶에 적용하여 비판적으로 해석하는 능력을 배양한다.\n2. 젠더라는 렌즈를 통해 기존의 사회, 정치, 문화 현실을 분석하고, 한 개체로서 자신 안에 내재된 여성성과 남성성을 균형 있게 발전시키는 성숙함을 갖게 한다.\n3. 남녀 사이의 갈등을 지혜롭게 풀어가기 위한 다양한 방법을 토론하며 자기성찰의 중요성을 인식한다.','출석,20,수업태도,10,중간고사,35,기말고사,35,발표,0,토의,0,과제,0,Quiz,','성차이의 심리,송관재,선학사','임혜선','lovennlove@nate.com',94),('2014/2-0000-1-3928-03','본 강좌에서는 개인 속에 자리 잡고 있는 남성으로서의 혹은 여성으로서의 성정체성이 어떻게 형성되어 왔는지를 분석하고 토론하게 된다. 더 나아가 사랑과 성에 관한 담론이 다양해지면서 그 다양성속에 표류하는 현대인의 성을 짚어본다. “동거가 이혼을 줄일 수 있다던데... 매춘이 강간을 방지할 수도 있다던데... 성형수술은 일종의 화장 같은 것이 아닐까... 아무리 그래도 호모 섹슈얼리티는 인간의 본질을 거스르는 게 아닐까...” 등과 같은 성에 관한 이슈, 담론, 사회문화적 통념 등을 토론하면서, 성의 사회·정치적, 문화·심리적 함의에 관하여 생각해보고 성에 관한 자율적이고 성숙한 관점을 갖도록 한다.','1. 사랑과 연애, 직업과 결혼, 섹스와 심리 등의 의미를 살펴보고, 이러한 주제를 자신의 삶에 적용하여 비판적으로 해석하는 능력을 배양한다.\n2. 젠더라는 렌즈를 통해 기존의 사회, 정치, 문화 현실을 분석하고, 한 개체로서 자신 안에 내재된 여성성과 남성성을 균형 있게 발전시키는 성숙함을 갖게 한다.\n3. 남녀 사이의 갈등을 지혜롭게 풀어가기 위한 다양한 방법을 토론하며 자기성찰의 중요성을 인식한다.','출석,20,수업태도,10,중간고사,35,기말고사,35,발표,0,토의,0,과제,0,Quiz,','성차이의 심리,송관재,선학사','임혜선','lovennlove@nate.com',100),('2014/2-0000-1-3948-01','사회학적 상상력을 배우고 익힘으로써 다양하고 복잡한 현대사회의 특징과 변화 양상을 총체적 관점에서 이해하고 분석한다. 자본주의와 경제생활, 노동과 소외, 계급과 계층, 민주주의와 사회운동, 국가와 민족, 정치와 관료제, 가족과 젠더, 사회규범과 일탈, 도시사회와 지역문제, 세계화와 미래사회 등 다양한 주제들을 통해 현대사회의 변화 양상을 다각적으로 다룬다. 발표와 토론을 통해 학생들이 직접 보고 느끼는 현실문제에 대한 이해력과 분석력을 높인다.','1. 사회학적 상상력을 발휘하여 현대사회의 특성을 이해하고 설명할 수 있는 능력을 배양하고 복합적인 현실 문제를 이해하고 분석하는 시각을 기른다. \n2. 자본주의와 시장경제체제의 특성을 파악하고 세계화의 역사적 의미를 이해한다. \n3. 민주주의와 민족주의의 역사적 다양성과 다차원성을 파악한다.\n4. 가족, 학교, 직장 등 사회화와 주체화의 제도와 조직, 문화를 폭넓게 이해하고 사회의 규범적 통합과 일탈 현상의 함의를 이해한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,20,토의,0,과제,0,Quiz','사회학: 비판적 사회읽기,비판사회학회,한울아카데미,2012','','',45),('2014/2-0000-1-3948-02','사회학적 상상력을 배우고 익힘으로써 다양하고 복잡한 현대사회의 특징과 변화 양상을 총체적 관점에서 이해하고 분석한다. 자본주의와 경제생활, 노동과 소외, 계급과 계층, 민주주의와 사회운동, 국가와 민족, 정치와 관료제, 가족과 젠더, 사회규범과 일탈, 도시사회와 지역문제, 세계화와 미래사회 등 다양한 주제들을 통해 현대사회의 변화 양상을 다각적으로 다룬다. 발표와 토론을 통해 학생들이 직접 보고 느끼는 현실문제에 대한 이해력과 분석력을 높인다.','1. 사회학적 상상력을 발휘하여 현대사회의 특성을 이해하고 설명할 수 있는 능력을 배양하고 복합적인 현실 문제를 이해하고 분석하는 시각을 기른다. \n2. 자본주의와 시장경제체제의 특성을 파악하고 세계화의 역사적 의미를 이해한다. \n3. 민주주의와 민족주의의 역사적 다양성과 다차원성을 파악한다.\n4. 가족, 학교, 직장 등 사회화와 주체화의 제도와 조직, 문화를 폭넓게 이해하고 사회의 규범적 통합과 일탈 현상의 함의를 이해한다.','출석,10,수업태도,10,중간고사,40,기말고사,40,발표,0,토의,0,과제,0,Quiz,','매주 제시되는 강의안ppt','김현수','pdsuchup1@naver.com',47),('2014/2-0000-1-4061-01','- 한학기동안 학교에서 정해준 봉사기관 또는 개인적으로 봉사활동 기관(VMS 인증기관에 한함)을 정해 봉사활동 실시(평균적으로 일주일에 3시간)\n- 총 30시간 이상 현장봉사활동을 실시하고 학기말에 봉사활동 보고서를 제출해야만 과정 수료 가능\n- 봉사활동 기관 선정과 오리엔테이션이 이루어지는 개강 첫째날과 봉사활동 보고서를 제출하는 마지막 강의장 수업 불참시 과정 이수가 불가능함\n- 만일 추가등록을 생각하고 있는 경우 과정등록 전이어도 오리엔테이션에는 반드시 참석하여야만 과정 수료가 가능함(**오리엔테이션에 참석하지 않은 경우 추가등록 후 과정수료를 할 수 없음**)','지역 및 소외계층 봉사의 필요성과 중요성을 현장학습을 통해 체험하며, 소외 계층의 어려움을 함께 나누고 더불어 사는 방법을 습득한다','출석,90,중간고사,0,기말고사,0,과제보고서,10,수업태도,0,Quiz,0,기타,0','현장 학습으로 교재가 없음','오경숙','rudtnr0929@hanmail.net',97),('2014/2-0000-1-4081-01','우리는 미키마우스, 아톰, 둘리, 나우시카 등 애니메이션의 캐릭터들로부터 살아있는 인간 못지않은 친밀함을 느낄 수 있다. 본 강좌는 만화 및 애니메이션의 역사를 각 국가의 사회적, 문화적 특수성을 기반으로 개괄한다. 각 작품 및 작가/감독을 예술적 측면과 산업적 측면에서 조명하고 이를 비교분석한다. 학생들은 마음에 남는 작품을 선택하여 이를 보다 다양한 시각으로 조사 및 분석하여 보고서를 작성한다.','1. 만화 및 애니메이션의 역사를 이해하고 동굴벽화에서 현대 애니메이션까지의 지도를 그릴 수 있다.\n2. 데즈카 오사무, 월트 디즈니, 미야자키 하야오 등 거장들의 작품 및 정신세계를 이해하고 이를 비교분석한다.\n3. 한 편의 애니메이션을 예술적 및 상업적 측면에서 평가할 수 있다.\n4. 애니메이션의 창조적 상상력을 경험함으로써 보다 창의적인 사고능력을 배양한다.\n5. 선택한 작품에 관해 보고서를 작성하고 이에 대해 토론하는 과정을 통해 발표 및 소통능력을 배양한다.','출석,10,수업태도,0,중간고사,30,기말고사,30,발표,10,토의,0,과제,20,Quiz','교안중심','백인창','pullgg@naver.com',98),('2014/2-0000-1-4081-02','우리는 미키마우스, 아톰, 둘리, 나우시카 등 애니메이션의 캐릭터들로부터 살아있는 인간 못지않은 친밀함을 느낄 수 있다. 본 강좌는 만화 및 애니메이션의 역사를 각 국가의 사회적, 문화적 특수성을 기반으로 개괄한다. 각 작품 및 작가/감독을 예술적 측면과 산업적 측면에서 조명하고 이를 비교분석한다. 학생들은 마음에 남는 작품을 선택하여 이를 보다 다양한 시각으로 조사 및 분석하여 보고서를 작성한다.','1. 만화 및 애니메이션의 역사를 이해하고 동굴벽화에서 현대 애니메이션까지의 지도를 그릴 수 있다.\n2. 데즈카 오사무, 월트 디즈니, 미야자키 하야오 등 거장들의 작품 및 정신세계를 이해하고 이를 비교분석한다.\n3. 한 편의 애니메이션을 예술적 및 상업적 측면에서 평가할 수 있다.\n4. 애니메이션의 창조적 상상력을 경험함으로써 보다 창의적인 사고능력을 배양한다.\n5. 선택한 작품에 관해 보고서를 작성하고 이에 대해 토론하는 과정을 통해 발표 및 소통능력을 배양한다.','출석,10,수업태도,0,중간고사,30,기말고사,30,발표,10,토의,0,과제,20,Quiz','교안중심','홍종민','didlswhdals@naver.com',98),('2014/2-0000-1-4282-01','본 교과목은 1학년 학생들을 대상으로 한 강좌로서, 오늘날 지구촌과 한국사회가 직면하고 있는 다양한 사회문제들에 대해 관심을 갖고 조사하고, 그 원인과 해법에 대해 함께 생각하고 토론함으로써 현대사회에 대한 이해력과 통찰력을 높이고, 수강생들에게 발표력과 의사소통력을 배양하기 위해 마련된 교과목이다. 한 학기동안 수강생들은 매주 주어진 토론주제에 대해 예비조사, 발표 및 토론과정에 성실하고 적극적으로 참여해야 한다.','1. 현대사회의 다양한 사회현상들에 대한 관심의 폭과 이해의 수준을 높이고, 관련된 인문학과 사회과학의 연구성과를 이해할 수 있는 교양인으로서의 소양을 기른다. 2. 사회현상에 대해 스스로의 힘으로 조사하여 그 내용을 다른 사람에게 효과적으로 전달할 수 있는 발표력과, 자신의 생각을 논리적이고 설득력있게 전달할 수 있는 말하기 능력을 배양한다. 3. 다른 사람의 발표나 주장을 경청하고, 그 핵심 논지를 파악하여 적극적이고 능동적으로 토론에 참여할 수 있는 자세와 능력을 함양한다. 문제에 대한 이해력 당면된 현안의 구조적인 문제와 배경을 파악하고, 향후 사회에 진출하거나 한 단계 위의 공부를 할 때 스스로의 생각을 체계적으로 정리하고 논리적으로 설명하는 능력을 키운다. 2. 본 강좌는 토론수업의 형태로 진행되며, 따라서 수강생들이 다양한 주제에 대해 자신의 사고를 체계적으로 정리하고, 이를 논리적으로 표현하는 능력을 키운다.','출석,10,중간고사,20,기말고사,20,과제보고서,20,수업태도,30,Quiz,0,기타,0','교안중심','','',12),('2014/2-0000-1-4282-02','본 교과목은 오늘날 한반도와 국제사회가 직면하고 있는 다양한 문제들에 대해 분석하고 이해함으로서 문제 발생의 구조적인 이유와 배경을 파악하여, 우리 스스로의 일상생활에 대한 현실적 좌표를 파악하고 향후의 삶을 설계하는 능력을 배양하기 위한 교과목이다. 수강생들은 관련 문헌 및 자료를 숙지하여 자신의 생각을 체계적으로 요약하여 발표하고 조리있게 토론하는 훈련을 병행한다','1. 당면된 현안의 구조적인 문제와 배경을 파악하고, 향후 사회에 진출하거나 한 단계 위의 공부를 할 때 스스로의 생각을 체계적으로 정리하고 논리적으로 설명하는 능력을 키운다. 2. 본 강좌는 토론수업의 형태로 진행되며, 따라서 수강생들이 다양한 주제에 대해 자신의 사고를 체계적으로 정리하고, 이를 논리적으로 표현하는 능력을 키운다.','출석,20,중간고사,20,기말고사,30,과제보고서,20,수업태도,0,Quiz,0,기타,10','간결한 세계경제사,이 헌대옮김,법문사,2001','임종훈','jonghun92@naver.com',13),('2014/2-0000-1-4283-01','공학도들이 갖추어야할 기본 소양과 지식을 갖추기 위한 발표 방식의 세미나 수업.','공학을 처음 접하는 학생들에게 공학의 개념을 심어주어 학생들이 공학적 소양과 기본자질을 기질 수 있도록 한다. 수업은 학생들이 조사한 내용을 발표하고 이를 토의하는 방식으로 진행된다. 수업의 내용은 공학적 기술이 내포된 기기, 상황, 시스템 등이며, 수업이 진행될수록 공학적 요소가 많이 포함된 주제를 다룬다. 또한 수업이 세미나 방식으로 진행되기 때문에 세미나에 대한 기법 등이 같이 다루어지며, 수업이 진행됨에 따라 세미나를 기획하는 과정, 기법과 발표, 디자인 등을 주제 발표를 통하여 배우게 된다. 프리젠테이션 관련 과정에서 필요한 사고 및 접근기법 등을 각자의 수준 및 발표의 과정을 순차적으로 배워간다.','출석,10,중간고사,30,기말고사,0,과제보고서,0,수업태도,0,Quiz,0,기타,60','배포자료','김윤주','younjookim@kw.ac.kr',19),('2014/2-0000-1-4283-02','공학도들이 갖추어야할 기본 소양과 지식을 갖추기 위한 발표 방식의 세미나 수업.','공학을 처음 접하는 학생들에게 공학의 개념을 심어주어 학생들이 공학적 소양과 기본자질을 기질 수 있도록 한다. 수업은 학생들이 조사한 내용을 발표하고 이를 토의하는 방식으로 진행된다. 수업의 내용은 공학적 기술이 내포된 기기, 상황, 시스템 등이며, 수업이 진행될수록 공학적 요소가 많이 포함된 주제를 다룬다. 또한 수업이 세미나 방식으로 진행되기 때문에 세미나에 대한 기법 등이 같이 다루어지며, 수업이 진행됨에 따라 세미나를 기획하는 과정, 기법과 발표, 디자인 등을 주제 발표를 통하여 배우게 된다. 프리젠테이션 관련 과정에서 필요한 사고 및 접근기법 등을 각자의 수준 및 발표의 과정을 순차적으로 배워간다.','출석,20,중간고사,20,기말고사,0,과제보고서,0,수업태도,0,Quiz,0,기타,60','배포자료','','',19),('2014/2-0000-1-4390-01','영화와 문학, 문학과 영화는 상생의 관계에 있는 우리 시대의 주요한 문화적 코드이다. \n이 강좌는 문학을 바탕으로 한 영화들을 살펴보면서 문학과 영화 매체의 장 &#8228; 단점 및 그 차이를 생각해 보는 강좌이다. 또한 영화가 보여주는 각색의 미학을 살펴보고 문학의 언어가 영상언어로 바뀌며 어떻게 변화되는지 또한 살펴본다. \n문학과 영화가 표현하고자 하는 사회문제나 사상들에 대한 학생들의 발표와 토론을 통해 수강생들의 주체적 문화비평 활동을 실시한다.','1. 문학과 영화의 스토리텔링이 지닌 유사성과 차이성을 파악한다. \n2. 영상 서사를 구성하는 이미지들의 의미를 분석하고 비평한다. \n3. 영상언어와 활자언어의 부딪침 속에서 생성되는 비판적 문화감각을 길러내 인성교양 지식을 확대한다. \n4. 문학을 바탕으로 제작된 영화 속에서 다루어진 사회문제들에 대한 이해를 통해 복합문제해결능력을 배양한다.\n5. 한국과 해외 영화를 고르게 다루면서 글로벌 마인드를 고취하고, 학생들의 발표와 토론을 통해 의사소통능력을 증대한다.','출석,20,수업태도,10,중간고사,20,기말고사,30,발표,10,토의,10,과제,0,Qui','특별한 수업 주교재 없음','','',39),('2014/2-0000-1-4638-01','회계와 세무에 대해 이해하고 실제 생활에서 적용할 수 있도록 한다. 구체적으로는 부가가치세, 소득세, 상속세, 증여세, 법인세, 지방세, 취득세 등 각종 세금과 회계원리에 대한 기본적 이해를 도모한다. 이를 통해 세법에 대한 명확한 이해와 적응능력으로 세무회계를 쉽고 빠르게 처리할 수 있는 방법을 숙달한다.','1. 현실세계에서 회계와 세무와 관련한 상황에 직면한 경우 이에 대해 이해할 수 있고 올바른 의사결정을 내릴 수 있게 한다.\n2. 세금에 관한 기초지식을 확보함으로써 향후 경영자 또는 직장생활에서 필요한 절세 방법과 세법에 대한 이해를 꾀한다. \n3. 발표와 토의를 통해 회계 및 세무에 대한 여러 사람의 사고를 공유함으로써 연관된 복합적인 문제를 보다 합리적으로 해결하는 능력을 배양한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,1,토의,1,과제,3,Quiz,','2014 알기쉬운 세무실무,이형래, 전병린,어울림,2014','이현구','qhfl1207@naver.com',64),('2014/2-0000-1-4723-01','커뮤니케이션이란 나와 나, 나와 다른 사람들, 나와 세계의 관계맺기 행위이다. 본 교과목은 이러한 커뮤니케이션의 다양한 현상에 대해 열린 호기심을 가지고 주의깊게 관찰하고, 그 안에 담긴 뜻을 읽어내며, 나아가 인간, 사회, 문화, 미디어, 과학, 기술 등 다양한 영역에서의 커뮤니케이션 현상을 이해하고자 한다. 문자, 영상, 소리 등 다양한 형식으로 구성된 콘텐츠(예를 들어 책, 영화, 방송프로그램, 음악, 디지털 콘텐츠)를 풍부하게 활용하여 읽기, 비평, 평가, 제작 및 유통 등 디지털 정보 리터러시의 실제를 습득하고 발휘할 수 있도록 한다.','1. 사회의 구성 원리로서 커뮤니케이션의 의미와 역할을 이해하고, 커뮤니케이션학에 대한 관심을 고취시킨다. \n2. 현대 멀티미디어 환경에서 나타나는 다양한 커뮤니케이션 및 미디어 현상에 대해 비판적으로 분석하며 커뮤니케이션의 다양한 방식을 이해하는 과정에서 상상력과 창의력을 기른다.\n3. 나를 둘러싼 인간 및 세계와의 원활한 소통방식을 토론하고 다양한 매체의 활용을 통해 문화적 감수성을 배양하며 커뮤니케이션 능력을 향상시킨다.','출석,10,수업태도,10,중간고사,40,기말고사,40,발표,0,토의,0,과제,0,Quiz,','현대사회와 미디어 (2014년개정판),한균태 외,카뮤니케이션북스,2014','','',48),('2014/2-0000-1-4723-02','커뮤니케이션이란 나와 나, 나와 다른 사람들, 나와 세계의 관계맺기 행위이다. 본 교과목은 이러한 커뮤니케이션의 다양한 현상에 대해 열린 호기심을 가지고 주의깊게 관찰하고, 그 안에 담긴 뜻을 읽어내며, 나아가 인간, 사회, 문화, 미디어, 과학, 기술 등 다양한 영역에서의 커뮤니케이션 현상을 이해하고자 한다. 문자, 영상, 소리 등 다양한 형식으로 구성된 콘텐츠(예를 들어 책, 영화, 방송프로그램, 음악, 디지털 콘텐츠)를 풍부하게 활용하여 읽기, 비평, 평가, 제작 및 유통 등 디지털 정보 리터러시의 실제를 습득하고 발휘할 수 있도록 한다.','1. 사회의 구성 원리로서 커뮤니케이션의 의미와 역할을 이해하고, 커뮤니케이션학에 대한 관심을 고취시킨다. \n2. 현대 멀티미디어 환경에서 나타나는 다양한 커뮤니케이션 및 미디어 현상에 대해 비판적으로 분석하며 커뮤니케이션의 다양한 방식을 이해하는 과정에서 상상력과 창의력을 기른다.\n3. 나를 둘러싼 인간 및 세계와의 원활한 소통방식을 토론하고 다양한 매체의 활용을 통해 문화적 감수성을 배양하며 커뮤니케이션 능력을 향상시킨다.','출석,10,수업태도,10,중간고사,40,기말고사,40,발표,0,토의,0,과제,0,Quiz,','추후공지','','',68),('2014/2-0000-1-4723-03','커뮤니케이션이란 나와 나, 나와 다른 사람들, 나와 세계의 관계맺기 행위이다. 본 교과목은 이러한 커뮤니케이션의 다양한 현상에 대해 열린 호기심을 가지고 주의깊게 관찰하고, 그 안에 담긴 뜻을 읽어내며, 나아가 인간, 사회, 문화, 미디어, 과학, 기술 등 다양한 영역에서의 커뮤니케이션 현상을 이해하고자 한다. 문자, 영상, 소리 등 다양한 형식으로 구성된 콘텐츠(예를 들어 책, 영화, 방송프로그램, 음악, 디지털 콘텐츠)를 풍부하게 활용하여 읽기, 비평, 평가, 제작 및 유통 등 디지털 정보 리터러시의 실제를 습득하고 발휘할 수 있도록 한다.','1. 사회의 구성 원리로서 커뮤니케이션의 의미와 역할을 이해하고, 커뮤니케이션학에 대한 관심을 고취시킨다. \n2. 현대 멀티미디어 환경에서 나타나는 다양한 커뮤니케이션 및 미디어 현상에 대해 비판적으로 분석하며 커뮤니케이션의 다양한 방식을 이해하는 과정에서 상상력과 창의력을 기른다.\n3. 나를 둘러싼 인간 및 세계와의 원활한 소통방식을 토론하고 다양한 매체의 활용을 통해 문화적 감수성을 배양하며 커뮤니케이션 능력을 향상시킨다.','출석,10,수업태도,10,중간고사,40,기말고사,40,발표,0,토의,0,과제,0,Quiz,','추후공지','','',20),('2014/2-0000-1-4908-01','대학교 수준의 어휘와 문법이 적용된 최근의 재미있는 10개의 다양한 이슈를 Reading하고 기본적인 Writing을 할 수 있도록 함으로써, 학생들이 현실세계에 적용할 수 있도록 동기를 부여한다.따라서 학생들은 언어의 형태와 컨텐츠를 잘 배워 더 지적으로 정서적으로 복잡한 사고를 유연하게 표현할 수 있다.','Unit은 Focus on Topic, Reading, and Writing section으로 구성되어 있다. 학생들이 critical thinking, inferencing, synthesizing, paragraph writing에 초점을 맞춤으로써reading comprehension과 basic writing capability를 향상시키는데 목적이 있다.','출석,10,중간고사,25,기말고사,25,과제보고서,15,수업태도,10,Quiz,15,기타,','North Star - Reading and Writing Level 4, Third Edition,Andrew K. English and Laura Monahon,Pearson ','임경주','kj10047@naver.com',19),('2014/2-0000-1-4908-02','대학교 수준의 어휘와 문법이 적용된 최근의 재미있는 10개의 다양한 이슈를 Reading하고 기본적인 Writing을 할 수 있도록 함으로써, 학생들이 현실세계에 적용할 수 있도록 동기를 부여한다.따라서 학생들은 언어의 형태와 컨텐츠를 잘 배워 더 지적으로 정서적으로 복잡한 사고를 유연하게 표현할 수 있다.','Unit은 Focus on Topic, Reading, and Writing section으로 구성되어 있다. 학생들이 critical thinking, inferencing, synthesizing, paragraph writing에 초점을 맞춤으로써reading comprehension과 basic writing capability를 향상시키는데 목적이 있다.','출석,10,중간고사,25,기말고사,25,과제보고서,15,수업태도,10,Quiz,15,기타,','North Star - Reading and Writing Level 4, Third Edition,Andrew K. English and Laura Monahon,Pearson ','임경주','kj10047@naver.com',17),('2014/2-0000-1-4908-03','This course reads eight themes and contents in a variety of genres with relevant vocabulary and grammar in university level and guides students to successfully write paragraph essays with selected topics.','This course aims for the integration of reading and writing in English, for which it focuses on critical thinking, inferencing, synthesizing, test taking, and paragraph writing, in order to help students develop reading comprehension and writing capability.','출석,10,중간고사,30,기말고사,35,과제보고서,10,수업태도,5,Quiz,10,기타,0','North Star – Reading and Writing Level 4, Third Edition,Andrew K. English and Laura Monahon English,','류명경','tzjzlmq@outlook.com',18),('2014/2-0000-1-4908-04','This course reads six themes and contents in a variety of genres with relevant vocabulary and grammar in university level and guides students to successfully write paragraph essays with selected topics.','This course aims for the integration of reading and writing in English, for which it focuses on critical thinking, inferencing, synthesizing, test taking, and paragraph writing, in order to help students develop reading comprehension and writing capability.','출석,10,중간고사,25,기말고사,25,과제보고서,20,수업태도,10,Quiz,10,기타,','North Star – Reading and Writing Level 4, Third Edition,Andrew K. English and Laura Monahon English,','','',20),('2014/2-0000-1-4908-05','This course covers eight themes and contents in a variety of genres with relevant vocabulary and grammar in university level and guides students to successfully write paragraph essays with selected topics.','This course aims for the integration of reading and writing in English, for which it focuses on critical thinking, inferencing, synthesizing, test taking, and paragraph writing, in order to help students develop reading comprehension and writing capability.','출석,10,중간고사,25,기말고사,25,과제보고서,20,수업태도,5,Quiz,15,기타,0','North Star - Reading and Writing Level 4, Third Edition,Andrew K. English and Laura Monahon English,','','',20),('2014/2-0000-1-4908-06','This course reads eight themes and contents in a variety of genres with relevant vocabulary and grammar in university level and guides students to successfully write paragraph essays with selected topics.','This course aims for the integration of reading and writing in English, for which it focuses on critical thinking, inferencing, synthesizing, test taking, and paragraph writing, in order to help students develop reading comprehension and writing capability.','출석,10,중간고사,25,기말고사,25,과제보고서,20,수업태도,5,Quiz,10,기타,5','North Star – Reading and Writing Level 4, Third Edition,Andrew K. English and Laura Monahon English,','차성철','chasc88@naver.com',17),('2014/2-0000-1-4908-07','This course reads eight themes and contents in a variety of genres with relevant vocabulary and grammar in university level and guides students to successfully write paragraph essays with selected topics.','This course aims for the integration of reading and writing in English, for which it focuses on critical thinking, inferencing, synthesizing, test taking, and paragraph writing, in order to help students develop reading comprehension and writing capability.','출석,10,중간고사,25,기말고사,25,과제보고서,20,수업태도,5,Quiz,10,기타,5','North Star – Reading and Writing Level 4, Third Edition,Andrew K. English and Laura Monahon English,','차성철','chasc88@naver.com',16),('2014/2-0000-1-4908-08','This course reads eight themes and contents in a variety of genres with relevant vocabulary and grammar in university level and guides students to successfully write paragraph essays with selected topics.','This course aims for the integration of reading and writing in English, for which it focuses on critical thinking, inferencing, synthesizing, test taking, and paragraph writing, in order to help students develop reading comprehension and writing capability.','출석,10,중간고사,25,기말고사,25,과제보고서,20,수업태도,5,Quiz,10,기타,5','North Star – Reading and Writing Level 4, Third Edition,Andrew K. English and Laura Monahon English,','차성철','chasc88@naver.com',18),('2014/2-0000-1-4908-09','This course reads eight themes and contents in a variety of genres with relevant vocabulary and grammar in university level and guides students to successfully write paragraph essays with selected topics.\n(본 강좌는 100% 영어로 진행합니다. 편하게 이해할 수 있는 속도와 수준으로 할 예정이니 부담갖지 마세요! 학생은 한국어로 질문해도 좋습니다.)','This course aims for the integration of reading and writing in English, for which it focuses on critical thinking, inferencing, synthesizing, test taking, and paragraph writing, in order to help students develop reading comprehension and writing capability.','출석,5,중간고사,25,기말고사,25,과제보고서,25,수업태도,5,Quiz,10,기타,5','North Star; Reading and Writing Level 4, Third Edition,Andrew K. English and Laura Monahon English,P','','',19),('2014/2-0000-1-4908-10','This course reads eight themes and contents in a variety of genres with relevant vocabulary and grammar in university level and guides students to successfully write paragraph essays with selected topics.','This course aims for the integration of reading and writing in English, for which it focuses on critical thinking, inferencing, synthesizing, test taking, and paragraph writing, in order to help students develop reading comprehension and writing capability.','출석,10,중간고사,25,기말고사,25,과제보고서,20,수업태도,5,Quiz,10,기타,5','North Star-Reading and Writing Level 4, Third Edition,Andrew K. English and Laura Monahon English,Pe','임경주','kj10047@naver.com',19),('2014/2-0000-1-4933-01','중국어의 기초지식을 습득했거나 기초중국어회화1을 이수한 학생들을 위한 과목이다. 기초과정에서 배운 지식을 바탕으로 다양한 표현과 어법을 익히게 된다. 실용적인 회화를 중심으로 일상생활에서 꼭 필요한 중국어 표현을 배움으로써 실제 상황에 유연하게 대처할 수 있는 능력을 키울 수 있다. 이와 더불어 중국인들의 생활과　문화전반에　대한 이해에도 도움이 될 것이다.','1. 쉽고 다양한 표현방식을 익히면서 중국어 말하기에 자신감을 키운다.　　\n2. 결과보어, 가능보어 등 중국어의 독특한 어법을 익혀 중국어 표현 능력을 향상시킨다.\n3. 중국어　학습을　통해　중국　문화에　대한　이해의 폭을 넓힌다.　\n4. 중국　전문가로 성장할 학생들에게 유용한 지식을 제공한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','첫날 수업이 알려줘요','','',12),('2014/2-0000-1-4933-02','중국어의 기초지식을 습득했거나 기초중국어회화1을 이수한 학생들을 위한 과목이다. 기초과정에서 배운 지식을 바탕으로 다양한 표현과 어법을 익히게 된다. 실용적인 회화를 중심으로 일상생활에서 꼭 필요한 중국어 표현을 배움으로써 실제 상황에 유연하게 대처할 수 있는 능력을 키울 수 있다. 이와 더불어 중국인들의 생활과　문화전반에　대한 이해에도 도움이 될 것이다.','1. 쉽고 다양한 표현방식을 익히면서 중국어 말하기에 자신감을 키운다.　　\n2. 결과보어, 가능보어 등 중국어의 독특한 어법을 익혀 중국어 표현 능력을 향상시킨다.\n3. 중국어　학습을　통해　중국　문화에　대한　이해의 폭을 넓힌다.　\n4. 중국　전문가로 성장할 학생들에게 유용한 지식을 제공한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','친절한 중국어 level 2 (파란색),곡효운, 곡효여,그린','','',20),('2014/2-0000-1-4933-03','중국어의 기초지식을 습득했거나 기초중국어회화1을 이수한 학생들을 위한 과목이다. 기초과정에서 배운 지식을 바탕으로 다양한 표현과 어법을 익히게 된다. 실용적인 회화를 중심으로 일상생활에서 꼭 필요한 중국어 표현을 배움으로써 실제 상황에 유연하게 대처할 수 있는 능력을 키울 수 있다. 이와 더불어 중국인들의 생활과　문화전반에　대한 이해에도 도움이 될 것이다.','1. 쉽고 다양한 표현방식을 익히면서 중국어 말하기에 자신감을 키운다.　　\n2. 결과보어, 가능보어 등 중국어의 독특한 어법을 익혀 중국어 표현 능력을 향상시킨다.\n3. 중국어　학습을　통해　중국　문화에　대한　이해의 폭을 넓힌다.　\n4. 중국　전문가로 성장할 학생들에게 유용한 지식을 제공한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','친절한 중국어,곡효운, 곡효여,그린출판사','','',20),('2014/2-0000-1-4933-04','중국어의 기초지식을 습득했거나 기초중국어회화1을 이수한 학생들을 위한 과목이다. 기초과정에서 배운 지식을 바탕으로 다양한 표현과 어법을 익히게 된다. 실용적인 회화를 중심으로 일상생활에서 꼭 필요한 중국어 표현을 배움으로써 실제 상황에 유연하게 대처할 수 있는 능력을 키울 수 있다. 이와 더불어 중국인들의 생활과　문화전반에　대한 이해에도 도움이 될 것이다.','1. 쉽고 다양한 표현방식을 익히면서 중국어 말하기에 자신감을 키운다.　　\n2. 결과보어, 가능보어 등 중국어의 독특한 어법을 익혀 중국어 표현 능력을 향상시킨다.\n3. 중국어　학습을　통해　중국　문화에　대한　이해의 폭을 넓힌다.　\n4. 중국　전문가로 성장할 학생들에게 유용한 지식을 제공한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','친절한 중국어 LEVEL 2,곡효여, 곡효운,그린,2013','','',20),('2014/2-0000-1-4933-05','중국어의 기초지식을 습득했거나 기초중국어회화1을 이수한 학생들을 위한 과목이다. 기초과정에서 배운 지식을 바탕으로 다양한 표현과 어법을 익히게 된다. 실용적인 회화를 중심으로 일상생활에서 꼭 필요한 중국어 표현을 배움으로써 실제 상황에 유연하게 대처할 수 있는 능력을 키울 수 있다. 이와 더불어 중국인들의 생활과　문화전반에　대한 이해에도 도움이 될 것이다.','1. 쉽고 다양한 표현방식을 익히면서 중국어 말하기에 자신감을 키운다.　　\n2. 결과보어, 가능보어 등 중국어의 독특한 어법을 익혀 중국어 표현 능력을 향상시킨다.\n3. 중국어　학습을　통해　중국　문화에　대한　이해의 폭을 넓힌다.　\n4. 중국　전문가로 성장할 학생들에게 유용한 지식을 제공한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','친절한 중국어,곡효운, 곡효여,그린출판사','','',20),('2014/2-0000-1-4933-06','중국어의 기초지식을 습득했거나 기초중국어회화1을 이수한 학생들을 위한 과목이다. 기초과정에서 배운 지식을 바탕으로 다양한 표현과 어법을 익히게 된다. 실용적인 회화를 중심으로 일상생활에서 꼭 필요한 중국어 표현을 배움으로써 실제 상황에 유연하게 대처할 수 있는 능력을 키울 수 있다. 이와 더불어 중국인들의 생활과　문화전반에　대한 이해에도 도움이 될 것이다.','1. 쉽고 다양한 표현방식을 익히면서 중국어 말하기에 자신감을 키운다.　　\n2. 결과보어, 가능보어 등 중국어의 독특한 어법을 익혀 중국어 표현 능력을 향상시킨다.\n3. 중국어　학습을　통해　중국　문화에　대한　이해의 폭을 넓힌다.　\n4. 중국　전문가로 성장할 학생들에게 유용한 지식을 제공한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','친절한 중국어 LEVEL 2,곡효여, 곡효운,그린,2013','','',20),('2014/2-0000-1-5224-01','신화의 중요성, 신화를 바라보는 시각, 신화의 어떤 이야기 구조가 대중 내러티브에 차용되고 있는지 등을 다양하게 파악하면서, 동서양의 신화가 현대 사회에서 변용되는 다양한 원인과 사례를 분석한다. \n한국신화, 그리스&#8228;로마신화, 북유럽신화, 중국신화 등 동서양의 신화를 읽고, 신화적 상상력과 상징 및 기호체계에 대한 해석 능력을 키운다. \n현대문화에 녹아든 주요 신화들에 대한 발표와 토론을 실시한다.','1. 현대사회와 문화에 직간접적으로 영향을 미치는 동서양의 주요 신화의 내용을 파악하여 인성교양을 확대한다.\n2. 동서양의 신화에 대한 융합적 이해와 더불어 예술 텍스트와 다양한 문화 상품에서 신화가 변용돼 활용되고 있는 현상을 이해한다.\n3. 동서양 각국의 문화 속에 변용된 신화를 이해하여 글로벌 마인드를 고양한다.','출석,10,수업태도,0,중간고사,0,기말고사,50,발표,20,토의,10,과제,10,Quiz','친절한 중국어 LEVEL 2,곡효여, 곡효운,그린,2013','','',65),('2014/2-0000-1-5225-01','과학은 단순한 지식이 아닌, 그 시대의 사상과 제도 문화의 일부이며, 구체적인 역사적 맥락에서 생산된 것이다. 본 강좌는 일본, 중국, 한국 3국의 근대 이후의 과학기술의 역사성 이해를 목적으로 한다. 수업은 교수의 강의와 학생의 과제 발표로 진행한다. 본 강좌는 동북아대학 학생을 수강대상으로 한다.','동북아 특히 일본과 중국의 근현대 흐름의 특징을 파악하고 이들 국가의 과학기술 발전 추이의 이해를 도모한다.','출석,20,중간고사,0,기말고사,40,과제보고서,20,수업태도,20,Quiz,0,기타,0','강의텍스트(유캠에 매주 업로드)','','',45),('2014/2-0000-1-5226-01','한국전통문화란 무엇인가? 본 강좌에서는 한국전통문화를 각론적인 사례보다는 총론적인 포괄적 접근으로 개괄하여 한국전통문화의 사상적/미학적 이해를 도모한다. 전통, 문화, 원형, 일상성을 미학적인 관점으로 학습하고 한국사상사를 통해 그 기반을 이해한다. 한국문화에 내재하는 꿈과 멋과 힘의 미학을 알기 위해 전통문화의 현장을 찾아 우리의 정체성에 대해 고민하고 토론한다.','1. 한국전통문화를 미학적으로 이해하여 개별사례를 포괄할 수 있는 지도를 그린다. \n2. 전통문화의 재발견을 통해 우리문화의 우수성을 깨닫고 한국전통문화에 대한 자부심을 가진다.\n3. 생활현장에 녹아있는 한국적 미학을 찾아보고 이러한 과정을 통해 나의 정체성을 확립한다.\n4. 우리 전통문화를 다른 문화권의 전통문화와 비교할 수 있다.\n5. 자신의 전공에 연계될 수 있는 전통문화에 대해 고민하고 이를 문화콘텐츠 개발로 발전시킨다.\n6. 고궁답사 또는 박물관 견학에 대한 보고서를 작성하고 이에 대해 토론하는 과정을 통해 이론을 보다 실천적인 방식으로 체험한다.','출석,10,수업태도,10,중간고사,20,기말고사,20,발표,20,토의,0,과제,10,Qui','우리가 알고있는 한국문화 버리기,최경원,현 디자인 연구소,2013','','',41),('2014/2-0000-1-5378-01','본 수업은 댄스스포츠의 기본 지식과 스텝을 익히고 다양한 테크닉을 구사함과 동시에 스포츠를 통해 건전한 사고를 형성하고 체력을 발달시킴으로써 참된 대학인 육성에 기여한다.','본 수업은 현재 여가와 생활무용으로 각광받고 있는 댄스스포츠를 교양수업에 맞는 체계적인 교육으로 학생들의 사회적 성격을 발달시키고 이와 더불어 운동기능을 향상시키는데 그 목적이 있다.\n또한 댄스스포츠를 통한 상상력과 창의성, 자신감 있는 표현력으로 감성을 풍부하게 발달시키는데 의의가 있다.','출석,30,중간고사,25,기말고사,25,과제보고서,0,수업태도,20,Quiz,0,기타,0','라틴 아메리칸 댄스 테크닉 A to Z,김현덕, 김창호,카스모미디어,2009','','',16),('2014/2-0000-1-5378-02','본 수업은 댄스스포츠의 기본 지식과 스텝을 익히고 다양한 테크닉을 구사함과 동시에 스포츠를 통해 건전한 사고를 형성하고 체력을 발달시킴으로써 참된 대학인 육성에 기여한다.','본 수업은 현재 여가와 생활무용으로 각광받고 있는 댄스스포츠를 교양수업에 맞는 체계적인 교육으로 학생들의 사회적 성격을 발달시키고 이와 더불어 운동기능을 향상시키는데 그 목적이 있다.\n또한 댄스스포츠를 통한 상상력과 창의성, 자신감 있는 표현력으로 감성을 풍부하게 발달시키는데 의의가 있다.','출석,30,중간고사,25,기말고사,25,과제보고서,0,수업태도,20,Quiz,0,기타,0','라틴 아메리칸 댄스 테크닉 A to Z,김현덕, 김창호,카스모미디어,2009','','',18),('2014/2-0000-1-5655-01','현대 사회에서 새롭게 발생하는 문제들에 대한 윤리학적 성찰을 통해 보다 성숙한 인격을 함양하는 것을 목적으로 삼는다. 이를 위해 내용적으로는 컴퓨터 윤리, 생명 윤리. 환경 윤리로 논의되는 다양한 현대 윤리의 쟁점들을 다루어야 할 것이며 형식적으로는 보다 실천적인 접근을 강조하기 위해서 기존의 윤리학보다 이해하기 쉬운 수준에서 전달되어야 할 것이다. 이를 통해 수강생들이 윤리보다 빠르게 변화해가는 과학기술의 발전을 자신의 삶의 문제와 직접적으로 연계하여 성찰하게 함으로써 윤리적으로 보다 성숙한 인격을 함양을 강조하도록 한다.','1. 현대 사회에서 발생하는 다양한 윤리적 쟁점들을 검토\n 2. 윤리학의 기본적인 입장과 태도에 대한 이해.\n 3. 현대의 다양한 윤리적 문제들을 통해 자신의 삶을 바라보고 \n보다 성숙한 인격을 함양토록 함','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Qui','정의란 무엇인가?,마이클 샌델,김영사,2012','','',63),('2014/2-0000-1-5659-01','역사는 보는 관점에 따라 다양한 분석과 해석이 가능하므로 역사를 어떤 시각에서 어떻게 바라봐야 하는가에 대한 고민이 필요하다. \n이 강좌는 세계화시대 주체적인 역사이해를 위한 기초적 지식을 습득하고 세계화의 과정과 미래에 대한 역사적 분석과 전망을 시도한다.\n먼저 역사를 체계적으로 이해하기 위한 이론적 기초와 방법론, 역사해석의 다양한 시각들을 살펴본다. 이어 세계화의 역사, 세계사 속의 한국, 세계화시대 역사 갈등과 해소방안 등을 살펴본다. 이를 통해서 세계화의 의미를 명확히 이해하고 인류 문명사의 세계사적 미래 전망을 도출한다.','1. 역사를 바라보는 시각과 역사해석을 위한 주요 개념들을 파악한다.\n2. 세계화의 긍정적 성과와 부정적 요소들을 파악한다.\n3. 세계화시대의 동아시아의 역사 갈등과 이의 극복방안 등을 성찰하여 역사 교양지식의 심화뿐 아니라 복합문제해결능력과 글로벌 마인드가 확대되도록 한다.','출석,20,수업태도,0,중간고사,30,기말고사,30,발표,10,토의,10,과제,0,Quiz','세계의 역사1, 2,윌리엄 맥닐,이산','','',94),('2014/2-0000-1-5660-01','고대 그리스부터 오늘날의 유럽통합까지 유럽세계의 형성과 변화를 낳은역사적 주요 흐름을 파악하여 유럽의 정치, 경제, 사회, 문화적 전통과 그 역사적 의미를 고찰한다.\n그리스 폴리스 민주정치의 성과와 한계, 로마의 성장과 몰락, 중세봉건제도의 특성, 르네상스와 대항해시대의 의미, 종교개혁과 절대왕정의 형성, 시민혁명과 산업혁명, 근대국가의 발전, 제국주의와 세계대전, 유럽통합 등의 핵심테마들을 비판적으로 이해한다.','1. 유럽의 역사적 변화 발전의 원인, 과정, 결과, 의미 등을 이해한다. \n2. 유럽사의 전개과정에서 등장한 다양한 정치적 사회적 문화적 유산에 대한 지식을 통해 한국사회와 세계사적 미래에 대한 성찰의 기회를 제공한다.\n3. 유럽의 과거와 현재 전반에 관한 교양의 확대 뿐 아니라 세계사적 시각에서 유럽을 이해하여 글로벌 마인드를 고취시킨다.','출석,30,수업태도,0,중간고사,0,기말고사,30,발표,20,토의,10,과제,10,Quiz','서양사강의 (개정판),배영수 외,한울아카데미,2007','','',70),('2014/2-0000-1-5661-01','지식을 얻거나 힐링을 위한 인문학이 아니라 자산의 삶의 본질을 변화시켜 보다 지속가능한 행복을 얻기 위한 실천적 탐구 과정임','보다 나은 지속 가능한 행복을 추구할 수 있는 지적 능력 배양 및 실천적 실험을 통한 삶에 대한 자심감 배양','출석,10,중간고사,0,기말고사,40,과제보고서,30,수업태도,20,Quiz,0,기타,0','강의 계획서 참조','','',15),('2014/2-0000-1-5661-02','우리 과목의 목표는 특히 서양사의 기초 지식을 새롭게 재조명함으로써 인문 교양, 특히 역사 교양을 높이는 데 놓여 있다. 이 목표를 달성하기 위해 우리는 선사시대부터 고대, 중세, 근대, 현대에 이르기까지 각각의 시대를 대표하는 위대한 문화적 유산들을 심층적으로 분석하는 데 주안점을 둔다. 이 과정에서 적합한 역사영화를 적극적으로 활용하고 영화가 그린 역사적 풍경을 대면하는 흥미진진한 역사여행이 될 것이다.','- 역사 교양의 제고\n- 서양사의 기초 지식 터득\n- 유럽의 문화사 깊이 읽기\n- 역사영화 읽기 테크닉 터득\n- 영화로 읽는 유럽사 인식 제고','출석,30,중간고사,30,기말고사,40,과제보고서,0,수업태도,0,Quiz,0,기타,0','문학과 예술의 사회사 1-4,아르놀트 하우저,창비,1999','','',18),('2014/2-0000-1-5667-01','현대 한국의 국가형성과정에서 출발하여 이후 한국이 어떠한 진통의 과정을 거쳐 민주화를 이루어왔는지를 살펴보고, 어떠한 요인들이 민주화를 성취하는데 기여했으며, 민주화 이후 한국이 지향하고 성취해 나가야 할 방향과 과제를 논의하고 제시한다.','1. 한국이 성공적으로 민주화를 성취한 과정을 이해한다. \n2. 한국 사회가 현재 직면하고 있는 진보와 보수의 갈등구조, 지역감정, 분단의 문제 등을 토론을 통해 규명해 봄으로써 보다 성숙한 민주시민으로서의 소양을 함양한다.\n3. 한국의 정치적 이슈와 관련된 복잡하고 다양한 문제들을 토론을 통해 조명해 봄으로써 정치적/비정치적인 복합적이고 융합적인 문제를 해결하는 능력을 배양한다.','출석,10,수업태도,20,중간고사,30,기말고사,40,발표,0,토의,0,과제,0,Quiz,','문학과 예술의 사회사 1-4,아르놀트 하우저,창비,1999','','',61),('2014/2-0000-1-5667-02','현대 한국의 국가형성과정에서 출발하여 이후 한국이 어떠한 진통의 과정을 거쳐 민주화를 이루어왔는지를 살펴보고, 어떠한 요인들이 민주화를 성취하는데 기여했으며, 민주화 이후 한국이 지향하고 성취해 나가야 할 방향과 과제를 논의하고 제시한다.','1. 한국이 성공적으로 민주화를 성취한 과정을 이해한다. \n2. 한국 사회가 현재 직면하고 있는 진보와 보수의 갈등구조, 지역감정, 분단의 문제 등을 토론을 통해 규명해 봄으로써 보다 성숙한 민주시민으로서의 소양을 함양한다.\n3. 한국의 정치적 이슈와 관련된 복잡하고 다양한 문제들을 토론을 통해 조명해 봄으로써 정치적/비정치적인 복합적이고 융합적인 문제를 해결하는 능력을 배양한다.','출석,10,수업태도,20,중간고사,30,기말고사,40,발표,0,토의,0,과제,0,Quiz,','문학과 예술의 사회사 1-4,아르놀트 하우저,창비,1999','','',62),('2014/2-0000-1-5667-03','현대 한국의 국가형성과정에서 출발하여 이후 한국이 어떠한 진통의 과정을 거쳐 민주화를 이루어왔는지를 살펴보고, 어떠한 요인들이 민주화를 성취하는데 기여했으며, 민주화 이후 한국이 지향하고 성취해 나가야 할 방향과 과제를 논의하고 제시한다.','1. 한국이 성공적으로 민주화를 성취한 과정을 이해한다. \n2. 한국 사회가 현재 직면하고 있는 진보와 보수의 갈등구조, 지역감정, 분단의 문제 등을 토론을 통해 규명해 봄으로써 보다 성숙한 민주시민으로서의 소양을 함양한다.\n3. 한국의 정치적 이슈와 관련된 복잡하고 다양한 문제들을 토론을 통해 조명해 봄으로써 정치적/비정치적인 복합적이고 융합적인 문제를 해결하는 능력을 배양한다.','출석,20,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,10,Quiz','문학과 예술의 사회사 1-4,아르놀트 하우저,창비,1999','','',60),('2014/2-0000-1-5674-01','수학, 화학 및 물리는 자연현상에 대한 이해와 아울러 인류의 기술 문명을 이룩하는데 중요한 역할을 하고 있다. 본 강의에서는 실생활에서 다양하게 발견되고 적용되는 자연과학적 현상 및 원리에 대한 기초지식을 다루는데, 세부적으로는 1) 미술, 음악, 영화, 스포츠 분야에서의 수학적인 원리, 2) 에너지, 식품, 화학약품, 의약품 등에서의 기초 화학지식 그리고 3) 가정에서 사용 중인 기기, 첨단군사기술, 통신 및 기타 첨단기술 등에 응용되고 있는 물리현상 및 원리에 대한 세부 기초지식을 다룬다.','1. 본 강의는 학생들로 하여금 자연계에서 일어나는 제 현상에서의 수학적 규칙과 주위 환경, 문화매체 속에서의 수학적 원리에 대한 이해와, 여러 상용 제품들에서의 화학적인 원리 및 현상을 이해하고 또한 자연현상에서 관찰되거나 가정, 스포츠, 통신 및 군용으로 사용하는 각종 장비 및 장치들에서의 물리적인 원리에 대한 이해도를 높이도록 하는데 첫째 목적이 있다.\n2.이러한 실생활에서 다양하게 발견되고 적용되는 과학적 현상 및 원리가 인류에 미치는 유익성과 새로운 적용성에 대하여 자신의 주장을 논리적으로 전개하고 명확하게 전달 할 수 있는 능력을 심화하는데 두 번째 목적이 있다.','출석,10,수업태도,0,중간고사,35,기말고사,35,발표,20,토의,0,과제,0,Quiz,','따로 정하지 않음','김영주','lyoungjookiml@gmail.com',99),('2014/2-0000-1-5674-02','수학, 화학 및 물리는 자연현상에 대한 이해와 아울러 인류의 기술 문명을 이룩하는데 중요한 역할을 하고 있다. 본 강의에서는 실생활에서 다양하게 발견되고 적용되는 자연과학적 현상 및 원리에 대한 기초지식을 다루는데, 세부적으로는 1) 미술, 음악, 영화, 스포츠 분야에서의 수학적인 원리, 2) 에너지, 식품, 화학약품, 의약품 등에서의 기초 화학지식 그리고 3) 가정에서 사용 중인 기기, 첨단군사기술, 통신 및 기타 첨단기술 등에 응용되고 있는 물리현상 및 원리에 대한 세부 기초지식을 다룬다.','1. 본 강의는 학생들로 하여금 자연계에서 일어나는 제 현상에서의 수학적 규칙과 주위 환경, 문화매체 속에서의 수학적 원리에 대한 이해와, 여러 상용 제품들에서의 화학적인 원리 및 현상을 이해하고 또한 자연현상에서 관찰되거나 가정, 스포츠, 통신 및 군용으로 사용하는 각종 장비 및 장치들에서의 물리적인 원리에 대한 이해도를 높이도록 하는데 첫째 목적이 있다.\n2.이러한 실생활에서 다양하게 발견되고 적용되는 과학적 현상 및 원리가 인류에 미치는 유익성과 새로운 적용성에 대하여 자신의 주장을 논리적으로 전개하고 명확하게 전달 할 수 있는 능력을 심화하는데 두 번째 목적이 있다.','출석,10,수업태도,0,중간고사,30,기말고사,30,발표,20,토의,0,과제,10,Quiz','기후변화와 환경,김임순 외,동화기술,2014','최정옥','urinong@hanmail.net',18),('2014/2-0000-1-5674-03','수학, 화학 및 물리는 자연현상에 대한 이해와 아울러 인류의 기술 문명을 이룩하는데 중요한 역할을 하고 있다. 본 강의에서는 실생활에서 다양하게 발견되고 적용되는 자연과학적 현상 및 원리에 대한 기초지식을 다루는데, 세부적으로는 1) 미술, 음악, 영화, 스포츠 분야에서의 수학적인 원리, 2) 에너지, 식품, 화학약품, 의약품 등에서의 기초 화학지식 그리고 3) 가정에서 사용 중인 기기, 첨단군사기술, 통신 및 기타 첨단기술 등에 응용되고 있는 물리현상 및 원리에 대한 세부 기초지식을 다룬다.','1. 본 강의는 학생들로 하여금 자연계에서 일어나는 제 현상에서의 수학적 규칙과 주위 환경, 문화매체 속에서의 수학적 원리에 대한 이해와, 여러 상용 제품들에서의 화학적인 원리 및 현상을 이해하고 또한 자연현상에서 관찰되거나 가정, 스포츠, 통신 및 군용으로 사용하는 각종 장비 및 장치들에서의 물리적인 원리에 대한 이해도를 높이도록 하는데 첫째 목적이 있다.\n2.이러한 실생활에서 다양하게 발견되고 적용되는 과학적 현상 및 원리가 인류에 미치는 유익성과 새로운 적용성에 대하여 자신의 주장을 논리적으로 전개하고 명확하게 전달 할 수 있는 능력을 심화하는데 두 번째 목적이 있다.','출석,10,수업태도,0,중간고사,30,기말고사,30,발표,30,토의,0,과제,0,Quiz,','프린트물','','',63),('2014/2-0000-1-5678-01','대중문화의 이론에 대한 학습만으로는 놀이와 일상성을 이해할 수 없다. 본 강의는 이론의 개괄과 더불어 우리의 삶과 밀접하게 연관된 대중문화의 각 실천적 영역들을 살펴본다. 대중음악, 팝아트, 만화, 대중소설, 드라마 등을 구체적 사례 및 인물을 통해 학습하고 이를 오늘날의 대중문화 및 학생들의 놀이문화와 연계시킨다. 학생들은 자신이 동참하고 있는 대중문화의 놀이를 소개하고 이를 대중문화의 과거, 현재 및 미래 속에서 고찰한다.','1. 대중문화의 정의 및 역사를 이해한다.\n2. 대중문화 비판론을 숙지하고 그러한 시각을 극복하는 대중문화의 힘을 역사적 맥락에서 이해한다.\n3. 세계 대중문화의 패러다임을 바꾼 역사적 사건 및 인물들에 대해 살펴본다. \n4. 자신이 동참하여 만들어가는 문화에 대해 생각해보고 이를 세계 대중문화의 지도 속에 배치한다.\n5. 각자의 놀이를 당당히 즐기며 더 나아가 대중문화의 각 영역 속에서 적극적으로 문화를 생산한다.\n6. 자신의 놀이를 소개하고 더욱 깊은 차원에서 그 영역의 문제점을 지적하는 보고서를 작성하고, 이에 대해 발표 및 토론한다.','출석,20,수업태도,0,중간고사,30,기말고사,20,발표,10,토의,0,과제,20,Quiz','대중과 미적인 삶, 미학관련 자료,수업시간 제시','','',25),('2014/2-0000-1-5678-02','대중문화의 이론에 대한 학습만으로는 놀이와 일상성을 이해할 수 없다. 본 강의는 이론의 개괄과 더불어 우리의 삶과 밀접하게 연관된 대중문화의 각 실천적 영역들을 살펴본다. 대중음악, 팝아트, 만화, 대중소설, 드라마 등을 구체적 사례 및 인물을 통해 학습하고 이를 오늘날의 대중문화 및 학생들의 놀이문화와 연계시킨다. 학생들은 자신이 동참하고 있는 대중문화의 놀이를 소개하고 이를 대중문화의 과거, 현재 및 미래 속에서 고찰한다.','1. 대중문화의 정의 및 역사를 이해한다.\n2. 대중문화 비판론을 숙지하고 그러한 시각을 극복하는 대중문화의 힘을 역사적 맥락에서 이해한다.\n3. 세계 대중문화의 패러다임을 바꾼 역사적 사건 및 인물들에 대해 살펴본다. \n4. 자신이 동참하여 만들어가는 문화에 대해 생각해보고 이를 세계 대중문화의 지도 속에 배치한다.\n5. 각자의 놀이를 당당히 즐기며 더 나아가 대중문화의 각 영역 속에서 적극적으로 문화를 생산한다.\n6. 자신의 놀이를 소개하고 더욱 깊은 차원에서 그 영역의 문제점을 지적하는 보고서를 작성하고, 이에 대해 발표 및 토론한다.','출석,20,수업태도,0,중간고사,25,기말고사,25,발표,10,토의,0,과제,20,Quiz','제본','','',18),('2014/2-0000-1-5679-01','사진은 현대 멀티미디어 콘텐츠의 필수요소로서 현대 일상의 한 부분이 되었다. 본 강좌는 이론적, 실천적 방식으로 사진의 역사 및 촬영을 다루는 과목이다. 구체적 작품들의 감상을 통해 사진의 역사를 이해하고 사진미학을 배우며, 이와 더불어 직접 사진을 찍고 이에 대해 토론함으로써 이론과 실습을 병행한다. 학생들은 자신의 작품을 전시하고 소개하며 서로의 작품에 대해 토론하게 된다.','1. 사진의 기본적인 이론 및 촬영기술을 이해한다.\n2. 사진의 역사에 길이 남는 작품들을 감상하고 그 작품이 가치 있게 평가되는 이유를 이해함으로써 사진 이미지를 분석하는 미적 능력을 배양한다.\n3. 실제로 사진을 찍고 자신의 작품을 평가하는 과정을 통해 세상을 보는 성찰적 시각을 배양한다. \n4. 상상력과 창조력을 발휘하여 표현하고 소통하는 능력을 함양한다.','출석,20,수업태도,0,중간고사,20,기말고사,20,발표,20,토의,0,과제,20,Quiz','사진학강의,바바라런던,짐스톤,존업튼,PHOTOSPACE,2011','정영후','sgm200@naver.com',20),('2014/2-0000-1-5679-02','사진은 현대 멀티미디어 콘텐츠의 필수요소로서 현대 일상의 한 부분이 되었다. 본 강좌는 이론적, 실천적 방식으로 사진의 역사 및 촬영을 다루는 과목이다. 구체적 작품들의 감상을 통해 사진의 역사를 이해하고 사진미학을 배우며, 이와 더불어 직접 사진을 찍고 이에 대해 토론함으로써 이론과 실습을 병행한다. 학생들은 자신의 작품을 전시하고 소개하며 서로의 작품에 대해 토론하게 된다.','1. 사진의 기본적인 이론 및 촬영기술을 이해한다.\n2. 사진의 역사에 길이 남는 작품들을 감상하고 그 작품이 가치 있게 평가되는 이유를 이해함으로써 사진 이미지를 분석하는 미적 능력을 배양한다.\n3. 실제로 사진을 찍고 자신의 작품을 평가하는 과정을 통해 세상을 보는 성찰적 시각을 배양한다. \n4. 상상력과 창조력을 발휘하여 표현하고 소통하는 능력을 함양한다.','출석,20,수업태도,0,중간고사,20,기말고사,20,발표,20,토의,0,과제,20,Quiz','사진학강의,바바라런던,짐스톤,존업튼,PHOTOSPACE,2011','정영후','sgm200@naver.com',18),('2014/2-0000-1-5683-01','요가란 무엇인가?','요가에 대하여 알고 내 몸에 맞는 요가를 수행함으로서 심신의 안정과 마음의 편안함, 몸의 이완을 스스로 인지한다.','출석,20,중간고사,30,기말고사,30,과제보고서,0,수업태도,20,Quiz,0,기타,0','자체교제','','',19),('2014/2-0000-1-5683-02','요가란 무엇인가?','요가에 대하여 알고 내 몸에 맞는 요가를 수행함으로서 심신의 안정과 마음의 편안함, 몸의 이완을 스스로 인지한다.','출석,20,중간고사,30,기말고사,30,과제보고서,0,수업태도,20,Quiz,0,기타,0','자체교재','','',17),('2014/2-0000-1-5684-01','트레이닝시 정확한 자세와 운동방법에 대하여 집중적으로 배운다.','트레이닝의 정확한 자세와 자신에게 필요한 운동프로그램을 습득하여 근육량증가와 체지방감소등을 기대할 수 있으며, 건강한 몸을 만들어 유지하는데 본 수업의 목적이 있다.','출석,40,중간고사,25,기말고사,25,과제보고서,0,수업태도,10,Quiz,0,기타,0','근육운동가이드,프레데릭 데라비에,삼호미디어,2006','','',15),('2014/2-0000-1-5684-02','트레이닝시 정확한 자세와 운동방법에 대하여 집중적으로 배운다.','트레이닝의 정확한 자세와 자신에게 필요한 운동프로그램을 습득하여 근육량증가와 체지방감소등을 기대할 수 있으며, 건강한 몸을 만들어 유지하는데 본 수업의 목적이 있다.','출석,40,중간고사,25,기말고사,25,과제보고서,0,수업태도,10,Quiz,0,기타,0','근육운동가이드,프레데릭 데라비에,삼호미디어,2006','','',19),('2014/2-0000-1-5687-01','본 강의는 중국이라는 사회와 그 문화를 통하여, 중국어에 대한 관심과 이해를 증폭시킨다. 모든 언어의 형성은 문화의 영향을 받듯이 중국어와 중국 문화의 관계를 배움으로써 중국어를 더 쉽게 이해하고 중국어를 더 쉽게 외울 수 있어서 중국어 실력을 향상시킨다.','1. 중국의 현대 문화를 이해한다.\n2. 중국 고대문화가 현대 중국어와 현대 중국문화에 어떤 영향을 미쳤는지 학습한다.\n3. 중국의 언어와 문자가 권력을 갖추는 배경과 이로 인한 중화문화의 구축 과정을 이해한다.\n4. 이러한 과정을 통해 중국인의 사고방식과 그들의 문화를 이해하는데 궁극적 목표를 둔다.','출석,10,수업태도,5,중간고사,40,기말고사,40,발표,5,토의,0,과제,0,Quiz,0','중국의 전통과 문화,선정규,신서원','','',20),('2014/2-0000-1-5704-01','자연과학과 그 세계관과 관련된 다양한 여러 소주제에 대하여 세미나, 토론 및 발표를 실시한다. 주제에 관한 소개 및 배경설명은 담당교수가 하며 주어진 소주제에 관한 그룹별 토의, 혹은 그룹별 문제의 발견 및 토론 (그룹토의), 토의 내용에 관한 (그룹별) 발표 및 에세이 (개별)를 제출을 함으로서 자신의 의견을 논리적으로 전달하는 능력을 함양하는데 있다.','신입생들로 하여금 소주제에 대한 세미나, 토론 및 발표를 통하여, \n1. 논리적 사고의 훈련 및 합리적 토론결과의 도출, \n2. 자연과학과 인간사회발전과의 관계에 관한 숙고, \n3. 자연과학적 현상에 대한 분석과 해석의 숙달, \n4. 자연과학 분야에 대한 발표와 정확한 글쓰기, \n5. 과학발전에 걸맞는 비판과 수용에 관한 가치관의 정립\n을 할 수 있도록 한다.','출석,20,중간고사,0,기말고사,0,과제보고서,20,수업태도,10,Quiz,0,기타,50','주별 소주제에 관한 참고자료 및 시청각자료','서윤호','y2643@hanmail.net',20),('2014/2-0000-1-5704-02','FIXED','FIXED','출석,10,중간고사,30,기말고사,30,과제보고서,20,수업태도,10,Quiz,0,기타,0','기후변화와 환경,김임순 외,동화기술,2014','최정옥','urinong@hanmail.net',19),('2014/2-0000-1-5899-01','과학기술과 그 성과에 제기되는 다양한 철학적, 윤리적, 사회적 쟁점들을 고찰함으로서 현대 과학기술에 대한 이해와 그것과 인간이나 사회와의 관계에 대한 반성적 사유를 기르도록 한다. 나아가 21세기 과학의 시대를 살아가는 현대인으로서 당면한 문제에 대해 능동적으로 대처할 수 있는 능력을 함양하고자 한다.','과학 기술 사회에 대한 이해의 폭을 넓히고 비판적인 사고 능력을 함양하며, 세계에 대한 진지한 고민과 성찰의 계기를 마련하고자 한다.','출석,10,중간고사,0,기말고사,30,과제보고서,20,수업태도,10,Quiz,20,기타,1','차가운 공학, 따뜻한 기술 (드래프트 형식),도승연,2013','양병익','slzpf0311@gmail.com',54),('2014/2-0000-1-5909-01','미입력','미입력','출석,20,수업태도,5,중간고사,25,기말고사,35,발표,5,토의,0,과제,10,Quiz,','차가운 공학, 따뜻한 기술 (드래프트 형식),도승연,2013','','',44),('2014/2-0000-1-6049-01','초보자와 중,상급자로 나누어 각자의 수준에 맞춰 상황에 따라 바이올린 그룹렛슨 또는 개인렛슨 진행','바쁘게 돌아가는 현대사회에서 자칫 메말라 갈 수 있는 감성을 악기를 배움으로 정서를 순화하고 성취감 또한 높일 수 있다.나아가 문화 예술에 대한 폭넓은 이해와 관심을 유도하여 삶의 질을 높이기 위함이다.','출석,40,중간고사,0,기말고사,20,과제보고서,0,수업태도,30,Quiz,0,기타,10','시노자끼 바이올린 교본 (초보자),시노자끼,상관없음','','',11),('2014/2-0000-1-6051-01','악기 연주','악기를 연주 할수 있도록 한다.','출석,50,중간고사,0,기말고사,0,과제보고서,0,수업태도,50,Quiz,0,기타,0','시노자끼 바이올린 교본 (초보자),시노자끼,상관없음','','',6),('2014/2-0000-1-6054-01','클래식 기타 연주법을 통해 기타를 연주하고, 나아가 음악적 기본 상식을 습득할 수 있다.','클래식 깉의 연주법 습득 및 핑거스타일, 포크 스타일 등의 악기 특성을 알 수 있다.','출석,20,중간고사,30,기말고사,40,과제보고서,0,수업태도,10,Quiz,0,기타,0','어린이 기타 교실,김명표,삼호출판사','','',13),('2014/2-0000-1-6135-01','스페인어가 무엇인가를 알아보는 것\n딱딱한 수업을 최대한 즐겁고 유쾌하게 진행하고자 함\n수업대상: \"스페인어1를 수강했거나 전에 스페인어를 조금이라도 배운 학생\" \n주의 사항: 스페인어를 처음배우고자 하는 학생은 스페인어1 수업을 수강하기 바람','표현 중심으로 읽기와 외우기에 집중함으로써 \n단 한마디일지라도 스페인어를 할 수 있게 하는 것이 학습의 목표.','출석,20,중간고사,40,기말고사,40,과제보고서,0,수업태도,0,Quiz,0,기타,0','누구라도 말할 수 있는 스페인어,조혜진,월인,2003','','',16),('2014/2-0000-1-6376-01','본 수업은 러시아어 알파벳부터 발음, 기초단어, 기초문법과 나아가 러시아의 문화 즉 식사예절, 생활양식, 전통문화, 의복문화 등의 다양하고 재미있는 문화와 상식적인 역사를 배웁니다. 러시아는 물론 러시아와 관련된 동유럽의 문화와 역사도 함께 연구합니다.','본 수업은 러시아와 러시아어에 대한 기본적 이해를 제공하며 어학적 능력의 함양으로 일상적인 대화와 간단한 쓰기연습을 합니다. 또한 기초 러시아어 말하기 연습 등을 진행할 것입니다. 이와 같은 과정을 통해 많은 흥미를 느끼게 될 것입니다. 전반적인 수업진행은 한국어로 합니다.','출석,10,중간고사,30,기말고사,40,과제보고서,0,수업태도,0,Quiz,20,기타,0','재미있게 배워 보는 생존 러시아어 2,안병팔, 이수현,신아사,2008','','',19),('2014/2-0000-1-6381-01','인적네트워크의 형성 및 활용능력과 자기 PR능력이 현시대 성공의 열쇠이다. 본 과목에서 학생들은 마술이라는 독특한 기술과 이를 활용하는 방법론을 습득하고, 이 과정을 통해 능동적으로 자기를 표현함으로써 사교능력과 자신감을 배양하게 된다.','1. 창의적 아이디어로 스토리를 구성하고 자신만의 표현방식을 연구하여 기초적인 마술트릭을 연출해 낼 수 있게 된다. \n2. 자신감 향상을 통해 자기표현 및 PR능력을 증대시킨다.\n3. 자발성을 높이고 창의성을 고양하며 새로운 자아상을 확립할 수 있다.','출석,10,수업태도,10,중간고사,20,기말고사,20,발표,10,토의,20,과제,10,Qu','호감을 불러 일으키는 준비된 쇼맨십,Steve Cohen,Wisdom House,2006','김정재','kjj6929@kw.ac.kr',47),('2014/2-0000-1-6381-02','인적네트워크의 형성 및 활용능력과 자기 PR능력이 현시대 성공의 열쇠이다. 본 과목에서 학생들은 마술이라는 독특한 기술과 이를 활용하는 방법론을 습득하고, 이 과정을 통해 능동적으로 자기를 표현함으로써 사교능력과 자신감을 배양하게 된다.','1. 창의적 아이디어로 스토리를 구성하고 자신만의 표현방식을 연구하여 기초적인 마술트릭을 연출해 낼 수 있게 된다. \n2. 자신감 향상을 통해 자기표현 및 PR능력을 증대시킨다.\n3. 자발성을 높이고 창의성을 고양하며 새로운 자아상을 확립할 수 있다.','출석,10,수업태도,10,중간고사,20,기말고사,20,발표,10,토의,20,과제,10,Qu','호감을 불러 일으키는 준비된 쇼맨십,Steve Cohen,Wisdom House,2006','김정재','kjj6929@kw.ac.kr',47),('2014/2-0000-1-6518-01','의사소통의 기본 조건이라 할 수 있는 언어와 문화가 사회 속에서 어떤 관계를 맺고 있으며 이들의 관계가 어떤 사회적 현상으로 나타나는지 알아본다. 구체적으로 언어의 구조, 언어의 문화적 의미, 민족, 성, 계급, 인종에 따른 언어 차이, 언어 습득, 언어와 사회의 상호작용 등에 대해 살펴본다.','우리가 사용하는 언어가 사회 속에서 어떻게 의사소통의 수단으로 사용되며 어떻게 사회 문화와 상호작용하는지 앎으로써 스스로의 언어 사용을 검토하고 보다 바람직한 언어생활을 할 수 있도록 한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','언어와 사회-언어와 사회의 유쾌한 춤사위를 위하여,최기호&김미형&임소영,한국문화사,2004','','',53),('2014/2-0000-1-6519-01','최근 들어 3D 입체 형태의 미디어는 쉽게 접할 수 있게 되었다. 3D 입체 콘텐츠의 활용은 영화와 TV, 게임 등의 분야에서 급속히 증가하여 차세대 영상 미디어로 자리잡고 있다. 이와 같이 3D 입체 콘텐츠에 대한 관심도와 활용도가 급속히 높아짐에 따라서 3D 입체 콘텐츠를 올바르게 이해하고, 이를 바탕으로 콘텐츠를 제작할 수 있는 지식이 요구되고 있다. 본 교과목에서는 이와 같이 새로운 3D 입체 멀티미디어 시대에 따라서 새로운 미디어를 이해하고 이용할 수 있는 제반 지식을 강의하고자 한다.','본 교과목에서는 3D 입체감에 대한 기초 원리를 정확히 이해하고, 인간의 지각 기능과의 관계를 파악하여 다양한 3D 입체 영상에 대한 원리 및 기초 지식을 함양하는 것을 교육의 목표로 한다. 또한 3D 입체 수요자에서 개개인이 공급자가 되는 시대로의 변화에 맞추어서 자신이 보유한 미디어 기기 및 환경에서 간단히 3D 입체 영상을 제작하는 기법들을 익히고자 한다. 여기에는 3D 입체 영상을 촬영하는 기법들에 대한 강의도 병행된다. 또한 3D 입체 콘텐츠 뿐만 아니라 3D 입체 콘텐츠를 재현해주는 다양한 3D 디스플레이에 대한 기초 지식도 갖추고, 무안경 및 안경식 3D 입체 콘텐츠를 이해하고 제작해 본다. 이러한 기초 지식을 바탕으로 3D 입체 영화가 제작되는 과정을 살펴봄으로써 3D 입체 영화의 입체감에 대한 특성과 스토리와의 상관성도 이해해 본다.','출석,20,중간고사,30,기말고사,30,과제보고서,20,수업태도,0,Quiz,0,기타,0','Handout,서영호,Realistic Media Lab.,2012','김보라','brkim@kw.ac.kr',93),('2014/2-0000-1-6523-01','이 강좌를 통해 우리는 대학 내에서뿐만 아나리 사회 생활을 하면서도 합리적 의사소통을 원활히 할 수 있는 능력을 키우고자 한다. 이를 위해 기본적으로 토의와 토론을 통한 문제해결 방식을 익히고 지성인으로서 갖추어야할 비판적 사고에 대한 훈련을 하도록 한다.','','출석,30,중간고사,0,기말고사,30,과제보고서,20,수업태도,20,Quiz,0,기타,0','말하기, 이젠 문제없다,광운대 교양국어 교재 편찬위원회,보고사,2010','','',20),('2014/2-0000-1-6523-02','이 강좌를 통해 우리는 대학 내에서뿐만 아나리 사회 생활을 하면서도 합리적 의사소통을 원활히 할 수 있는 능력을 키우고자 한다. 이를 위해 기본적으로 토의와 토론을 통한 문제해결 방식을 익히고 지성인으로서 갖추어야할 비판적 사고에 대한 훈련을 하도록 한다.','','출석,30,중간고사,0,기말고사,30,과제보고서,20,수업태도,20,Quiz,0,기타,0','말하기, 이젠 문제없다,광운대 교양국어 교재 편찬위원회,보고사,2010','','',19),('2014/2-0000-1-6523-03','본 수업은 16주 동안 다양한 주제를 통하여 학생들 스스로가 비판적 사고를 통해, 이를 논리적으로 표현하는 능력을 배양하는 데 강의의 중점을 둔다.','자유롭고 창의적인 상상력과 합리적이고 비판적인 사고 능력을 키워 이를 말과 글로 표현하는 능력을 배양시키는데 목적이 있다.','출석,10,중간고사,0,기말고사,30,과제보고서,30,수업태도,10,Quiz,0,기타,20','말하기, 이젠 문제없다,광운대 교양국어 교재편찬위원회,보고사,2009','','',18),('2014/2-0000-1-6524-01','FIXED','FIXED','출석,10,중간고사,30,기말고사,40,과제보고서,10,수업태도,10,Quiz,0,기타,0','신 일본어듣기첫걸음 상,미야키 사치에 외3명,시사일본어사,1999','이정원','milk_way@naver.com',19),('2014/2-0000-1-6524-02','일본어 초급 수준의 학생을 대상으로, 듣기와 쓰기 훈련을 통해 기초문형과 문법을 다져가는 과목입니다. 또한 흥미로운 수업을 위해, 강의와 팀기반 학습으로 진행됩니다.','듣기의 밑바탕이 되는 기본 단어들과 쓰기에 초석이 되는 기초 한자 어휘들을, 받아쓰기 및 되내어 말하기 연습, 팀 구성원간의 미션수행으로 자연스럽게 익혀나갑니다.','출석,10,중간고사,40,기말고사,40,과제보고서,0,수업태도,10,Quiz,0,기타,0','일본어 리스닝 초급에서 중급으로,야스이 아케미,다락원,2013','','',12),('2014/2-0000-1-6588-01','이 수업은 공부로 지쳐있는 학생들이 즐길 수 있도록 small ensemble을 만들어 공연까지 코치해 주는 수업입니다. 현재 많은 국내 대학교 학생들은 밴드 동아리에 참여하고있습니다. 하지만 그 친구들은 아무런 지도 없이 학생들끼리 만들어서 하는 동아리인 만큼 실력도 많이 떨어지고 본인들도 답답해하는 경우가 많습니다. 실용음악과가 있는 대학교에서는 음대 전공생이라면 앙상블 수업은 필수과목입니다. 그만큼 음악에서 앙상블이 차지하는 비중이 높습니다. 따라서 비중이 있는 만큼 전문 교수님의 음악적 지도가 꼭 필요합니다. 또한 이 수업에서는 간락한 화성학과 음악 역사도 함께 진행됩니다. 악기구성: 대부분의 악기와 보컬은 모두 가능합니다.\n*Please feel free to e-mail me if you have any questions.\n\nRhythm section - Piano, keyboard, Acoustic Bass, Drum, Percussion, Guitar, bass Guitar and etc... Lead section - Vocal, saxophone, trumpet, trombone, flute, Violin and etc...\n\n* Students with basic musical background can be helpful but any level of students are welcome to join the class. Feel free to contact me if you have any questions.','이 수업을 통해 학생들이 직접 학교축제에서도 수준 높은 공연도 하고, 봉사 활동 등 각종 의미 있는 행사도 참여할 수 있도록 하는 것이 목표입니다.','출석,30,중간고사,15,기말고사,20,과제보고서,10,수업태도,15,Quiz,10,기타,','The New Real Book 1,2,3,Various Artists,Sher Music Co.,1988','','',14),('2014/2-0000-1-6590-01','국가의 행정이 어떠한 과정으로 이루어지고, 정책이란 무엇인지에 대한 기본지식을 습득한 후 경찰행정 및 정책과 관련된 주요 현안들, 이를테면 수사권, 정치적 중립성, 자치경찰제, 지역경찰제, 경찰입직, 성과주의, 예산문제 등에 대해서 집중적으로 토론방식에 의해 수업한다.','경찰행정에 관한 기본개념을 전반적으로 정리하고 경찰운영과정에서 나타나는 문제점과 현재 논의되고 있거나 추진되고 있는 정책들을 연구ㆍ검토하는데 목표를 둔다.','출석,20,중간고사,30,기말고사,30,과제보고서,20,수업태도,0,Quiz,0,기타,0','경찰정책론,배철효, 김용태,21세기사,2013','','',37),('2014/2-0000-1-6993-01','토익필수1은 100% 온라인 수업으로서 토익의 구성과 성격을 기초적으로 파악하며 이해에 촛점을 둔다. TOEIC에서 다루어지는 듣기, 어휘, 문법, 독해 등을 초보 수준에서 점검하고, 더 나아가 각 영역의 특징을 분석하여 어떻게 실전감각과 실력을 향상시키는지 살펴본다.\n** 개강일 기준 토익 550점 이상인 학생은 추후 수강생 명단에서 자동 삭제됩니다.\n** 영문과 학생은 담당교수 뜻에 따라 토익필수1은 수업불가 입니다.','토익필수1은 다양한 토익 기출문제를 통하여 듣기, 어휘, 문법, 독해의 기초를 다룸으로써 학생들의 토익 실력을 향상시키는 데에 그 목적이 있다. 토익 문제를 영역별, 유형별로 특징을 파악하여 학생 스스로 효과적인 실전대비 전략을 세울 수 있도록 도와준다.\n** 개강일 기준 토익 550점 이상인 학생은 추후 수강생 명단에서 자동 삭제됩니다.\n** 영문과 학생은 담당교수 뜻에 따라 토익필수1은 수업불가 입니다.','출석,10,중간고사,40,기말고사,40,과제보고서,0,수업태도,0,Quiz,10,기타,0','How To TEOIC - 기출모의 1200제 문제집,엄대섭, 강진오, 강원기,넥서스,2010','김진희','jinhee2447@hanmail.net',99),('2014/2-0000-1-7000-01','한국 근현대에 대한 역사적 통찰(역사적 사건이 벌어진 시간과 공간의 의미를 특별히 부각 시키는 방법)을 바탕으로 각 시대별로 기업의 인재상을 대체적으로 탐구하고, 향후 21세기를 사는데 있어서 기업과 사회가 바라는 인재가 무엇인지를 구체적으로 살펴보고자 한다. 나아가 한국사회에 있어서 시대적 역할에 부응하고 개인의 발전을 도모할 수 있는 소위 사회가 요구하는 ‘한국적 인재상’ 수립을 목적으로 한다.','소위 기업이 바라는 인재상을 탐구하여, 취업전략 수립에 적극적으로 반영하고 성공취업의 선순환 구조 생성의 계기를 마련한다.','출석,30,중간고사,0,기말고사,50,과제보고서,20,수업태도,0,Quiz,0,기타,0','한국의 인적자원,김장호,법문사,2006','김진희','jinhee2447@hanmail.net',147),('2014/2-0000-1-7464-01','악기의 숙달성과 상관없이 수업시간에 앙상블에 필요한 각 악기의 기초를 배우고 학생들간의 팀을 만들어 실제 음악을 연주해 보고 서로 평가한다. 펑크와 힙합의 역사와 함께 각 장르만의 개성을 배우게 된다.','현재 대중음악에서의 흑인음악적인 요소를 직접 체험학습으로 익히면서 문화인의 자질을 키운다','출석,30,중간고사,20,기말고사,20,과제보고서,0,수업태도,30,Quiz,0,기타,0','한국의 인적자원,김장호,법문사,2006','','',12),('2014/2-0000-1-7475-01','국경 없는 글로벌 무한경쟁 시대에서 성공적인 계약의 체결 및 이행, 그리고 이를 위한 치밀하고도 효과적인 협상 능력은 개인은 물론 기업, 국가의 생존과 직결되는 경우가 많다. \n본 과목은 일상생활 및 비즈니스 활동에서 발생하는 각종 상거래 관련 계약 및 협상에 대해 이론적 지식의 습득과 함께 국내외 사례 분석 등을 통해 실무적 활용능력을 높일 수 있도록 개설된 온라인 강좌이다.','1. 계약 및 협상의 개념과 특징, 구성요소 등에 대한 이론적 지식을 습득한다. \n2. 성공적인 계약 및 협상의 절차, 전략 등에 대한 실무적 식견을 함양한다.\n3. 국내외 사례분석을 통해 계약 및 협상 관련 현장 적용 능력을 제고한다.','출석,20,중간고사,30,기말고사,30,과제보고서,15,수업태도,5,Quiz,0,기타,0','무역계약론,김석철,두남,2013','주이화','wiz301@nate.com',148),('2014/2-0000-2-0254-01','교육과정은 학습자에게 의도적인 행동의 변화를 위하여, 학교교육에서 무엇을 어떤 목적으로 가르칠 것인가에 대한 계획을 결정화하는 활동이다. 즉, 교육과정이란 학교에서 무엇을 가르칠 것인가에 대한 전문적인 이론을 연구하는 것이라고 할 수 있다. 본 교과목은 학습자가 교육과정의 기초적인 이론을 이해하고 습득할 수 있도록 교과목 내용을 다음과 같이 구성한다. 같다. 첫째, 교육과정의 개념(교육과정의 개념, 표면적 교육과정, 잠재적 교육과정, 영교육과정), 둘째, 교육과정을 구성하는 기초 개념들(철학, 학습자 심리이론, 사회, 교사, 국가), 셋째, 교육과정의 역사적 이해(스펜서 교육과정, 다양한 교육과정, 듀이 교육과정, 타일러 8년 연구), 넷째, 교육과정의 유형(교과중심 교육과정, 경험중심 교육과정, 학문중심 교육과정, 사회재건주의 교육과정, 인간중심 교육과정, 행동주의 교육과정, 인지주의 교육과정, 구성주의 교육과정, 재개념주의 교육과정), 다섯째, 교육목표 설정(목적과 목표, Tyler 목표진술, Tyler 목표설정기준, Tyler 목표설정장단점, 교육목표와 내용의 일관성, 교육목표와 평가의 일관성, 교육목표 기초자원, Bloom의 교육목표, Gagne의 교육목표, Bloom의 이원분류표), 여섯째, 학습내용의 선정과 조질(학습내용 의미, 선정기준, 조직원칙, 교육과정 조직의 유형), 일곱째, 교육방법(Bloom, Bruner, Ausbel), 여덟째, 한국교육과정(교육과정개발, 중등학교 교육과정 변천, 유치원 교육과정 변천).','본 교과목의 교육목적은 교육과정 교직과정을 이수하는 학생들에게 교육과정에 대한 기본적인 이해를 고양하는 것에 초점을 둔다. 이를 위하여, 구체적인 목표는 다음과 같다. 첫째, 교육과정의 개념과 종류를 규명하는 것이다. 둘째, 교육과정을 구성하는 요인들을 철학적 관점, 심리학적 관점, 사회적 관점, 국가적 관점 등으로 기초요인들을 탐색하는 것이다. 셋째, 과거부터 현재까지 교육과정의 유형이 어떻게 변천했는지를 규명하고 각 교육과정의 특징과 장단점을 분류하는 것이다. 넷째,Tyler 교육과정 모형을 구체적으로 학습하는 것이다. 다섯째, 교육목표설정, 학습내용 선정조직, 학습방법론을 습득하는 것이다. 여섯째, 우리나라 교육과정의 변천사를 학습하는 것이다.','출석,10,중간고사,30,기말고사,30,과제보고서,10,수업태도,0,Quiz,0,기타,20','교육과정의 이해와 개발,홍후조,문음사,2002','','',6),('2014/2-0000-2-0258-01','교수자로서 갖추어야 할 내용지식, 교수지식, 테크놀로지 지식 뿐만 아니라 이를 모두 통합할 수 있는 융합적 지식을 함양하고 자질을 갖추기 위한 교직과목임.','교육방법 및 교육공학분야에서 변하지 않고 지속적으로 가치를 가지고 있는 기본적인 개념 및 이론들을 살펴보고, 시대흐름을 반영하는 신경향이론이나 방법론들을 제시하여 유의미하게 연결시킴과 동시에 교사로서의 자질을 함양하기 위해 교육방법 및 교육공학에 관한 이론을 습득하고 실천능력을 향상시키는 데 그 목적을 두고 있음.','출석,10,중간고사,20,기말고사,30,과제보고서,10,수업태도,0,Quiz,0,기타,30','21세기 교사를 위한 교육방법 및 교육공학(2판),한정선 외,교육과학사,2011','','',22),('2014/2-0000-2-0266-01','교육평가는 학습자의 학습정보를 수집하여서 학습자의 교육목표 성취정도 및 교육과정 모형의 효율성을 판단하는 일련의 의사결정이다. 본 교육평가의 교과목은 다음과 같이 위계적으로 제시한다. 첫째, 교육평가의 개념, 둘째, 교육관(선발적 교육관, 발달적 교육관), 셋째, 검사관(측정관, 평가관, 총평관), 셋째, 규준, 준거, 성장, 능력 평가, 넷째, 진단, 형성, 총합 평가, 다섯째, 검사문항제작원리, 여섯째, 문항분석(고전검사이론, 문항반응이론), 일곱째, 검사도구 분석(신뢰도, 타당도), 여덟째, 수행평가 분석(채점자내 신뢰도, 채점자간 신뢰도), 아홉째, 정의적 영역의 평가, 열번째, 컴퓨터화 능력 적응 검사와 수행평가, 열한번째, 교육평가모형과 학교 평가.','교육평가 교과목의 목적은 교육평가에 대한 기초적인 이론(교육과 평가, 교육관, 검사관, 평가분류, 문항제작, 문항분석, 검사도구 분석, 수행평가, 교사평가)뿐만 아니라, 교육평가에 대한 새로운 이론(능력평가, 성장평가, 문항반응이론, 채점자간 신뢰도, 컴퓨터화 적응검사)를 학습하며, 이를 통해서 교육현장에 학습한 교육평가이론을 적용할 수 있는 응용능력의 함양에 있다. 즉, 교육평가의 이론적 지식을 교육현장에 적용하여 학습자의 능력을 옳바르게 평가할 수 있는 전문성을 함양하는 것에 있다.','출석,10,중간고사,30,기말고사,30,과제보고서,10,수업태도,0,Quiz,0,기타,20','현대교육평가론,성태제,학지사,2009','','',14),('2014/2-0000-2-0268-01','이 강좌에서는 본격적인 교육학 학습 이전에 교육학의 개별 영역에 대한 개론적인 이해를 도모한다. 아울러 교육의 주요 개념 및 주제에 대한 대략적 맛보기를 병행한다.','*교육학의 개별 영역에 대한 개론적인 이해\n*교육의 여러 문제들에 대한 사고 증진\n*교육의 주요 개념, 주제들에 대한 학습을 통한 교육의 본질 탐구','출석,10,중간고사,40,기말고사,40,과제보고서,0,수업태도,10,Quiz,0,기타,0','최신 교육학개론,성태제 외,학지사,2012','','',21),('2014/2-0000-2-2038-01','본 강좌는 인간 정신세계의 중요한 차원을 구성하고 있는 종교에 대한 이해를 문화적 차원에서 접근해 봄으로써 종교에 대한 일반적인 이해를 요구하는 수강생들에게 다양한 문화권내에서 발생, 기능하고 있는 종교적 함의들의 전달을 주 내용으로 삼는다. 그리하여 수강생들은 이러한 이해를 통해 각각의 문화권에서 논의되는 종교의 문화적 차원을 검토함으로써 보다 통합적인 시각을 겸비하여 세계와 인간을 이해하게 될 것이다.','1. 종교의 기원과 본질에 대한 이해\n 2. 다양한 종교가 가진 각각의 특성을 문화적으로 검토함으로서 \n세계와 인간에 대한 보다 폭넓은 이해를 갖도록 함','출석,30,수업태도,10,중간고사,20,기말고사,30,발표,10,토의,0,과제,0,Quiz','강의안 프린트/화성에서온남자금성에서온여자,존그레이','','',42),('2014/2-0000-2-2419-01','본 과목은 HSK 4급에 준하는 실력으로 주어진 주제에 중국어로 토론을 할 수 있게 한다. 정확한 문법으로 독해와 작문의 기초를 다진다. 이를 위해 매일 3시간씩 3주간 중국어를 학습하고, 1200개의 상용어휘를 익힌다.','본 과목은 HSK 4급(기존 HSK3-5級에 해당)의 실력을 갖추는 것을 목표로 시험을 대비하여 듣기, 독해, 쓰기 세 영역의 기초를 단단히 할 것이다.','출석,10,중간고사,30,기말고사,40,과제보고서,0,수업태도,0,Quiz,20,기타,0','新HSK 이거하나면 끝! 실전모의고사4급,劉雲 외,동양북스,2014','','',17),('2014/2-0000-2-2942-01','고급문화와 대중문화의 경계가 허물어진 현재, 미술은 예술가들의 영역이라기보다는 일상에서 대중이 경험하고 생산할 수 있는 범주가 되었다. 본 강좌는 주요 작가들의 생애 및 작품에 초점을 맞추어 미술의 역사와 기본적인 이론들을 살펴본다. 강의 초반부에서는 미술사를 개괄하고 이후 한 주에 1~2명씩 주요 작가들을 소개한다. 한 학기 동안 언급되는 작가들은 서양미술, 동양미술, 현대미술 중 적어도 두 영역 이상에서 선택된다. 학생들은 미술관 및 전시회를 관람한 후 이에 대해 토론한다.','1. 미술작품 감상에 필요한 기본적인 용어 및 이론을 습득한다.\n2. 미술을 일상의 한 부분으로서 체험하여 미술 감상 및 전시회에 가는 경험을 친근하게 느끼도록 만든다.\n3. 자신이 감상하는 작품을 미술사의 지도 속에 배치할 수 있다.\n4. 미술사에 길이 남는 업적을 이룩하는 과정에서 당대의 거장들이 처했던 상황 및 그들이 보여준 창조적 상상력을 이해한다.','출석,15,수업태도,0,중간고사,30,기말고사,30,발표,15,토의,0,과제,10,Quiz','서양미술사,이은기,미진사,2011','백승진','baeksj0604@naver.com',97),('2014/2-0000-2-2942-02','고급문화와 대중문화의 경계가 허물어진 현재, 미술은 예술가들의 영역이라기보다는 일상에서 대중이 경험하고 생산할 수 있는 범주가 되었다. 본 강좌는 주요 작가들의 생애 및 작품에 초점을 맞추어 미술의 역사와 기본적인 이론들을 살펴본다. 강의 초반부에서는 미술사를 개괄하고 이후 한 주에 1~2명씩 주요 작가들을 소개한다. 한 학기 동안 언급되는 작가들은 서양미술, 동양미술, 현대미술 중 적어도 두 영역 이상에서 선택된다. 학생들은 미술관 및 전시회를 관람한 후 이에 대해 토론한다.','1. 미술작품 감상에 필요한 기본적인 용어 및 이론을 습득한다.\n2. 미술을 일상의 한 부분으로서 체험하여 미술 감상 및 전시회에 가는 경험을 친근하게 느끼도록 만든다.\n3. 자신이 감상하는 작품을 미술사의 지도 속에 배치할 수 있다.\n4. 미술사에 길이 남는 업적을 이룩하는 과정에서 당대의 거장들이 처했던 상황 및 그들이 보여준 창조적 상상력을 이해한다.','출석,20,수업태도,0,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Quiz','서양미술사,EH 곰브리치,예경출판사,1997','','',52),('2014/2-0000-2-2968-01','우리는 모두 경복궁에 대해 자신 있게 설명할 수 있을까? 본 강좌는 한국의 문화유산을 개괄함으로써 학생들이 가장 기본적인 한국의 문화를 이해하도록 조력한다. 강좌는 석굴암, 불국사 등 한국의 세계문화유산을 비롯하여 그 외 궁궐, 사찰, 성 등을 살펴보고 이와 더불어 한국의 공예, 서화 등을 다룬다. 학생들은 궁궐 또는 사찰을 한 곳 정하여 이와 관련된 자료를 수집한 후 답사하고 이에 대해 토론한다.','1. 한국의 문화유산에 대한 지식을 습득하고, 이에 대해 설명할 수 있는 수준에 이른다.\n2. 우리의 과거를 배우고 익힘으로써 우리의 자랑스러운 문화유산에 대해 자부심을 가지고 궁극적으로 자신의 정체성을 확립한다.\n3. 항상 생활 속에서 접하면서도 정확히 알고 있지 못하던 역사적, 문화적 사실들을 이해한다.\n4. 이를 바탕으로 우리의 독창성이 배어있는 문화유산을 창조할 수 있는 기반을 마련한다.','출석,20,수업태도,0,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Quiz','수업 중 강의 노트,이지양','안형준','lehiphop@nate.com',93),('2014/2-0000-2-2968-02','우리는 모두 경복궁에 대해 자신 있게 설명할 수 있을까? 본 강좌는 한국의 문화유산을 개괄함으로써 학생들이 가장 기본적인 한국의 문화를 이해하도록 조력한다. 강좌는 석굴암, 불국사 등 한국의 세계문화유산을 비롯하여 그 외 궁궐, 사찰, 성 등을 살펴보고 이와 더불어 한국의 공예, 서화 등을 다룬다. 학생들은 궁궐 또는 사찰을 한 곳 정하여 이와 관련된 자료를 수집한 후 답사하고 이에 대해 토론한다.','1. 한국의 문화유산에 대한 지식을 습득하고, 이에 대해 설명할 수 있는 수준에 이른다.\n2. 우리의 과거를 배우고 익힘으로써 우리의 자랑스러운 문화유산에 대해 자부심을 가지고 궁극적으로 자신의 정체성을 확립한다.\n3. 항상 생활 속에서 접하면서도 정확히 알고 있지 못하던 역사적, 문화적 사실들을 이해한다.\n4. 이를 바탕으로 우리의 독창성이 배어있는 문화유산을 창조할 수 있는 기반을 마련한다.','출석,20,수업태도,0,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Quiz','담당교수가 준비한 사진, 그림, 동영상 자료','','',26),('2014/2-0000-2-2970-01','우리 민족이 삶과 죽음에 대면하여 실천해 온 일상 속에는 고유한 문화적 규칙들과 방식들이 존재한다. 본 강좌는 민간 생활과 결부된 문화 일반을 살펴봄으로써 우리가 현재 공유하는 문화의 원형을 찾아나가는 여정이다. 강의는 민간신앙, 세시풍속, 민속예술, 관혼상제의 의례 등으로 구성되며 학생들은 한 학기에 한 번 이상 놀이와 축제의 현장 등 직접 우리의 민속을 체험할 수 있는 장소를 방문하고 이에 대해 함께 토론한다.','1. 한국의 민속 일반에 대해 이해하고, 잊고 있었던 우리의 과거를 되찾아 현재의 일상에 복원한다.\n2. 면면히 이어져 내려온 우리의 민속을 이해하고 이를 바탕으로 현재 일상에서 관찰할 수 있는 그 반향들을 이해한다. \n3. 한국 민속에 대한 자부심을 가지고 우리 민족의 정체성을 확립한다.\n4. 더 나아가 나의 정체성을 확립하고 우리 고유의 문화를 배우고 익히며, 이를 통해 과거의 숨결과 지혜가 동화된 독창적인 문화발전을 도모한다.','출석,20,수업태도,0,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Quiz','수업 중 강의 노트,이지양','이수아','lee-sooah@hanmail.net',92),('2014/2-0000-2-2970-02','우리 민족이 삶과 죽음에 대면하여 실천해 온 일상 속에는 고유한 문화적 규칙들과 방식들이 존재한다. 본 강좌는 민간 생활과 결부된 문화 일반을 살펴봄으로써 우리가 현재 공유하는 문화의 원형을 찾아나가는 여정이다. 강의는 민간신앙, 세시풍속, 민속예술, 관혼상제의 의례 등으로 구성되며 학생들은 한 학기에 한 번 이상 놀이와 축제의 현장 등 직접 우리의 민속을 체험할 수 있는 장소를 방문하고 이에 대해 함께 토론한다.','1. 한국의 민속 일반에 대해 이해하고, 잊고 있었던 우리의 과거를 되찾아 현재의 일상에 복원한다.\n2. 면면히 이어져 내려온 우리의 민속을 이해하고 이를 바탕으로 현재 일상에서 관찰할 수 있는 그 반향들을 이해한다. \n3. 한국 민속에 대한 자부심을 가지고 우리 민족의 정체성을 확립한다.\n4. 더 나아가 나의 정체성을 확립하고 우리 고유의 문화를 배우고 익히며, 이를 통해 과거의 숨결과 지혜가 동화된 독창적인 문화발전을 도모한다.','출석,20,수업태도,0,중간고사,20,기말고사,20,발표,20,토의,0,과제,20,Quiz','한국민속학,이두현 등,일조각,2004','','',53),('2014/2-0000-2-2977-01','한국과 중국의 경전 및 시, 산문 가운데 정평있는 문장과 글을 선별하여 강독, 감상합니다. 교재 본문을 해석하는 가운데 기본 한자어를 습득하고 활용할 수 있도록 우리말 어휘와 연관성을 살펴 봅니다. 더불어 교양의 폭을 넓히기 위하여 고사 성어를 써서 익히도록 합니다.','기본 한자인 부수를 이해하고 한자의 형성 원리를 파악합니다. 한 단계 더 나아가 일상 생활에 유용한 어휘들을 익히며, 전공과 교양을 위한 초보적인 한문 해독 능력을 키우는데 그 목표가 있습니다. 더불어 동양적 전통문화에 대한 이해를 통해 인문학적 교양의 폭을 넓히는 계기를 마련하고자 합니다.','출석,15,중간고사,35,기말고사,35,과제보고서,15,수업태도,0,Quiz,0,기타,0','한문의 세계,광운대한문학교실,월인,2000','','',36),('2014/2-0000-2-2977-02','한국과 중국의 경전 및 시, 산문 가운데 정평있는 문장과 글을 선별하여 강독, 감상합니다. 교재 본문을 해석하는 가운데 기본 한자어를 습득하고 활용할 수 있도록 우리말 어휘와 연관성을 살펴 봅니다. 더불어 교양의 폭을 넓히기 위하여 고사 성어를 써서 익히도록 합니다.','기본 한자인 부수를 이해하고 한자의 형성 원리를 파악합니다. 한 단계 더 나아가 일상 생활에 유용한 어휘들을 익히며, 전공과 교양을 위한 초보적인 한문 해독 능력을 키우는데 그 목표가 있습니다. 더불어 동양적 전통문화에 대한 이해를 통해 인문학적 교양의 폭을 넓히는 계기를 마련하고자 합니다.','출석,15,중간고사,35,기말고사,35,과제보고서,15,수업태도,0,Quiz,0,기타,0','한문의 세계,광운대한문학교실,월인,2000','','',19),('2014/2-0000-2-2977-03','한문을 공부할 때 한자의 음과 뜻을 정확히 파악하고 글귀의 의미를 따진 뒤, 관련 사항들을 두루 공부하여 전체 글의 사상을 파악하는 단계로 나아가야 한다. 박학(博學:관련 지식의 습득)과 독서(讀書:꼼꼼한 읽기)를 두축으로 한다.','동아시아 보편 문화권 속에서 널리 사용된 한자어나 우리의 사회 문화 환경 속에서 만들어진 한자어들도 있다. 한자어도 우리말이다. 한자어에 대한 공부는 우리말과 글에 대한 적확하게 알 수 있도록 해준다. 또한 한문고전은 우리 고유 문화를 형성하는데 큰 기여를 했고 또 우리의 문화를 담고 있다. 이처럼 한문 고전 속에 담긴 삶의 여러 모습들을 공부한다면, 각자의 삶을 좀더 객관적으로 바라볼 수 있는 유력한 시점을 확보할 수 있다. 그러기 위해서 먼저 한자의 생성 원리 및 한자의 뜻과 음을 배우고 익힐 것이다.','출석,10,중간고사,20,기말고사,25,과제보고서,10,수업태도,5,Quiz,30,기타,0','한문대강,권중구,보고사,2011','','',39),('2014/2-0000-2-2977-04','한문을 공부할 때 한자의 음과 뜻을 정확히 파악하고 글귀의 의미를 따진 뒤, 관련 사항들을 두루 공부하여 전체 글의 사상을 파악하는 단계로 나아가야 한다. 박학(博學:관련 지식의 습득)과 독서(讀書:꼼꼼한 읽기)를 두축으로 한다.','동아시아 보편 문화권 속에서 널리 사용된 한자어나 우리의 사회 문화 환경 속에서 만들어진 한자어들도 있다. 한자어도 우리말이다. 한자어에 대한 공부는 우리말과 글에 대한 적확하게 알 수 있도록 해준다. 또한 한문고전은 우리 고유 문화를 형성하는데 큰 기여를 했고 또 우리의 문화를 담고 있다. 이처럼 한문 고전 속에 담긴 삶의 여러 모습들을 공부한다면, 각자의 삶을 좀더 객관적으로 바라볼 수 있는 유력한 시점을 확보할 수 있다. 그러기 위해서 먼저 한자의 생성 원리 및 한자의 뜻과 음을 배우고 익힐 것이다.','출석,10,중간고사,20,기말고사,25,과제보고서,10,수업태도,5,Quiz,30,기타,0','한문대강,권중구,보고사,2011','','',20),('2014/2-0000-2-2977-05','한문을 공부할 때 한자의 음과 뜻을 정확히 파악하고 글귀의 의미를 따진 뒤, 관련 사항들을 두루 공부하여 전체 글의 사상을 파악하는 단계로 나아가야 한다. 박학(博學:관련 지식의 습득)과 독서(讀書:꼼꼼한 읽기)를 두축으로 한다.','동아시아 보편 문화권 속에서 널리 사용된 한자어나 우리의 사회 문화 환경 속에서 만들어진 한자어들도 있다. 한자어도 우리말이다. 한자어에 대한 공부는 우리말과 글에 대한 적확하게 알 수 있도록 해준다. \n또한 한문고전은 우리 고유 문화를 형성하는데 큰 기여를 했고 또 우리의 문화를 담고 있다. 이처럼 한문 고전 속에 담긴 삶의 여러 모습들을 공부한다면, 각자의 삶을 좀더 객관적으로 바라볼 수 있는 유력한 시점을 확보할 수 있다.\n그러기 위해서 먼저 한자의 생성 원리 및 한자의 뜻과 음을 배우고 익힐 것이다.','출석,15,중간고사,20,기말고사,20,과제보고서,20,수업태도,5,Quiz,20,기타,0','한문강해,민병수,태학사,1995','','',37),('2014/2-0000-2-2977-06','한문을 공부할 때 한자의 음과 뜻을 정확히 파악하고 글귀의 의미를 따진 뒤, 관련 사항들을 두루 공부하여 전체 글의 사상을 파악하는 단계로 나아가야 한다. 박학(博學:관련 지식의 습득)과 독서(讀書:꼼꼼한 읽기)를 두축으로 한다.','동아시아 보편 문화권 속에서 널리 사용된 한자어나 우리의 사회 문화 환경 속에서 만들어진 한자어들도 있다. 한자어도 우리말이다. 한자어에 대한 공부는 우리말과 글에 대한 적확하게 알 수 있도록 해준다. \n또한 한문고전은 우리 고유 문화를 형성하는데 큰 기여를 했고 또 우리의 문화를 담고 있다. 이처럼 한문 고전 속에 담긴 삶의 여러 모습들을 공부한다면, 각자의 삶을 좀더 객관적으로 바라볼 수 있는 유력한 시점을 확보할 수 있다.\n그러기 위해서 먼저 한자의 생성 원리 및 한자의 뜻과 음을 배우고 익힐 것이다.','출석,15,중간고사,20,기말고사,20,과제보고서,20,수업태도,5,Quiz,20,기타,0','한문강해,민병수,태학사,1995','','',20),('2014/2-0000-2-2977-07','현재 세계의 언어문화의 흐름은 거대한 영어문화권과 한자문화권, 아랍어나 인도어문화권 등으로 재편되고 있다. 그 중에서 한자문화권은 중국을 비롯하여 한국, 일본, 대만, 월남, 홍콩등과 싱가포르, 말레시아 등 동남아시아, 구미의 한자문화권 교포사회까지 매우 다양하다. 즉 현재 60억의 세계인구 중 한자문화권은 약 30%인, 20억 명으로 추산되고, 그 경제규모 역시 나날이 향상되어 세계무역과 경제의 중심축으로 부상하고 있다. 이러한 시점에서 한자문화권의 주요 성원인 우리나라는 그 이점을 최대한 활용하여 국가발전의 견인차로 삼아야 한다.','한자는 국어의 어원을 탐색하고 변별함은 물론이고, 다른 한자문화권의 언어 즉, 현대중국어와 일본어를 학습함에 있어서 큰 기초가 된다. 물론 발음문제는 별도로 학습해야 하지만 이는 구미(歐美)의 상이한 외국어를 어원부터 발음까지 일일이 학습하는 것에 비해서 엄청난 이점을 지니고 있다. 따라서 한자의 기원과 형성, 변천과정을 살펴보고, 한자가 과거 동아시아의 공용문자로서의 역할을 되새겨보며, 향후 국제적인 문자로서 발전 가능성과 역내문화권에 끼칠 영향을 되새겨 본다.','출석,10,중간고사,35,기말고사,35,과제보고서,10,수업태도,10,Quiz,0,기타,0','한문선독(漢文選讀),김영진,임의출판물','','',30),('2014/2-0000-2-2977-08','강의는 교재와 강의자료(유인물)을 중심으로 강독 위주의 진행을 하되, 필요한 경우 학생들의 참여를 유도한다.1. 한자의 이해(한자의 구성, 허자의 쓰임 등) 2. 사자성어 3. 경전강독 4. 고전 명문 강독','본 강좌의 목적은 전공인과 교양인을 위한 초보적인 한문독해능력을 기름과 동시에 우리말 어휘의 70%를 차지하는 한자 어휘를 이해하고 사용하는데 도움이 될 수 있도록 하는데 두었다.','출석,20,중간고사,30,기말고사,30,과제보고서,20,수업태도,0,Quiz,0,기타,0','한문의 세계,광운대 한문학교실,월인,2001','','',37),('2014/2-0000-2-2981-01','국가는 무엇이며 국가의 행정활동은 우리의 일상생활에 어떤 영향을 미치는가? 현대사회에서 국가는 가장 큰 조직체로서 그 행정력은 인간의 생활에 광범위한 영향을 미치고 있다. 이 과목은 국가 또는 정부의 행정 활동을 이해하도록 소개하고 중앙정부와 지방정부간의 협조와 견제, 정부와 사회 사이의 관계를 다양한 사례를 통해 이해하도록 한다. 구체적으로는 국가와 공공성, 행정의 보편성과 특수성, 미시와 거시, 중앙과 지방 등의 내용들이 포함된다.','1. 시민으로서 정부와 국가행정의 본질적 성격과 사회적 의의에 대하여 기본적인 이해의 시야를 확보한다. \n2. 시대와 환경의 변화에 따른 국가의 공공문제 해결방식을 이해하고 그 이론을 습득한다. \n3. 한국 행정의 보편성과 특수성을 이해하고 행정의 미시적 차원과 거시적 차원을 아우르는 행정의 통합성을 이해한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','행정국가의 이해와 전망,김정헌,대구대학교출판부,2010','오정헌','arima731@naver.com',56),('2014/2-0000-2-2981-02','국가는 무엇이며 국가의 행정활동은 우리의 일상생활에 어떤 영향을 미치는가? 현대사회에서 국가는 가장 큰 조직체로서 그 행정력은 인간의 생활에 광범위한 영향을 미치고 있다. 이 과목은 국가 또는 정부의 행정 활동을 이해하도록 소개하고 중앙정부와 지방정부간의 협조와 견제, 정부와 사회 사이의 관계를 다양한 사례를 통해 이해하도록 한다. 구체적으로는 국가와 공공성, 행정의 보편성과 특수성, 미시와 거시, 중앙과 지방 등의 내용들이 포함된다.','1. 시민으로서 정부와 국가행정의 본질적 성격과 사회적 의의에 대하여 기본적인 이해의 시야를 확보한다. \n2. 시대와 환경의 변화에 따른 국가의 공공문제 해결방식을 이해하고 그 이론을 습득한다. \n3. 한국 행정의 보편성과 특수성을 이해하고 행정의 미시적 차원과 거시적 차원을 아우르는 행정의 통합성을 이해한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','행정국가의 이해와 전망,김정헌,대구대학교출판부,2010','이보라','erestuq3@naver.com',20),('2014/2-0000-2-2987-01','오늘날 경영 마인드는 단지 경영자 뿐만 아니라 기업에서 일하는 모든 사람이 가져야 할 기본 덕목으로 자리매김된다. 본 교과목은 21세기 글로벌 비즈니스 환경 속에서 현대 기업의 경영원리와 특징 그리고 기업의 나아갈 방향과 미래의 경영자가 될 학생들이 갖추어야 할 자질과 학문적·실용적 기초를 다양한 사례를 통해 제시한다. 구체적으로는 기업과 경영의 변천사, 조직의 설계와 유형, 경영활동의 기본과정, 제품의 생산과 판매, 자산과 사람의 관리, 정보와 지식의 관리, 글로벌 시장에서의 경영, 변화와 혁신관리 등의 주제들에 대하여 강의와 토론, 발표를 통해 기업경영의 본질과 의미에 대한 이해의 수준을 높인다.','1. 본 과목은 비단 경영학을 전공하고자 하는 학생이나 향후 기업을 경영하고자 하는 학생들 뿐만 아니라 기업의 일원으로서 갖추어야 할 기업 경영과 조직생활에 필요한 기초 지식을 습득하고 기본적인 경영 마인드를 고취시킨다.\n2. 기업경영의 의미와 본질을 이해함으로써 기업윤리와 기업문화, 기업의 사회적 책임에 대해 숙지하고 이를 직장생활에서 활용하도록 한다.\n3. 디지털 시대, 글로벌 시대에 적합한 바람직한 기업과 경영의 모델을 학습한다.','출석,10,수업태도,10,중간고사,0,기말고사,35,발표,0,토의,10,과제,10,Quiz','경영학 - 유쾌한 이야기,김광희,내하출판사','','',49),('2014/2-0000-2-2988-01','고도로 분화되고 발달된 현대 사회에서 법은 “사회생활은 곧 법생활”이라고 표현될 정도로 중요한 사회 통합의 구심점 역할을 하고 있다. 본 수업은 법학 전반에 대하여 살펴본 다음, 민사(금전거래, 공증, 보증, 내용증명, 가압류, 소송, 소액심판, 강제집행, 주택임대차, 부동산, 교통사고,등)와 형사(범죄 일반, 소송절차 등), 그리고 조세와 행정소송 등의 내용들을 차례로 학습한다.','1. 사회교양인으로서 기본적으로 갖추어야 할 법지식을 학생들에게 전달함으로써 건전한 법상식가로서의 광운인 배출을 그 목적으로 한다.\n2. 건전한 시민으로서의 일상적 경제활동에 필요한 법적 지식을 확보한다.\n3. 법적 정의의 관점에서 경제생활을 이해함으로써 현대사회의 복잡한 경제현상을 비롯하여 사회생활에서 봉착하는 다양한 문제를 해결하는 능력을 키운다.','출석,10,수업태도,10,중간고사,40,기말고사,40,발표,0,토의,0,과제,0,Quiz,','강의안 게시,김동옥','김기웅','krkgo@naver.com',98),('2014/2-0000-2-2991-01','현대사회에서 과학의 모습을 역사적으로 살펴봄으로써 과학이 사회, 문화와 어떤 관계를 맺으며 변해왔는지를 포괄적으로 이해할 수 있도록 한다. 이를 위해 고대로부터 현대까지, 그리고 서양과 동양에서의 자연과학의 변천과정 및 그것이 사회에 미친 영향을 균형 있게 다룬다.','1. 과학을 기술발전의 도구로만 이해하는 것이 아니라 인간이 사회 안에서 갖는 세계관으로 이해하도록 돕는다. \n2. 역사를 통해 사회와 과학의 관계에 대한 자신의 주장을 논리적으로 종합 정리하여 명확하게 전달할 수 있는 능력을 심화하도록 하며, 교양인으로서 인문학적인 지식을 늘려주고, 이에 기반으로 하여 자연과학을 이해하게 함으로써 현대사회를 이끌 균형 있는 지성인으로 성장하도록 하는 데 본 교과목의 목적을 둔다.','출석,20,수업태도,0,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz,','과학적 과학사,조성관,수업자료,2014','안영준','yjan084@naver.com',62),('2014/2-0000-2-2993-01','정보 및 정보화, 정보사회를 정의하고 정치, 경제, 사회, 문화 및 교육 등 전반적인 변화를 학습 및 고찰하여 정보사회의 바람직한 적응력을 함양한다.','정보화 사회의 각 영역별 특징과 변화를 학습한다.\n정보의 중요성과 정보화 사회의 윤리 및 주요 법률을 학습한다.\n정보화 사회의 창의산업과 성장동력을 학습한다.\n정보화 사회의 진척방향을 학습하고, 세계화와 변화 및 개혁에 능동적으로 대처할 능력과 적응력을 함양한다.','출석,10,수업태도,5,중간고사,30,기말고사,40,발표,5,토의,0,과제,10,Quiz,','정보사회의 이해,이종구 외,미래인,2008','김재성','baikal9185@naver.com',94),('2014/2-0000-2-2994-01','환경은 크게 자연환경과 생활환경으로 구분된다. 본 교과목에서는 이들의 차이 및 특성과 함께 지구 생태계의 구성 및 원리, 환경과 생태계 및 그 구성원들 간의 상호관계 및 영향을 포괄적으로 다루며, 환경문제의 접근을 위해서는 여러 분야와의 상호관련성을 이해하여야 한다는 것과 또한 지구차원의 문제이며 자신의 생명과 직/간접으로 연관되는 것임을 다룬다.','1. 이 과목의 목적은 학생들로 하여금 지구온난화, 환경호르몬, 유전자 변형식품(GMO) 등의 주요 글로벌 환경이슈들을 다루면서 글로벌마인드를 높이도록 하는데 있다.\n2. 또한 이들 문제들을 해결하기 위해 현재의 사회적, 경제적, 기술적, 그리고 법률적인 면을 종합적으로 고려하는 능력을 기른다.\n3. 이를 바탕으로 복합적인 사고능력을 배양하여 복합적이고 융합적인 문제를 해결하는 능력을 배양한다.','출석,10,수업태도,0,중간고사,20,기말고사,40,발표,10,토의,5,과제,10,Quiz','지구환경과학,Enger, Smith,(주)북스힐,2006','윤대근','dbseorms777@naver.com',19),('2014/2-0000-2-2994-02','환경은 크게 자연환경과 생활환경으로 구분된다. 본 교과목에서는 이들의 차이 및 특성과 함께 지구 생태계의 구성 및 원리, 환경과 생태계 및 그 구성원들 간의 상호관계 및 영향을 포괄적으로 다루며, 환경문제의 접근을 위해서는 여러 분야와의 상호관련성을 이해하여야 한다는 것과 또한 지구차원의 문제이며 자신의 생명과 직/간접으로 연관되는 것임을 다룬다.','1. 이 과목의 목적은 학생들로 하여금 지구온난화, 환경호르몬, 유전자 변형식품(GMO) 등의 주요 글로벌 환경이슈들을 다루면서 글로벌마인드를 높이도록 하는데 있다.\n2. 또한 이들 문제들을 해결하기 위해 현재의 사회적, 경제적, 기술적, 그리고 법률적인 면을 종합적으로 고려하는 능력을 기른다.\n3. 이를 바탕으로 복합적인 사고능력을 배양하여 복합적이고 융합적인 문제를 해결하는 능력을 배양한다.','출석,10,수업태도,0,중간고사,20,기말고사,40,발표,10,토의,5,과제,10,Quiz','지구환경과학,Enger, Smith,(주)북스힐,2006','윤대근','dbseorms777@naver.com',20),('2014/2-0000-2-2994-03','환경은 크게 자연환경과 생활환경으로 구분된다. 본 교과목에서는 이들의 차이 및 특성과 함께 지구 생태계의 구성 및 원리, 환경과 생태계 및 그 구성원들 간의 상호관계 및 영향을 포괄적으로 다루며, 환경문제의 접근을 위해서는 여러 분야와의 상호관련성을 이해하여야 한다는 것과 또한 지구차원의 문제이며 자신의 생명과 직/간접으로 연관되는 것임을 다룬다.','1. 이 과목의 목적은 학생들로 하여금 지구온난화, 환경호르몬, 유전자 변형식품(GMO) 등의 주요 글로벌 환경이슈들을 다루면서 글로벌마인드를 높이도록 하는데 있다.\n2. 또한 이들 문제들을 해결하기 위해 현재의 사회적, 경제적, 기술적, 그리고 법률적인 면을 종합적으로 고려하는 능력을 기른다.\n3. 이를 바탕으로 복합적인 사고능력을 배양하여 복합적이고 융합적인 문제를 해결하는 능력을 배양한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Qui','새로운 환경과생태,김임순 외,동화기술,2013','강영국','ykkang123@naver.com',20),('2014/2-0000-2-3037-01','(1) 본 강의는on-line 강의로서2 인의 교수가 담당하며, 중간고사 이전까지는 영어의 시제, 조동사, 가정법, 수동태 등 Unit 44까지 주제별로 강의가 진행되고, 평가가 시행되며, 중간고사 이후에는 Unit 51부터, 동명사, 부정사 등의 용법과, 분사구문, 관사, 명사, 대명사, 관계사, 접속사, 전치사 등을 중심으로 강의가 진행된다. 단, 중간고사와 기말고사는 고사 기간 중 토요일에 반드시 off-line으로 광운대학교에서 시험을 본다.\n(2) 본 강의는 온라인으로 진행됨으로 학생 개인과온라인 강의사이트와 접속이 원활하게 유지될 수 있는 것을 전제로 하며, 교수와 학생의 상호작용이 온라인상의 Q&A나 이메일 등으로 제한되어 있어 교수-학생의 의사소통이 실시간으로 이루어지지 못하는 것을 전제로 한다','영어를 전공하지 않는 일반학생들에게 영어 문법에 대한 체계적인 교육을 가능하게 하기 위하여, 정확하고 표준적인 형태의 영어문장을 제시하고 그와 관련된 의미와 용법을 익힘으로써 영어 독해력의 향상은 물론, 작문, 말하기 등의 영역에서 영어문법을 응용할 수 있는 기본 능력을 기르는 것을 목적으로 한다.','출석,10,중간고사,40,기말고사,40,과제보고서,0,수업태도,0,Quiz,10,기타,0','Grammar in Use (Intermediate)3rdedition,Raymond Murphy,Cambridge Univ. Press,2009','김승열','4611567@naver.com',99),('2014/2-0000-2-3037-02','(1) 본 강의는on-line 강의로서 중간고사 이전까지는 시제, 가정법, 수동태, 화법 등 Unit 44까지 주제별로 강의가 진행되고, 중간고사 이후에는 Unit 51부터 동명사, 부정사 등의 용법과, 관사, 명사, 대명사, 관계사, 형용사, 부사, 전치사 등 품사 등을 중심으로 Unit 96까지 강의가 진행된다. 온라인 퀴즈 평가가 2회 시행되며, 중간고사와 기말고사는 고사 기간 중 토요일에 off-line으로 시행된다.\n(2) 본 강의는 온라인으로 진행됨으로 학생 개인과온라인 강의사이트와 접속이 원활하게 유지될 수 있는 것을 전제로 하며, 교수와 학생의 상호작용이 온라인상의 Q&A나 이메일 등으로 제한되어 있어 교수-학생의 의사소통이 실시간으로 이루어지지 못하는 것을 전제로 한다','영어를 전공하지 않는 일반학생들에게 영어 문법에 대한 체계적인 교육을 가능하게 하기 위하여 정확하고 표준적인 형태의 영어문장을 제시하고 그와 관련된 의미와 용법을 익힘으로써 영어 독해력의 향상은 물론, 작문, 말하기 등의 영역에서 영어문법을 응용할 수 있는 기본 능력을 기르는 것을 목적으로 한다.','출석,10,중간고사,40,기말고사,40,과제보고서,0,수업태도,0,Quiz,10,기타,0','Grammar in Use (intermediate) 3rd edition,Raymond Murphy,Cambridge Univ. Press,2009','김승열','4611567@naver.com',98),('2014/2-0000-2-3196-01','고급문화와 대중문화의 경계가 허물어지는 현대에는 음악을 일상에서 경험하고 즐길 수 있는 다양한 기회들이 확장되고 있다. 본 강좌는 주요 음악가들의 생애 및 작품에 초점을 맞추어 음악사 및 음악에 관련된 기본이론들을 살펴본다. 강의 초반부에서는 음악사를 개괄하고 이후 한 주에 1~2명씩 주요 음악가들을 소개한다. 한 학기 동안 언급되는 음악가들은 중세, 고전낭만, 현대, 한국음악, 대중음악의 다섯 범주 중 적어도 셋 이상의 영역에서 선택된다. 학생들은 공연을 관람하고 자신이 감상한 음악에 대해 토론한다.','1. 음악 감상에 필요한 기본적인 용어 및 이론을 습득한다.\n2. 음악을 일상의 한 부분으로서 체험하여 음악 감상 및 음악회에 가는 경험을 친근하게 느낄 수 있도록 만든다.\n3. 자신이 감상하는 음악을 음악사의 지도 속에 배치할 수 있다.\n4. 음악사에 길이 남는 업적을 이룩하는 과정에서 당대의 거장들이 처했던 상황 및 그들이 보여준 용기와 지혜를 이해하고, 이를 통해 강인한 정신과 창조적 상상력을 본받는다.','출석,10,수업태도,20,중간고사,10,기말고사,10,발표,10,토의,20,과제,10,Qu','Grammar in Use (intermediate) 3rd edition,Raymond Murphy,Cambridge Univ. Press,2009','김태민','taminee@kw.ac.kr',99),('2014/2-0000-2-3196-02','고급문화와 대중문화의 경계가 허물어지는 현대에는 음악을 일상에서 경험하고 즐길 수 있는 다양한 기회들이 확장되고 있다. 본 강좌는 주요 음악가들의 생애 및 작품에 초점을 맞추어 음악사 및 음악에 관련된 기본이론들을 살펴본다. 강의 초반부에서는 음악사를 개괄하고 이후 한 주에 1~2명씩 주요 음악가들을 소개한다. 한 학기 동안 언급되는 음악가들은 중세, 고전낭만, 현대, 한국음악, 대중음악의 다섯 범주 중 적어도 셋 이상의 영역에서 선택된다. 학생들은 공연을 관람하고 자신이 감상한 음악에 대해 토론한다.','1. 음악 감상에 필요한 기본적인 용어 및 이론을 습득한다.\n2. 음악을 일상의 한 부분으로서 체험하여 음악 감상 및 음악회에 가는 경험을 친근하게 느낄 수 있도록 만든다.\n3. 자신이 감상하는 음악을 음악사의 지도 속에 배치할 수 있다.\n4. 음악사에 길이 남는 업적을 이룩하는 과정에서 당대의 거장들이 처했던 상황 및 그들이 보여준 용기와 지혜를 이해하고, 이를 통해 강인한 정신과 창조적 상상력을 본받는다.','출석,10,수업태도,20,중간고사,10,기말고사,10,발표,10,토의,20,과제,10,Qu','Grammar in Use (intermediate) 3rd edition,Raymond Murphy,Cambridge Univ. Press,2009','안재민','jaemin118@hanmail.net',100),('2014/2-0000-2-3349-01','초급일본어 학습자를 대상으로 일본 영화와 드라마를 교재로 실생활에 활용할 수 있는 일본어 능력을 배양한다. 스크린의 반복 시청, 대본 받아쓰기,암기 등을 통하여 일본어 구사력에 대한 자신감과 중급 및 상급 단계의 일본어 수업을 이해할 수 있는 토대를 마련한다.','1.일본어 시청각 자료(드라마와 애니메이션 등)를 통해 실제 일본에서 사용되는 회화나 표현 등을 익힌다. 이 때 자연스럽게 일본인들의 정서와 문화를 익히도록 한다. \n2.주로 일본어 초급 내지 중급 학습자를 주 대상으로, 비디오 시청과 함께 그에 따른 어구 및 문법 사항을 학습한다. \n3.자연스럽게 일본어 회화 표현에 대한 자신감을 유도한다.\n4.일본어 학습뿐만 아니라 일본인 및 일본의 사회와 문화에 대한 지식을 전달하여 일본의 문화와 사회에 대한 관심과 흥미를 고취시킨다.','출석,10,수업태도,10,중간고사,20,기말고사,20,발표,10,토의,0,과제,20,Qui','Grammar in Use (intermediate) 3rd edition,Raymond Murphy,Cambridge Univ. Press,2009','','',30),('2014/2-0000-2-3349-02','초급일본어 학습자를 대상으로 일본 영화와 드라마를 교재로 실생활에 활용할 수 있는 일본어 능력을 배양한다. 스크린의 반복 시청, 대본 받아쓰기,암기 등을 통하여 일본어 구사력에 대한 자신감과 중급 및 상급 단계의 일본어 수업을 이해할 수 있는 토대를 마련한다.','1.일본어 시청각 자료(드라마와 애니메이션 등)를 통해 실제 일본에서 사용되는 회화나 표현 등을 익힌다. 이 때 자연스럽게 일본인들의 정서와 문화를 익히도록 한다. \n2.주로 일본어 초급 내지 중급 학습자를 주 대상으로, 비디오 시청과 함께 그에 따른 어구 및 문법 사항을 학습한다. \n3.자연스럽게 일본어 회화 표현에 대한 자신감을 유도한다.\n4.일본어 학습뿐만 아니라 일본인 및 일본의 사회와 문화에 대한 지식을 전달하여 일본의 문화와 사회에 대한 관심과 흥미를 고취시킨다.','출석,30,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,0,Quiz,','Grammar in Use (intermediate) 3rd edition,Raymond Murphy,Cambridge Univ. Press,2009','','',25),('2014/2-0000-2-3645-01','미입력','This course will be divided into five segments, though not of equal length. First, we will briefly study the underlying theory of and rationale for the law and overarching the political system. We will examine philosophical, religious, political, and economic theories and rationales for the law/political system. Second, we will take a comparative look at law and politics. Third, we will look at constitutional law as the major form of protection for the people from the government. Fourth, we will examine civil/human rights as the major form of protection for the people from society at large. Last, we will examine labor rights as a major form of protection for laborers from businesses.','출석,20,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,10,Quiz','국사·사회와 법,장태환,법경사21C,2012','','',65),('2014/2-0000-2-3730-01','오늘날 매스미디어는 정보와 의견, 이야기와 환상을 생산, 배포하는 주요한 역할을 담당한다. 신문, 방송, 인터넷 등의 미디어는 우리의 일상생활 속에 깊숙이 자리잡고 있으며 정치, 경제, 문화 등 폭넓은 사회적 현상들과 긴밀하게 얽혀있다. 이 강좌는 현대 사회에서 정치, 경제, 문화 등 폭넓은 사회적 현상과 긴밀히 얽혀있는 신문, 방송, 인터넷 등 다양한 매스미디어에 대하여 살펴봄으로써 미디어가 개인의 일상생활에서부터 사회의 다양한 측면에 이르기까지 어떠한 영향을 미치는지 살펴본다.','1. 매스미디어에 대한 여러 가지 관련 이론들과 최근의 쟁점을 파악함으로써 매스미디어의 사회적 기능과 역할, 다른 사회영역들과의 관계를 이해한다.\n2. 매스미디어와 디지털 미디어에 대한 전반적이고 체계적인 이해의 틀을 확립하고 다양한 매스미디어 및 그와 연관된 사회현상에 대한 지식과 비판적 안목을 갖춘다.\n3. 매스미디어를 통해 사회의 복합적이고 융합적인 문제들을 인식·파악·분석함으로써 복합문제를 인식하고 해결하는 안목을 기른다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','현대사회와 미디어 (2014년 개정판),한균태 외,커뮤니케이션북스,2014','','',37),('2014/2-0000-2-3730-02','오늘날 매스미디어는 정보와 의견, 이야기와 환상을 생산, 배포하는 주요한 역할을 담당한다. 신문, 방송, 인터넷 등의 미디어는 우리의 일상생활 속에 깊숙이 자리잡고 있으며 정치, 경제, 문화 등 폭넓은 사회적 현상들과 긴밀하게 얽혀있다. 이 강좌는 현대 사회에서 정치, 경제, 문화 등 폭넓은 사회적 현상과 긴밀히 얽혀있는 신문, 방송, 인터넷 등 다양한 매스미디어에 대하여 살펴봄으로써 미디어가 개인의 일상생활에서부터 사회의 다양한 측면에 이르기까지 어떠한 영향을 미치는지 살펴본다.','1. 매스미디어에 대한 여러 가지 관련 이론들과 최근의 쟁점을 파악함으로써 매스미디어의 사회적 기능과 역할, 다른 사회영역들과의 관계를 이해한다.\n2. 매스미디어와 디지털 미디어에 대한 전반적이고 체계적인 이해의 틀을 확립하고 다양한 매스미디어 및 그와 연관된 사회현상에 대한 지식과 비판적 안목을 갖춘다.\n3. 매스미디어를 통해 사회의 복합적이고 융합적인 문제들을 인식·파악·분석함으로써 복합문제를 인식하고 해결하는 안목을 기른다.','출석,20,수업태도,10,중간고사,0,기말고사,40,발표,0,토의,0,과제,30,Quiz,','현대사회와 미디어,한균태 외,커뮤니케이션북스,2014','','',67),('2014/2-0000-2-3932-01','한국근현대사는 우리 사회의 현실적 문제들의 역사적 근원을 이해하게 해주며 이를 바탕으로 미래를 전망하기 위한 강좌이다. \n19세기 중반부터 오늘날까지, 개항과 청일·러일전쟁, 식민화와 저항, 해방과 한국전쟁, 4·19와 5·16, 경제성장과 민주화에 이르는 한국사의 격동의 양상을 거시사적 조망과 미시사적 분석을 결합하여 파악한다. \n한국근현대사의 주요 사건에 대한 상이한 해석의 내용과 배경, 의미 등을 토론한다.','1. 격동기 한국사회의 역사적 경험에 대한 한층 심도 깊은 이해를 추구한다. \n2. 식민화와 전쟁, 분단과 독재, 경제성장과 민주화 등 격동의 한국 근현대사에 대한 균형 잡힌 인식 틀을 확보한다.\n3. 역사에 비춰본 현재의 모습을 재조명하고 이를 근거로 미래를 예견할 수 있는 바탕을 마련한다.\n4. 근현대사가 현재에 미친 영향을 심도있게 통찰하고 더 밝은 미래를 만들어 갈 역량을 키운다.','출석,10,수업태도,10,중간고사,20,기말고사,30,발표,10,토의,10,과제,10,Qu','현대사회와 미디어,한균태 외,커뮤니케이션북스,2014','임종훈','jonghun92@naver.com',93),('2014/2-0000-2-3932-02','한국근현대사는 우리 사회의 현실적 문제들의 역사적 근원을 이해하게 해주며 이를 바탕으로 미래를 전망하기 위한 강좌이다. \n19세기 중반부터 오늘날까지, 개항과 청일·러일전쟁, 식민화와 저항, 해방과 한국전쟁, 4·19와 5·16, 경제성장과 민주화에 이르는 한국사의 격동의 양상을 거시사적 조망과 미시사적 분석을 결합하여 파악한다. \n한국근현대사의 주요 사건에 대한 상이한 해석의 내용과 배경, 의미 등을 토론한다.','1. 격동기 한국사회의 역사적 경험에 대한 한층 심도 깊은 이해를 추구한다. \n2. 식민화와 전쟁, 분단과 독재, 경제성장과 민주화 등 격동의 한국 근현대사에 대한 균형 잡힌 인식 틀을 확보한다.\n3. 역사에 비춰본 현재의 모습을 재조명하고 이를 근거로 미래를 예견할 수 있는 바탕을 마련한다.\n4. 근현대사가 현재에 미친 영향을 심도있게 통찰하고 더 밝은 미래를 만들어 갈 역량을 키운다.','출석,10,수업태도,0,중간고사,30,기말고사,30,발표,20,토의,10,과제,0,Quiz','매주 제시되는 수업자료','','',73),('2014/2-0000-2-3933-01','본 과목은 우리의 삶에 있어서 예술이 차지하는 중요성에 대한 인식을 높이고, 예술적 정서를 계발하며, 예술적 체험의 기회를 제공하고 이러한 정서적 함양이 지성적 함양 못지 않는 중요성을 가지고 있다는 것을 깨닫게 하며 삶의 질적 수준을 향상시키는 데 그 목적을 두고 있다. 이를 위해 미의 본질과 그 본질을 설명하려는 이론과 개념들을 검토하고, 나아가 체험자로서, 감상자로서 나와 예술이 맺는 관계를 성찰함으로써 궁극적으로 특정 사회의 모습과 그 안에서 삶을 살아가고 있는 인간의 모습을 이해하도록 한다.','1. 예술의 본질에 대한 다양한 관점과 이론을 역사적으로, \n주제별로 검토\n 2. 체험과 감상, 창조로서의 예술을 자신의 삶과 연관시켜 \n반성적으로 고찰','출석,15,수업태도,0,중간고사,20,기말고사,20,발표,25,토의,20,과제,0,Quiz','《미학의 역사》,미학대계간행회,서울대학교출판부,2008','','',47),('2014/2-0000-2-3937-01','고대부터 현대까지 한·중·일 동아시아 삼국의 국제관계사를 개괄한다. \n전 근대 시기의 문화교류와 전쟁, 제국주의적 팽창과 더불어 맞이한 상이한 근대화와 국민국가의 형성과정, 삼국의 갈등과 침략, 전후 국제관계의 정상화, 영토분쟁, 교과서분쟁, 과거사문제 등을 심층적으로 분석한다.','역사적으로 형성된 동아시아 삼국의 교류와 협력, 갈등과 긴장의 이중적 국제관계를 이해하여 현재 동아시아 삼국간의 상호협력의 기반이 무엇이며 반목의 근원이 무엇인지를 파악한다.','출석,10,수업태도,5,중간고사,20,기말고사,35,발표,20,토의,10,과제,0,Quiz','한중일이 함께 쓴 동아시아 근현대사,한중일3국공동역사편찬위원회,휴머니스트,2012','','',57),('2014/2-0000-2-3939-01','미국의 주요 문학작품들을 통해 미국사회의 문화적 특성을 이해한다. \n미국 사회의 주요 기반이 된 청교주의, 프론티어 정신, 실용주의 정신, 미국의 꿈, 다문화주의 등을 저명한 미국문학 작품들을 통해 고찰한다. 예들 들어 포, 호손, 드라이저, 제럴드, 헤밍웨이 등의 문학에 반영된 미국적 가치와 사회상을 분석한다. 영상자료와 주요 문학작품의 해설자료를 보충하여 강의의 이해도를 높이며 미국의 사회와 문화에 대한 발표와 토의를 실시한다.','1. 문학작품을 통해 미국의 역사적, 사회적, 문화적 주요 특성을 살펴봄으로서 미국사회와 문화에 대한 이해를 심화시킨다. \n2. 미국 문학작품들에 표상된 인간과 세상과의 관계를 파악하며 사회적 환경 속에서 인간 개개인의 삶의 의미가 지닌 보편성을 이해한다.\n3. 미국의 가치와 문화가 지닌 보편성과 특수성을 이해하여 글로벌 마인드를 고취한다.','출석,10,수업태도,0,중간고사,30,기말고사,30,발표,10,토의,10,과제,10,Qui','The Great Gatsby,F. Scott Fitzgerald,Oxford Worlds Classics,2008','','',45),('2014/2-0000-2-3944-01','한국 근현대문학의 장르별 명작이라고 평가되는 작품들이 지닌 사회의식과 역사의식, 삶의 의미와 가치의 고양, 문학사적 위치 등 명작의 다양한 요소들을 파악한다. 예들 들어 이광수, 김유정, 황석영, 윤동주, 서정주, 청록파 시인들 등의 주요 작품을 다룬다.\n명작에 대한 이해와 재평가를 시도하며, 수강생 개개인이 한국명작 목록을 작성하여 발표하면서 문학의 가치와 본질에 대해 토론한다.','1. 한국문학의 명작들을 감상하고 수강생들이 주체적으로 분석할 수 있는 시각을 갖도록 한다. \n2. 수강생이 생각하는 명작의 이유들에 대한 발표와 토론을 통해 의사소통능력을 배양한다. \n3. 한국문학의 명작들이 보여주는 한국적인 문화코드와 감성, 정체성 등을 이해하여 한국인으로서의 자긍심을 고취한다.','출석,10,수업태도,10,중간고사,0,기말고사,50,발표,20,토의,10,과제,0,Quiz','없음','','',70),('2014/2-0000-2-3950-01','현대 인권문제의 주요 현안과 기초정보를 제공하며 인권의 사각지대에 놓인 소수자들의 삶과 문제를 이해한다. 인권영화와 다큐멘터리 영상물 등을 통해 현대사회의 인권문제에 대한 다각적이고 심층적인 이해를 도모한다. 이를 통해 현재 우리의 삶을 규정하는 다양한 권력관계, 제도 및 관습, 실천형태 등을 살펴보고 인간존재에 대한 이해의 폭을 넓힌다.','1. 개인, 시민, 국민, 세계시민 등 개인의 삶을 규정하는 사회적 관계성에 대한 이해수준을 높여 삶의 주체의식을 고양한다.\n2. 인권문제를 둘러싼 사회적 쟁점들을 검토함으로써 다양한 현대 사회문제에 대한 이해의 폭을 넓힌다. \n3. 국내외의 주요 인권 현안과 인권운동에 대한 기초 정보를 제공한다.\n4. 현대의 인권문제를 조사·발표·토의함으로써 인권에 대한 다양한 시각들을 확인하고 인권을 바라보는 올바른 시각을 갖도록 한다.','출석,20,수업태도,10,중간고사,0,기말고사,30,발표,10,토의,10,과제,20,Qui','사람답게 산다는 것,오창익,너머학교,2014','','',57),('2014/2-0000-2-4048-01','영국, 프랑스, 독일, 러시아, 스페인 등 유럽 문학의 주요작품들을 감상하면서 이들 각국의 문학이 지닌 상상력과 독창성의 근원이 무엇인지를 각국의 역사적 경험과 사회현실, 문화적 전통과 관련지어 고찰한다. \n유럽의 특정한 한 국가의 문학에 한정되지 않고 인간과 사회에 관한 보편적 가치를 지닌 것으로 평가되는 문학작품들, 예들 들어 세익스피어, 세르반테스, 위고, 괴테, 톨스토이 등을 고르게 다룬다. \n유럽의 주요 문학작품이 보여주는 사회상과 시대정신, 역사적 배경에 대해 토론한다.','1. 세계문학사에 많은 영향을 미친 유럽 문학의 주요 작가들과 작품들을 주체적으로 이해 분석하는 시각을 갖도록 한다. \n2. 문학작품에 나타난 유럽 사회의 특성을 파악한다.\n3. 이를 바탕으로 현재에 나타난 유럽사회의 정신과 문화를 이해하고 유럽에 대한 글로벌 마인드를 배양시킨다.','출석,10,수업태도,10,중간고사,20,기말고사,20,발표,10,토의,10,과제,20,Qu','사이렌의 침묵,김영룡,이담,2009','이은지','dldmswl3737@hanmail.net',61),('2014/2-0000-2-4050-01','이 강좌는 동아시아 각국의 문학을 통해 사회상을 파악하는 강좌이다. 전근대와 근대, 현대라는 역사시대별 기준을 두고 각 시대별 한·중·일 3국의 대표적 문학작품을 분석하여 작품이 담아내는 사회상을 파악한다. 동아시아 3국의 문학과 문학이 반영하는 사회상과 역사적 변화가 지닌 보편성과 특수성을 비교 고찰한다.','1. 동아시아의 문학의 주요 흐름을 고찰하면서 동아시아의 역사적 경험과 사회적 변화 양상을 고찰한다. \n2. 이를 통해 동아시아 지역 전반에 대한 이해를 심화시키고, 동아시아 지역 공동체 속에서의 한국의 정체성 파악에 기여한다.\n3. 문학속의 사회상들이 어떻게 내포되어 있는지를 파악하고 이를 바탕으로 각 작가들이 사회를 인식하는 눈과 방법을 나의 것으로 수용하도록 하는 능력을 배양한다.','출석,15,수업태도,5,중간고사,0,기말고사,30,발표,20,토의,20,과제,10,Quiz','교수직접준비','','',40),('2014/2-0000-2-4051-01','예술은 그 태생에서부터 예술가들만의 영역이라기보다는 인간의 생활과 밀접한 관계를 지닌 일상의 범주에 속하는 것이었다. 본 강좌는 예술 영역의 구체적 작품들을 통해 인간의 정신세계 및 인생의 기본적인 의문들을 통찰하는 수업이다. 영화, 연극, 문학, 미술, 음악 등의 예술 영역에서 선택된 작품들을 중심으로 인간의 삶, 죽음, 동기, 감정, 선택, 책임 등에 대해 토론한다. 학생들은 직접 예술 작품을 체험하고 이에 대해 토론하며 서로의 생각을 비교분석한다.','1. 학생들은 예술 일반을 자신들의 생활과 밀접하게 관련된 친밀한 영역으로 인식하게 된다.\n2. 역사에 길이 남는 예술 작품들을 숙지하고 이를 기반으로 더욱 성숙한 사고를 할 수 있다. \n3. 한 학기 동안 접한 인물들 및 작품들을 기준으로 설정하여 자신의 시공간적 좌표를 그릴 수 있다.\n4. 자신의 생활에 예술적 감성을 동화함으로써 보다 다채롭고 성숙한 방식으로 문화를 향유할 수 있다.','출석,20,수업태도,0,중간고사,25,기말고사,25,발표,10,토의,0,과제,20,Quiz','the collected papers chosen by the tutor','','',18),('2014/2-0000-2-4051-02','예술은 그 태생에서부터 예술가들만의 영역이라기보다는 인간의 생활과 밀접한 관계를 지닌 일상의 범주에 속하는 것이었다. 본 강좌는 예술 영역의 구체적 작품들을 통해 인간의 정신세계 및 인생의 기본적인 의문들을 통찰하는 수업이다. 영화, 연극, 문학, 미술, 음악 등의 예술 영역에서 선택된 작품들을 중심으로 인간의 삶, 죽음, 동기, 감정, 선택, 책임 등에 대해 토론한다. 학생들은 직접 예술 작품을 체험하고 이에 대해 토론하며 서로의 생각을 비교분석한다.','1. 학생들은 예술 일반을 자신들의 생활과 밀접하게 관련된 친밀한 영역으로 인식하게 된다.\n2. 역사에 길이 남는 예술 작품들을 숙지하고 이를 기반으로 더욱 성숙한 사고를 할 수 있다. \n3. 한 학기 동안 접한 인물들 및 작품들을 기준으로 설정하여 자신의 시공간적 좌표를 그릴 수 있다.\n4. 자신의 생활에 예술적 감성을 동화함으로써 보다 다채롭고 성숙한 방식으로 문화를 향유할 수 있다.','출석,20,수업태도,0,중간고사,25,기말고사,25,발표,10,토의,0,과제,20,Quiz','the collected papers chosen by the tutor','','',18),('2014/2-0000-2-4052-01','21세기 지구화와 시공간압축으로 전 세계는 유례없이 밀도 높은 문화적 교류와 소통을 경험하고 있다. 이 강좌는 지구촌 각 지역의 문화적 다양성을 지리적 지식에 기초하여 개관한다. 대륙·해양·반도·섬, 온대·열대·한대 등 지구상에 펼쳐진 자연환경과 사회현상의 다양한 사회문화적 조합과 역사적 형성과정을 학습한다. 문화지리학에 대한 기본적인 이해를 바탕으로 세계 각 지역의 역사와 문화에 대한 학생들의 조사·발표와 토론을 통해 문화의 다양성과 해석의 차이를 존중하고 공유하는 태도를 익힌다.','1. 세계 각지의 자연지리와 인문지리의 연관성을 파악한다.\n2. 세계 각 지역의 문화적 다양성과 차이를 이해함으로써 다른 문화를 이해하는 개방적이고 관용적인 문화상대주의적 태도를 익힌다.\n3. 지구촌 시대 세계로 진출하는 글로벌 마인드와 다양한 세계문화에 대한 이해의 토대 위에서 새로운 문화를 발굴하고 창안해내는 창조적 마인드를 고취시킨다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,10,토의,10,과제,0,Qui','세계의 풍속과 문화,오명석 외,한국방송통신대출판부,2005','','',45),('2014/2-0000-2-4052-02','21세기 지구화와 시공간압축으로 전 세계는 유례없이 밀도 높은 문화적 교류와 소통을 경험하고 있다. 이 강좌는 지구촌 각 지역의 문화적 다양성을 지리적 지식에 기초하여 개관한다. 대륙·해양·반도·섬, 온대·열대·한대 등 지구상에 펼쳐진 자연환경과 사회현상의 다양한 사회문화적 조합과 역사적 형성과정을 학습한다. 문화지리학에 대한 기본적인 이해를 바탕으로 세계 각 지역의 역사와 문화에 대한 학생들의 조사·발표와 토론을 통해 문화의 다양성과 해석의 차이를 존중하고 공유하는 태도를 익힌다.','1. 세계 각지의 자연지리와 인문지리의 연관성을 파악한다.\n2. 세계 각 지역의 문화적 다양성과 차이를 이해함으로써 다른 문화를 이해하는 개방적이고 관용적인 문화상대주의적 태도를 익힌다.\n3. 지구촌 시대 세계로 진출하는 글로벌 마인드와 다양한 세계문화에 대한 이해의 토대 위에서 새로운 문화를 발굴하고 창안해내는 창조적 마인드를 고취시킨다.','출석,10,수업태도,10,중간고사,20,기말고사,30,발표,15,토의,15,과제,0,Qui','대국굴기,왕지아평,Credu,2007','','',47),('2014/2-0000-2-4052-03','21세기 지구화와 시공간압축으로 전 세계는 유례없이 밀도 높은 문화적 교류와 소통을 경험하고 있다. 이 강좌는 지구촌 각 지역의 문화적 다양성을 지리적 지식에 기초하여 개관한다. 대륙·해양·반도·섬, 온대·열대·한대 등 지구상에 펼쳐진 자연환경과 사회현상의 다양한 사회문화적 조합과 역사적 형성과정을 학습한다. 문화지리학에 대한 기본적인 이해를 바탕으로 세계 각 지역의 역사와 문화에 대한 학생들의 조사·발표와 토론을 통해 문화의 다양성과 해석의 차이를 존중하고 공유하는 태도를 익힌다.','1. 세계 각지의 자연지리와 인문지리의 연관성을 파악한다.\n2. 세계 각 지역의 문화적 다양성과 차이를 이해함으로써 다른 문화를 이해하는 개방적이고 관용적인 문화상대주의적 태도를 익힌다.\n3. 지구촌 시대 세계로 진출하는 글로벌 마인드와 다양한 세계문화에 대한 이해의 토대 위에서 새로운 문화를 발굴하고 창안해내는 창조적 마인드를 고취시킨다.','출석,10,수업태도,10,중간고사,20,기말고사,30,발표,0,토의,10,과제,20,Qui','진화하는 세계화 - 현대 세계의 문화적 다양성,피터 버거 외,아이필드,2005','','',30),('2014/2-0000-2-4054-01','비판적 사고란 다른 사람의 주장이나 의견에 대해 맹목적으로 수용하는것이 아니라 그 주장의 근거를 묻고, 그 근거의 적절성, 나아가 특정 근거에 기반하여 도출된 주장의 논리성을 따져보는 사고의 과정이다. 또한 이것은 창조적 대안이나 보다 더 나은 문제 해결을 추구하는 창의적 사고이므로 구체적인 상황에서 문제가 무엇인지를 파악하는 능력이며, 그 문제에 대한 결론과 근거는 무엇인지를 물을 줄 아는 사고를 말한다. \n 따라서 본 강좌에서는 이러한 비판적 사고의 기본 특성을 이해한 뒤에 우리가 처해있는 정치 문화 사회 역사 경제 등등의 분야에 걸쳐 구체적인 여러 문제들을 가지고 실제로 그 문제에 대해서 어떻게 사고하는 것이 비판적으로 사고하는 방법들을 발표와 토론, 강의를 통해 훈련하게 될 것이다.','1. 비판적으로 사고하기 위한 논리적 개념들을 학습하고 이를 바탕으로구체적인 문제를 논리적으로 문제화할 수 있는 능력의 배양. \n 2. 나아가 문제화된 논의들을 타인들과 적절하게 소통하고 조율하기 위해 필요한 발표와 토론의 기본 태도를 습득하고 이를 실생활에 활용하도록 함.','출석,10,수업태도,0,중간고사,25,기말고사,25,발표,20,토의,0,과제,20,Quiz','논리적 사고,퍼틸,서광사,1994','','',35),('2014/2-0000-2-4054-02','비판적 사고란 다른 사람의 주장이나 의견에 대해 맹목적으로 수용하는것이 아니라 그 주장의 근거를 묻고, 그 근거의 적절성, 나아가 특정 근거에 기반하여 도출된 주장의 논리성을 따져보는 사고의 과정이다. 또한 이것은 창조적 대안이나 보다 더 나은 문제 해결을 추구하는 창의적 사고이므로 구체적인 상황에서 문제가 무엇인지를 파악하는 능력이며, 그 문제에 대한 결론과 근거는 무엇인지를 물을 줄 아는 사고를 말한다. \n 따라서 본 강좌에서는 이러한 비판적 사고의 기본 특성을 이해한 뒤에 우리가 처해있는 정치 문화 사회 역사 경제 등등의 분야에 걸쳐 구체적인 여러 문제들을 가지고 실제로 그 문제에 대해서 어떻게 사고하는 것이 비판적으로 사고하는 방법들을 발표와 토론, 강의를 통해 훈련하게 될 것이다.','1. 비판적으로 사고하기 위한 논리적 개념들을 학습하고 이를 바탕으로구체적인 문제를 논리적으로 문제화할 수 있는 능력의 배양. \n 2. 나아가 문제화된 논의들을 타인들과 적절하게 소통하고 조율하기 위해 필요한 발표와 토론의 기본 태도를 습득하고 이를 실생활에 활용하도록 함.','출석,10,수업태도,4,중간고사,20,기말고사,30,발표,10,토의,0,과제,0,Quiz,','발표와 토론,숙대출판부','','',43),('2014/2-0000-2-4054-03','비판적 사고란 다른 사람의 주장이나 의견에 대해 맹목적으로 수용하는것이 아니라 그 주장의 근거를 묻고, 그 근거의 적절성, 나아가 특정 근거에 기반하여 도출된 주장의 논리성을 따져보는 사고의 과정이다. 또한 이것은 창조적 대안이나 보다 더 나은 문제 해결을 추구하는 창의적 사고이므로 구체적인 상황에서 문제가 무엇인지를 파악하는 능력이며, 그 문제에 대한 결론과 근거는 무엇인지를 물을 줄 아는 사고를 말한다. \n 따라서 본 강좌에서는 이러한 비판적 사고의 기본 특성을 이해한 뒤에 우리가 처해있는 정치 문화 사회 역사 경제 등등의 분야에 걸쳐 구체적인 여러 문제들을 가지고 실제로 그 문제에 대해서 어떻게 사고하는 것이 비판적으로 사고하는 방법들을 발표와 토론, 강의를 통해 훈련하게 될 것이다.','1. 비판적으로 사고하기 위한 논리적 개념들을 학습하고 이를 바탕으로구체적인 문제를 논리적으로 문제화할 수 있는 능력의 배양. \n 2. 나아가 문제화된 논의들을 타인들과 적절하게 소통하고 조율하기 위해 필요한 발표와 토론의 기본 태도를 습득하고 이를 실생활에 활용하도록 함.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Qui','비판적 사고를 위한 논리,김희정, 박은진,아카넷,2008','','',47),('2014/2-0000-2-4054-04','비판적 사고란 다른 사람의 주장이나 의견에 대해 맹목적으로 수용하는것이 아니라 그 주장의 근거를 묻고, 그 근거의 적절성, 나아가 특정 근거에 기반하여 도출된 주장의 논리성을 따져보는 사고의 과정이다. 또한 이것은 창조적 대안이나 보다 더 나은 문제 해결을 추구하는 창의적 사고이므로 구체적인 상황에서 문제가 무엇인지를 파악하는 능력이며, 그 문제에 대한 결론과 근거는 무엇인지를 물을 줄 아는 사고를 말한다. \n 따라서 본 강좌에서는 이러한 비판적 사고의 기본 특성을 이해한 뒤에 우리가 처해있는 정치 문화 사회 역사 경제 등등의 분야에 걸쳐 구체적인 여러 문제들을 가지고 실제로 그 문제에 대해서 어떻게 사고하는 것이 비판적으로 사고하는 방법들을 발표와 토론, 강의를 통해 훈련하게 될 것이다.','1. 비판적으로 사고하기 위한 논리적 개념들을 학습하고 이를 바탕으로구체적인 문제를 논리적으로 문제화할 수 있는 능력의 배양. \n 2. 나아가 문제화된 논의들을 타인들과 적절하게 소통하고 조율하기 위해 필요한 발표와 토론의 기본 태도를 습득하고 이를 실생활에 활용하도록 함.','출석,10,수업태도,10,중간고사,25,기말고사,35,발표,10,토의,0,과제,10,Qui','비판적 사고 실용적입문 3판(반드시 3판을 구입),앤 톰슨, 최원배 옮김,서광사,2012','','',33),('2014/2-0000-2-4067-01','영작문 교과목 시리즈 중 가장 기초 과정으로서 영문법에 기반을 두어 구, 절, 단문, 복문, 장문 등등 다양한 종류의 영어구조를 작문하는 연습을 한다. 궁극적으로 문단 수준 이전 짧은 글을 작문할 수 있는 능력을 키운다.','에세이나 실용문 등을 세련된 영어 문장으로 표현하는데 있어서 기본이 되는 문법, 어휘, 구문 전개 방식 등을 반복 연습하여 자연스럽게 짧은 에세이 쓰기로 옮겨가도록 한다. 다시 말하면 어휘를 배열하여 단문을 만들고, 짧은 문장을 이어서 단락을 구성해 보고 나아가 단락과 단락을 연결하여 에세이를 작성하는데 필요한 과정을 익히게 한다.','출석,10,중간고사,35,기말고사,35,과제보고서,0,수업태도,0,Quiz,20,기타,0','HANDOUT','','',19),('2014/2-0000-2-4067-02','영작문 교과목 시리즈 중 가장 기초 과정으로서 영문법에 기반을 두어 구, 문장, 단락으로 발전하도록 다양한 종류의 주제를 통하여 작문을 할 수 있는 능력을 갖게 한다.','어휘를 배열하여 단문을 만들고, 짧은 문장을 이어서 단락을 구성해 보고 나아가 단락과 단락을 연결하여 에세이를 작성하는데 필요한 과정을 습득하게 한다.','출석,10,중간고사,30,기말고사,35,과제보고서,15,수업태도,0,Quiz,10,기타,0','Writers at Work,Laurie Blass,Cambridge,2010','','',20),('2014/2-0000-2-4263-01','21세기 정보화시대에 접어들어 세계경제권이 단일화 되고 국가 간 치열한 경제전쟁을 치루면서 최근 Global Standard(세계적 표준)가 세계를 지배하는 도구로 활용되고 있다. 즉,표준을 지배한 나라가 세계를 지배하고, 세계를 지배하는 나라가 표준을 지배 한다 라는 말이 현실화되고 있어 해당 분야의 표준전문가 양성과 표준마인드의 확산이 시급하다고 할수 있다.','본 강좌에서는 표준화의 중요성과 표준화의 목적, 최근의 표준화 동향 등에 대하여 표준전문가 Pool를 통하여 Team Teaching 방식으로 강의하도록 하며 아울러 우리나라의 산업표준화, 국제표준화에 대해서도 관련 전문가를 통하여 구체적으로 다루도록 하여 표준화마인드를 갖춘 고급인력의 양성을 목적으로 한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Qui','미래 사회와 표준 ( 무료 제공),한국표준협회,한국표준협회,2006','허민','nicemin@kw.ac.kr',36),('2014/2-0000-2-4263-02','21세기 정보화시대에 접어들어 세계경제권이 단일화 되고 국가 간 치열한 경제전쟁을 치루면서 최근 Global Standard(세계적 표준)가 세계를 지배하는 도구로 활용되고 있다. 즉,표준을 지배한 나라가 세계를 지배하고, 세계를 지배하는 나라가 표준을 지배 한다 라는 말이 현실화되고 있어 해당 분야의 표준전문가 양성과 표준마인드의 확산이 시급하다고 할수 있다.','본 강좌에서는 표준화의 중요성과 표준화의 목적, 최근의 표준화 동향 등에 대하여 표준전문가 Pool를 통하여 Team Teaching 방식으로 강의하도록 하며 아울러 우리나라의 산업표준화, 국제표준화에 대해서도 관련 전문가를 통하여 구체적으로 다루도록 하여 표준화마인드를 갖춘 고급인력의 양성을 목적으로 한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Qui','미래 사회와 표준 ( 무료 제공),한국표준협회,한국표준협회,2006','허민','nicemin@kw.ac.kr',49),('2014/2-0000-2-4909-01','영어의 리듬과 음에 적응시켜 청취력을 기르고 문형의 습득 및 구어적 표현을 익힌다. 한국인 영어 학습자들이 흔히 놓치기 쉬운 영어 발음들을 여러 가지 유형별로 묶어 집중적으로 연습하며 받아쓰기 훈련을 한다. This course helps students improve their listening ability by helping them repeatedly practice some of the English pronunciations that most Koreans find difficult to pronounce. “Students will study colloquial expressions through various types of English sentence structures','본 강의는 실제 라디오 방송에서 다루어진 다양한 내용을 접하게 함으로써 학생들의 듣기능력과 함께 비판적 사고능력을 향상시키는 데에 그 목적이 있다. 주제와 특정내용 파악하기, 추론하기, 그리고 발음, 억양, 어휘, 문법 등을 다룸으로써 학생들의 듣기와 쓰기 능력향상에 실질적인 도움을 준다. 또한 그룹간의 토의를 통하여 다양한 의견을 접하고 자신의 논점을 표현하는 방법을 익힌다','출석,10,중간고사,30,기말고사,30,과제보고서,15,수업태도,5,Quiz,10,기타,0','Face the Issues,Carol Numrich,Longman,2007','','',29),('2014/2-0000-2-4909-02','Designed for intermediate students of English as a second language, this course presents an integrated approach to developing listening comprehension and writing skills.','The primary goal of this course is to help students build listening comprehension & writing skills.','출석,10,중간고사,30,기말고사,30,과제보고서,10,수업태도,0,Quiz,20,기타,0','Face the Issues: intermediate Listening and Critical Thinking skills,Carol Numrich,Pearson Longman,2','','',35),('2014/2-0000-2-4909-03','영어의 리듬과 음에 적응시켜 청취력을 기르고 문형의 습득 및 구어적 표현을 익힌다. 한국인 영어 학습자들이 흔히 놓치기 쉬운 영어 발음들을 여러 가지 유형별로 묶어 집중적으로 연습하며 받아쓰기 훈련을 한다. This course helps students improve their listening ability by helping them repeatedly practice some of the English pronunciations that most Koreans find difficult to pronounce. “Students will study colloquial expressions through various types of English sentence structures','본 강의는 실제 라디오 방송에서 다루어진 다양한 내용을 접하게 함으로써 학생들의 듣기능력과 함께 비판적 사고능력을 향상시키는 데에 그 목적이 있다. 주제와 특정내용 파악하기, 추론하기, 그리고 발음, 억양, 어휘, 문법 등을 다룸으로써 학생들의 듣기와 쓰기 능력향상에 실질적인 도움을 준다. 또한 그룹간의 토의를 통하여 다양한 의견을 접하고 자신의 논점을 표현하는 방법을 익힌다','출석,10,중간고사,30,기말고사,30,과제보고서,15,수업태도,5,Quiz,10,기타,0','Face the Issues,Carol Numrich,Longman,2007','','',26),('2014/2-0000-2-4910-01','사회, 문화, 과학, 기술, 환경, 종교, 예술 등에 관한 여러가지 글을 강독하면서 어휘력과 독해력을 기르고, 아울러 내용을 요약하여 영어로 프리젠테이션함으로서 영어읽기와 말하기를 연습하는 강의이다.','영어 원문을 독해하면서 영어 구문과 어휘에 관한 지식을 늘리고 아울러 내용을 요약 발표하면서 영어 말하기 능력을 향상시키는데 도움을 주려는 강좌이다.','출석,10,중간고사,30,기말고사,30,과제보고서,30,수업태도,0,Quiz,0,기타,0','Q: Skills for Success, Reading and Writing 4 /HANDOUT(reading materials),Daise, D., C. Norloff, and ','','',20),('2014/2-0000-2-4910-02','다양한 주제의 영문들을 읽으면서 글의 main idea와 detail들을 파악하는 능력을 기르고, 글의 주제와 관련한 토의 및 발표를 통해 논리적 사고와 영어 말하기 능력도 함께 배양한다.','논리적이고 체계적인 글을 통해 영어 독해 능력을 향상시키고, 더불어 관련 주제에 대한 토의와 발표로 말하기 능력을 함께 향상시키는 것이 이 강좌의 목적이다.','출석,10,중간고사,30,기말고사,30,과제보고서,20,수업태도,10,Quiz,0,기타,0','hand-out(reading materials)','','',23),('2014/2-0000-2-4910-03','다양한 분야의 영문들을 읽으면서 글의 요지 파악 능력을 기른다. 학생들의 수준에 맞게, 정확한 독해와 말하기 능력을 기르는 데 필요한 정도의 어휘, 문법, 문장구조 관련 지식을 학습한다.','다양한 분야의 영문들을 많이 접하며, 그 개요 및 내용파악을 정확히 할 수 있도록 반복 학습하는 과정을 통해 영어 독해 능력과 말하기 능력을 함께 향상시키도록 하는 것이 이 강좌의 목적이다.','출석,10,중간고사,30,기말고사,30,과제보고서,10,수업태도,10,Quiz,5,기타,5','Cover to cover 3,Richard R. Day,OXFORD,2008','','',17),('2014/2-0000-2-4925-01','현대사회에서 스포츠가 어떠한 형태로 행해지고 있으며, 그 의미와 역할에 대해서 이해한다. 또한 스포츠와 사회, 문화, 경제, 정치 등의 영역들이 어떻게 상호작용을 하고 있으며, 이로 인해 발생되는 현상들을 이해한다.','현대사회의 스포츠 현상에 대한 다양한 해석과 스포츠와 관련된 사회적 문제에 대한 비판의식을 함양시키고 진정한 스포츠의 가치를 통한 현재 우리 삶과의 연관성을 알아본다.','출석,20,중간고사,30,기말고사,30,과제보고서,15,수업태도,0,Quiz,5,기타,0','스포츠사회학 플러스(제2전정판),원영신,대경북스,2012','이외솔','thefrontofarmament@nate.com',75),('2014/2-0000-2-4925-02','스포츠란 무엇인가, 스포츠 사회학이란 무엇인가 등을시작으로 스포츠와 도덕성,스포츠와 지역공동체, 지역스포츠 활동과 지역공동체, 스포츠와 경제, 스포츠와 청소년 비행에 이르기까지 현대사회와 밀접한 관련이 있는 스포츠를 본질적 측면과 함께 고찰해본다.','','출석,20,중간고사,30,기말고사,30,과제보고서,10,수업태도,10,Quiz,0,기타,0','현대사회와 스포츠,김범식 외,홍경','','',73),('2014/2-0000-2-5073-01','본 강좌는 영미권의 드라마를 보고 청취하며 영어 듣기 실력과 현대의 구어적 영어표현에 대한 이해와 활용을 높이고 변화하는 현대문화에 대한 이해를 높이려는 취지로 개설되었다.','영어로 된 드라마를 통해 일상적인 구어 환경의 영어 표현을 익히고, 이런 표현들을 듣고 이해하는 훈련을 통해 영어 듣기능력을 함양하고자하며, 나아가 일상인 미국의 가정, 친구, 직장, 젠더, 성 역할, 소비, 패션, 도시 문화에 접함으로써 미국 문화에 대한 이해를 높이고자 한다.','출석,10,중간고사,30,기말고사,30,과제보고서,10,수업태도,5,Quiz,10,기타,5','Hand-out','','',39),('2014/2-0000-2-5073-02','본 강좌는 영미권의 드라마를 보고 청취하며 영어 듣기 실력과 현대의 구어적 영어표현에 대한 이해와 활용을 높이고 변화하는 현대문화에 대한 이해를 높이려는 취지로 개설되었다.','영어로 된 드라마를 통해 일상적인 구어 환경의 영어 표현을 익히고, 이런 표현들을 듣고 이해하는 훈련을 통해 영어 듣기능력을 함양하고자하며, 나아가 일상인 미국의 가정, 친구, 직장, 젠더, 성 역할, 소비, 패션, 도시 문화에 접함으로써 미국 문화에 대한 이해를 높이고자 한다.','출석,10,중간고사,30,기말고사,30,과제보고서,10,수업태도,5,Quiz,10,기타,5','Hand-out','','',35),('2014/2-0000-2-5090-01','경기 침체와 고용불안정 속에서 \n새로운 해법으로 등장하고 있는 창업시장에 대한 \n학문적 이해와 실무적 접근을 통해 \n실질적인 창업과 경영 수단을 이해하고자 함.','창업의 기본적인 이해와 현장의 이해를 통한 경영의 접목을 시도함','출석,10,중간고사,35,기말고사,35,과제보고서,20,수업태도,0,Quiz,0,기타,0','창업경영론,박주영,이상호,인플로우,2011','','',27),('2014/2-0000-2-5090-02','창업에 대한 사업아이템 도출, 전략, 마케팅, 세무에 대한 기초적인 사항을 이해하고,실무적 접근을 통해 실질적인 창업과 경영을 이해함.','창업을 준비하거나 향후 창업을 고려하는 재학생이 필요한 내용을 이해하고, 스스로 사업을 기획하고 준비하여 모의 창업을 경험함.','출석,10,중간고사,0,기말고사,30,과제보고서,20,수업태도,0,Quiz,0,기타,40','창업경영론,박주영,이상호,인플로우,2011','','',8),('2014/2-0000-2-5092-01','서비스이론은 고객만족의 가장 근간이 되는 기본 과목으로 고객 만족 서비스의 이론과 실제에 대한 총체적 소양과 안목의 형성 및 전문성 함양을 그 목적으로 한다. \n이를 위해 고객만족의 필요성과 중요성, 비즈니스 매너, 커뮤니케이션 스킬 등을 익히는 것을 목적으로 한다.','고객만족의 중요성과 가치를 인식하고 업무현장에서 항상 고객 우선의 정신으로 실천할 수 있는 비즈니스 매너 및 커뮤니케이션 응대 스킬을 익혀서, 향후 직장생활에 활용할 수 있도록 한다.','출석,20,중간고사,20,기말고사,20,과제보고서,20,수업태도,10,Quiz,0,기타,1','Course Packet','','',26),('2014/2-0000-2-5219-01','제2외국어인증제와 취업에 유효한 JPT시험 점수를 끌어올리기 위해, 이에 필수적인 어휘와 문법 및 독해와 청해 능력을 실제 JPT시험과 유사한 문제를 충분히 풀고 검토하는 과정을 통해 단기간에 최대한으로 습득해가는 수업이다. 학생들은 과제로 매 수업에서 다루게 될 문법 사항과 어휘를 예습하고, 수업에서 JPT 유사 문제를 풀면서 예습한 내용을 확인하며, 교수로부터 어휘와 표현, 문법에 관련한 보충설명을 들음으로써 종합적인 일본어능력을 배양하고 이를 최대한으로 증빙할 수 있도록 연습한다.','1. 제2외국어 인증제를 통과할 수 있도록 JPT 점수를 끌어올린다.\n2. 다양한 예문과 관련 어구를 제시하며 일본어 어휘와 문법을 설명함으로써, 정확하고도 풍부한 일본어 표현력과 함께 탄탄하고 체계적인 이론적 토대를 갖추게 한다.\n3. 어휘와 문법, 독해, 청해 파트의 JPT유사 문제 풀이 연습을 통해 종합적인 일본어 능력을 함양하고 이를 최대한으로 증빙할 수 있도록 준비한다.','출석,10,수업태도,10,중간고사,40,기말고사,40,발표,0,토의,0,과제,0,Quiz,','450점에 딱 맞춘 JPT,이장우,사람in,2011','','',18),('2014/2-0000-2-5367-01','주로 중국드라마와 재미있는동영상을 위주로 수업이 진행합니다.\n\n*******이 과목을 신청하고 싶은 학생은 반드시 중국어를 2년이상 배운학생입니다. \n 수업이 주로 중국어로 진행됩니다.','이수업이 통해서 중국의 여러가지 방면을 알게되고, 현재 중국에 있는 인기드라마, 문화, 생활습관등등 잘 이해하기 위해 \n다','출석,10,중간고사,30,기말고사,30,과제보고서,10,수업태도,10,Quiz,10,기타,','프린트물','','',19),('2014/2-0000-2-5368-01','미국의 현재를 이해하는 가장 좋은 방법은 현재를 낳은 과거를 통찰하는 것이다. 이 과목은 유럽인의 아메리카 대륙 도착, 미국혁명, 서부개척, 남북전쟁, 산업화, 제국주의, 혁신주의, 경제대공황과 뉴딜정책, 1960년대 진보주의, 1980년대 보수주의, 신자유주의 등 미국사의 주요 이슈들을 다룬다. 그 내용에 있어서 미국사회 변천과정의 맥락과 배경, 그리고 그 결과와 대외정책의 변화 등을 포함한다.','1. 미국의 성장과정과 사회변화, 문화적 영향력, 국제관계 속에서의 미국의 역할 등을 역사적 맥락에서 이해한다. \n2. 미국의 성장과정에서 표출된 내적 갈등, 사회문제, 인종갈등, 대외정책 등을 파악한다.\n3. 미국이 18세기 이후 초강대국이 된 배경과 이유를 심도있게 통찰하여 이를 우리의 역사 만들기에 응용할 수 있는 능력을 배양한다.\n4. 미국의 대내외적인 정책과 지구촌에 대한 mind를 분석하여 글로벌시대에 지구촌으로의 올바른 진출 mind를 형성한다.','출석,10,수업태도,0,중간고사,30,기말고사,30,발표,0,토의,10,과제,20,Quiz','프린트물','','',48),('2014/2-0000-2-5662-01','1. 동북아근현대사의 전개과정과 특질을 검토함으로써 현대동북아 주요국가의 사회경제적 구조를 이해한다. 한·중·일 동북아 3국의 근대세계체제에 편입/수용/저항하는 과정을 비교 검토함으로써 동북아와 세계간의 상호관계를 비교사적으로 이해한다. \n2. 19세기 서구의 영향, 각국의 개항, 일본의 메이지 유신, 한국의 자체적 근대화 노력, 청일전쟁, 신해혁명, 일본의 제국주의적 팽창, 중일전쟁, 중국혁명, 한국의 분단과 주변국의 역할 등 주요 역사적 흐름을 비판적으로 접근한다.\n3. 전통/반란/개혁/혁명 등 상이한 역사전개과정을 거친 동북아 국민들의 삶의 양태와 질을 비교함으로써 신동북아 실서가 형성되고 있는 21세기 동북아가 나아가야 할 미래의 역사적 방향성을 토론한다.','1. 한·중·일 동북아 3국의 근현대사에 대한 비교사적 이해를 통해 향후 동북아의 역사전개 과정을 예측한다. \n2. 동북아 3국의 역사경험이 지닌 보편성과 특수성을 파악하여 동북아 지역공동체의 가능성을 모색한다.','출석,10,수업태도,0,중간고사,25,기말고사,25,발표,20,토의,10,과제,10,Qui','함께읽는 동아시아 근현대사,박진우,창비','박준형','alchemist56@naver.com',42),('2014/2-0000-2-5663-01','한국사 속의 특정한 역사적 국면의 변화를 좌우한 주요 인물들의 역사적 판단과 실천의 의미를 중심으로 역사적 사건을 재해석함으로써 역사에 대한 주체적 인식을 도모한다. \n한국사의 주요 국면들에서 주요 인물들, 예를 들어 고려 말과 조선 초의 정몽주와 정도전, 병자호란 당시의 주전파와 주화파, 구한말의 다양한 개화사상가들, 독립투쟁기의 김구와 이승만이 보여준 역사적 결단과 전망, 사상들을 재검토한다. \n수강생 개인별 혹은 조별로 역사적 인물에 대한 다양한 평가들을 조사 발표 토론하면서 역사적 인물들에 대한 주체적 재평가를 시도한다.','1. 한국사의 주요 인물들과 그들을 둘러싼 역사적 조건과 사회적 환경에 대하여 이해함으로써 역사를 종합적이고 분석적으로 인식한다. \n2. 역사를 주체의 역사적 실천의 산물로서 이해함으로써 일상적 행위에 대한 책임감과 역사의식을 고양한다.\n3. 한국 역사속의 인물들을 재조명함으로써 그들의 삶을 나의 것으로 재구성하여 종합적인 판단능력을 갖춘 실천적 인간으로서의 자아를 형성한다.','출석,10,수업태도,0,중간고사,20,기말고사,20,발표,25,토의,20,과제,5,Quiz','한국사의 길잡이,박경안, 김무진,혜안,1995','','',48),('2014/2-0000-2-5663-02','한국사 속의 특정한 역사적 국면의 변화를 좌우한 주요 인물들의 역사적 판단과 실천의 의미를 중심으로 역사적 사건을 재해석함으로써 역사에 대한 주체적 인식을 도모한다. \n한국사의 주요 국면들에서 주요 인물들, 예를 들어 고려 말과 조선 초의 정몽주와 정도전, 병자호란 당시의 주전파와 주화파, 구한말의 다양한 개화사상가들, 독립투쟁기의 김구와 이승만이 보여준 역사적 결단과 전망, 사상들을 재검토한다. \n수강생 개인별 혹은 조별로 역사적 인물에 대한 다양한 평가들을 조사 발표 토론하면서 역사적 인물들에 대한 주체적 재평가를 시도한다.','1. 한국사의 주요 인물들과 그들을 둘러싼 역사적 조건과 사회적 환경에 대하여 이해함으로써 역사를 종합적이고 분석적으로 인식한다. \n2. 역사를 주체의 역사적 실천의 산물로서 이해함으로써 일상적 행위에 대한 책임감과 역사의식을 고양한다.\n3. 한국 역사속의 인물들을 재조명함으로써 그들의 삶을 나의 것으로 재구성하여 종합적인 판단능력을 갖춘 실천적 인간으로서의 자아를 형성한다.','출석,10,수업태도,0,중간고사,20,기말고사,20,발표,25,토의,20,과제,5,Quiz','한국사의 길잡이,박경안, 김무진,혜안,1995','','',42),('2014/2-0000-2-5668-01','블록화되고 있는 21세기 세계화의 흐름 속에서 세계 유일 초강대국인 미국과 한반도를 비롯한 동북아의 정치·경제·사회적 관계의 구조와 변화 양상을 전반적으로 이해한다. 주요 내용으로는 미국의 세계 및 동아시아정책 기조를 살펴보고, 미국과 동북아 주요 국가간의 양자관계 현황과 전망, 그리고 동북아의 평화와 번영에 대한 미국의 입장과 정책의 변화 등을 다룬다.','1. 미국의 동북아 전략과 동북아의 지정학적 특수성을 파악함으로써 향후 동북아 국제질서 변동에 대한 예측력을 기른다.\n2. 미국 중심 세계질서가 향후 어떻게 변화할 것인지 전망해봄으로써 21세기 글로벌 사회에서의 한국의 대응 방안을 고민해본다. \n3. 21세기 동북아 국제질서의 변화 방향에 대한 통찰력을 기르고 한반도의 분단문제와 한중일 3국 관계의 바람직한 변화 방향을 모색해본다.','출석,30,수업태도,20,중간고사,20,기말고사,30,발표,0,토의,0,과제,0,Quiz,','한국사의 길잡이,박경안, 김무진,혜안,1995','김태윤','mgcs2002@naver.com',91),('2014/2-0000-2-5668-02','블록화되고 있는 21세기 세계화의 흐름 속에서 세계 유일 초강대국인 미국과 한반도를 비롯한 동북아의 정치·경제·사회적 관계의 구조와 변화 양상을 전반적으로 이해한다. 주요 내용으로는 미국의 세계 및 동아시아정책 기조를 살펴보고, 미국과 동북아 주요 국가간의 양자관계 현황과 전망, 그리고 동북아의 평화와 번영에 대한 미국의 입장과 정책의 변화 등을 다룬다.','1. 미국의 동북아 전략과 동북아의 지정학적 특수성을 파악함으로써 향후 동북아 국제질서 변동에 대한 예측력을 기른다.\n2. 미국 중심 세계질서가 향후 어떻게 변화할 것인지 전망해봄으로써 21세기 글로벌 사회에서의 한국의 대응 방안을 고민해본다. \n3. 21세기 동북아 국제질서의 변화 방향에 대한 통찰력을 기르고 한반도의 분단문제와 한중일 3국 관계의 바람직한 변화 방향을 모색해본다.','출석,10,수업태도,10,중간고사,40,기말고사,40,발표,0,토의,0,과제,0,Quiz,','한국사의 길잡이,박경안, 김무진,혜안,1995','','',63),('2014/2-0000-2-5671-01','북한사회의 역사와 현실, 제도와 문화에 대한 다각적 이해를 모색한다. 구체적으로는 북한의 선군정치, 계획경제 체제의 실상과 개혁·개방 가능성, 통제사회의 이완현상 및 인권실태 등 북한 내부 현황을 살펴보고, 남북교류협력의 현주소와 통합 가능성을 분석·전망한다.','1. 남북한의 분단현실과 이질성을 극복하기 위한 구체적 방안을 모색해본다.\n2. 통일의 필요성과 의의가 무엇인지 인식한다. \n3. 통일을 위해 우리 세대는 무엇을 할 수 있는지 고민하고 남북한을 통합한 한반도의 미래상을 준비한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,10,과제,10,Qui','북한이해,통일부 통일교육원,통일부 통일교육원,2014','','',49),('2014/2-0000-2-5675-01','현대사회에서 모든 분야에서 폭넓게 이용되고 있는 디지털영상의 기초 원리와 의미를 이해하고, 영상편집도구를 이용하여 다양하고 효과적인 표현방법을 익힘으로써 실제 응용분야를 위한 아이디어 구현과 활용방법을 습득한다. 본 강의를 통해서 대표적으로 다루어지는 디지털콘텐츠는 2차원 자연영상과 비디오이고, 이들을 편집하기 위한 도구로는 포토샵과 프리미어 등을 이용한다. 디지털영상편집 기법을 익힘으로써 광고, 일러스트, 웹, 미술, 및 UCC를 비롯한 다양한 콘텐츠를 직접 제작하도록 한다.','1. 포토샵을 이용하여 사진 이미지의 수정, 합성, 색상 보정 기법을 비롯하여 광고, 포스터, 엽서 등의 디자인 기법에 대해서 학습한다.\n2. 이미지 또는 디지털 카메라로 찍은 사진의 합성과 보정 및 편집 방법에 대해서 학습하고, 리터치 기법과 레이어의 활용을 통해서 입체 영상을 제작하는 기법에 대해서 익힌다. \n3. 순수미술 및 디자인 작업, 일러스트 이미지 작업을 비롯하여 홈페이지 배너와 다양한 버튼 이미지 제작 기법을 익혀서 웹페이지 작성 방법을 학습한다. \n4. 프리미어를 이용하여 비디오를 프레임별로 조작하고, 편집하는 기법에 대해서 학습하고, 타이틀과 자막을 처리하는 방법에 대해서 배운다.\n5. 비디오에 다양한 효과를 부여하는 방법을 배우고, 코덱에 대한 지식을 얻어서 다양한 재생 환경에 따른 비디오 제작 방법을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','디지털 영상 편집,서영호','장수진','boradori125@naver.com',41),('2014/2-0000-2-5675-02','현대사회에서 모든 분야에서 폭넓게 이용되고 있는 디지털영상의 기초 원리와 의미를 이해하고, 영상편집도구를 이용하여 다양하고 효과적인 표현방법을 익힘으로써 실제 응용분야를 위한 아이디어 구현과 활용방법을 습득한다. 본 강의를 통해서 대표적으로 다루어지는 디지털콘텐츠는 2차원 자연영상과 비디오이고, 이들을 편집하기 위한 도구로는 포토샵과 프리미어 등을 이용한다. 디지털영상편집 기법을 익힘으로써 광고, 일러스트, 웹, 미술, 및 UCC를 비롯한 다양한 콘텐츠를 직접 제작하도록 한다.','1. 포토샵을 이용하여 사진 이미지의 수정, 합성, 색상 보정 기법을 비롯하여 광고, 포스터, 엽서 등의 디자인 기법에 대해서 학습한다.\n2. 이미지 또는 디지털 카메라로 찍은 사진의 합성과 보정 및 편집 방법에 대해서 학습하고, 리터치 기법과 레이어의 활용을 통해서 입체 영상을 제작하는 기법에 대해서 익힌다. \n3. 순수미술 및 디자인 작업, 일러스트 이미지 작업을 비롯하여 홈페이지 배너와 다양한 버튼 이미지 제작 기법을 익혀서 웹페이지 작성 방법을 학습한다. \n4. 프리미어를 이용하여 비디오를 프레임별로 조작하고, 편집하는 기법에 대해서 학습하고, 타이틀과 자막을 처리하는 방법에 대해서 배운다.\n5. 비디오에 다양한 효과를 부여하는 방법을 배우고, 코덱에 대한 지식을 얻어서 다양한 재생 환경에 따른 비디오 제작 방법을 습득한다.','출석,20,수업태도,10,중간고사,25,기말고사,25,발표,0,토의,0,과제,20,Quiz','포토샵 디자인 스타일북,김 혜경,한빛미디어,2013','','',16),('2014/2-0000-2-5675-03','현대사회에서 모든 분야에서 폭넓게 이용되고 있는 디지털영상의 기초 원리와 의미를 이해하고, 영상편집도구를 이용하여 다양하고 효과적인 표현방법을 익힘으로써 실제 응용분야를 위한 아이디어 구현과 활용방법을 습득한다. 본 강의를 통해서 대표적으로 다루어지는 디지털콘텐츠는 2차원 자연영상과 비디오이고, 이들을 편집하기 위한 도구로는 포토샵과 프리미어 등을 이용한다. 디지털영상편집 기법을 익힘으로써 광고, 일러스트, 웹, 미술, 및 UCC를 비롯한 다양한 콘텐츠를 직접 제작하도록 한다.','1. 포토샵을 이용하여 사진 이미지의 수정, 합성, 색상 보정 기법을 비롯하여 광고, 포스터, 엽서 등의 디자인 기법에 대해서 학습한다.\n2. 이미지 또는 디지털 카메라로 찍은 사진의 합성과 보정 및 편집 방법에 대해서 학습하고, 리터치 기법과 레이어의 활용을 통해서 입체 영상을 제작하는 기법에 대해서 익힌다. \n3. 순수미술 및 디자인 작업, 일러스트 이미지 작업을 비롯하여 홈페이지 배너와 다양한 버튼 이미지 제작 기법을 익혀서 웹페이지 작성 방법을 학습한다. \n4. 프리미어를 이용하여 비디오를 프레임별로 조작하고, 편집하는 기법에 대해서 학습하고, 타이틀과 자막을 처리하는 방법에 대해서 배운다.\n5. 비디오에 다양한 효과를 부여하는 방법을 배우고, 코덱에 대한 지식을 얻어서 다양한 재생 환경에 따른 비디오 제작 방법을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','포토샵 디자인 스타일북,김 혜경,한빛미디어,2013','','',41),('2014/2-0000-2-5675-04','현대사회에서 모든 분야에서 폭넓게 이용되고 있는 디지털영상의 기초 원리와 의미를 이해하고, 영상편집도구를 이용하여 다양하고 효과적인 표현방법을 익힘으로써 실제 응용분야를 위한 아이디어 구현과 활용방법을 습득한다. 본 강의를 통해서 대표적으로 다루어지는 디지털콘텐츠는 2차원 자연영상과 비디오이고, 이들을 편집하기 위한 도구로는 포토샵과 프리미어 등을 이용한다. 디지털영상편집 기법을 익힘으로써 광고, 일러스트, 웹, 미술, 및 UCC를 비롯한 다양한 콘텐츠를 직접 제작하도록 한다.','1. 포토샵을 이용하여 사진 이미지의 수정, 합성, 색상 보정 기법을 비롯하여 광고, 포스터, 엽서 등의 디자인 기법에 대해서 학습한다.\n2. 이미지 또는 디지털 카메라로 찍은 사진의 합성과 보정 및 편집 방법에 대해서 학습하고, 리터치 기법과 레이어의 활용을 통해서 입체 영상을 제작하는 기법에 대해서 익힌다. \n3. 순수미술 및 디자인 작업, 일러스트 이미지 작업을 비롯하여 홈페이지 배너와 다양한 버튼 이미지 제작 기법을 익혀서 웹페이지 작성 방법을 학습한다. \n4. 프리미어를 이용하여 비디오를 프레임별로 조작하고, 편집하는 기법에 대해서 학습하고, 타이틀과 자막을 처리하는 방법에 대해서 배운다.\n5. 비디오에 다양한 효과를 부여하는 방법을 배우고, 코덱에 대한 지식을 얻어서 다양한 재생 환경에 따른 비디오 제작 방법을 습득한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,20,Quiz','포토샵 디자인 스타일북,김 혜경,한빛미디어,2013','','',20),('2014/2-0000-2-5680-01','오늘날 세계는 100년 전 사람들이 상상했던 모습을 닮았는가? 본 강좌는 판타지 장르의 각 작품들을 통해 우리의 미래를 예측하고 상상하는 과목이다. 영화, 만화, 소설 등의 영역 중 판타지 장르에 속하는 작품에 나타난 미래의 모습을 살펴보고 미래학 이론 일반을 개괄한다. 학생들은 미래를 예측했던 과거의 작품들과, 미래를 예견하는 현재의 작품들을 비교해보고 자신만의 상상력으로 다가올 미래를 그려본다.','1. 현재 당연하게 간주되는 많은 사실들이 상상력의 한계를 무너뜨리는 창조력의 산물임을 이해한다.\n2. 판타지 장르가 가진 창조적 상상력을 살펴보고 이를 바탕으로 현재가 설정한 사유의 한계들을 극복한다.\n3. 과거에 상상된 미래의 모습과 현재의 모습을 비교분석하고 이를 바탕으로 미래를 예측해본다.\n4. 현재란 고정불변의 상태가 아니라 끊임없는 변화의 과정임을 인식하고, 그 변화에 능동적으로 대처할 수 있는 순발력 및 열린 삶의 자세를 배운다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Qui','원서 발췌 Hand Out,JRR Tolkien & J.K. Rowling','전재임','kkkdfdf@hanmail.net',96),('2014/2-0000-2-5681-01','현재 많은 영역에서 각 분야를 연계하여 복합문제해결능력을 배양하는 통섭적 접근이 시도되고 있다. 본 강좌는 과학적 시선으로 예술사를 조명하여 예술에 깃든 과학적 맥락을 개괄한다. 구체적 작품들을 중심으로 지동설, 원근법 등의 과학이론이 예술에 미친 영향을 살펴보고, 다빈치, 미켈란젤로 등의 거장들을 통해 드러나는 예술과 과학의 필연적 공존방식을 이해한다. 학생들은 현대예술의 각 영역에서 볼 수 있는, 과학적 원리에 의해 탄생한 예술의 사례를 조사하고 이에 대해 토론한다.','1. 예술과 과학의 친밀성을 인식하고 두 영역의 기반은 궁극적으로 창조적 상상력임을 이해한다.\n2. 서로 다른 영역의 접점에서 새로운 사건들이 일어나는 방식을 살펴보고 자신의 전공분야와 타 분야의 연대가능성을 모색한다. \n3. 다원적 가치관, 열린 사고 및 창의적 사유를 배양하여 사유의 지평을 확장한다.','출석,10,수업태도,0,중간고사,35,기말고사,35,발표,10,토의,0,과제,10,Quiz','프린트 핸드 아웃','김은지','ekdh1739@naver.com',65),('2014/2-0000-2-5685-01','This is an Intermediate level training course in spoken English. Students will have a chance to further develop their speaking ability for communicative proficiency.','The purpose of this class is to improve students ability and confidence using English. This will be accomplished through repeated practice and exposure to English-language materials.','출석,10,중간고사,30,기말고사,30,과제보고서,0,수업태도,20,Quiz,0,기타,10','American English File 3,Clive Oxenden,Oxford,2008','','',18),('2014/2-0000-2-5685-02','This is an Intermediate level training course in spoken English. Students will have a chance to further develop their speaking ability for communicative proficiency.','The purpose of this class is to improve students ability and confidence using English. This will be accomplished through repeated practice and exposure to English-language materials.','출석,10,중간고사,25,기말고사,35,과제보고서,0,수업태도,20,Quiz,0,기타,10','American English File 3,Clive Oxenden,Oxford,2008','','',19),('2014/2-0000-2-5685-03','This is an Intermediate level training course in spoken English. Students will have a chance to further develop their speaking ability for communicative proficiency.','The purpose of this class is to improve students ability and confidence using English. This will be accomplished through repeated practice and exposure to English-language materials.','출석,10,중간고사,30,기말고사,30,과제보고서,0,수업태도,20,Quiz,0,기타,10','American English File 3,Clive Oxenden,Oxford,2008','','',19),('2014/2-0000-2-5688-01','일본인의 삶과 일본문화에 대해 공부함과 동시에 그와 관련된 일본어표현을 익힘으로서 일본어능력을 배양하는 수업이다. 이와 같이 일본인과 일본문화에 대한 지식을 습득하고 이해를 심화하는 기회가 되도록 일본문화 관련 영상자료 등의 보충자료를 사용하여 수업에 재미와 흥미를 더한다.','1. 일본어 어휘와 표현을 늘려 일본어 능력을향상시킨다.\n2. 일본인의 삶과 일본문화에 대한 기본적인 지식을 습득한다.\n3. 일본문화 관련 영상자료 등의 보충자료 설명을 통해 일본문화를 보다 구체적으로 살피고 한국인으로서 한국의 문화와 비교하며 일본문화의 특질을 이해한다.\n4. 일정 주제에 대해 논리적으로 설명한 글의 논지를 정확하고도 신속하게 읽어내는 능력을 함양한다.\n5. 새로 익힌 어휘와 표현을 활용한 작문 및 회화 연습을 통해 일본어 구사력을 향상시킨다.','출석,30,수업태도,0,중간고사,30,기말고사,30,발표,0,토의,0,과제,0,Quiz,1','학교에서 배울 수 없는 일본 문화,마에다 히로미,넥서스,2009','','',19),('2014/2-0000-2-5688-02','일본인의 삶과 일본문화에 대해 공부함과 동시에 그와 관련된 일본어표현을 익힘으로서 일본어능력을 배양하는 수업이다. 이와 같이 일본인과 일본문화에 대한 지식을 습득하고 이해를 심화하는 기회가 되도록 일본문화 관련 영상자료 등의 보충자료를 사용하여 수업에 재미와 흥미를 더한다.','1. 일본어 어휘와 표현을 늘려 일본어 능력을향상시킨다.\n2. 일본인의 삶과 일본문화에 대한 기본적인 지식을 습득한다.\n3. 일본문화 관련 영상자료 등의 보충자료 설명을 통해 일본문화를 보다 구체적으로 살피고 한국인으로서 한국의 문화와 비교하며 일본문화의 특질을 이해한다.\n4. 일정 주제에 대해 논리적으로 설명한 글의 논지를 정확하고도 신속하게 읽어내는 능력을 함양한다.\n5. 새로 익힌 어휘와 표현을 활용한 작문 및 회화 연습을 통해 일본어 구사력을 향상시킨다.','출석,10,수업태도,0,중간고사,40,기말고사,0,발표,20,토의,0,과제,20,Quiz,','학교에서 배울수 없는 일본문화,마에다 히로미(前田裕美,넥서스,2008','','',20),('2014/2-0000-2-5689-01','제2외국어인증제 실시에 따라, 일본어관련 각종 자격증을 취득하기 위해, 일본어를 문법적인 측면에서 정리하여 기본문법을 완성시킴과 동시에 일본어적 어법과 관용적 표현의 특성과 다양한 문형을 학습한다. 또한 일본어회화의 기본이라 할 수 있는 문법의 체계적 지도를 통해 최근의 일본어문법과 그 활용을 학습함으로써 일본어의 정확한 이해와 올바른 표현능력을 기른다.','1.현대 일본어법에 따른 자유로운 의사소통을 목적으로 하기 때문에 말하기,쓰기,듣기에서의 실용문법적 지식의 함양이 중요한 과목이다.\n2.실용적인 연습문제를 많이 다룸으로써 문법활용 능력을 높여 일본어능력시험2급 이상의 일본어실력을 습득한다.\n3.일본어 기본문법을 완성함으로써 보다 정확한 청해능력을 함양하고,실생활에 응용할 수 있는 세련된 회화를 가능케 한다.\n4. 일본어뿐만이 아니라 일본에 대해 다각적으로 이해할 수 있는 기회를 제공한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,10,Quiz','초급 일본어문법 요점정리 Point 20,友松悦子, 和栗雅子,시사일본어사,2005','양현우','hyunwoo242@naver.com',22),('2014/2-0000-2-5763-01','If you want or desire to polish speaking skills including effecitve and rational rhetoric, you can register for this course. Various topics will be covered in class to enhance students communicative skills.','Students are able to express their ideas in logical and persuasive ways.','출석,10,중간고사,30,기말고사,30,과제보고서,0,수업태도,15,Quiz,15,기타,0','영어회화 핵심패턴 233,백선엽,길벗이지톡,2011','','',17),('2014/2-0000-2-5763-02','This is a course of English conversation at the high intermediate level.','By the end of the course, students will be able to express their ideas at the high intermediate conversational level and speak about various everyday and social issues affecting both Korea and the world, formulate ideas and develop reasoning skills that will help them debate more effectively, and prepare for and give effective speeches/presentations.','출석,10,중간고사,25,기말고사,25,과제보고서,30,수업태도,0,Quiz,10,기타,0','영어회화 핵심패턴 233,백선엽,길벗이지톡,2011','','',20),('2014/2-0000-2-5906-01','This course reads and discusses different kinds of cultural texts such as essays, movies, lyrics, and news articles which guide us to appreciate contemporary popular culture.','This course encourgages students to build vocabulary relevant to pop culture, increase reading flunency, and cultivate linguistic competence and cultural literacy.','출석,10,중간고사,35,기말고사,35,과제보고서,10,수업태도,10,Quiz,0,기타,0','printed materials','','',38),('2014/2-0000-2-5906-02','본 강좌는 영어 듣기와 쓰기에 중점을 두고 최근 영미권의 드라마를 중심으로 현대의 구어적 영어표현에 대한 이해와 활용을 높이고 변화하는 현대문화에 대한 이해를 높이려는 취지로 개설되었다.','영어로 된 드라마를 통해 일상적인 구어 환경의 영어 표현을 익히고, 이런 표현들을 듣고 이해하는 훈련을 통해 영어 듣기능력을 함양하고자하며, 나아가 일상인 미국의 가정, 친구, 직장, 젠더, 성 역할, 소비, 패션, 도시 문화에 접함으로써 미국 문화에 대한 이해를 높이고자 한다.','출석,10,중간고사,35,기말고사,35,과제보고서,10,수업태도,0,Quiz,0,기타,10','movie, Notting Hil>','','',37),('2014/2-0000-2-6018-01','고대부터 현대까지 동양사에 커다란 영향을 남긴 주요 역사인물들의 역사적 판단과 실천, 리더십 등을 중심으로 역사적 사건을 재해석함으로써 역사에 대한 주체적 인식을 도모한다. \n수강생 개인별 혹은 조별로 역사적 인물에 대한 다양한 평가들을 조사 발표 토론하면서 역사적 인물들에 대한 주체적 재평가를 시도한다.','1. 동양사의 주요 인물들과 그들을 둘러싼 역사적 조건과 사회적 환경에 대하여 이해함으로써 역사를 종합적이고 분석적으로 인식한다. \n2. 역사를 주체의 역사적 실천의 산물로서 이해함으로써 일상적 행위에 대한 책임감과 역사의식을 고양한다.\n3. 동양사 속의 주요 인물들을 재조명함으로써 그들의 삶을 나의 것으로 재구성하여 종합적인 판단능력을 갖춘 실천적 인간으로서의 자아를 형성한다.','출석,10,수업태도,0,중간고사,25,기말고사,35,발표,20,토의,0,과제,10,Quiz','movie, Notting Hil>','박대희','qkreomgl7@naver.com',33),('2014/2-0000-2-6019-01','FIXED','FIXED','출석,30,중간고사,20,기말고사,20,과제보고서,10,수업태도,20,Quiz,0,기타,0','프리젠테이션 기획&실무 테크닉,임영규,북스홀릭,2010','김지훈','kimjihoon@kw.ac.kr',18),('2014/2-0000-2-6020-01','FIXED','FIXED','출석,10,중간고사,0,기말고사,20,과제보고서,40,수업태도,10,Quiz,0,기타,20','개별과제에 관한 별도의 개별 참고문헌 및 시청각자료','윤대근','dbseorms777@naver.com',18),('2014/2-0000-2-6021-01','이 과목은 외국인 유학생을 대상으로 합니다. 학부와 대학원에 재학 중인 외국인으로 한국어 수준이 중급 이상인 학생을 대상으로 다양한 주제와 상황을 통해 말하기 능력을 향상시키고자 합니다.','말하기에 대한 이론 공부와 실제 연습을 통해 다양한 주제와 상황에 맞게 자연스럽고 유창하게 말할 수 있도록 합니다.','출석,10,중간고사,25,기말고사,25,과제보고서,30,수업태도,10,Quiz,0,기타,0','대학 강의 수강을 위한 한국어 말하기 중급 2,연세대학교 한국어학당,연세대학교 출판문화원,2012','','',25),('2014/2-0000-2-6022-01','본 강의는 중국어 초급 학습자를 위해 중국 생활 속에서 일어나는 다양한 상황들을 대화 위주로 진행한다. 수강생들이 원어민 교수의 정확한 발음을 배우고 원어민선생님이 학생들의 발음을 교정하며, 원어민선생님과의 1대1의 연습을 통해 중국어를 유창하게 말할 수 있게 된다. 다양한 매체를 통해 생생한 중국어를 접해보고 따라해 보고 연습해 보는 식으로 한 학기 동안의 학습을 통해 중급과정에 들어가게 하는 기초를 다질 수 있음에 목적을 두고 있다.','1. 정확한 중국어 발음을 구사하도록 한다. \n2. 자연스러운 회화를 할 수 있게 한다． \n3. 중국어　학습을　통해　중국　문화에　대해서도　이해하도록　한다.\n4. 중국　전문가　되고자　하는　학생들이　가장　기본적인　기초　지식을　습득하게　된다．\n5. 초급 단계의 읽기ㆍ쓰기ㆍ듣기도 잘 할 수 있게 한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','프린트물','','',19),('2014/2-0000-2-6023-01','초급단계에서 배운 문법과 표현을 토대로 일본어로 자연스럽게 의사소통하는 법을 익힌다. 일본사회 및 문화와 관련된 주제를 선정하여 회화연습을 진행하고 한국인이 틀리기 쉬운 문법사항과 표현들을 다루어서 실전에 강한 회화능력을 배양한다.','1. 일본현지에서 생활이 가능한 의사소통 능력 양성\n2. 상황에 맞는 자연스러운 일본어 구사 능력 배양\n3. 일본 문화와 사회에 대한 관심과 흥미를 고취','출석,20,수업태도,10,중간고사,20,기말고사,20,발표,10,토의,0,과제,20,Qui','뉴코스 일본어Step6,조영남 외4명,다락원,2014','','',19),('2014/2-0000-2-6023-02','초급단계에서 배운 문법과 표현을 토대로 일본어로 자연스럽게 의사소통하는 법을 익힌다. 일본사회 및 문화와 관련된 주제를 선정하여 회화연습을 진행하고 한국인이 틀리기 쉬운 문법사항과 표현들을 다루어서 실전에 강한 회화능력을 배양한다.','1. 일본현지에서 생활이 가능한 의사소통 능력 양성\n2. 상황에 맞는 자연스러운 일본어 구사 능력 배양\n3. 일본 문화와 사회에 대한 관심과 흥미를 고취','출석,20,수업태도,0,중간고사,20,기말고사,20,발표,10,토의,10,과제,10,Qui','뉴코스 일본어 step6,조영남, 채성식, 아이자와 유카 기타,다락원,2014','','',20),('2014/2-0000-2-6056-01','초보자와 중,상급자로 나누어 각자의 수준에 맞춰 상황에 따라 바이올린 그룹렛슨 또는 개인렛슨 진행','바쁘게 돌아가는 현대사회에서 자칫 메말라 갈 수 있는 감성을 악기를 배움으로 정서를 순화하고 성취감 또한 높일 수 있다.나아가 문화 예술에 대한 폭넓은 이해와 관심을 유도하여 삶의 질을 높이기 위함이다','출석,40,중간고사,0,기말고사,20,과제보고서,0,수업태도,30,Quiz,0,기타,10','시노자끼 바이올린 교본 (초보자),시노자끼','','',5),('2014/2-0000-2-6057-01','악기 연주','악기를 연주 할수 있도록 한다.','출석,50,중간고사,0,기말고사,0,과제보고서,0,수업태도,50,Quiz,0,기타,0','시노자끼 바이올린 교본 (초보자),시노자끼','','',8),('2014/2-0000-2-6058-01','클래식 기타 연주법을 통해 기타를 연주하고, 나아가 음악적 기본 상식을 습득할 수 있다.','클래식 기타의 연주법 습득 및 핑거스타일, 포크스타일 등의 악기 특성을 알 수 있다.','출석,30,중간고사,20,기말고사,50,과제보고서,0,수업태도,0,Quiz,0,기타,0','어린이 기타교본,김명표,삼호뮤직','','',11),('2014/2-0000-2-6133-01','세계 도시사와 도시문명을 융합학문의 시각에서 탐구한다. 동양과 서양의 주요 도시들의 기원, 도시 구조, 특성을 살펴보고, 도시문화의 정치적, 경제적, 사회적 의미를 파악한다. 또한 근현대 아시아의 전통적 역사도시들의 변모 양상을 고찰한다. 과목에서 다루어지는 도시들은 런던, 파리, 베를린, 뉴욕, 서울, 도쿄, 베이징, 상하이 등이다.','인류사를 도시문명을 통해 파악하면서 도시 발달을 낳은 정치적, 경제적, 사회적, 과학기술적 요인들을 융합적으로 이해한다. 세계적인 역사도시의 공간 구조, 도시사회와 문화의 특성을 고찰하면서 도시의 미래에 대한 역사적 전망을 모색한다.','출석,20,수업태도,0,중간고사,40,기말고사,40,발표,0,토의,0,과제,0,Quiz,0','강의안','','',58),('2014/2-0000-2-6137-01','미입력','미입력','출석,30,수업태도,0,중간고사,30,기말고사,30,발표,0,토의,0,과제,10,Quiz,','현대사회와 범죄,이윤호,박영사,2011','임용진','yongjin1085@gmail.com',63),('2014/2-0000-2-6372-01','앙상블은 한성부를 하나의 악기가 담당하여 두사람 이상이 연주한다. 각성부가 대등한 위치에서 독립되 있다는 것이며..즉 어느쪽에서든 주연과 조연이 될수있으며 각각의 악기는 음악에있어서 동등한 자격으로 연주하게된다. 각 악기의 특성있는 연주 기술을 통해 그 음향과 선율의 조화, 악기와 사람과 사람, 어울림의 묘미를 느끼는데있다.','연주자의 기술적인 면과 곡의해석 앙상블에 필요한 기초 지식을 배우고. 개개인의 생각과 개성있는 각 악기의 특징을 통일성있는 표현력으로 악보를 통해 조성한다. 앙상블을 통한 연주할동의 기초를 다진다. *모집악기* 피아노,바이올린,비올라,첼로,베이스기타,플륫,오보,클라리넷,리코더,바순,섹소폰,트럼펫,트럼본,혼,튜바 클래식기타,통기타,타악기...악기는 개인이 지참해야합니다 기본적인 연주능력도 있어야 하구요(조금) 기본적으로 클래식 악기로 구성되지만 상황에따라 연습곡들은 대중가요나 팝송 영화음악으로 진행될수있습니다.','출석,80,중간고사,0,기말고사,20,과제보고서,0,수업태도,0,Quiz,0,기타,0','현대사회와 범죄,이윤호,박영사,2011','','',6),('2014/2-0000-2-6377-01','본 과목에서는 사회복지 실천현장에서 필요한 사회복지 실천의 기본 개념 및 가치, 상담에 필요한 관계론, 면담론, 과정론, 그리고 사회복지 실천의 기본 모델에 대해 학습한다.','본 과목은 사회복지 현장에서 대상자들과 이루어지는 현장 실천을 위한 기본 지식, 기술, 윤리, 가치 등의 전반적인 기초지식을 살펴보는데 목적이 있다. 사회복지 현장에서 실천이 어떻게 진행되는지 파악하고, 사회복지 실천가로서 역할을 다 할 수 있도록 실천의 과정론, 관계론, 실천모델 등에 대해 학습함으로써 효과적인 사회복지사가 되도록 훈련한다. 사회복지실천론 학습을 통해 대상자들과 관계를 형성하는 법, 실천방법, 면담하는 방법 등을 익히고 과정을 경험해 보도록 한다.','출석,20,중간고사,30,기말고사,0,과제보고서,40,수업태도,10,Quiz,0,기타,0','사회복지실천론 개정 4판.,양옥경 외,나남,2010','','',19),('2014/2-0000-2-6378-01','본 과목은 지역사회 복지를 위한 이론, 실제를 총괄적으로 학습할 수 있는 내용으로 구성되어 있으며, 지역사회의 복지 증진 및 이를 위해 구체적으로 실천가들이 갖추어야 하는 역량, 기술, 지식에 관한 학습 내용을 담고 있습니다.\n 국내의 지역사회 이슈, 온라인을 통한 새로운 지역사회 복지의 대두, 그리고 세계화를 통해 점차 글로벌한 이슈에 개입하고 있는 사회복지에 관한 전반을 다루고 이에대한 문제해결 능력을 배웁니다.','학생들의 지역사회에 대한 개념, 지역사회 복지란 무엇이며, 이를 실천하기 위해서는 어떠한 기본적 지식을 갖추고 있어야 하는지를 알 수 있는 수업입니다.','출석,10,중간고사,30,기말고사,30,과제보고서,20,수업태도,10,Quiz,0,기타,0','지역사회복지 실천론,강철희. 정무성,나남출판사,2010','','',17),('2014/2-0000-2-6382-01','경찰학을 연구하는데 있어서 체계적인 연구방법론 지식의 습득은 매우 중요하다. 경찰학은 사회과학의 한 분야로 연구방법은 사회과학연구방법론들과 일맥상통한다. 이 수업에서는 연구조사를 수행하는데 필히 습득해야 할 사회과학 연구방법론에 대해 공부한 후 이를 경찰학 연구방법에 적용해보는 연습을 진행할 것이다.','1. 사회과학 연구방법론 전반에 대한 체계적인 지식을 습득한다.\n2. 연구논문의 설계 및 작성방법에 대해 이해한다.\n3. 궁극적으로 경찰학 연구논문을 이해하고 해석하는 능력을 함양한다.','출석,20,중간고사,20,기말고사,30,과제보고서,30,수업태도,0,Quiz,0,기타,0','경찰연구방법론,한기민, 김구,한국학술정보,2006','안상원','asw0831@kw.ac.kr',18),('2014/2-0000-2-6387-01','스마트폰 혹은 패드와 같은 모바일 기기를 위한 프로그래밍 개발 환경과 프로그래밍 기법을 이해하고 간단한 모바일 프로그램을 개발한다.','스마트폰 등의 모바일 기기에 원하는 프로그래밍을 할 수 있는 능력을 배양한다.\n- 모바일 개발 환경 설정\n- 프로그래밍 기법의 이해\n- 프로그램 설계\n- 프로그램 개발','출석,10,수업태도,0,중간고사,10,기말고사,10,발표,20,토의,0,과제,50,Quiz','그림으로 쉽게 설명하는 안드로이드 프로그래밍,천인국 저,생능출판사,2014','','',20),('2014/2-0000-2-6387-02','스마트폰 혹은 패드와 같은 모바일 기기를 위한 프로그래밍 개발 환경과 프로그래밍 기법을 이해하고 간단한 모바일 프로그램을 개발한다.','스마트폰 등의 모바일 기기에 원하는 프로그래밍을 할 수 있는 능력을 배양한다.\n- 모바일 개발 환경 설정\n- 프로그래밍 기법의 이해\n- 프로그램 설계\n- 프로그램 개발','출석,10,수업태도,0,중간고사,10,기말고사,10,발표,20,토의,0,과제,50,Quiz','그림으로 쉽게 설명하는 안드로이드 프로그래밍,천인국 저,생능출판사,2014','','',48),('2014/2-0000-2-6397-01','사회가 존재하고 인간에 생존하는 동안은 범죄(crime)는 발생한다. 그리고 범죄는 없어질 수 없다. 다만, 인간은 안전한 삶을 살기위해 범죄를 예방할 수는 있다. 범죄예방(crime prevention)은 인간의 노력에서 얻을 수 있는 결과물이다. 이러한 관점에서 본 교과목은 범죄에대한 이해와 발생빈도 및 원인적 측면을 고찰 분석하여 범죄예방에 대한 정책적 대한을 모색한다.','본 교과목의 목적은 다음과 같다.\n1. 현대사회에서 발생하는 범죄에 대한 이해\n2. 범죄에 대한 원인론적 고찰 및 분석\n3. 범죄원인에 대한 사례분석\n4. 분석을 통한 대안 모색\n5. 현대사회에서 발생하는 범죄예방적 접근\n이상의 학습목표를 통해 범죄학의 이론과 현실적적 대안을 모색하는데 목표가 있다.','출석,20,중간고사,25,기말고사,25,과제보고서,10,수업태도,10,Quiz,5,기타,5','추후공지','안상원','asw0831@kw.ac.kr',36),('2014/2-0000-2-6525-01','제2외국어인증제를 통과하기위한 중국어 청취위주의 수업으로 중국어 습득에 있어 기본적이고 필수적인 어휘/문형/문법/회화 능력 등을 배양해 중국어 청해력과 회화력의 기초를 다진다. 간단한 중국어 문장과 한자 쓰기부터 기본적이고 필수적인 회화, 어휘, 문법 등의 습득 능력을 키워 중국어 청해력과 회화능력의 기초를 다진다. 학습자가 흥미를 갖고 쉽게 중국어를 접할 수 있도록 청취 후 따라서 발음하고 회화에 적용하는 과정에서 자연스럽게 관련 문법을 습득하게 된다.','1.중국어 회화, 어휘, 한자 쓰기를 익히도록 한다. \n2.중국어 학습의 정확한 발음을 구사할 수 있도록 유도한다. \n3.중국어를 듣고 이해하는 데 필요한 기초표현을 익히도록 한다. \n4.자연스러운 회화문 위주로 구성된 교재의 내용을 이해하여, 받아쓰고 말해보는 연습을 반복함으로써 중국어 실력을 향상시키도록 한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,0,과제,10,Quiz','프린트물','','',18),('2014/2-0000-2-6526-01','중국어는 매려적인 언어이다. 중국어에 리듬이 담겨 있어서 노래하듯 말하다. 문장표현 역시 복잡하지 않아서 영어와 같은 격변화나 시제변화 없어도 모든 표현을 다 해낼 수 있다. 본 과목은 중국어를 학습함에 있어서 가장 관심을 갖고 이해해야 할 부분은 어순과 문장구조이자. 본 과목은 중국어를 막 시작한 학습자로부터 중급 수준의 학습자에게까지 널리 도움이 될 수 있도록 설계하였으며, 강의, 토론, 작문 등의 연습을 통해 중국어 문법의 특징과 계통을 체계적으로 이해하고 주요표현을 익힐 수 있도록 한다.','1.현대 중국어문법을 바탕으로 하는 자유로운 의사소통이 목적이기 때문에 말하기, 쓰기, 듣기에서의 실용문법적 지식 함양이 중요한 과목이다. 2.실용적인 연습문제를 많이 다룸으로써 문법활용능력을 높여 HSK시험 2급 이상의 중국어실력을 습득한다. 3.중국어 기본문법을 완성함으로써 보다 정확한 청해 능력을 갖추고, 실생활에 응용할 수 있는 회화를 가능케 한다. 4. 중국어뿐만이 아니라 중국문화에 대해 다각적으로 이해할 수 있는 기회를 제공한다.','출석,10,수업태도,10,중간고사,25,기말고사,40,발표,0,토의,0,과제,10,Quiz','왜?라는 질문 속 시원히 답해주는 중국어문법책,상원무 외,시사중국어사,2014','유재문','ou0818@hanmail.net',26),('2014/2-0000-2-6580-01','본 강좌는 글로벌 시대 속에서 예술이란 무엇인가의 근본적인 질문에 답해가는 과정을 목표로 한다. 이를 위해 일차적으로는 예술의 본질에 대한 설명을 시도했던 다양한 역사적 이론과 개념들을 검토하고, 나아가 감상자로서 나와 예술이 맺는 관계를 반성적으로 성찰하고 창조자로서 새로운 시도들을 실현해봄으로써 글로벌 시대의 예술의 일상성과 가치를 제고하도록 한다.','예술작품의 감상과 창조를 통해 글로벌 시대를 살아하는 인재가 필수적으로 겸비해야할 다양한 가치관의 수용과 창조적 능력을 기르도록 할 것이다','출석,30,중간고사,20,기말고사,20,과제보고서,20,수업태도,0,Quiz,10,기타,0','예술에 대한 일곱가지 답변의 역사,김진엽,첵세상','백기준','lalilac@naver.com',148),('2014/2-0000-2-6728-01','강의계획서는 첼로를 처음 접하는 학생이 쉽게 이해하고 연주 할 수 있도록 계획되어 있습니다. 처음 첼로를 접하는 학생이나 또는 첼로 전공자의 실력까지 갖추고자 하는 학생을 포함하여 1:1 실기 및 이론 수업으로 진행이 됩니다. 이 수업을 통해 첼로에 대한 악기 연주 및 다양한 클래식의 이해 및 즐길 거리를 제공할 수 있는 좋은 기회이길 바랍니다. 수업을 하기 위해서는 악기를 대여하거나 구입하여야 됩니다.','첼로연주에 의한 기술 습득 및 총보능력과 전반적인 클래식의 이해 입니다.','출석,50,중간고사,0,기말고사,40,과제보고서,0,수업태도,10,Quiz,0,기타,0','suzuki violoncello','','',5),('2014/2-0000-2-6730-01','쉽게 접할수 있는 타악기(북) 를 가지고 간단한 난타식 합주 부터 좀더 난이도 있는 난타합주까지 배우고 즐길수 있는 강좌가 될것입니다.','인간의 심장박동을 울릴 수 있는 가장 기본적인 악기 북. 누구나 한번쯤은 두들겨 보고 싶고 두드리면서 스트레스 해소를 느껴볼수 있는 악기입니다. 마구잡이로 그냥 두들기는 것보다는 교육을 통해 일정하게 또,서로 호흡을 맞추면서 더 큰 기쁨과 성과를 느끼게 될것입니다. 난타 합주를 통해 학부생활에 좀더 활력과 힘이 되기를 바라는 취지입니다','출석,50,중간고사,0,기말고사,20,과제보고서,0,수업태도,30,Quiz,0,기타,0','suzuki violoncello','','',15),('2014/2-0000-2-6948-01','현재 지구상에 존제하는 대중음악은 그뿌리를 흑인음악에 두고 있다. \n19세기 후반 흑인음악이 어떻게 백인사회에 뿌리를 내리게 되었으며 어떠한 장르를 탄생시키고 발전해왔는지를 교육한다. 초기 Blues부터 R&B, Rock N Roll, Funk의 탄생, Beatles, Fusion, Hip-Hop 그리고 한국 트로트 부터 아이돌 음악까지 대중음악의 변천사와 그 효과를 학습한다.','21세기 지식인으로서 갖추어야할 대중문화에 대한 이해를 돕고. 현대 음악산업의 흐름을 파악하여음악이 단지 즐길거리가 아닌 하나의 학문이며 산업이라는 인식을 키운다.','출석,30,중간고사,25,기말고사,25,과제보고서,20,수업태도,0,Quiz,0,기타,0','suzuki violoncello','이두영','duyounglee@hanmail.net',92),('2014/2-0000-2-6948-02','현재 지구상에 존제하는 대중음악은 그뿌리를 흑인음악에 두고 있다. \n19세기 후반 흑인음악이 어떻게 백인사회에 뿌리를 내리게 되었으며 어떠한 장르를 탄생시키고 발전해왔는지를 교육한다. 초기 Blues부터 R&B, Rock N Roll, Funk의 탄생, Beatles, Fusion, Hip-Hop 그리고 한국 트로트 부터 아이돌 음악까지 대중음악의 변천사와 그 효과를 학습한다.','21세기 지식인으로서 갖추어야할 대중문화에 대한 이해를 돕고. 현대 음악산업의 흐름을 파악하여음악이 단지 즐길거리가 아닌 하나의 학문이며 산업이라는 인식을 키운다.','출석,30,중간고사,25,기말고사,25,과제보고서,20,수업태도,0,Quiz,0,기타,0','suzuki violoncello','오태양','mabinogi999@naver.com',98),('2014/2-0000-2-6957-01','컴퓨터를 이용해 새로운 음향을 만들고 가공하는 기초를 익힌다.','음원의 녹음 및 재생, 가공, 실시간 음향 처리에 대한 다양한 원리를 이해하여 GUI기반의 Eisenkraut 및 FScape, SPEAR, Pauls Extreme Sound Stretch, Audacity, Adobe Audition에서 실습함으로써 전문 sound editing software 및 DAW(digital audio workstation)의 사용법을 익히며, 더 나아가 SuperCollider에서의 프로그래밍 실습을 통해 digital sound 제작에 필요한 programming의 기초를 함양한다.','출석,15,중간고사,0,기말고사,30,과제보고서,20,수업태도,0,Quiz,15,기타,20','Introduction to Computer Music,Nick Collins,Wiley,2010','','',26),('2014/2-0000-2-6957-02','컴퓨터를 이용해 새로운 음향을 만들고 가공하는 기초를 익힌다.','음원의 녹음 및 재생, 가공, 실시간 음향 처리에 대한 다양한 원리를 이해하여 GUI기반의 Eisenkraut 및 FScape, SPEAR, Pauls Extreme Sound Stretch, Audacity, Adobe Audition에서 실습함으로써 전문 sound editing software 및 DAW(digital audio workstation)의 사용법을 익히며, 더 나아가 SuperCollider에서의 프로그래밍 실습을 통해 digital sound 제작에 필요한 programming의 기초를 함양한다.','출석,15,중간고사,0,기말고사,30,과제보고서,20,수업태도,0,Quiz,15,기타,20','Introduction to Computer Music,Nick Collins,Wiley,2010','','',10),('2014/2-0000-2-6966-01','특수아동과 특수교육에 대한 이해를 통해 통합교육 전문가로서 자질을 함양할 수 있는 수업입니다.','1) 특수교육의 기본적인 용어와 개념을 이해하고 전반적인 역사와 동향을 학습한다.\n 2) 장애 유형별 장애의 정의, 원인, 특성, 교육에 대한 기본지식을 습득하여 특수아동에 대한 이해를 향상시킨다.\n 3) 특수 아동의 다양한 요구를 파악하며 그들을 효과적으로 지원할 수 있는 태도와 자질을함양한다. \n 4) 특수 아동 및 일반 아동의 성공적인 학교생활을 위한 다양한 접근 방법을 이해한다.','출석,10,중간고사,25,기말고사,30,과제보고서,25,수업태도,10,Quiz,0,기타,0','특수아동교육 3판,이소현, 박은혜,학지사,2011','','',11),('2014/2-0000-2-6994-01','토익필수2는 정규토익/광운 토익을 본 적이 있는 학생, 즉 토익의 유형을 기본적으로 경험해 본 적이 있는 학생들을 대상으로 진행됩니다. 600점 이상의 점수를 목표로 하는 수업으로써 토익시험에 필요한 기초적인 영어실력을 확실히 완성하고 중급의 실력으로 키워가는 단계의 수업입니다. 토익의 전반적인 소개와 더불어 각 Part 별 유형 연구와 실전문제에서 다룰 수 있는 듣기와 문법에 대한 것을 100% 온라인 강의를 통해 연습을 합니다. ** 수강기준:U-Campus에 등록된 점수가 550~600점 미만의 학생들만 신청가능!!! ** 영문과 학생은 담당교수 뜻에 따라 토익필수2는 수강할 수 없습니다. (수강하더라도 학점 부여가 되지 않습니다.)','최신 출제 경향을 바탕으로 다양한 유형을 파악하여 실전에서 편안하게 Listening test에 대비할 수 있는 연습을 하며, 토익 시험에 필요한 기본적인 영문법 분석을 통해 다양한 문제 유형에 대한 효과적인 전략을 제시하고, 이 전략을 연습 문제에 적용하여 각 유형별로 집중 학습이 가능하게 합니다. ** 수강기준:U-Campus에 등록된 점수가 550~600점 미만의 학생들만 신청가능!!! ** 영문과 학생은 담당교수 뜻에 따라 토익필수2는 수강할 수 없습니다. (수강하더라도 학점 부여가 되지 않습니다.)','출석,10,중간고사,40,기말고사,40,과제보고서,0,수업태도,0,Quiz,10,기타,0','시나공 토익 750 LC / 시나공 토익 750 RC,조강수,길벗이지톡,2012','이연희','iee91@naver.com',93),('2014/2-0000-2-6999-01','색에 대한 기초이론을 습득하고, 생활 속에서 색채를 이용하여 사람의 다양한 감정을 사로잡는 색채심리의 사례를 연구한다. 이를 토대로 현대 사회에서 다양하게 쓰이는 색에 대한 바른 이해와 응용력을 키움을 목표로 한다.','1.색채에 대한 기본적 이해를 위한 색채학 이론과 색채심리를 학습한다. \n2.현대 생활에서 색의 심리가 적용되는 사례와 생활에서 유용하게 이용할 수 있는 색채에 대하여 시청각 등의 다양한 방법으로 학습한다. \n3.현대 사회가 규정하고 있는 다양한 색의 쓰임에 대한 안목을 키운다.','출석,20,중간고사,35,기말고사,35,과제보고서,0,수업태도,0,Quiz,0,기타,10','색채심리와 현대생활,김문석,교안','','',148),('2014/2-0000-2-7145-01','클레식 피아노 연주가 아닌 가요 및 팝 또는 교회음악반주 법을 배운다.가장 기초적인 음정부터 코드의 이해 그리고 각 음악 스타일별 특징적인 테크닉을 배운다.','피아노 반주법을 통해 좀더 깊이 대중음악을 이해하고 즐길수 있는 능력을 키운다.','출석,30,중간고사,25,기말고사,25,과제보고서,0,수업태도,20,Quiz,0,기타,0','색채심리와 현대생활,김문석,교안','','',15),('2014/2-0000-2-7471-01','집단상담은 개인의 심리적 문제를 소집단의 경험을 통해서 해결하고자 하는 상담의 한 유형으로 흔히 개인 상담과 대비되는 개념으로 사용되고 있습니다. 개인의 어려움이 집단의 역동속에서 어떻게 경헙되며, 변화를 촉진하는 과정과 요소들을 파악합니다.','청소년 집단상담에서는 특히, 집단상담의 특징과 기법적 요소들이 청소년이라는 구체적 집단에게 어떻게 적용되고 효과를 보이는지 이론과 실습 등을 통해 경험해 보는데 목적을 둡니다. 구체적으로 학교 장면에서 청소년들이 경험할 수 있는 특징적인 주제들에 맞춰 그에 맞는 집단 프로그램을 구성, 적용하는 훈련도 포함되도록 하겠습니다.','출석,10,중간고사,35,기말고사,35,과제보고서,20,수업태도,0,Quiz,0,기타,0','집단상담 이론과 실제,노안영,학지사,2012','','',32),('2014/2-0000-2-7472-01','인간은 사물적 대상과 달리 객관적으로 설명될 수 없는 존재로서 스스로 자기이해를 추구한다. 본 강좌는 그러한 인간을 교육과 연결하여 설명하고자 한다. 인간과 교육을 둘러 싼 여러 관점을 이해하고, 그 가운데 자신만의 교육적 관점 정립을 시도한다.','*인간과 교육의 본질을 논의하고, 그 둘 사이의 관계에 대해 이해한다.\n*교육을 중심으로 인간에 대한 다각적인 접근을 시도한다.','출석,10,중간고사,40,기말고사,40,과제보고서,0,수업태도,10,Quiz,0,기타,0','인간이해와 교육학,정영근,문음사,2000','','',24),('2014/2-0000-2-7476-01','','','출석,0,중간고사,0,기말고사,0,과제보고서,0,수업태도,0,Quiz,0,기타,0','인간이해와 교육학,정영근,문음사,2000','','',146),('2014/2-0000-2-7595-01','정통재즈의역사를 이어 현대 재즈의 뿌리인 Be-Bop 재즈를 시작으로, 지금 시대에 살아서 활동하는 재즈 거장들의 음악을 듣고 분석하는 강의입니다.*정통재즈의역사 수업을 안들었던 학생들도 강의 신청이 가능합니다*Be-Bop 에서 Cool Jazz, Hard Bop, Free Jazz, Fusion Jazz 까지 다양한 재즈 음악을 접할수있는 강의가 될겁니다.\nThe history of modern jazz will cover from 1950s Be-Bop and go on till present day jazz.Not like the history of traditional jazz, Many of Musicians that well be discussing in the lecture will be still alive and active as a musician.They often visit Korea for concert(jazz festivals like Jarasum and Seoul jazz festival and so on..) so itll be good opportunity to learn their music in the lecture and go to actual concert.\n\n재즈음악의 역사는 19세기말에서 20세기초, 아프리카에 뿌리를 둔 미국 흑인들의 음악에서 시작됩니다. 재즈의 본 고장은 2005년도에 Hurricane Katrina로 인해 초토화가 된 도시 New Oreleans이며, 이 도시에서 다 인종의 문화가 융합되기 시작한 20세기 초부터 자연스럽게 만들어진 음악입니다. 재즈음악의 독특한 스타일은 현재 모든 Pop 음악의 시작이며, 그 역사 또한 독특한 인물들로 인해 만들어졌습니다. The history of jazz is full of stories. These stories contains original sound and character. Well be covering most significant stories of jazz using multimedia.','재즈라는 장르가 아직 생소하게 느껴지는 우리나라에서는 많은 사람들이 재즈를 어렵고 따분한 음악이라고 생각합니다. 그 이유는 재즈라는 언어를 이해하지 못하기 때문입니다. 하지만 재즈의 역사, 등장 인물, 장소, 가장 영향력 있던 음악을 듣고 배우고 이해하면 재즈라는 장르가 왜 지금 pop 음악의 시초가 될 수 있었는지 이해 할 수 있을 것입니다. The goal is simple. Its to let you understand how to enjoy the great american art form.(jazz) Once you learned the basic form of jazz, this music will be much more enjoyable.','출석,20,중간고사,20,기말고사,30,과제보고서,10,수업태도,0,Quiz,20,기타,0','Jazz (DVD) Film,Ken Burns,PBS,2000','','',96),('2014/2-0000-2-7595-02','정통재즈의역사를 이어 현대 재즈의 뿌리인 Be-Bop 재즈를 시작으로, 지금 시대에 살아서 활동하는 재즈 거장들의 음악을 듣고 분석하는 강의입니다.*정통재즈의역사 수업을 안들었던 학생들도 강의 신청이 가능합니다*Be-Bop 에서 Cool Jazz, Hard Bop, Free Jazz, Fusion Jazz 까지 다양한 재즈 음악을 접할수있는 강의가 될겁니다.\nThe history of modern jazz will cover from 1950s Be-Bop and go on till present day jazz.Not like the history of traditional jazz, Many of Musicians that well be discussing in the lecture will be still alive and active as a musician.They often visit Korea for concert(jazz festivals like Jarasum and Seoul jazz festival and so on..) so itll be good opportunity to learn their music in the lecture and go to actual concert.\n\n재즈음악의 역사는 19세기말에서 20세기초, 아프리카에 뿌리를 둔 미국 흑인들의 음악에서 시작됩니다. 재즈의 본 고장은 2005년도에 Hurricane Katrina로 인해 초토화가 된 도시 New Oreleans이며, 이 도시에서 다 인종의 문화가 융합되기 시작한 20세기 초부터 자연스럽게 만들어진 음악입니다. 재즈음악의 독특한 스타일은 현재 모든 Pop 음악의 시작이며, 그 역사 또한 독특한 인물들로 인해 만들어졌습니다. The history of jazz is full of stories. These stories contains original sound and character. Well be covering most significant stories of jazz using multimedia.','재즈라는 장르가 아직 생소하게 느껴지는 우리나라에서는 많은 사람들이 재즈를 어렵고 따분한 음악이라고 생각합니다. 그 이유는 재즈라는 언어를 이해하지 못하기 때문입니다. 하지만 재즈의 역사, 등장 인물, 장소, 가장 영향력 있던 음악을 듣고 배우고 이해하면 재즈라는 장르가 왜 지금 pop 음악의 시초가 될 수 있었는지 이해 할 수 있을 것입니다. The goal is simple. Its to let you understand how to enjoy the great american art form.(jazz) Once you learned the basic form of jazz, this music will be much more enjoyable.','출석,20,중간고사,20,기말고사,30,과제보고서,10,수업태도,0,Quiz,20,기타,0','Jazz (DVD) Film,Ken Burns,PBS,2000','','',99),('2014/2-0000-3-0261-01','敎育은 어떤 한 가지 요인으로 구성되는 것이 아니다. 교육에는 인간의 삶살이와 관련한 여러 요소들이 관련되어 있다. 생명의 탄생에서부터 소멸에 이르기까지 인간이 경험하게 되는 모든 관점들이 연결되어 있다. 그러므로 교육현상을 이해하는 데는 교육학이라는 하나의 관점으로도 가능하다는 주장은 그 타당성이 부족하다고 할 수 있다. 이 강의는 다종다양한 교육현상들을 교육사회학의 시각으로 바라보고 논의해봄으로써 인간 이해의 지평을 넓혀보려는 작업이다.','이 강의는 교육문제라고 불리는 여러 사회문제들에 대한 정답을 제시하는데 목적이 있다기보다는 이 강의에 참여하는 모든 이들 스스로가 나름대로 해답을 찾고, 그에 따라 결심하고 결단하여 이를 실천하는데 그 목적이 있다.','출석,20,중간고사,10,기말고사,20,과제보고서,10,수업태도,10,Quiz,0,기타,3','배움의 사회학적 관점전환론,한준상, 최항석, 김성길,공동체,2012','','',37),('2014/2-0000-3-0265-01','본 강좌는 교직이론 강의로 교육철학의 주요 이론을 탐구한다. 또한 제도사를 중심으로 우리나라와 서양의 교육사를 탐구한다.','교육에 대한 상식적 수준 이상의 철학적 안목을 형성하는 것을 목적으로 한다.','출석,15,중간고사,20,기말고사,40,과제보고서,20,수업태도,5,Quiz,0,기타,0','쉽게 풀어 쓴 교육철학 및 교육사,신득렬, 이병승, 우영효, 김회용,양서원,2007','','',14),('2014/2-0000-3-0910-01','- 세계적인 주요 신문과 잡지에서 발췌한 유익한 글을 강독하면서 그 내용을 이해하고 토론 한다. 시사적으로 중요한 사건과 이슈가 부각되는 경우, 심층적으로 다룬 글을 보강해서 강독하고 토론한다.\n- 강의 1회 당 1개의 기사를 조별 발표(본문해석 20분, 관련 문헌, 동영상 10분 내외), 그리고 질의 응답으로 강의를 진행한다.','점차 글로벌화 되어가는 현대 사회를 살아가는데 있어서 유익한 정보를 얻는 방법 중 하나는 영어로 쓰인 매체를 효과적으로 이용하는 것이다. 이 강좌는 Korea Times, Korea Herald 등 국내 주요 영자 신문뿐만 아니라 New York Times, Washington Post, Time, Newsweek, Economist 등 세계적인 주요 정기 간행물에 실린 여러 가지 다양한 시사성의 글을 읽음으로써 독해력을 향상시키고 동시에 국제 사회를 보는 안목을 길러 주는데 목적이 있다.','출석,10,중간고사,35,기말고사,35,과제보고서,20,수업태도,0,Quiz,0,기타,0','Reading the News,Pete Sharma,Thomson,2007','','',27),('2014/2-0000-3-0910-02','This class is designed to improve students reading and language skillsthrough current English.','This course deals with current issues through reading articles from various areas. Students will learn and improve their reading and language skills. The articles from different subject areas will also provide insightful understandings on the happenings around the globe.','출석,10,중간고사,30,기말고사,30,과제보고서,10,수업태도,10,Quiz,10,기타,','Issues Now in the News,Adam Worcester,Compass Publishing,2008','','',30),('2014/2-0000-3-0910-03','1.Voice of America 뉴스와 BBC, CNN 뉴스를 듣고 정치, 경제, 과학, 문학, 문화 등 다양한 주제에 대한 지식을 얻고 이해를 높인다. \n2.영자신문과 잡지에서 발췌한 중요한 시사 관련 기사를 읽고 토론하고 글을 쓴다.','1. 시사성 있는 영어 뉴스를 듣고, 읽고, 토론한 후 자신의 의견을 글로 작성함으로써 listening, reading, speaking, writing 분야의 실력을 향상시킨다.\n2. 시사영어 읽기와 듣기를 통해서 국제화 시대에 필요한 정치. 경제.문화. 과학에 대한 지식과 이해를 기른다.','출석,10,중간고사,30,기말고사,30,과제보고서,15,수업태도,5,Quiz,10,기타,0','Listening to the News 3: Voice of America,Karl Nordvall,Compass Publishing,2012','','',24),('2014/2-0000-3-1879-01','본 강좌는 과거와는 구별되는 현대라는 시대에 대한 사상사적 맥락을 검토하고 나아가 ‘지금, 여기’를 살고 있는 우리에게 의미 있는 현대의 철학적 문제들을 검토함으로써 각자의 문제화 역량을 키우는 것에 역점을 둘 것이다. 나아가 이를 통해 수강생들 스스로 세계와 삶이라는 거대한 두 축을 이전보다 진지한 방식으로 성찰하도록 이끌 것이다.','1. 과거와는 구별되는 ‘현대’라는 시대가 담보하고 있는 특징과 문제들을 사상사적으로 조망하고 그 문제의식을 공유.\n2. 나아가 이러한 문제의식을 통해 보다 성숙한 식견을 겸비하고 자신의 삶에 대한 진지한 성찰을 하도록 이끔.','출석,15,수업태도,0,중간고사,20,기말고사,20,발표,25,토의,20,과제,0,Quiz','《현대 철학의 흐름》,박정호 외,동녘,1996','','',40),('2014/2-0000-3-1879-02','본 강좌는 과거와는 구별되는 현대라는 시대에 대한 사상사적 맥락을 검토하고 나아가 ‘지금, 여기’를 살고 있는 우리에게 의미 있는 현대의 철학적 문제들을 검토함으로써 각자의 문제화 역량을 키우는 것에 역점을 둘 것이다. 나아가 이를 통해 수강생들 스스로 세계와 삶이라는 거대한 두 축을 이전보다 진지한 방식으로 성찰하도록 이끌 것이다.','1. 과거와는 구별되는 ‘현대’라는 시대가 담보하고 있는 특징과 문제들을 사상사적으로 조망하고 그 문제의식을 공유.\n2. 나아가 이러한 문제의식을 통해 보다 성숙한 식견을 겸비하고 자신의 삶에 대한 진지한 성찰을 하도록 이끔.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Qui','요약자료- 핸드아웃','','',49),('2014/2-0000-3-2997-01','자연과 환경, 환경오염의 예방과 관리, 지구환경보존, 기업과 환경의 주제를 기본으로 하여 환경오염으로부터 야기되는 지구의 위기를 극복하고 생태계를 보존하기 위한 방법으로, \n-자원과 에너지의 효율적인 이용, \n-환경오염 규제 및 오염방지 기술 개발, \n-폐기물 발생억제 및 재활용 방안 \n등에 대하여 언급하고 대처방안에 대해 알아본다.','국내 및 국외의 산업화와 경제발전으로 인한 지구촌의 지역적 및 광역적인 다양한 환경오염과 이로 인하여 나타나는 피해와 부작용을 이해한다. 또한 이러한 환경오염을 예방할 수 있는 다양한 기술들에 대한 이해를 통하여 복합적인 문제해결능력을 배양하고 지구촌 공동체 의식을 함양하고자 한다. 뿐만 아니라 보고서 작성, 토론 및 발표를 통해 다양한 의사표현력을 높이는 것 또한 본 교과목의 목적이다.','출석,20,수업태도,10,중간고사,20,기말고사,20,발표,10,토의,10,과제,10,Qu','사회와 환경,환경교재연구회,녹문당','김현아','hyun_ah0@nate.com',63),('2014/2-0000-3-3200-01','인간의 신체는 사용하면 할수록 더욱 강해지고 발달하는 반면, 사용하지 않으면 오히려 약해지고 쇠퇴한다. 운동은 현대인의 건강한 체력을 유지시키는데 가장 필수적인 요건으로서, 건강한 삶을 위해서는 반드시 꾸준한 운동이 행해져야 한다.','운동에 관한 과학적 지식을 토대로 건강을 유지, 향상시키기 위한 실제적 방법을 알기 쉽게 설명하여, 새로운 지식과 건강 정보를 제공하고자 한다.','출석,20,중간고사,30,기말고사,30,과제보고서,10,수업태도,10,Quiz,0,기타,0','ppt 자료','심상훈','simsanghoon@naver.com',71),('2014/2-0000-3-3200-02','본 교과목은 현대인들이 건강을 유지 증진하는데 있어서, 올바른 운동방법 및 건강관리법을 제시하므로서 대학인의 삶의 질을 높이는데 있다.','특히 운동(exercise)과 건강(health)를 통하여 근육량을 증가시키고, 체지방을 감소시켜 성인병(adult diesease) 예방은 물론 질환별 운동방법을 통해 건강한 삶을 영위할 수 있는 방법을 제시해 주는 것이 학습목표이다.','출석,20,중간고사,30,기말고사,30,과제보고서,15,수업태도,5,Quiz,0,기타,0','ppt 자료','','',70),('2014/2-0000-3-3200-03','현대 사회에서는 과학 기술과 의학의 발달로 영양 부족, 과도한 신체 노동, 전염성 질환 등으로 건강 문제는 상당 부분 해소되었으나, 기계 문명의 발달로 인한 운동부족, 복잡한 생활에서 오는 정신적 스트레스, 영양 과잉으로 인한 비만, 환경오염으로 인한 유해 물질의 증가 드이 새로운 건강 위협 요인으로 등장 하였다. 따라서 이러한 위협 요인을 해결하기 위한 개인의 건전한 생활 방식의 필요를 충족시키고자 한다.','1. 현대 생활에서 건강의 의미와 건강 관리의 중요성을 이해한다.\n2. 건강을 증진시키기 위한 운동 처방의 방법을 이해하고 활용한다.\n3. 체력 향상을 위한 운동 프로그램을 구성하여 실생활에 활용한다.','출석,20,중간고사,30,기말고사,30,과제보고서,15,수업태도,5,Quiz,0,기타,0','현대인의 건강과 운동이야기,임재현, 김남영, 배영훈,대경북스,2013','김경환','akdmf4022@hotmail.com',75),('2014/2-0000-3-3240-01','3-4학년 학생들에게 최근 소위 기업이 바라는 인재상을 이해시키고, 채용허들, 조직적응, 문제해결 등 성공취업의 구체적인 구비요건을 숙지시켜 기업의 서류 및 면접전형에 실질적으로 대비하고자 함.','1. 기업과 사회가 바라는 인재상에 대한 모색과 그 트렌드에 대한 이해를 목적으로 함. \n2. 2014년 하반기 취업시즌에 대비한 전략적 성공취업 분기점 마련의 계기를 목적으로 함.','출석,60,중간고사,0,기말고사,0,과제보고서,20,수업태도,20,Quiz,0,기타,0','성공취업의 새로운 전략,김정권,cnu press,2007','김경환','akdmf4022@hotmail.com',146),('2014/2-0000-3-3240-02','3-4학년 학생들에게 최근 소위 기업이 바라는 인재상을 이해시키고, 채용허들, 조직적응, 문제해결 등 성공취업의 구체적인 구비요건을 숙지시켜 기업의 서류 및 면접전형에 실질적으로 대비하고자 함.','1. 기업과 사회가 바라는 인재상에 대한 모색과 그 트렌드에 대한 이해를 목적으로 함. \n2. 2014년 하반기 취업시즌에 대비한 전략적 성공취업 분기점 마련의 계기를 목적으로 함.','출석,60,중간고사,0,기말고사,0,과제보고서,20,수업태도,20,Quiz,0,기타,0','성공취업의 새로운 전략,김정권,cnu press,2007','김경환','akdmf4022@hotmail.com',146),('2014/2-0000-3-3924-01','자본주의는 어떻게 형성, 변화, 발전하여 왔는가, 자본주의 경제는 정치제도와 사회시스템 및 문화와 어떤 관련성을 갖고 있는가를 역사적으로 고찰한다. \n근대 상업자본주의와 세계체제, 중상주의, 시민혁명, 산업혁명, 자유주의 경제학, 사회주의와 노동운동, 경제대공황, 케인즈주의와 수정자본주의, 신자유주의적 세계화 등 자본주의의 주요 역사적 전개를 파악한다. 자본주의의 주요 국면이 낳은 정치적 사회적 문화적 변화, 성장과 분배, 시장중심주의의 성과와 한계 등에 대해 살펴본다.','1. 역사적으로 형성된 자본주의의 본질과 특성 및 장단점을 이해하고 경제적 자유를 추구하면서도 사회적 약자들을 포용하는 인간적 자본주의의에 대해 성찰한다. \n2. 자본주의와 연결된 현대세계의 다양한 복합문제에 대한 주체적인 해결방안을 모색한다. \n3. 자본주의 체제의 전세계적 확대 적용 변용 등을 이해하여 글로벌 마인드를 고취한다.','출석,20,수업태도,0,중간고사,40,기말고사,40,발표,0,토의,0,과제,0,Quiz,0','장기 20세기,지오반니 아리기,그린비','','',56),('2014/2-0000-3-3931-01','동서를 막론하고 고전은 인간 삶의 지혜를 담은 것으로 역사의 전범이 되어 왔다. 중국의 고전 사서삼경과 한국의 고전 한문학 작품들을 중심으로 동양고전의 역사적 배경을 이해하고 고전의 주요한 내용들을 학습한다. 고전에 담긴 세계관과 사상의 심오함과 다양성을 이해하고 그 현대적 함의를 파악한다. \n동아시아 고전이 현대사회에 어떤 유의미성을 갖는지를 토론한다.','1. 동아시아 고전이 전해주는 인간존재와 삶의 가치들을 파악하여 인성교양 지식을 확대한다. \n2. 동아시아 고전이 지닌 세계관과 사상의 현대적 유용성을 파악하여 일상생활의 적재적소에 활용할 수 있도록 복합문제 해결 능력을 배양한다. \n3. 동아시아 고전에 대한 이해를 통해 아시아의 문화 정체성을 재정립하면서 글로벌 마인드를 고취한다.','출석,10,수업태도,0,중간고사,20,기말고사,50,발표,10,토의,10,과제,0,Quiz','논어교양강의,진순신,돌베개,2010','','',37),('2014/2-0000-3-3934-01','우리는 현재 우리가 살고 있는 시대의 특성을 어떻게 정의할 수 있을까? 본 강좌는 전근대, 근대, 탈근대의 정의 및 특징들을 살펴봄으로써 시대의 흐름에 따른 변화를 개괄한다. 역사, 문화 및 철학적 관점에서 시대의 변화를 학습하고 현재란 고정된 순간이 아니라 그러한 변화의 한 과정임을 이해한다. 학생들은 각 시기의 특징적인 예술 작품들을 조사하고 이에 대해 토론한다.','1. 고전에 관한 수업과 대중문화에 관련된 수업이 서로 다른 전제를 가지고 있음을 알게 된다. 즉 시대의 변화에 대한 지도를 그리고 각 시기의 서로 다른 특징을 중심으로 교양과목 일반에 대한 접근방식을 이해한다. \n2. 전근대, 근대, 탈근대의 정의, 특징 및 현상을 살펴본다.\n3. 개별적 체험을 통시적 지도 속에서 재조명하여 이론적으로 설명할 수 있다.\n4. 포스트모더니즘의 위치를 이해하고 이를 변화의 한 과정으로 인식하며 현재와는 다른 미래를 구상해본다.\n5. 서로 다른 시대적 맥락에서 생산된 예술작품을 비교분석하고 그 차이를 이해한다.','출석,10,수업태도,0,중간고사,30,기말고사,40,발표,0,토의,0,과제,20,Quiz,','철학과 굴뚝 청소부,이진경,그린비,2005','','',60),('2014/2-0000-3-3935-01','진정한 보편성 및 개별성은 한국의 문화유산을 타문화의 유산들과 비교분석하는 과정에서 드러나게 된다. 본 강좌는 동아시아 국가 중 한국, 중국, 일본의 문화유산을 비교분석하여 각 문화의 정신적 공통 기반 및 차별성을 개괄한다. 강의는 일상의 의식주, 삶과 죽음과 같은 보편적인 주제를 중심으로 이와 관련된 한국, 중국, 일본의 문화유산을 살펴보는 과정으로 구성된다. 학생들은 이와 관련된 동아시아 문화의 역사적 배경을 조사하고 이에 대해 토론하게 된다.','1. 한국, 중국, 일본의 대표적 문화유산들을 이해하고 이에 대해 설명할 수 있는 수준에 이른다.\n2. 각 나라가 공유하는 정신적 기반을 이해하고 이에 따른 문화의 보편성을 인식한다.\n3. 각 문화의 체현물들을 비교분석함으로써 우리 문화유산의 정체성을 이해한다. \n4. 100단위 및 200단위에서 이수한 과목들의 내용을 보다 포괄적인 관점에서 재조명한다.\n5. 보다 폭 넓고 깊이 있는 과거에 대한 지도를 그리고, 이를 바탕으로 현재를 재조명하며, 궁극적으로 미래의 문화적 발전에 기여할 수 있는 기반을 조성한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Qui','한국인을 위한 중국사,신성곤 외','','',41),('2014/2-0000-3-3936-01','헬레니즘과 히브리즘은 서양문화의 기반으로서 개별 텍스트들의 사상적 원형으로 기능한다. 본 강좌는 유럽의 문화유산이 구축된 공통기반을 살펴보고 이를 바탕으로 고대 그리스에서 현대에 이르는 문화유산들을 개괄한다. 강의는 구체적 작품 및 대상을 중심으로 통시적 관점에서 문학, 미술, 음악, 건축 등의 문화유산을 살펴보는 과정으로 구성된다. 학생들은 유럽의 문화유산에 대한 지식이 어떤 방식으로 현재 자신의 일상에 도움이 될 수 있는가에 대해 토론한다.','1. 유럽의 세계문화유산들을 이해하고 이에 대해 설명할 수 있는 수준에 이른다.\n2. 공연, 전시 등의 문화적 경험을 체험하고 이를 자신의 일부로 동화시킨다.\n3. 우리 문화를 바탕으로 유럽의 문화유산을 동화하는 방식을 모색하고 이를 창조적인 방식으로 자신의 생활 및 미래에 연계시킨다.\n4. 100단위 및 200단위에서 이수한 과목들의 내용을 보다 포괄적인 관점에서 재조명한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,10,토의,0,과제,10,Qui','수업 중 제시하는 강의안','','',71),('2014/2-0000-3-3946-01','남성과 여성에 대한 기본적인 이해를 바탕으로 성역할, 성차, 성차별, 이성교제, 연애의 문화, 성폭력, 포르노그래피 등에 관하여 학습하게 하고 이를 통해 남녀 간의 갈등과 사랑의 문제를 이해하고 해결하는 능력을 배양시키고자 한다.','1. 성의 평등을 실현하기 위해 여성과 남성을 편견 없는 시각으로 이해하도록 한다.\n2. 성숙한 사랑의 이해를 통해 보다 성숙한 이성교제와 결혼 등을 선택하게 돕는다.\n3. 현대사회의 가족의 해체를 이해하고 다양한 결혼형태와 가족형태를 수용할 수 있는 열린 태도를 갖도록 한다.','출석,10,수업태도,10,중간고사,20,기말고사,30,발표,0,토의,10,과제,20,Qui','여성학,이재경외,동녘,2007','나찬울','nacw1506@naver.com',46),('2014/2-0000-3-3946-02','남성과 여성에 대한 기본적인 이해를 바탕으로 성역할, 성차, 성차별, 이성교제, 연애의 문화, 성폭력, 포르노그래피 등에 관하여 학습하게 하고 이를 통해 남녀 간의 갈등과 사랑의 문제를 이해하고 해결하는 능력을 배양시키고자 한다.','1. 성의 평등을 실현하기 위해 여성과 남성을 편견 없는 시각으로 이해하도록 한다.\n2. 성숙한 사랑의 이해를 통해 보다 성숙한 이성교제와 결혼 등을 선택하게 돕는다.\n3. 현대사회의 가족의 해체를 이해하고 다양한 결혼형태와 가족형태를 수용할 수 있는 열린 태도를 갖도록 한다.','출석,10,수업태도,10,중간고사,30,기말고사,30,발표,0,토의,10,과제,10,Qui','새 여성학 강의,한국여성연구소,동녁,2005','','',48),('2014/2-0000-3-3946-03','남성과 여성에 대한 기본적인 이해를 바탕으로 성역할, 성차, 성차별, 이성교제, 연애의 문화, 성폭력, 포르노그래피 등에 관하여 학습하게 하고 이를 통해 남녀 간의 갈등과 사랑의 문제를 이해하고 해결하는 능력을 배양시키고자 한다.','1. 성의 평등을 실현하기 위해 여성과 남성을 편견 없는 시각으로 이해하도록 한다.\n2. 성숙한 사랑의 이해를 통해 보다 성숙한 이성교제와 결혼 등을 선택하게 돕는다.\n3. 현대사회의 가족의 해체를 이해하고 다양한 결혼형태와 가족형태를 수용할 수 있는 열린 태도를 갖도록 한다.','출석,10,수업태도,10,중간고사,20,기말고사,30,발표,0,토의,10,과제,20,Qui','여성학,이재경외,동녘,2007','박진성','pjswm@naver.com',49),('2014/2-0000-3-4650-01','In order for students to present effectively in English, they must, above all, practice. This class focuses on doing actual presentations in order to accomplish that goal. Students will gain knowledge of a variety of useful expressions and organizational skills required to express themselves clearly and intelligently.','Students will learn presentation structure. This starts with becoming familiar with the introduction, body and conclusion.From there, we will include how to make points clearly through the use of examples. Other course goals are to improve vocabulary and grammar skills, improve discussion ability in English, and finally, to increase competence and confidence in using English.','출석,10,중간고사,25,기말고사,35,과제보고서,0,수업태도,20,Quiz,0,기타,10','No textbook required','','',15),('2014/2-0000-3-4650-02','In order for students to present effectively in English, they must, above all, practice. This class focuses on doing actual presentations in order to accomplish that goal. Students will gain knowledge of a variety of useful expressions and organizational skills required to express themselves clearly and intelligently.','Students will learn presentation structure. This starts with becoming familiar with the introduction, body and conclusion.From there, we will include how to make points clearly through the use of examples. Other course goals are to improve vocabulary and grammar skills, improve discussion ability in English, and finally, to increase competence and confidence in using English.','출석,10,중간고사,25,기말고사,35,과제보고서,0,수업태도,20,Quiz,0,기타,10','No textbook required','','',14),('2014/2-0000-3-4650-03','This course will take a step-by-step approach to developing presentation skills and encourage students to find their own effective style for presenting. Students must actively participate in class activities and prepare presentations.','The objective of this course is to help students to build and improve skills and knowledge required to make effective presentations in English. They will learn how to go about structuring and shaping a presentation, and how to use English to maximum effect during a presentation.','출석,10,중간고사,30,기말고사,40,과제보고서,0,수업태도,10,Quiz,0,기타,10','no textbook required','','',15),('2014/2-0000-3-4650-04','영어 프레젠테이션 발표 기초 단계부터 실전 단계까지 스킬을 연마한다.','영어 프레젠테이션 초기 단계부터 메시지를 구성하는 기획단계까지 정확환 프로세스 이해한다.','출석,10,중간고사,30,기말고사,30,과제보고서,0,수업태도,20,Quiz,10,기타,0','영어프레젠테이션 불패노트,이지윤,길벗이지톡,2013','','',14),('2014/2-0000-3-4717-01','경제질서가 고도로 복잡화된 현대사회에서 금융경제에 대한 이해는 현명한 경제활동을 위한 필수적 지혜이다. 본 교과목은 현대인들의 실생활에서 필요한 다양한 금융지식의 교육에 초점을 맞춘다. 주요 내용으로는 평생소비계획에 따른 수입과 지출의 관리, 자산과 부채에 대한 종합적 관리요령, 다양한 금융상품상품의 종류와 특성에 대한 이해, 현명한 재무의사결정을 위한 경제학적 기초지식 등을 다룬다.','1. 돈에 대한 올바른 가치관을 확립한다. \n2. 건전한 소비생활과 개인신용 관리능력을 배양한다. \n3. 금융지식과 금융역량을 제고한다.','출석,10,수업태도,10,중간고사,40,기말고사,30,발표,0,토의,0,과제,10,Quiz','행복한 부자 되기, 2판,김기영, 송영출,비즈프라임,2012','송단양','songdanyang@naver.com',95),('2014/2-0000-3-4717-02','경제질서가 고도로 복잡화된 현대사회에서 금융경제에 대한 이해는 현명한 경제활동을 위한 필수적 지혜이다. 본 교과목은 현대인들의 실생활에서 필요한 다양한 금융지식의 교육에 초점을 맞춘다. 주요 내용으로는 평생소비계획에 따른 수입과 지출의 관리, 자산과 부채에 대한 종합적 관리요령, 다양한 금융상품상품의 종류와 특성에 대한 이해, 현명한 재무의사결정을 위한 경제학적 기초지식 등을 다룬다.','1. 돈에 대한 올바른 가치관을 확립한다. \n2. 건전한 소비생활과 개인신용 관리능력을 배양한다. \n3. 금융지식과 금융역량을 제고한다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,0,과제,10,Quiz','행복한 부자되기,김기영 송영출,두남,2012','송단양','songdanyang@naver.com',95),('2014/2-0000-3-4735-01','본 교과목은 고도 금융사회를 살아갈 예비 사회인에게 필요한 자산관리의 기본 개념과 실천 가능한 재테크의 다양한 노하우를 습득하게 한다. 구체적으로는 금융자산과 실물자산에 대한 이해, 인생주기별로 적합한 자산관리 및 투자전략, 다양한 금융상품의 이해 및 이용방법, 자산으로서의 부동산의 활용 및 관리방안 등의 내용을 다룬다.','1. 합리적 자산관리와 다양한 재테크 기법을 연구함으로써 풍요로운 Life cycle을 준비할 수 있다. \n2. 경제 기초 지식, 금융 교육 효과로 자산 투자 및 관리에 대한 새로운 인식으로 미래의 인생 설계에 도움을 준다.\n3. 급변하는 경제환경변화에 주체적으로 대응할 수 있는 자립적이고 합리적인 경제인 마인드를 체득케 한다.','출석,10,수업태도,10,중간고사,20,기말고사,30,발표,0,토의,0,과제,30,Quiz','실전 개인재무설계,임계희,미래의 창,2007','양재영','achasic@naver.com',64),('2014/2-0000-3-4930-01','This course is designed to help students prepare for discussions and interviews that they might need in the process of applying for jobs. They are going to learn frequently used colloquial English expressions, correct pronunciation and stress, natural intonation, as well as discussion and interview skills. \n \nAfter this course, students will be able to maintain discussions and interview with accuracy of meaning and confidence. Students will gain knowledge of a variety of useful expressions and organizational skills required to express themselves clearly and intelligently.','This course seeks to improve students skills in discussions and interviews using English in order to prepare for future employment.','출석,10,중간고사,30,기말고사,30,과제보고서,15,수업태도,5,Quiz,0,기타,10','No textbook required','','',18),('2014/2-0000-3-4930-02','This course is designed to help students prepare for discussions and interviews that they might need in the process of applying for jobs. They are going to learn frequently used colloquial English expressions, correct pronunciation and stress, natural intonation, as well as discussion and interview skills. \n \nAfter this course, students will be able to maintain discussions and interview with accuracy of meaning and confidence. Students will gain knowledge of a variety of useful expressions and organizational skills required to express themselves clearly and intelligently.','This course seeks to improve students skills in discussions and interviews using English in order to prepare for future employment.','출석,10,중간고사,30,기말고사,30,과제보고서,15,수업태도,5,Quiz,0,기타,10','No textbook required','','',19),('2014/2-0000-3-4930-03','This class aims at developing students practical English communicative competence required for getting their future jobs. Class activities include preparing for resumes and the letter of self introduction and participating in various job-relating simulations such as job-interviews and presentations.','1. Prepare appropriate documents in English: letter of introduction, resume, etc.\n2. Prepare job interviews and presentations.','출석,10,중간고사,40,기말고사,40,과제보고서,0,수업태도,10,Quiz,0,기타,0','no textbook required','','',19),('2014/2-0000-3-4930-04','This class aims at developing students practical English communicative competence required for getting their future jobs. Class activities include preparing for resumes and the letter of self introduction and participating in various job-relating simulations such as job-interviews and presentations.','1. Prepare appropriate documents in English: letter of introduction, resume, etc.\n2. Prepare job interviews and presentations.','출석,10,중간고사,40,기말고사,40,과제보고서,0,수업태도,10,Quiz,0,기타,0','no textbook required','','',17),('2014/2-0000-3-5657-01','본 강좌는 동양적 예술 작품에 대한 이해를 통하여 전통예술의 가치와 그것의 미학적 의미를 학습하고 단순한 작품의 이해에 그치는 것이 아니라 보다 궁극적으로는 그러한 작품 활동의 바탕이 되고 있는 동양 사상을 검토하여 보다 통합적인 차원에서의 인문학적 성찰을 꾀한다. 한편, 도구적인 차원에서는 이러한 인문학적, 예술적 성찰을 문화산업이나 콘텐츠 제작에 적용하여 전통적 가치를 새로운 미디어에 담아낼 수 있는 창조력의 배양으로 이끌 것이다.','1. 동양의 예술 작품에 대한 미학적 접근을 통한 \n 동양 사상 전반에 대한 학습.\n2. 이러한 인문학적, 예술적 이해를 바탕으로 하여 \n 문화산업이나 콘텐츠 제작의 측면에서의 문화가치를 \n 재창출 하는 활용의 효과를 기대','출석,10,수업태도,0,중간고사,20,기말고사,20,발표,10,토의,0,과제,15,Quiz','(중국)회화예술,린츠, 배연희 올김,도서출판 대가','이은혜','thex1004@naver.com',81),('2014/2-0000-3-5658-01','뉴미디어와 이미지에 대한 인문학적 접근을 통해 다양한 문화콘텐츠의 기반을 이해한다.\n문학이나 다양한 예술 작품 속의 이미지가 영화, 광고 등 현대의 다양한 매체에 어떻게 변용 및 활용되는가를 살펴본다. \n수강생들이 직접 다양한 이미지를 활용해 UCC 등의 뉴미디어로 제작하여 이를 발표하고 이에 대한 비평을 시도한다.','1. 이미지의 홍수 시대에 이미지의 활용에 대한 인문학적 이해를 도모한다.\n2. 디지털 시대를 맞아 지성과 감성, 기술과 인간의 융합을 이해하고, 인문학의 응용 및 그 실용적 성격, 미래적 비전을 생각해 본다.\n3. 뉴미디어와 이미지의 활용에 대한 주체적 수용과 비평의식을 고취한다.','출석,10,수업태도,5,중간고사,30,기말고사,30,발표,15,토의,0,과제,10,Quiz','강의자료','','',59),('2014/2-0000-3-5664-01','전쟁 발발의 총체적인 원인과 종전 후의 사회와 세계질서 변화를 역사적으로 고찰한다.\n세계사에 영향을 미친 주요 전쟁들의 원인과 진행과정, 전쟁이 변화시킨 정치, 경제, 사회, 문화 양상과 국제질서 등을 이해한다. 예들 들어 나폴레옹 전쟁과 빈 체제의 형성, 1차 세계대전과 베르사유 체제, 2차 세계대전과 국제연합의 탄생, 한국전쟁과 냉전 및 그 외의 주요 전쟁과 전쟁 이후의 국제질서 변화를 분석한다. \n전쟁과 국제질서의 변화가 추동한 사회문화적 변동에 대해 토론한다.','1. 전쟁사를 통해 국가와 사회에 대한 미래적 사고와 복합문제 해결 능력을 증진시킨다. \n2. 전쟁방지와 평화유지 방안에 대해 성찰하면서 인간 본성에 대해 깊이 있는 사고와 역사에 대한 종합적 분석력을 향상시킨다.','출석,10,수업태도,0,중간고사,40,기말고사,40,발표,0,토의,10,과제,0,Quiz,','전쟁의 세계사,윌리엄 맥닐,이산','','',62),('2014/2-0000-3-5665-01','한국의 과학기술에 대한 역사적 접근을 통해 전통 과학기술의 내용과 의미를 파악하고, 전통과학과 근대과학의 관계, 근대과학의 전파, 이식, 수용, 변용의 과정, 그리고 과학기술문명의 역사적 사회적 의미를 분석한다. \n한국의 전통 과학기술을 천문, 건축, 무기, 의학 등의 영역으로 구분하여 살펴본다. 한국의 과학기술문명이 어떤 시대적 맥락의 산물인지 토론한다.','1. 한국 과학기술발전의 배경과 전개과정을 이해하여 한국인의 과학정신과 기술문화에 대한 자긍심을 고취한다. \n2. 한국의 과학기술 발달과정을 동아시아 과학기술, 서양과학기술과 비교하면서 한국의 고유한 과학기술 지식체계가 지닌 역사적 의미를 파악한다. \n3. 21세기 과학기술문명의 시대에 과학기술의 사회적 의미에 대해 성찰하는 기회를 마련한다.','출석,10,수업태도,10,중간고사,20,기말고사,40,발표,10,토의,0,과제,10,Qui','참고 문헌 강의 시간에 제시','','',57),('2014/2-0000-3-5666-01','역사학의 주요 주제들을 다룬 동서양 각국과 한국 영화들의 과거의 재현방식을 이해한다. 역사적 실체와 영화에 재현된, 즉 연출된 역사를 비교하는 비평 방법론을 습득한다. \n초반 강의를 통해 습득한 이론적 방법론적 기초지식을 바탕으로 조별 영화 감상과 비평, 집단 토론을 통해 영화화된 역사의 재해석을 시도한다.','1. 역사의 주요 테마를 다루는 한국과 세계 각국의 영화들을 상호 비교함으로써 역사이해를 증진시킴과 동시에 글로벌 마인드를 고취시킨다.\n2. 영화로 연출된 역사에 대한 재해석과 비평을 시도하여 주체적 역사이해와 분석능력을 배양한다.\n3. 역사콘텐츠와 영상문화의 결합 가능성을 모색하여 문화콘텐츠 산업에 대한 인문학적 이해를 심화시킨다.','출석,10,수업태도,0,중간고사,30,기말고사,40,발표,10,토의,0,과제,10,Quiz','강의시 제공하는 ppt 및 영상자료','','',68),('2014/2-0000-3-5669-01','세계 각지에서 발생한 사건들이 다른 지역에 실시간대로 영향을 미치고 있는 21세기 지구촌 사회를 맞아 국제관계의 다양한 현상에 대한 이해와 대비가 절실해지고 있다. 자원 확보를 둘러싼 국가 간 경쟁, 환경오염, 보건위생문제, 마약거래, 국제테러, 기후변화 등 국경을 초월해서 일어나는 국제적 이슈들에 대해 고찰하고 그 대응책을 논의한다.','1. 자원문제, 환경문제, 국제테러, 기후변화 등 21세기 인류의 새로운 도전요인에 대한 이해력을 기른다. \n2. 글로벌시대 세계시민의 일원으로서 국제적 문제에 대해 주체적이고 능동적으로 대처할 수 있는 능력을 함양한다.\n3. 지구촌시대에 적합한 국제관계와 적합하지 않은 국제관계를 조명해 봄으로써 이 시대에 적합한 글로벌 마인드를 고취시킨다.','출석,10,수업태도,10,중간고사,30,기말고사,40,발표,0,토의,10,과제,0,Quiz','세계화?,토머스 슈뢰더,푸른나무,2007','','',49),('2014/2-0000-3-5672-01','글로벌리제이션을 어떻게 이해할 것인가? 글로벌 시대에 인류 사회가 봉착한 당면 현안은 무엇이며 우리는 이에 어떻게 대처할 것인가? 본 교과목에서는 글로벌리제이션과 연관된 전세계적 변화 양상을 양극화 문제를 중심으로 살펴본다. 구체적으로는 장기적·거시적 관점에서의 세계사적 전환, 탈냉전 이후 국제관계의 변화, 자본주의 세계경제의 동학, 지역질서와 지방문화의 변동 등의 다양한 주제들이 포함된다. 현재 인류사회가 직면한 양극화의 문제점들을 다각도에서 살펴봄으로써 글로벌리제이션의 원인·동학·경향 등을 이해하고, 그 결과 초래되는 여러가지 사회문제들을 극복할 수 있는 실천적 대안들을 모색해본다.','1. 글로벌 시대의 주체로서 인간과 세계에 대한 이해력 증진을 통하여 급변하는 전지구적 상황에 대처하는 글로벌 마인드를 기른다. \n2. 글로벌 시대의 무한경쟁 속에서 발생하는 사회적 및 경제적 양극화에 대한 다양한 사례조사를 통하여 사회구조상 발생하는 제 현상 등에 대한 쟁점들을 분석할 수 있는 능력을 갖춘다.\n3. 글로벌 시대의 다양한 사회문제들을 어떻게 해결할 것인지에 대하여 자신의 주장을 논리적으로 전개하고 명확하게 전달할 수 있는 능력을 심화한다.','출석,10,수업태도,0,중간고사,30,기말고사,40,발표,20,토의,0,과제,0,Quiz,','누가 금융 세계화를 만들었나,에릭 헬라이너,후마니타스,2010','','',52),('2014/2-0000-3-5672-02','글로벌리제이션을 어떻게 이해할 것인가? 글로벌 시대에 인류 사회가 봉착한 당면 현안은 무엇이며 우리는 이에 어떻게 대처할 것인가? 본 교과목에서는 글로벌리제이션과 연관된 전세계적 변화 양상을 양극화 문제를 중심으로 살펴본다. 구체적으로는 장기적·거시적 관점에서의 세계사적 전환, 탈냉전 이후 국제관계의 변화, 자본주의 세계경제의 동학, 지역질서와 지방문화의 변동 등의 다양한 주제들이 포함된다. 현재 인류사회가 직면한 양극화의 문제점들을 다각도에서 살펴봄으로써 글로벌리제이션의 원인·동학·경향 등을 이해하고, 그 결과 초래되는 여러가지 사회문제들을 극복할 수 있는 실천적 대안들을 모색해본다.','1. 글로벌 시대의 주체로서 인간과 세계에 대한 이해력 증진을 통하여 급변하는 전지구적 상황에 대처하는 글로벌 마인드를 기른다. \n2. 글로벌 시대의 무한경쟁 속에서 발생하는 사회적 및 경제적 양극화에 대한 다양한 사례조사를 통하여 사회구조상 발생하는 제 현상 등에 대한 쟁점들을 분석할 수 있는 능력을 갖춘다.\n3. 글로벌 시대의 다양한 사회문제들을 어떻게 해결할 것인지에 대하여 자신의 주장을 논리적으로 전개하고 명확하게 전달할 수 있는 능력을 심화한다.','출석,10,수업태도,0,중간고사,30,기말고사,40,발표,20,토의,0,과제,0,Quiz,','누가 금융 세계화를 만들었나,에릭 헬라이너,후마니타스,2010','','',34),('2014/2-0000-3-5682-01','디자인은 21세기 문화의 필수요소로서 어떤 문화 및 산업영역에서도 디자인에 대한 논의를 제외할 수는 없다. 본 강좌는 디자인의 기본이론을 개괄하고 이를 구체적인 사례에 초점을 맞추어 실천적인 방식으로 살펴본다. 강의는 디자인에 대한 기초적 이론을 바탕으로 문화에 나타난 시각 디자인, 산업 디자인, 환경 디자인 등을 다루게 되며, 학생들은 자신의 생활과 밀접하게 관련된 디자인의 역할에 대해 조사하고 이를 응용하는 창작물을 만들게 된다.','1. 디자인의 이론를 이해하고 그 중요성을 인식한다.\n2. 디자인의 각 영역을 숙지하고 자신의 전공에 관련된 분야에서 디자인이 맡은 역할을 이해한다.\n3. 디자인 창작물을 만들고 자신의 작품에 대해 토론하는 과정을 통해 상상력 및 창조력을 배양한다.\n4. 미래의 재현방식에 대해 상상해보고 이를 통해 새로운 미래의 생활을 설계한다.','출석,20,중간고사,30,기말고사,30,과제보고서,10,수업태도,10,Quiz,0,기타,0','디자인개론,김병억, 이웅직,태학원,2001','윤인성','yis12345@nate.com',95),('2014/2-0000-3-5765-01','\"현대 디지털 미디어 아트와 테크놀로지 기반의 예술 작품을 자료를 통해 감상하고 이해한다. 현대 음악과 미술에서 뉴미디어 아트가 갖는 미학적 위상과 총체적 예술작품으로서의 가능성을 살펴본다. 또한 당해 학기에 선정된 디지털 미디어를 위한 테크놀로지를 숙지하고 간단한 디지털 미디어 작품을 만들어 본다. 또한 인터액티브 미디어의 작품화 가능성을 탐구한다.','1. 디지털 미디어 아트의 개념을 이해한다\n2. 미디어 아트의 미학적 위상을 이해한다\n3. 다양한 미디어 아트 기법을 이해한다\n4. 미디어 아트를 위한 기법을 익힌다\n5. 디지털 미디어 아트 작품을 만든다','출석,10,수업태도,10,중간고사,25,기말고사,25,발표,10,토의,0,과제,20,Qui','프로세싱, 날개를 달다.,다니엘 쉬프만 저/랜덤웍스 역,비제이퍼블릭,2011','','',21),('2014/2-0000-3-5907-01','직장생활에서 흔히 접할 수 있는 가장 일반적인 상황 - 자신을 소개하기, 회의나 일정의 조정, 보고서 쓰기, 물건구매 주문 등- 에서 서면적 의사 소통에 필요한 다양한 서식을 이해하고 작성하는 방법을 익힌다.','실용문을 작성하는데 필요한 문장구성, 문법, 어휘를 학습하고 반복적으로 연습해 봄으로써 취업시 경험할 비지니스 상황에서 서면적 의사 소통을 자신있게 할 수 있도록 한다.','출석,10,중간고사,30,기말고사,30,과제보고서,20,수업태도,10,Quiz,0,기타,0','Writing for the Real World 2 -an Introduction to Business Writing,Roger Barnard, antoinette Meehan,O','','',20),('2014/2-0000-3-5907-02','This course will develop the communication skills you need to succeed in business and cover common types of speaking and writing that occur in the business world.','The course has two main goals:\n1) to promote fluency in speaking and correctness in writing Business English\n2) to provide language models that will be directly relevant to students real needs.','출석,10,중간고사,30,기말고사,30,과제보고서,10,수업태도,0,Quiz,0,기타,20','Profile 2: intermediate,Jon Naunton,Oxford University Press,2005','','',23),('2014/2-0000-3-6026-01','*효율적인 의사소통에 내재된 철학을 배운다.\n*의사소통의 다양한 기술을 배우고 익힌다.\n*학생지도와 상담에 필요한 기법을 익힌다.','현대 사회에서 요구되는 능력중의 하나인 효과적인 의사소통 기술을 통해 상담력을 향상시키고 학생들을 지도 하는 능력을 배양하도록 한다.','출석,10,중간고사,26,기말고사,27,과제보고서,27,수업태도,10,Quiz,0,기타,0','상담의 필수기술,성숙진,나남출판,2006','윤명선','msun9017@hanmail.net',22);
/*!40000 ALTER TABLE `Detail_Information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Info_Table`
--

DROP TABLE IF EXISTS `Info_Table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Info_Table` (
  `ID` varchar(22) NOT NULL,
  `Year_Semester` char(8) NOT NULL,
  `Subject_Number` char(14) NOT NULL,
  `Professor_ID` int(11) NOT NULL,
  `Completion` char(2) NOT NULL,
  `Accreditation` char(2) DEFAULT NULL,
  `Subject_Time` varchar(15) DEFAULT NULL,
  `Subject_Time2` varchar(15) DEFAULT NULL,
  `Classroom` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Professor_ID` (`Professor_ID`),
  KEY `Subject_Number` (`Subject_Number`),
  CONSTRAINT `Info_Table_ibfk_1` FOREIGN KEY (`Professor_ID`) REFERENCES `Prof_Table` (`Professor_ID`),
  CONSTRAINT `Info_Table_ibfk_2` FOREIGN KEY (`Subject_Number`) REFERENCES `Sub_Table` (`Subject_Number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Info_Table`
--

LOCK TABLES `Info_Table` WRITE;
/*!40000 ALTER TABLE `Info_Table` DISABLE KEYS */;
INSERT INTO `Info_Table` VALUES ('2014/2-0000-1-0019-01','2014/2','0000-1-0019-01',1,'교선','','금 3','금 4','연204'),('2014/2-0000-1-0387-01','2014/2','0000-1-0387-01',2,'교선','','화 3','화 4','옥210'),('2014/2-0000-1-0387-02','2014/2','0000-1-0387-02',3,'교선','','목 3','목 4','옥210'),('2014/2-0000-1-0670-01','2014/2','0000-1-0670-01',4,'교선','','화 4','화 5','참B106'),('2014/2-0000-1-0670-02','2014/2','0000-1-0670-02',5,'교선','','월 2','월 3','참B107'),('2014/2-0000-1-0670-03','2014/2','0000-1-0670-03',4,'교선','','목 3','목 4','참B106'),('2014/2-0000-1-0670-04','2014/2','0000-1-0670-04',6,'교선','','금 3','금 4','한울B101'),('2014/2-0000-1-0670-05','2014/2','0000-1-0670-05',7,'교선','','수 5','수 6','한울B101'),('2014/2-0000-1-0709-01','2014/2','0000-1-0709-01',8,'교선','','','',''),('2014/2-0000-1-0709-02','2014/2','0000-1-0709-02',8,'교선','','','',''),('2014/2-0000-1-0806-01','2014/2','0000-1-0806-01',9,'교선','','월 3','수 4','비519'),('2014/2-0000-1-0806-02','2014/2','0000-1-0806-02',10,'교선','','목 7','목 8','한울404'),('2014/2-0000-1-0806-03','2014/2','0000-1-0806-03',9,'교선','','월 4','수 3','비519'),('2014/2-0000-1-0806-04','2014/2','0000-1-0806-04',11,'교선','','월 4','수 3','한울408'),('2014/2-0000-1-0858-01','2014/2','0000-1-0858-01',12,'교선','','수 3','수 4','옥210'),('2014/2-0000-1-0858-02','2014/2','0000-1-0858-02',12,'교선','','수 5','수 6','옥210'),('2014/2-0000-1-1077-01','2014/2','0000-1-1077-01',13,'교필','','월 1','수 2','문114'),('2014/2-0000-1-1077-02','2014/2','0000-1-1077-02',13,'교필','','월 2','수 1','문114'),('2014/2-0000-1-1077-03','2014/2','0000-1-1077-03',13,'교필','','월 3','수 4','문114'),('2014/2-0000-1-1077-04','2014/2','0000-1-1077-04',14,'교필','','월 4','수 3','문114'),('2014/2-0000-1-1077-05','2014/2','0000-1-1077-05',15,'교필','','월 5','수 6','연404'),('2014/2-0000-1-1077-06','2014/2','0000-1-1077-06',15,'교필','','월 6','수 5','연404'),('2014/2-0000-1-1077-07','2014/2','0000-1-1077-07',16,'교필','','화 1','목 2','한울305'),('2014/2-0000-1-1077-08','2014/2','0000-1-1077-08',16,'교필','','화 2','목 1','한울305'),('2014/2-0000-1-1077-09','2014/2','0000-1-1077-09',17,'교필','','화 3','목 4','한울305'),('2014/2-0000-1-1077-10','2014/2','0000-1-1077-10',14,'교필','','화 3','목 4','문119'),('2014/2-0000-1-1077-11','2014/2','0000-1-1077-11',14,'교필','','화 5','목 6','문119'),('2014/2-0000-1-1077-12','2014/2','0000-1-1077-12',17,'교필','','화 6','목 5','한울305'),('2014/2-0000-1-1077-13','2014/2','0000-1-1077-13',16,'교필','','금 5','금 6','연404'),('2014/2-0000-1-1077-14','2014/2','0000-1-1077-14',17,'교필','','금 3','금 4','문114'),('2014/2-0000-1-1077-15','2014/2','0000-1-1077-15',18,'교필','','화 7','화 8','문120'),('2014/2-0000-1-1077-16','2014/2','0000-1-1077-16',13,'교필','','월 7','월 8','문120'),('2014/2-0000-1-1077-17','2014/2','0000-1-1077-17',15,'교필','','수 7','수 8','문119'),('2014/2-0000-1-1077-18','2014/2','0000-1-1077-18',16,'교필','','화 7','화 8','문114'),('2014/2-0000-1-1077-19','2014/2','0000-1-1077-19',17,'교필','','목 7','목 8','연404'),('2014/2-0000-1-1077-20','2014/2','0000-1-1077-20',16,'교필','','화 4','목 3','한울305'),('2014/2-0000-1-1077-21','2014/2','0000-1-1077-21',19,'교필','','금 3','금 4','문120'),('2014/2-0000-1-1618-01','2014/2','0000-1-1618-01',20,'교선','','화 5','화 6','옥210'),('2014/2-0000-1-1618-02','2014/2','0000-1-1618-02',21,'교선','','수 5','수 6','옥207'),('2014/2-0000-1-1659-01','2014/2','0000-1-1659-01',22,'교선','','월 1','수 2','연205'),('2014/2-0000-1-1659-02','2014/2','0000-1-1659-02',22,'교선','','월 2','수 1','연205'),('2014/2-0000-1-1679-01','2014/2','0000-1-1679-01',2,'교선','','','',''),('2014/2-0000-1-1679-02','2014/2','0000-1-1679-02',2,'교선','','목 5','목 6','옥210'),('2014/2-0000-1-1794-01','2014/2','0000-1-1794-01',24,'교선','','월 2','수 1','한울B103'),('2014/2-0000-1-1794-02','2014/2','0000-1-1794-02',24,'교선','','월 1','수 2','옥206'),('2014/2-0000-1-2238-01','2014/2','0000-1-2238-01',25,'교선','','','',''),('2014/2-0000-1-2238-02','2014/2','0000-1-2238-02',25,'교선','','','',''),('2014/2-0000-1-2337-01','2014/2','0000-1-2337-01',26,'교선','','목 3','목 4','비427'),('2014/2-0000-1-2437-01','2014/2','0000-1-2437-01',27,'교선','','수 3','수 4','참B107'),('2014/2-0000-1-2665-01','2014/2','0000-1-2665-01',28,'교선','','화 3','목 4','옥207'),('2014/2-0000-1-2665-02','2014/2','0000-1-2665-02',28,'교선','','화 4','목 3','옥207'),('2014/2-0000-1-2948-01','2014/2','0000-1-2948-01',29,'교선','','화 3','목 4','참102'),('2014/2-0000-1-2959-01','2014/2','0000-1-2959-01',30,'교선','','목 1','목 2','비420'),('2014/2-0000-1-2959-02','2014/2','0000-1-2959-02',31,'교선','','수 7','수 8','비405'),('2014/2-0000-1-2982-01','2014/2','0000-1-2982-01',32,'교선','','화 1','화 2','비427'),('2014/2-0000-1-2982-02','2014/2','0000-1-2982-02',33,'교선','','금 3','금 4','비519'),('2014/2-0000-1-2982-03','2014/2','0000-1-2982-03',34,'교선','','화 5','화 6','비427'),('2014/2-0000-1-3008-01','2014/2','0000-1-3008-01',35,'교선','','화 7','화 8','비403'),('2014/2-0000-1-3008-02','2014/2','0000-1-3008-02',36,'교선','','화 5','화 6','참203'),('2014/2-0000-1-3029-01','2014/2','0000-1-3029-01',37,'교선','','금 5','금 6','연202'),('2014/2-0000-1-3029-02','2014/2','0000-1-3029-02',37,'교선','','금 7','금 8','연202'),('2014/2-0000-1-3029-03','2014/2','0000-1-3029-03',38,'교선','','화 5','목 6','연202'),('2014/2-0000-1-3029-04','2014/2','0000-1-3029-04',39,'교선','','화 4','목 3','연205'),('2014/2-0000-1-3029-05','2014/2','0000-1-3029-05',40,'교선','','금 1','금 2','연201'),('2014/2-0000-1-3029-06','2014/2','0000-1-3029-06',41,'교선','','금 3','금 4','연205'),('2014/2-0000-1-3223-01','2014/2','0000-1-3223-01',42,'교선','','금 3','금 4','옥210'),('2014/2-0000-1-3223-02','2014/2','0000-1-3223-02',42,'교선','','금 5','금 6','옥210'),('2014/2-0000-1-3293-01','2014/2','0000-1-3293-01',43,'교선','','화 6','목 5','연202'),('2014/2-0000-1-3293-02','2014/2','0000-1-3293-02',39,'교선','','화 3','목 4','연205'),('2014/2-0000-1-3293-03','2014/2','0000-1-3293-03',40,'교선','','금 3','금 4','연401'),('2014/2-0000-1-3589-01','2014/2','0000-1-3589-01',44,'교선','','화 4','목 3','참203'),('2014/2-0000-1-3593-01','2014/2','0000-1-3593-01',45,'교선','','화 2','목 1','한울311'),('2014/2-0000-1-3593-02','2014/2','0000-1-3593-02',46,'교선','','화 1','목 2','한울204'),('2014/2-0000-1-3593-03','2014/2','0000-1-3593-03',47,'교선','','월 5','수 6','참202'),('2014/2-0000-1-3593-04','2014/2','0000-1-3593-04',48,'교선','','월 1','수 2','한울204'),('2014/2-0000-1-3593-05','2014/2','0000-1-3593-05',49,'교선','','화 3','목 4','참103'),('2014/2-0000-1-3593-06','2014/2','0000-1-3593-06',50,'교선','','월 4','수 3','한울204'),('2014/2-0000-1-3593-07','2014/2','0000-1-3593-07',51,'교선','','화 5','목 6','옥102'),('2014/2-0000-1-3693-01','2014/2','0000-1-3693-01',52,'교선','','금 3','금 4','한울204'),('2014/2-0000-1-3696-01','2014/2','0000-1-3696-01',45,'교선','','목 5','','한울B102'),('2014/2-0000-1-3696-02','2014/2','0000-1-3696-02',47,'교선','','월 6','수 5','참202'),('2014/2-0000-1-3696-03','2014/2','0000-1-3696-03',53,'교선','','목 3','목 4','비405'),('2014/2-0000-1-3696-04','2014/2','0000-1-3696-04',54,'교선','','목 5','목 6','비405'),('2014/2-0000-1-3696-05','2014/2','0000-1-3696-05',49,'교선','','화 4','목 3','참102'),('2014/2-0000-1-3696-06','2014/2','0000-1-3696-06',48,'교선','','월 2','수 1','참102'),('2014/2-0000-1-3697-01','2014/2','0000-1-3697-01',55,'교선','','금 1','금 2','한울204'),('2014/2-0000-1-3812-01','2014/2','0000-1-3812-01',56,'교선','','목 5','목 6','참203'),('2014/2-0000-1-3814-01','2014/2','0000-1-3814-01',57,'교선','','화 2','목 1','한울B103'),('2014/2-0000-1-3814-02','2014/2','0000-1-3814-02',58,'교선','','화 5','화 6','한울B102'),('2014/2-0000-1-3814-03','2014/2','0000-1-3814-03',59,'교선','','화 7','화 8','한울B103'),('2014/2-0000-1-3919-01','2014/2','0000-1-3919-01',60,'교선','','목 1','목 2','옥206'),('2014/2-0000-1-3925-01','2014/2','0000-1-3925-01',61,'교선','','화 1','목 2','한울203'),('2014/2-0000-1-3928-01','2014/2','0000-1-3928-01',62,'교선','','화 5','화 6','참B101'),('2014/2-0000-1-3928-02','2014/2','0000-1-3928-02',62,'교선','','목 7','목 8','참B101'),('2014/2-0000-1-3928-03','2014/2','0000-1-3928-03',62,'교선','','목 5','목 6','참B101'),('2014/2-0000-1-3948-01','2014/2','0000-1-3948-01',63,'교선','','수 5','수 6','한울504'),('2014/2-0000-1-3948-02','2014/2','0000-1-3948-02',64,'교선','','월 2','월 3','한울204'),('2014/2-0000-1-4061-01','2014/2','0000-1-4061-01',65,'교선','','토 1','토 2','한울B101'),('2014/2-0000-1-4081-01','2014/2','0000-1-4081-01',66,'교선','','화 3','목 4','한울B102'),('2014/2-0000-1-4081-02','2014/2','0000-1-4081-02',66,'교선','','화 4','목 3','한울B102'),('2014/2-0000-1-4282-01','2014/2','0000-1-4282-01',63,'교선','','수 2','수 3','한울301'),('2014/2-0000-1-4282-02','2014/2','0000-1-4282-02',9,'교선','','화 2','화 3','한울301'),('2014/2-0000-1-4283-01','2014/2','0000-1-4283-01',67,'교선','','수 4','수 5','한울301'),('2014/2-0000-1-4283-02','2014/2','0000-1-4283-02',68,'교선','','월 3','월 4','한울401'),('2014/2-0000-1-4390-01','2014/2','0000-1-4390-01',69,'교선','','월 3','월 4','비520'),('2014/2-0000-1-4638-01','2014/2','0000-1-4638-01',70,'교선','','월 6','수 5','비403'),('2014/2-0000-1-4723-01','2014/2','0000-1-4723-01',71,'교선','','화 3','화 4','비427'),('2014/2-0000-1-4723-02','2014/2','0000-1-4723-02',72,'교선','','월 5','월 6','비519'),('2014/2-0000-1-4723-03','2014/2','0000-1-4723-03',72,'교선','','월 2','월 3','화311'),('2014/2-0000-1-4908-01','2014/2','0000-1-4908-01',73,'교선','','월 1','수 2','연404'),('2014/2-0000-1-4908-02','2014/2','0000-1-4908-02',73,'교선','','월 2','수 1','연404'),('2014/2-0000-1-4908-03','2014/2','0000-1-4908-03',75,'교선','','월 3','수 4','문121'),('2014/2-0000-1-4908-04','2014/2','0000-1-4908-04',76,'교선','','월 4','수 3','문119'),('2014/2-0000-1-4908-05','2014/2','0000-1-4908-05',77,'교선','','화 3','목 4','연404'),('2014/2-0000-1-4908-06','2014/2','0000-1-4908-06',78,'교선','','화 1','목 2','문120'),('2014/2-0000-1-4908-07','2014/2','0000-1-4908-07',78,'교선','','화 2','목 1','문120'),('2014/2-0000-1-4908-08','2014/2','0000-1-4908-08',78,'교선','','화 3','목 4','문121'),('2014/2-0000-1-4908-09','2014/2','0000-1-4908-09',79,'교선','','화 4','목 3','문119'),('2014/2-0000-1-4908-10','2014/2','0000-1-4908-10',80,'교선','','화 6','목 5','문119'),('2014/2-0000-1-4933-01','2014/2','0000-1-4933-01',81,'교선','','화 7','화 8','한울204'),('2014/2-0000-1-4933-02','2014/2','0000-1-4933-02',82,'교선','','월 3','수 4','옥210-1'),('2014/2-0000-1-4933-03','2014/2','0000-1-4933-03',83,'교선','','월 5','수 6','한울204'),('2014/2-0000-1-4933-04','2014/2','0000-1-4933-04',84,'교선','','화 3','목 4','한울204'),('2014/2-0000-1-4933-05','2014/2','0000-1-4933-05',83,'교선','','월 6','수 5','한울204'),('2014/2-0000-1-4933-06','2014/2','0000-1-4933-06',84,'교선','','화 4','목 3','한울204'),('2014/2-0000-1-5224-01','2014/2','0000-1-5224-01',85,'교선','','수 2','수 3','참102'),('2014/2-0000-1-5225-01','2014/2','0000-1-5225-01',86,'교선','','월 1','월 2','한울311'),('2014/2-0000-1-5226-01','2014/2','0000-1-5226-01',87,'교선','','월 1','월 2','한울409'),('2014/2-0000-1-5378-01','2014/2','0000-1-5378-01',88,'교선','','금 3','금 4','한천B101'),('2014/2-0000-1-5378-02','2014/2','0000-1-5378-02',88,'교선','','금 5','금 6','한천B101'),('2014/2-0000-1-5655-01','2014/2','0000-1-5655-01',89,'교선','','화 4','화 5','비405'),('2014/2-0000-1-5659-01','2014/2','0000-1-5659-01',90,'교선','','월 4','수 3','한울B104'),('2014/2-0000-1-5660-01','2014/2','0000-1-5660-01',91,'교선','','월 1','수 2','한울B103'),('2014/2-0000-1-5661-01','2014/2','0000-1-5661-01',61,'교선','','목 5','목 6','한울301'),('2014/2-0000-1-5661-02','2014/2','0000-1-5661-02',92,'교선','','목 1','목 2','한울301'),('2014/2-0000-1-5667-01','2014/2','0000-1-5667-01',93,'교선','','수 7','수 8','비403'),('2014/2-0000-1-5667-02','2014/2','0000-1-5667-02',93,'교선','','수 4','수 5','비520'),('2014/2-0000-1-5667-03','2014/2','0000-1-5667-03',94,'교선','','화 1','화 2','한울410'),('2014/2-0000-1-5674-01','2014/2','0000-1-5674-01',95,'교선','','월 2','수 1','옥206'),('2014/2-0000-1-5674-02','2014/2','0000-1-5674-02',96,'교선','','목 7','목 8','비403'),('2014/2-0000-1-5674-03','2014/2','0000-1-5674-03',97,'교선','','화 5','목 6','비520'),('2014/2-0000-1-5678-01','2014/2','0000-1-5678-01',98,'교선','','목 8','목 9','참B107'),('2014/2-0000-1-5678-02','2014/2','0000-1-5678-02',57,'교선','','금 1','금 2','한울304'),('2014/2-0000-1-5679-01','2014/2','0000-1-5679-01',99,'교선','','월 1','월 2','비403'),('2014/2-0000-1-5679-02','2014/2','0000-1-5679-02',99,'교선','','수 1','수 2','비403'),('2014/2-0000-1-5683-01','2014/2','0000-1-5683-01',100,'교선','','월 3','월 4','한천B101'),('2014/2-0000-1-5683-02','2014/2','0000-1-5683-02',100,'교선','','월 5','월 6','한천B101'),('2014/2-0000-1-5684-01','2014/2','0000-1-5684-01',25,'교선','','','',''),('2014/2-0000-1-5684-02','2014/2','0000-1-5684-02',25,'교선','','','',''),('2014/2-0000-1-5687-01','2014/2','0000-1-5687-01',101,'교선','','수 1','수 2','한울502'),('2014/2-0000-1-5704-01','2014/2','0000-1-5704-01',102,'교선','','월 5','월 6','한울301'),('2014/2-0000-1-5704-02','2014/2','0000-1-5704-02',96,'교선','','목 3','목 4','한울301'),('2014/2-0000-1-5899-01','2014/2','0000-1-5899-01',103,'교선','','화 4','목 5','비520'),('2014/2-0000-1-5909-01','2014/2','0000-1-5909-01',104,'교선','','월 6','수 5','참103'),('2014/2-0000-1-6049-01','2014/2','0000-1-6049-01',105,'교선','','목 5','목 6','연B107'),('2014/2-0000-1-6051-01','2014/2','0000-1-6051-01',106,'교선','','화 7','화 8','한천401'),('2014/2-0000-1-6054-01','2014/2','0000-1-6054-01',107,'교선','','화 3','화 4','연B105'),('2014/2-0000-1-6135-01','2014/2','0000-1-6135-01',56,'교선','','수 4','목 4','한울204'),('2014/2-0000-1-6376-01','2014/2','0000-1-6376-01',108,'교선','','월 3','수 4','비403'),('2014/2-0000-1-6381-01','2014/2','0000-1-6381-01',109,'교선','','화 3','목 4','비420'),('2014/2-0000-1-6381-02','2014/2','0000-1-6381-02',109,'교선','','화 4','목 3','비420'),('2014/2-0000-1-6518-01','2014/2','0000-1-6518-01',26,'교선','','목 5','목 6','비519'),('2014/2-0000-1-6519-01','2014/2','0000-1-6519-01',67,'교선','','','',''),('2014/2-0000-1-6523-01','2014/2','0000-1-6523-01',110,'교선','','월 5','수 6','한울305'),('2014/2-0000-1-6523-02','2014/2','0000-1-6523-02',110,'교선','','수 7','수 8','비302'),('2014/2-0000-1-6523-03','2014/2','0000-1-6523-03',111,'교선','','월 2','수 1','한울304'),('2014/2-0000-1-6524-01','2014/2','0000-1-6524-01',46,'교선','','화 2','목 1','한울204'),('2014/2-0000-1-6524-02','2014/2','0000-1-6524-02',112,'교선','','화 5','목 6','한울204'),('2014/2-0000-1-6588-01','2014/2','0000-1-6588-01',113,'교선','','목 2','목 3','연B105'),('2014/2-0000-1-6590-01','2014/2','0000-1-6590-01',114,'전선','','수 3','수 4','한울504'),('2014/2-0000-1-6993-01','2014/2','0000-1-6993-01',79,'교선','','','',''),('2014/2-0000-1-7000-01','2014/2','0000-1-7000-01',115,'교선','','','',''),('2014/2-0000-1-7464-01','2014/2','0000-1-7464-01',116,'교선','','목 3','목 4','연B101'),('2014/2-0000-1-7475-01','2014/2','0000-1-7475-01',117,'교선','','','',''),('2014/2-0000-2-0254-01','2014/2','0000-2-0254-01',118,'교직','','수 3','수 4','옥204'),('2014/2-0000-2-0258-01','2014/2','0000-2-0258-01',119,'교직','','월 3','월 4','한울202'),('2014/2-0000-2-0266-01','2014/2','0000-2-0266-01',118,'교직','','수 5','수 6','한울202'),('2014/2-0000-2-0268-01','2014/2','0000-2-0268-01',120,'교직','','화 1','화 2','한울202'),('2014/2-0000-2-2038-01','2014/2','0000-2-2038-01',121,'교선','','화 1','목 2','참105'),('2014/2-0000-2-2419-01','2014/2','0000-2-2419-01',122,'교선','','화 1','목 2','한울404'),('2014/2-0000-2-2942-01','2014/2','0000-2-2942-01',123,'교선','','수 3','수 4','한울B103'),('2014/2-0000-2-2942-02','2014/2','0000-2-2942-02',124,'교선','','목 8','목 9','한울B103'),('2014/2-0000-2-2968-01','2014/2','0000-2-2968-01',125,'교선','','월 6','수 5','옥103'),('2014/2-0000-2-2968-02','2014/2','0000-2-2968-02',126,'교선','','화 5','화 6','참102'),('2014/2-0000-2-2970-01','2014/2','0000-2-2970-01',125,'교선','','월 5','수 6','옥103'),('2014/2-0000-2-2970-02','2014/2','0000-2-2970-02',127,'교선','','화 1','화 2','한울B104'),('2014/2-0000-2-2977-01','2014/2','0000-2-2977-01',128,'교선','','화 3','목 4','참B102'),('2014/2-0000-2-2977-02','2014/2','0000-2-2977-02',128,'교선','','화 4','목 3','참B102'),('2014/2-0000-2-2977-03','2014/2','0000-2-2977-03',129,'교선','','월 3','수 4','참B102'),('2014/2-0000-2-2977-04','2014/2','0000-2-2977-04',129,'교선','','월 4','수 3','참B102'),('2014/2-0000-2-2977-05','2014/2','0000-2-2977-05',130,'교선','','화 5','목 6','참B102'),('2014/2-0000-2-2977-06','2014/2','0000-2-2977-06',130,'교선','','화 6','목 5','참B102'),('2014/2-0000-2-2977-07','2014/2','0000-2-2977-07',131,'교선','','월 7','월 8','참B102'),('2014/2-0000-2-2977-08','2014/2','0000-2-2977-08',132,'교선','','금 1','금 2','참B102'),('2014/2-0000-2-2981-01','2014/2','0000-2-2981-01',36,'교선','','목 3','목 4','비519'),('2014/2-0000-2-2981-02','2014/2','0000-2-2981-02',36,'교선','','목 5','목 6','옥206'),('2014/2-0000-2-2987-01','2014/2','0000-2-2987-01',133,'교선','','금 3','금 4','비427'),('2014/2-0000-2-2988-01','2014/2','0000-2-2988-01',134,'교선','','월 5','월 6','한울B101'),('2014/2-0000-2-2991-01','2014/2','0000-2-2991-01',135,'교선','','월 3','수 4','참102'),('2014/2-0000-2-2993-01','2014/2','0000-2-2993-01',136,'교선','','화 1','목 2','한울B103'),('2014/2-0000-2-2994-01','2014/2','0000-2-2994-01',137,'교선','','월 5','수 6','비427'),('2014/2-0000-2-2994-02','2014/2','0000-2-2994-02',137,'교선','','월 6','수 5','비427'),('2014/2-0000-2-2994-03','2014/2','0000-2-2994-03',96,'교선','','금 3','금 4','한울504'),('2014/2-0000-2-3037-01','2014/2','0000-2-3037-01',138,'교선','','','',''),('2014/2-0000-2-3037-02','2014/2','0000-2-3037-02',139,'교선','','','',''),('2014/2-0000-2-3196-01','2014/2','0000-2-3196-01',106,'교선','','화 5','목 6','한울B101'),('2014/2-0000-2-3196-02','2014/2','0000-2-3196-02',106,'교선','','화 6','목 5','한울B101'),('2014/2-0000-2-3349-01','2014/2','0000-2-3349-01',140,'교선','','월 1','수 2','한울504'),('2014/2-0000-2-3349-02','2014/2','0000-2-3349-02',54,'교선','','목 7','목 8','한울311'),('2014/2-0000-2-3645-01','2014/2','0000-2-3645-01',141,'교선','','수 1','수 2','비519'),('2014/2-0000-2-3730-01','2014/2','0000-2-3730-01',142,'교선','','월 1','월 2','비427'),('2014/2-0000-2-3730-02','2014/2','0000-2-3730-02',143,'교선','','화 5','화 6','비619'),('2014/2-0000-2-3932-01','2014/2','0000-2-3932-01',144,'교선','','월 1','수 2','옥103'),('2014/2-0000-2-3932-02','2014/2','0000-2-3932-02',63,'교선','','화 5','목 6','옥103'),('2014/2-0000-2-3933-01','2014/2','0000-2-3933-01',145,'교선','','금 3','금 4','비403'),('2014/2-0000-2-3937-01','2014/2','0000-2-3937-01',146,'교선','','수 5','수 6','비405'),('2014/2-0000-2-3939-01','2014/2','0000-2-3939-01',147,'교선','','금 3','금 4','한울311'),('2014/2-0000-2-3944-01','2014/2','0000-2-3944-01',148,'교선','','월 3','수 4','한울310'),('2014/2-0000-2-3950-01','2014/2','0000-2-3950-01',149,'교선','','금 1','금 2','비403'),('2014/2-0000-2-4048-01','2014/2','0000-2-4048-01',150,'교선','','수 2','수 3','비520'),('2014/2-0000-2-4050-01','2014/2','0000-2-4050-01',151,'교선','','화 5','목 6','비420'),('2014/2-0000-2-4051-01','2014/2','0000-2-4051-01',57,'교선','','화 1','목 2','한울303'),('2014/2-0000-2-4051-02','2014/2','0000-2-4051-02',57,'교선','','화 7','화 8','한울303'),('2014/2-0000-2-4052-01','2014/2','0000-2-4052-01',152,'교선','','목 2','목 3','참202'),('2014/2-0000-2-4052-02','2014/2','0000-2-4052-02',44,'교선','','화 3','목 4','비520'),('2014/2-0000-2-4052-03','2014/2','0000-2-4052-03',153,'교선','','수 4','수 5','누405'),('2014/2-0000-2-4054-01','2014/2','0000-2-4054-01',154,'교선','','월 1','월 2','비520'),('2014/2-0000-2-4054-02','2014/2','0000-2-4054-02',155,'교선','','월 4','월 5','비403'),('2014/2-0000-2-4054-03','2014/2','0000-2-4054-03',156,'교선','','목 1','목 2','비427'),('2014/2-0000-2-4054-04','2014/2','0000-2-4054-04',89,'교선','','화 8','화 9','비520'),('2014/2-0000-2-4067-01','2014/2','0000-2-4067-01',157,'교선','','화 2','목 1','연404'),('2014/2-0000-2-4067-02','2014/2','0000-2-4067-02',158,'교선','','금 1','금 2','한울504'),('2014/2-0000-2-4263-01','2014/2','0000-2-4263-01',159,'교선','','수 1','수 2','참203'),('2014/2-0000-2-4263-02','2014/2','0000-2-4263-02',159,'교선','','수 4','수 5','참203'),('2014/2-0000-2-4909-01','2014/2','0000-2-4909-01',160,'교선','','월 2','수 1','문120'),('2014/2-0000-2-4909-02','2014/2','0000-2-4909-02',147,'교선','','금 1','금 2','문120'),('2014/2-0000-2-4909-03','2014/2','0000-2-4909-03',161,'교선','','월 5','수 6','문121'),('2014/2-0000-2-4910-01','2014/2','0000-2-4910-01',157,'교선','','화 1','목 2','연404'),('2014/2-0000-2-4910-02','2014/2','0000-2-4910-02',162,'교선','','월 1','수 2','문121'),('2014/2-0000-2-4910-03','2014/2','0000-2-4910-03',163,'교선','','목 7','목 8','한울305'),('2014/2-0000-2-4925-01','2014/2','0000-2-4925-01',2,'교선','','화 7','화 8','옥206'),('2014/2-0000-2-4925-02','2014/2','0000-2-4925-02',164,'교선','','목 7','목 8','옥206'),('2014/2-0000-2-5073-01','2014/2','0000-2-5073-01',160,'교선','','월 1','수 2','문120'),('2014/2-0000-2-5073-02','2014/2','0000-2-5073-02',161,'교선','','월 6','수 5','문120'),('2014/2-0000-2-5090-01','2014/2','0000-2-5090-01',166,'교선','','금 4','금 5','비420'),('2014/2-0000-2-5090-02','2014/2','0000-2-5090-02',167,'교선','','금 4','금 5','비520'),('2014/2-0000-2-5092-01','2014/2','0000-2-5092-01',168,'교선','','수 2','수 3','비405'),('2014/2-0000-2-5219-01','2014/2','0000-2-5219-01',112,'교선','','화 6','목 5','한울311'),('2014/2-0000-2-5367-01','2014/2','0000-2-5367-01',81,'교선','','화 4','목 3','한울409'),('2014/2-0000-2-5368-01','2014/2','0000-2-5368-01',169,'교선','','수 2','수 3','참202'),('2014/2-0000-2-5662-01','2014/2','0000-2-5662-01',170,'교선','','목 5','목 6','비427'),('2014/2-0000-2-5663-01','2014/2','0000-2-5663-01',171,'교선','','화 2','화 3','참105'),('2014/2-0000-2-5663-02','2014/2','0000-2-5663-02',171,'교선','','화 5','화 6','참B107'),('2014/2-0000-2-5668-01','2014/2','0000-2-5668-01',172,'교선','','월 5','월 6','한울B103'),('2014/2-0000-2-5668-02','2014/2','0000-2-5668-02',173,'교선','','월 2','월 3','참203'),('2014/2-0000-2-5671-01','2014/2','0000-2-5671-01',174,'교선','','수 1','수 2','비427'),('2014/2-0000-2-5675-01','2014/2','0000-2-5675-01',67,'교선','','월 2','수 1','연202'),('2014/2-0000-2-5675-02','2014/2','0000-2-5675-02',175,'교선','','수 7','수 8','연202'),('2014/2-0000-2-5675-03','2014/2','0000-2-5675-03',176,'교선','','화 1','목 2','연202'),('2014/2-0000-2-5675-04','2014/2','0000-2-5675-04',176,'교선','','화 2','목 1','연202'),('2014/2-0000-2-5680-01','2014/2','0000-2-5680-01',163,'교선','','목 3','목 4','한울B103'),('2014/2-0000-2-5681-01','2014/2','0000-2-5681-01',178,'교선','','금 1','금 2','한울B101'),('2014/2-0000-2-5685-01','2014/2','0000-2-5685-01',179,'교선','','월 5','수 6','문120'),('2014/2-0000-2-5685-02','2014/2','0000-2-5685-02',180,'교선','','화 2','목 1','문114'),('2014/2-0000-2-5685-03','2014/2','0000-2-5685-03',147,'교선','','금 5','금 6','문114'),('2014/2-0000-2-5688-01','2014/2','0000-2-5688-01',59,'교선','','화 5','목 6','비403'),('2014/2-0000-2-5688-02','2014/2','0000-2-5688-02',112,'교선','','화 4','목 3','비403'),('2014/2-0000-2-5689-01','2014/2','0000-2-5689-01',45,'교선','','화 4','목 3','한울311'),('2014/2-0000-2-5763-01','2014/2','0000-2-5763-01',181,'교선','','월 3','수 4','연404'),('2014/2-0000-2-5763-02','2014/2','0000-2-5763-02',182,'교선','','월 4','수 3','한울305'),('2014/2-0000-2-5906-01','2014/2','0000-2-5906-01',183,'교선','','화 4','목 3','문121'),('2014/2-0000-2-5906-02','2014/2','0000-2-5906-02',184,'교선','','화 1','목 2','문121'),('2014/2-0000-2-6018-01','2014/2','0000-2-6018-01',170,'교선','','화 5','화 6','비519'),('2014/2-0000-2-6019-01','2014/2','0000-2-6019-01',185,'교선','','월 5','월 6','한울401'),('2014/2-0000-2-6020-01','2014/2','0000-2-6020-01',137,'교선','','화 5','화 6','한울301'),('2014/2-0000-2-6021-01','2014/2','0000-2-6021-01',186,'교선','','화 5','목 6','옥204'),('2014/2-0000-2-6022-01','2014/2','0000-2-6022-01',81,'교선','','화 5','목 6','한울311'),('2014/2-0000-2-6023-01','2014/2','0000-2-6023-01',187,'교선','','월 3','수 4','옥207'),('2014/2-0000-2-6023-02','2014/2','0000-2-6023-02',140,'교선','','화 3','목 4','비403'),('2014/2-0000-2-6056-01','2014/2','0000-2-6056-01',105,'교선','','목 7','목 8','연B101'),('2014/2-0000-2-6057-01','2014/2','0000-2-6057-01',106,'교선','','수 5','수 6','한천401'),('2014/2-0000-2-6058-01','2014/2','0000-2-6058-01',107,'교선','','화 5','화 6','연B105'),('2014/2-0000-2-6133-01','2014/2','0000-2-6133-01',188,'교선','','월 4','월 5','비420'),('2014/2-0000-2-6137-01','2014/2','0000-2-6137-01',189,'교선','','금 5','금 6','비405'),('2014/2-0000-2-6372-01','2014/2','0000-2-6372-01',190,'교선','','수 7','수 8','연B101'),('2014/2-0000-2-6377-01','2014/2','0000-2-6377-01',191,'전선','','화 2','화 3','한울203'),('2014/2-0000-2-6378-01','2014/2','0000-2-6378-01',192,'전선','','화 5','화 6','비409'),('2014/2-0000-2-6382-01','2014/2','0000-2-6382-01',193,'전선','','화 5','화 6','참202'),('2014/2-0000-2-6387-01','2014/2','0000-2-6387-01',194,'교선','','월 3','수 4','연401'),('2014/2-0000-2-6387-02','2014/2','0000-2-6387-02',194,'교선','','월 4','수 3','연401'),('2014/2-0000-2-6397-01','2014/2','0000-2-6397-01',195,'전선','','금 3','금 4','한울404'),('2014/2-0000-2-6525-01','2014/2','0000-2-6525-01',81,'교선','','화 3','목 4','한울311'),('2014/2-0000-2-6526-01','2014/2','0000-2-6526-01',84,'교선','','화 6','목 5','한울204'),('2014/2-0000-2-6580-01','2014/2','0000-2-6580-01',103,'교선','','','',''),('2014/2-0000-2-6728-01','2014/2','0000-2-6728-01',196,'교선','','월 7','월 8','연B101'),('2014/2-0000-2-6730-01','2014/2','0000-2-6730-01',197,'교선','','월 7','월 8','연B105'),('2014/2-0000-2-6948-01','2014/2','0000-2-6948-01',116,'교선','','화 1','목 2','한울B101'),('2014/2-0000-2-6948-02','2014/2','0000-2-6948-02',116,'교선','','화 2','목 1','한울B101'),('2014/2-0000-2-6957-01','2014/2','0000-2-6957-01',198,'교선','','월 5','수 6','연401'),('2014/2-0000-2-6957-02','2014/2','0000-2-6957-02',198,'교선','','월 6','수 5','연401'),('2014/2-0000-2-6966-01','2014/2','0000-2-6966-01',199,'교직','','월 5','월 6','한울504'),('2014/2-0000-2-6994-01','2014/2','0000-2-6994-01',80,'교선','','','',''),('2014/2-0000-2-6999-01','2014/2','0000-2-6999-01',200,'교선','','','',''),('2014/2-0000-2-7145-01','2014/2','0000-2-7145-01',116,'교선','','화 3','화 4','연10키'),('2014/2-0000-2-7471-01','2014/2','0000-2-7471-01',201,'교직','','목 5','목 6','비409'),('2014/2-0000-2-7472-01','2014/2','0000-2-7472-01',120,'교직','','화 3','화 4','한울202'),('2014/2-0000-2-7476-01','2014/2','0000-2-7476-01',202,'교선','','','',''),('2014/2-0000-2-7595-01','2014/2','0000-2-7595-01',113,'교선','','월 3','수 4','한울B101'),('2014/2-0000-2-7595-02','2014/2','0000-2-7595-02',113,'교선','','월 4','수 3','한울B101'),('2014/2-0000-3-0261-01','2014/2','0000-3-0261-01',203,'교직','','목 3','목 4','한울202'),('2014/2-0000-3-0265-01','2014/2','0000-3-0265-01',204,'교직','','금 1','금 2','한울303'),('2014/2-0000-3-0910-01','2014/2','0000-3-0910-01',157,'교선','','화 4','목 3','연404'),('2014/2-0000-3-0910-02','2014/2','0000-3-0910-02',158,'교선','','금 3','금 4','문119'),('2014/2-0000-3-0910-03','2014/2','0000-3-0910-03',205,'교선','','금 1','금 2','연404'),('2014/2-0000-3-1879-01','2014/2','0000-3-1879-01',145,'교선','','수 7','수 8','비420'),('2014/2-0000-3-1879-02','2014/2','0000-3-1879-02',103,'교선','','화 2','목 3','한울404'),('2014/2-0000-3-2997-01','2014/2','0000-3-2997-01',206,'교선','','금 1','금 2','옥206'),('2014/2-0000-3-3200-01','2014/2','0000-3-3200-01',207,'교선','','월 7','월 8','옥206'),('2014/2-0000-3-3200-02','2014/2','0000-3-3200-02',208,'교선','','수 7','수 8','옥206'),('2014/2-0000-3-3200-03','2014/2','0000-3-3200-03',209,'교선','','화 5','화 6','옥206'),('2014/2-0000-3-3240-01','2014/2','0000-3-3240-01',115,'교선','','목 3','목 4','참B107'),('2014/2-0000-3-3240-02','2014/2','0000-3-3240-02',115,'교선','','목 6','목 7','참B107'),('2014/2-0000-3-3924-01','2014/2','0000-3-3924-01',90,'교선','','월 3','수 4','비420'),('2014/2-0000-3-3931-01','2014/2','0000-3-3931-01',210,'교선','','화 1','화 2','비420'),('2014/2-0000-3-3934-01','2014/2','0000-3-3934-01',211,'교선','','수 5','수 6','참102'),('2014/2-0000-3-3935-01','2014/2','0000-3-3935-01',212,'교선','','수 5','수 6','한울311'),('2014/2-0000-3-3936-01','2014/2','0000-3-3936-01',213,'교선','','수 3','수 4','한울311'),('2014/2-0000-3-3946-01','2014/2','0000-3-3946-01',214,'교선','','화 3','화 4','비519'),('2014/2-0000-3-3946-02','2014/2','0000-3-3946-02',153,'교선','','수 1','수 2','한울404'),('2014/2-0000-3-3946-03','2014/2','0000-3-3946-03',214,'교선','','목 5','목 6','참202'),('2014/2-0000-3-4650-01','2014/2','0000-3-4650-01',180,'교선','','화 1','목 2','문114'),('2014/2-0000-3-4650-02','2014/2','0000-3-4650-02',180,'교선','','금 1','금 2','문114'),('2014/2-0000-3-4650-03','2014/2','0000-3-4650-03',215,'교선','','월 7','월 8','문119'),('2014/2-0000-3-4650-04','2014/2','0000-3-4650-04',181,'교선','','월 4','수 3','연404'),('2014/2-0000-3-4717-01','2014/2','0000-3-4717-01',216,'교선','','화 6','목 5','한울B103'),('2014/2-0000-3-4717-02','2014/2','0000-3-4717-02',217,'교선','','화 5','목 6','한울B103'),('2014/2-0000-3-4735-01','2014/2','0000-3-4735-01',218,'교선','','수 5','수 6','비519'),('2014/2-0000-3-4930-01','2014/2','0000-3-4930-01',179,'교선','','월 3','수 4','문120'),('2014/2-0000-3-4930-02','2014/2','0000-3-4930-02',179,'교선','','월 4','수 3','문120'),('2014/2-0000-3-4930-03','2014/2','0000-3-4930-03',215,'교선','','월 6','수 5','문119'),('2014/2-0000-3-4930-04','2014/2','0000-3-4930-04',215,'교선','','월 5','수 6','문119'),('2014/2-0000-3-5657-01','2014/2','0000-3-5657-01',219,'교선','','월 3','수 4','한울B104'),('2014/2-0000-3-5658-01','2014/2','0000-3-5658-01',220,'교선','','금 3','금 4','한울B104'),('2014/2-0000-3-5664-01','2014/2','0000-3-5664-01',221,'교선','','수 5','수 6','비420'),('2014/2-0000-3-5665-01','2014/2','0000-3-5665-01',144,'교선','','월 2','수 1','비420'),('2014/2-0000-3-5666-01','2014/2','0000-3-5666-01',222,'교선','','금 5','금 6','한울B103'),('2014/2-0000-3-5669-01','2014/2','0000-3-5669-01',223,'교선','','월 3','월 4','한울504'),('2014/2-0000-3-5672-01','2014/2','0000-3-5672-01',223,'교선','','월 5','월 6','한울404'),('2014/2-0000-3-5672-02','2014/2','0000-3-5672-02',223,'교선','','수 7','수 8','옥103'),('2014/2-0000-3-5682-01','2014/2','0000-3-5682-01',224,'교선','','월 3','월 4','한울B103'),('2014/2-0000-3-5765-01','2014/2','0000-3-5765-01',66,'교선','','월 1','월 2','연201'),('2014/2-0000-3-5907-01','2014/2','0000-3-5907-01',162,'교선','','월 2','수 1','문121'),('2014/2-0000-3-5907-02','2014/2','0000-3-5907-02',184,'교선','','화 2','목 1','문121'),('2014/2-0000-3-6026-01','2014/2','0000-3-6026-01',226,'교직','','수 3','수 4','한울202');
/*!40000 ALTER TABLE `Info_Table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Prof_Sub_Relation`
--

DROP TABLE IF EXISTS `Prof_Sub_Relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Prof_Sub_Relation` (
  `Subject_Number` char(14) DEFAULT NULL,
  `Professor_ID` int(11) NOT NULL,
  KEY `Professor_ID` (`Professor_ID`),
  KEY `Subject_Number` (`Subject_Number`),
  CONSTRAINT `Prof_Sub_Relation_ibfk_1` FOREIGN KEY (`Professor_ID`) REFERENCES `Prof_Table` (`Professor_ID`),
  CONSTRAINT `Prof_Sub_Relation_ibfk_2` FOREIGN KEY (`Subject_Number`) REFERENCES `Sub_Table` (`Subject_Number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Prof_Sub_Relation`
--

LOCK TABLES `Prof_Sub_Relation` WRITE;
/*!40000 ALTER TABLE `Prof_Sub_Relation` DISABLE KEYS */;
INSERT INTO `Prof_Sub_Relation` VALUES ('0000-1-0019-01',1),('0000-1-0387-01',2),('0000-1-0387-02',3),('0000-1-0670-01',4),('0000-1-0670-02',5),('0000-1-0670-03',4),('0000-1-0670-04',6),('0000-1-0670-05',7),('0000-1-0709-01',8),('0000-1-0709-02',8),('0000-1-0806-01',9),('0000-1-0806-02',10),('0000-1-0806-03',9),('0000-1-0806-04',11),('0000-1-0858-01',12),('0000-1-0858-02',12),('0000-1-1077-01',13),('0000-1-1077-02',13),('0000-1-1077-03',13),('0000-1-1077-04',14),('0000-1-1077-05',15),('0000-1-1077-06',15),('0000-1-1077-07',16),('0000-1-1077-08',16),('0000-1-1077-09',17),('0000-1-1077-10',14),('0000-1-1077-11',14),('0000-1-1077-12',17),('0000-1-1077-13',16),('0000-1-1077-14',17),('0000-1-1077-15',18),('0000-1-1077-16',13),('0000-1-1077-17',15),('0000-1-1077-18',16),('0000-1-1077-19',17),('0000-1-1077-20',16),('0000-1-1077-21',19),('0000-1-1618-01',20),('0000-1-1618-02',21),('0000-1-1659-01',22),('0000-1-1659-02',22),('0000-1-1679-01',2),('0000-1-1679-02',2),('0000-1-1794-01',24),('0000-1-1794-02',24),('0000-1-2238-01',25),('0000-1-2238-02',25),('0000-1-2337-01',26),('0000-1-2437-01',27),('0000-1-2665-01',28),('0000-1-2665-02',28),('0000-1-2948-01',29),('0000-1-2959-01',30),('0000-1-2959-02',31),('0000-1-2982-01',32),('0000-1-2982-02',33),('0000-1-2982-03',34),('0000-1-3008-01',35),('0000-1-3008-02',36),('0000-1-3029-01',37),('0000-1-3029-02',37),('0000-1-3029-03',38),('0000-1-3029-04',39),('0000-1-3029-05',40),('0000-1-3029-06',41),('0000-1-3223-01',42),('0000-1-3223-02',42),('0000-1-3293-01',43),('0000-1-3293-02',39),('0000-1-3293-03',40),('0000-1-3589-01',44),('0000-1-3593-01',45),('0000-1-3593-02',46),('0000-1-3593-03',47),('0000-1-3593-04',48),('0000-1-3593-05',49),('0000-1-3593-06',50),('0000-1-3593-07',51),('0000-1-3693-01',52),('0000-1-3696-01',45),('0000-1-3696-02',47),('0000-1-3696-03',53),('0000-1-3696-04',54),('0000-1-3696-05',49),('0000-1-3696-06',48),('0000-1-3697-01',55),('0000-1-3812-01',56),('0000-1-3814-01',57),('0000-1-3814-02',58),('0000-1-3814-03',59),('0000-1-3919-01',60),('0000-1-3925-01',61),('0000-1-3928-01',62),('0000-1-3928-02',62),('0000-1-3928-03',62),('0000-1-3948-01',63),('0000-1-3948-02',64),('0000-1-4061-01',65),('0000-1-4081-01',66),('0000-1-4081-02',66),('0000-1-4282-01',63),('0000-1-4282-02',9),('0000-1-4283-01',67),('0000-1-4283-02',68),('0000-1-4390-01',69),('0000-1-4638-01',70),('0000-1-4723-01',71),('0000-1-4723-02',72),('0000-1-4723-03',72),('0000-1-4908-01',73),('0000-1-4908-02',73),('0000-1-4908-03',75),('0000-1-4908-04',76),('0000-1-4908-05',77),('0000-1-4908-06',78),('0000-1-4908-07',78),('0000-1-4908-08',78),('0000-1-4908-09',79),('0000-1-4908-10',80),('0000-1-4933-01',81),('0000-1-4933-02',82),('0000-1-4933-03',83),('0000-1-4933-04',84),('0000-1-4933-05',83),('0000-1-4933-06',84),('0000-1-5224-01',85),('0000-1-5225-01',86),('0000-1-5226-01',87),('0000-1-5378-01',88),('0000-1-5378-02',88),('0000-1-5655-01',89),('0000-1-5659-01',90),('0000-1-5660-01',91),('0000-1-5661-01',61),('0000-1-5661-02',92),('0000-1-5667-01',93),('0000-1-5667-02',93),('0000-1-5667-03',94),('0000-1-5674-01',95),('0000-1-5674-02',96),('0000-1-5674-03',97),('0000-1-5678-01',98),('0000-1-5678-02',57),('0000-1-5679-01',99),('0000-1-5679-02',99),('0000-1-5683-01',100),('0000-1-5683-02',100),('0000-1-5684-01',25),('0000-1-5684-02',25),('0000-1-5687-01',101),('0000-1-5704-01',102),('0000-1-5704-02',96),('0000-1-5899-01',103),('0000-1-5909-01',104),('0000-1-6049-01',105),('0000-1-6051-01',106),('0000-1-6054-01',107),('0000-1-6135-01',56),('0000-1-6376-01',108),('0000-1-6381-01',109),('0000-1-6381-02',109),('0000-1-6518-01',26),('0000-1-6519-01',67),('0000-1-6523-01',110),('0000-1-6523-02',110),('0000-1-6523-03',111),('0000-1-6524-01',46),('0000-1-6524-02',112),('0000-1-6588-01',113),('0000-1-6590-01',114),('0000-1-6993-01',79),('0000-1-7000-01',115),('0000-1-7464-01',116),('0000-1-7475-01',117),('0000-2-0254-01',118),('0000-2-0258-01',119),('0000-2-0266-01',118),('0000-2-0268-01',120),('0000-2-2038-01',121),('0000-2-2419-01',122),('0000-2-2942-01',123),('0000-2-2942-02',124),('0000-2-2968-01',125),('0000-2-2968-02',126),('0000-2-2970-01',125),('0000-2-2970-02',127),('0000-2-2977-01',128),('0000-2-2977-02',128),('0000-2-2977-03',129),('0000-2-2977-04',129),('0000-2-2977-05',130),('0000-2-2977-06',130),('0000-2-2977-07',131),('0000-2-2977-08',132),('0000-2-2981-01',36),('0000-2-2981-02',36),('0000-2-2987-01',133),('0000-2-2988-01',134),('0000-2-2991-01',135),('0000-2-2993-01',136),('0000-2-2994-01',137),('0000-2-2994-02',137),('0000-2-2994-03',96),('0000-2-3037-01',138),('0000-2-3037-02',139),('0000-2-3196-01',106),('0000-2-3196-02',106),('0000-2-3349-01',140),('0000-2-3349-02',54),('0000-2-3645-01',141),('0000-2-3730-01',142),('0000-2-3730-02',143),('0000-2-3932-01',144),('0000-2-3932-02',63),('0000-2-3933-01',145),('0000-2-3937-01',146),('0000-2-3939-01',147),('0000-2-3944-01',148),('0000-2-3950-01',149),('0000-2-4048-01',150),('0000-2-4050-01',151),('0000-2-4051-01',57),('0000-2-4051-02',57),('0000-2-4052-01',152),('0000-2-4052-02',44),('0000-2-4052-03',153),('0000-2-4054-01',154),('0000-2-4054-02',155),('0000-2-4054-03',156),('0000-2-4054-04',89),('0000-2-4067-01',157),('0000-2-4067-02',158),('0000-2-4263-01',159),('0000-2-4263-02',159),('0000-2-4909-01',160),('0000-2-4909-02',147),('0000-2-4909-03',161),('0000-2-4910-01',157),('0000-2-4910-02',162),('0000-2-4910-03',163),('0000-2-4925-01',2),('0000-2-4925-02',164),('0000-2-5073-01',160),('0000-2-5073-02',161),('0000-2-5090-01',166),('0000-2-5090-02',167),('0000-2-5092-01',168),('0000-2-5219-01',112),('0000-2-5367-01',81),('0000-2-5368-01',169),('0000-2-5662-01',170),('0000-2-5663-01',171),('0000-2-5663-02',171),('0000-2-5668-01',172),('0000-2-5668-02',173),('0000-2-5671-01',174),('0000-2-5675-01',67),('0000-2-5675-02',175),('0000-2-5675-03',176),('0000-2-5675-04',176),('0000-2-5680-01',163),('0000-2-5681-01',178),('0000-2-5685-01',179),('0000-2-5685-02',180),('0000-2-5685-03',147),('0000-2-5688-01',59),('0000-2-5688-02',112),('0000-2-5689-01',45),('0000-2-5763-01',181),('0000-2-5763-02',182),('0000-2-5906-01',183),('0000-2-5906-02',184),('0000-2-6018-01',170),('0000-2-6019-01',185),('0000-2-6020-01',137),('0000-2-6021-01',186),('0000-2-6022-01',81),('0000-2-6023-01',187),('0000-2-6023-02',140),('0000-2-6056-01',105),('0000-2-6057-01',106),('0000-2-6058-01',107),('0000-2-6133-01',188),('0000-2-6137-01',189),('0000-2-6372-01',190),('0000-2-6377-01',191),('0000-2-6378-01',192),('0000-2-6382-01',193),('0000-2-6387-01',194),('0000-2-6387-02',194),('0000-2-6397-01',195),('0000-2-6525-01',81),('0000-2-6526-01',84),('0000-2-6580-01',103),('0000-2-6728-01',196),('0000-2-6730-01',197),('0000-2-6948-01',116),('0000-2-6948-02',116),('0000-2-6957-01',198),('0000-2-6957-02',198),('0000-2-6966-01',199),('0000-2-6994-01',80),('0000-2-6999-01',200),('0000-2-7145-01',116),('0000-2-7471-01',201),('0000-2-7472-01',120),('0000-2-7476-01',202),('0000-2-7595-01',113),('0000-2-7595-02',113),('0000-3-0261-01',203),('0000-3-0265-01',204),('0000-3-0910-01',157),('0000-3-0910-02',158),('0000-3-0910-03',205),('0000-3-1879-01',145),('0000-3-1879-02',103),('0000-3-2997-01',206),('0000-3-3200-01',207),('0000-3-3200-02',208),('0000-3-3200-03',209),('0000-3-3240-01',115),('0000-3-3240-02',115),('0000-3-3924-01',90),('0000-3-3931-01',210),('0000-3-3934-01',211),('0000-3-3935-01',212),('0000-3-3936-01',213),('0000-3-3946-01',214),('0000-3-3946-02',153),('0000-3-3946-03',214),('0000-3-4650-01',180),('0000-3-4650-02',180),('0000-3-4650-03',215),('0000-3-4650-04',181),('0000-3-4717-01',216),('0000-3-4717-02',217),('0000-3-4735-01',218),('0000-3-4930-01',179),('0000-3-4930-02',179),('0000-3-4930-03',215),('0000-3-4930-04',215),('0000-3-5657-01',219),('0000-3-5658-01',220),('0000-3-5664-01',221),('0000-3-5665-01',144),('0000-3-5666-01',222),('0000-3-5669-01',223),('0000-3-5672-01',223),('0000-3-5672-02',223),('0000-3-5682-01',224),('0000-3-5765-01',66),('0000-3-5907-01',162),('0000-3-5907-02',184),('0000-3-6026-01',226);
/*!40000 ALTER TABLE `Prof_Sub_Relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Prof_Table`
--

DROP TABLE IF EXISTS `Prof_Table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Prof_Table` (
  `Professor_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Professor` varchar(10) NOT NULL,
  `Prof_Search_Freq` int(11) NOT NULL,
  `Prof_Phone` varchar(15) DEFAULT NULL,
  `Prof_CellPhone` varchar(15) DEFAULT NULL,
  `Prof_Email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Professor_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Prof_Table`
--

LOCK TABLES `Prof_Table` WRITE;
/*!40000 ALTER TABLE `Prof_Table` DISABLE KEYS */;
INSERT INTO `Prof_Table` VALUES (1,'차현종',0,'','010-8999-2498','ch826@naver.com'),(2,'구송광',0,'02-2123-6582','','se7en5254@naver.vom'),(3,'김재형',0,'02-352-0071','','pro@yonsei.ac.kr'),(4,'전민규',0,'010-9916-0417','010-9916-0417','jeonmir@kw.ac.kr'),(5,'전정화',0,'010-7660-9999','010-7660-9999','nwx2@kw.ac.kr'),(6,'이무선',0,'','010-8893-3229','pleems@naver.com'),(7,'이환경',0,'010-3401-0514','010-3401-0514','likelaw@dreamwiz.com'),(8,'최석환',0,'','','ovlc@yonsei.ac.kr'),(9,'김일식',0,'','010-9938-0730','minhakim@kw.ac.kr'),(10,'이무영',0,'','010-3176-7549','2mooyoung@naver.com'),(11,'류기환',0,'02-940-8623','010-2785-8300','allryu@kw.ac.kr'),(12,'박지혜',0,'01083451514','','hobbang1922@hotmail.com'),(13,'브라질',0,'','','mbrazil21@gmail.com'),(14,'메리',0,'010-5646-1336','','msmaryyoo@gmail.com'),(15,'쟈넷',0,'010-9068-3319','','jhilts@kw.ac.kr'),(16,'케빈',0,'010-3214-7327','','lacethepuppy@gmail.com'),(17,'앨런',0,'','','alanmarsee@yahoo.com'),(18,'로렌',0,'','','lashmorgan@gmail.com'),(19,'토미',0,'940-6244','','tommyteacher@hanmail.net'),(20,'최철순',0,'02-940-5205','','choi1234@kw.ac.kr'),(21,'황의룡',0,'02-940-5206','','euilyoun@hanmail.net'),(22,'이강성',0,'02-940-5284','02-940-5284','gslee@kw.ac.kr'),(23,'구송광',0,'01062822486','','se7en5254@naver.com'),(24,'윤성재',0,'','비공개','ysJ_cotton@naver.com'),(25,'김영조',0,'011-318-7415','','hockey89@hanmail.net'),(26,'임서현',0,'','010-7392-3102','seohyunim71@gmail.com'),(27,'문경연',0,'','010-5545-7988','terrarossa@hanmail.net'),(28,'김정인',0,'','개강일에 공지','okjuhin@hanmail.net'),(29,'김미정',0,'010-4282-1890','','null8@hanmail.net'),(30,'이승준',0,'010-2716-0746','010-2716-0746','redsboy@hanmail.net'),(31,'이지영',0,'','010-2255-5970','rtour@dreamwiz.com'),(32,'성충모',0,'','019-308-2718','ysmacdung@naver.com'),(33,'안은미',0,'','010-3438-7901','fuppy@daum.net'),(34,'염준규',0,'','010-2247-8187','jyoum1012@gmail.com'),(35,'양림',0,'02-940-5330','010-9558-8156','4smooth78@kw.ac.kr'),(36,'주용환',0,'02-940-5330','010-9558-8156','4smooth78@kw.ac.kr'),(37,'김진수',0,'940-5287','010-9705-5702','cwicc@hanmail.net'),(38,'박영수',0,'02-940-8624','010-8697-5683','yspark@kw.ac.kr'),(39,'이종용',0,'940-5289','010-3204-8066','jyonglee@kw.ac.kr'),(40,'황치곤',0,'02-940-5288','010-3959-9490','duck1052@kw.ac.kr'),(41,'조성수',0,'940-8101','010-4763-3352','css@kw.ac.kr'),(42,'최인석',0,'','',''),(43,'정계동',0,'940-5288','940-5288','gdchung@kw.ac.kr'),(44,'장진원',0,'01089207999','','jinwonjang@gmail.com'),(45,'이승영',0,'02-940-8369','01067640812','sylee27@kw.ac.kr'),(46,'김경수',0,'07086326536','01042266536','kyungsookim1@hanmail.net'),(47,'김덕미',0,'01095150079','01095150079','kd-jangmi@hanmail.net'),(48,'강경완',0,'','010-4193-1678','kang2007@hotmail.com'),(49,'박현정',0,'','010-4852-4906','hjpark32@gmail.com'),(50,'전유나',0,'','010-2287-7554','hkjeon50@naver.com'),(51,'한정연',0,'02-940-5485(학과)','02-940-5485(학과)','oborlungo@gmail.com'),(52,'문윤덕',0,'010.4788.0741','','drmoonyd@hoseo.edu'),(53,'이지은',0,'','01092717126','zie111@hanmail.net'),(54,'최태화',0,'010-2758-3505','010-2758-3505','saikun25@gmail.com'),(55,'이윤수',0,'','','tourbel@naver.com'),(56,'신태식',0,'','','tlalpan@hanmail.net'),(57,'김서영',0,'02-940-8360','010-3126-9199','fidus@kw.ac.kr'),(58,'이종승',0,'02)2264-3698','2264-3698','roksa@daum.net'),(59,'유양근',0,'010-6302-0608','010-6302-0608','yanggeuny@naver.com'),(60,'이유성',0,'','01028950969','eus4004@daum.net'),(61,'김희교',0,'02-940-5482','01092595482','hgkim@kw.ac.kr'),(62,'김종대',0,'010-5249-4992','010-5249-4992','jdkim@yonsei.ac.kr'),(63,'김백영',0,'8365','010','kimby@kw.ac.kr'),(64,'이정연',0,'','01076172928','seek21c@gmail.com'),(65,'이대희',0,'016-711-2003','','nulbo@hanafos.com'),(66,'윤지현',0,'','01046905535','younj03@gmail.com'),(67,'서영호',0,'02-940-8362','','yhseo@kw.ac.kr'),(68,'이준웅',0,'02-940-5017','','joonung@kw.ac.kr'),(69,'유승호',0,'','010-2415-0231','youqe@naver.com'),(70,'이수욱',0,'02-940-5649','010-9188-5649','wook@kw.ac.kr'),(71,'주동황',0,'교내5375','010-8455-4729','dwjoo@naver.com'),(72,'김정호',0,'','010-0000-1111','jkim34@kw.ac.kr'),(73,'김수봉',0,'','','sbkim0326@naver.com'),(74,'김수봉',0,'','','sbkim0326@never.com'),(75,'김채남',0,'010-2503-7590','','kkccnn3579@daum.net'),(76,'신정아',0,'02-940-8679','','jashin@kw.ac.kr'),(77,'황은하',0,'','','ehwang117@gmail.com'),(78,'김미연',0,'','','meeserin@hanmail.net'),(79,'이일재',0,'029405429','','ijlee@kw.ac.kr'),(80,'이완',0,'','','onelee0310@gmail.com'),(81,'이명숙',0,'','010-4652-6906','lms3679@hanmail.net'),(82,'이현선',0,'.','.','cutelambs@hanmail.net'),(83,'김태경',0,'','010-6697-5627','drtkkim@hanmail.net'),(84,'곡효운',0,'940-8366','010-8646-2826','xiaoyun@kw.ac.kr'),(85,'고경주',0,'','019-477-6349','kokyungjoo@hanmail.net'),(86,'김봉식',0,'','','jphi@naver.com'),(87,'이선영',0,'010-2818-0305','010-2818-0305','damdam328@naver.com'),(88,'정재연',0,'010-8712-7135','','kkevrc1114@naver.com'),(89,'김상원',0,'02-6227-7454','010-6227-7454','swkim54@kw.ac.kr'),(90,'김동혁',0,'','','hdonghak@gmail.com'),(91,'원태준',0,'','010-2657-1836','taejoonwon@hotmail.com'),(92,'최용찬',0,'','','filmhistoria@hanmail.net'),(93,'김종헌',0,'','010-5234-5784','1003kimjh@hanmail.net'),(94,'이인원',0,'','010-5311-9938','iwlee@unn.net'),(95,'현재우',0,'','01043728084','ejhyun@hanmail.net'),(96,'김임순',0,'02-940-5088','010-8720-6135','imsoon9@hanmail.net'),(97,'노훈',0,'010-4783-5142','010-4783-5142','Ihsky62@kw.ac.kr'),(98,'남인숙',0,'','010-3502-2387','namis77@daum.net'),(99,'권순철',0,'02-912-6683','010-2261-3258','ksc0226@kw.ac.kr'),(100,'국승희',0,'','','kookseunghee@gmail.com'),(101,'조미원',0,'','010-9101-4155','jolim57@naver.com'),(102,'김영희',0,'02-940-5221','','yhkim@kw.ac.kr'),(103,'도승연',0,'940-8358','','coraa@kw.ac.kr'),(104,'유미란',0,'010-8396-4585','010-8396-4595','milan71@empas.com'),(105,'배윤진',0,'','','jenebae@naver.com'),(106,'조선낭',0,'02-940-5777','','slucy79@gmail.com'),(107,'김용주',0,'010 2094 5135','','rozas74@empal.com'),(108,'덜거르마',0,'','','hana_hear@yahoo.com'),(109,'문준호',0,'010-8584-1539','010-8584-1539','selfmarketing@hanmail.net'),(110,'박판식',0,'','','lifediver@hanmail.net'),(111,'김문정',0,'010-7317-2193','','bluemoon-76@hanmail.net'),(112,'권혁인',0,'02-940-5477','','khi@kw.ac.kr'),(113,'노정진',0,'02-940-8662','','jazzrho@kw.ac.kr'),(114,'전은주',0,'010-5232-4509','','jeoneunjoo@hanmail.net'),(115,'김정권',0,'02-940-5037','','kjg@kw.ac.kr'),(116,'김철수',0,'','','kwmusic@kw.ac.kr'),(117,'심상렬',0,'02-940-5321','','srshim@kw.ac.kr'),(118,'오숙영',0,'02-3290-1638','','syoh24@korea.ac.kr'),(119,'엄미리',0,'010-3356-6656','','mirinai97@hanmail.net'),(120,'한상인',0,'','','sighhan@gmail.com'),(121,'박정우',0,'01090775794','01090775794','pauljwpark3@naver.com'),(122,'나상진',0,'','','nsj777@hanmail.net'),(123,'김숙영',0,'','','sookyoungkim@empal.com'),(124,'전수경',0,'01062382077','01037423928','skchun313@naver.com / saemam@naver.com'),(125,'이지양',0,'01042159636','01042159636','jichorancho@hanmail.net'),(126,'김광섭',0,'','010-5202-4343','river0815@hanmail.net'),(127,'이철희',0,'02)760-0786','010-5389-0558','silsi94@hanmail.net'),(128,'이서영',0,'029405350','','angel3375@naver.com'),(129,'권민정',0,'02 940 5350','','budminting@hanmail.net'),(130,'김지현',0,'029405350','','hyoun375@hanmail.net'),(131,'김영진',0,'010-8283-2151','','oky333@hanmail.net'),(132,'김기종',0,'010-5030-2196','','wkiji@hanmail.net'),(133,'김정수',0,'','010-7106-8895','aron78@naver.com'),(134,'김동옥',0,'595-8308','010-5168-7703','kdok53@naver.com'),(135,'조성관',0,'','01054469676','johnnyb@nate.com'),(136,'정형원',0,'02-940-5097','01079405097','kwugame@naver.com'),(137,'양재규',0,'940-5769','010-5439-2341','jkyang@kw.ac.kr'),(138,'계원봉',0,'010-3572-8243','','wbgyeh@hanmail.net'),(139,'전봉주',0,'010-2624-3001','','joobongj@hanmail.net'),(140,'후지무라마이',0,'02-940-8668','010-6817-1780','vientoviento@naver.com'),(141,'장인호',0,'010--3094-3003','010-3094-3003','inhochang104@naver.com'),(142,'김미희',0,'','010 2825 0774','mhkim2008@gmail.com'),(143,'이현지',0,'','010-9024-6393','hyunjilee.good@gmail.com'),(144,'김인호',0,'5655','0','inho621@paran.com'),(145,'김덕천',0,'','010-2772-8298','alias-remigius@hanmail.net'),(146,'허부문',0,'','010-8974-5426','neungyong@naver.com'),(147,'임정명',0,'','02-940-5306','mildred1516@gmail.com'),(148,'장석원',0,'940-5353','01088404417','ultravox@kw.ac.kr'),(149,'오창익',0,'','01062356267','oci9004@hanmail.net'),(150,'김영룡',0,'05027437000','05027437000','umweg@gmx.com'),(151,'권지영',0,'','010-3270-8707','clarak@naver.com'),(152,'김순주',0,'','010-9390-7891','sunju.km@gmail.com'),(153,'주윤정',0,'','01089200453','arabyjoo@gmail.com'),(154,'김정환',0,'','010-5920-2557','hsc985@yahoo.com'),(155,'강지연',0,'02-542-5353','010-8892-1107','lightsolemn@daum.net'),(156,'이지',0,'','010-9485-3960','susannah39@naver.com'),(157,'노진서',0,'940-5579','','sweetnoh@kw.ac.kr'),(158,'정원일',0,'','','wonilchung@naver.com'),(159,'오용철',0,'940-5291','010-8889-8675','ycoh00@kw.ac.kr'),(160,'서현원',0,'01047492338','','objet0110@hotmail.com'),(161,'정현주',0,'01052067719','','imcool7719@hanmail.net'),(162,'여명자',0,'02-940-5304','','writinglover@hanmail.net'),(163,'정관희',0,'','','arirang202@hanmail.net'),(164,'이민섭',0,'031-841-5813','','wassupgolf@naver.com'),(165,'서현원',0,'','','objet0110@hotmaill.com'),(166,'김형수',0,'02-761-1212','','hskim53@hanmail.net'),(167,'조용천',0,'','','cyc@kw.ac.kr'),(168,'송미애',0,'','','masong@yestm.co.kr'),(169,'허현',0,'0232776929','0232776926','hurmadison@gmail.com'),(170,'정애영',0,'','010-8327-7450','aeyoungj@hanmail.net'),(171,'박경안',0,'031-983-3445','010-9093-3448','gabak@naver.com'),(172,'양영모',0,'','010-5072-9220','ym3040@hanmail.net'),(173,'김하영',0,'010-9303-8986','010-9303-8986','hayk001@hanmail.net'),(174,'양재성',0,'010-3263-6339','010-3263-6339','yjs1628@naver.com'),(175,'김혜경',0,'010-6668-0574','010-6668-0574','artnuvo@nate.com'),(176,'이영훈',0,'010-9420-4988','010-9420-4988','poliem@nate.com'),(177,'정관희',0,'','02-940-5306','arirang202@hotmail.com'),(178,'김정현',0,'','01090897520','bluesbass@hanmail.net'),(179,'고현아',0,'','','itgirl08@naver.com'),(180,'이은아',0,'940-6244','','unamilling@hotmail.com'),(181,'최재용',0,'','','incredible78@gmail.com'),(182,'에이미',0,'940-5360','','amyharp@kw.ac.kr'),(183,'박미경',0,'010-3278-7230','','mkbach21@naver.com'),(184,'조현준',0,'','','scarletcho555@gmail.com'),(185,'이상훈',0,'940-5287','','leesh58@kw.ac.kr'),(186,'이경수',0,'02-940-5307','','arbrerest@hanmail.net'),(187,'다와라기하루미',0,'02-940-8667','010-8311-6759','springriver@kw.ac.kr'),(188,'홍용진',0,'','010-9383-8923','hadrianu75@hotmail.com'),(189,'이종화',0,'031-743-1275','010-4059-3388','lchonghwalee838@gmail.com'),(190,'안준만',0,'','','ajul74@hanmail.net'),(191,'김고은',0,'02-940-8260','','goeunk7720@hanmail.net'),(192,'구지윤',0,'','','sesil798@hanmail.net'),(193,'고효진',0,'02-940-5330','','hyojin@kw.ac.kr'),(194,'김대영',0,'010-5695-5675','010-5695-5675','com@kw.ac.kr'),(195,'한상철',0,'029405335','','crime78@kw.ac.kr'),(196,'정연재',0,'','','yeonjc@hotmail.com'),(197,'김한규',0,'01092913580','','jclsg3@hanmai.net'),(198,'고병량',0,'010-7256-7241','','pyoungryang.ko@gmail.com'),(199,'박나리',0,'010-2009-1267','','hugjasmin@naver.com'),(200,'이민영',0,'02-940-5545','','designclara@naver.com'),(201,'최승미',0,'02-940-8654','','kucc1905@gmail.com'),(202,'박종구',0,'','',''),(203,'김성길',0,'02-940-5624','','momocute@kw.ac.kr'),(204,'박진은',0,'','','olyb0701@gmail.com'),(205,'손영희',0,'940-5285','','younghee@kw.ac.kr'),(206,'김성봉',0,'02-921-3791','010-4576-7915','seanlearns@gmail.com'),(207,'김아람',0,'','','ramii21@nate.com'),(208,'안지희',0,'','','bulgami@hanmail.net'),(209,'신기택',0,'','','wajipro@naver.com'),(210,'서은숙',0,'','강의시 기재','xuzixian@hanmail.net'),(211,'강신환',0,'','010-5120-2200','kanghds@hanmail.net'),(212,'석미자',0,'.','.','smjmeca@hanmail.net'),(213,'한명숙',0,'','010-3240-7386','hannams@hanmail.net'),(214,'변경원',0,'010-2300-2905','010-2300-2905','byk10105@hanmail.net'),(215,'박보경',0,'01057662826','','njpbk@hotmail.com'),(216,'송영출',0,'02-940-5315','010-8912-3222','chool@kw.ac.kr'),(217,'김기영',0,'940-5322','01033099303','kyk@kw.ac.kr'),(218,'박태원',0,'940-5093','010-2734-3092','realestate@kw.ac.kr'),(219,'김상희',0,'01077565321','01077565321','gaea5@hanmail.net'),(220,'서세림',0,'','','hiromic@naver.com'),(221,'김민수',0,'','','iooof97@gmail.com'),(222,'여금미',0,'','010-6424-6517','yennyfr@naver.com'),(223,'조정우',0,'','01052056066','cho_jungwoo@yahoo.co.kr'),(224,'조윤성',0,'','','ys0079@naver.com'),(225,'윤지현',0,'','010-6605-1676','ygdrasilcave1@gmail.com'),(226,'박경애',0,'02-940-5617','','kapark@kw.ac.kr');
/*!40000 ALTER TABLE `Prof_Table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sub_Table`
--

DROP TABLE IF EXISTS `Sub_Table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sub_Table` (
  `Subject_Number` char(14) NOT NULL,
  `Subject_Name` varchar(50) DEFAULT NULL,
  `Grade` int(11) NOT NULL,
  `Sub_Search_Freq` int(11) DEFAULT NULL,
  PRIMARY KEY (`Subject_Number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sub_Table`
--

LOCK TABLES `Sub_Table` WRITE;
/*!40000 ALTER TABLE `Sub_Table` DISABLE KEYS */;
INSERT INTO `Sub_Table` VALUES ('0000-1-0019-01','C프로그래밍-C Programing',3,0),('0000-1-0387-01','농구 - Basketball',1,0),('0000-1-0387-02','농구 - Basketball',1,0),('0000-1-0670-01','법과생활-Law and Life',3,0),('0000-1-0670-02','법과생활-Law and Life',3,0),('0000-1-0670-03','법과생활-Law and Life',3,0),('0000-1-0670-04','법과생활-Law and Life',3,0),('0000-1-0670-05','법과생활-Law and Life',3,0),('0000-1-0709-01','볼링 - Bowling',1,0),('0000-1-0709-02','볼링 - Bowling',1,0),('0000-1-0806-01','생활속의경제-Economics in Life',3,0),('0000-1-0806-02','생활속의경제-Economics in Life',3,0),('0000-1-0806-03','생활속의경제-Economics in Life',3,0),('0000-1-0806-04','생활속의경제-Economics in Life',3,0),('0000-1-0858-01','수영 - Swimming',1,0),('0000-1-0858-02','수영 - Swimming',1,0),('0000-1-1077-01','영어회화 - English Conversation',3,0),('0000-1-1077-02','영어회화 - English Conversation',3,0),('0000-1-1077-03','영어회화 - English Conversation',3,0),('0000-1-1077-04','영어회화 - English Conversation',3,0),('0000-1-1077-05','영어회화 - English Conversation',3,0),('0000-1-1077-06','영어회화 - English Conversation',3,0),('0000-1-1077-07','영어회화 - English Conversation',3,0),('0000-1-1077-08','영어회화 - English Conversation',3,0),('0000-1-1077-09','영어회화 - English Conversation',3,0),('0000-1-1077-10','영어회화 - English Conversation',3,0),('0000-1-1077-11','영어회화 - English Conversation',3,0),('0000-1-1077-12','영어회화 - English Conversation',3,0),('0000-1-1077-13','영어회화 - English Conversation',3,0),('0000-1-1077-14','영어회화 - English Conversation',3,0),('0000-1-1077-15','영어회화 - English Conversation',3,0),('0000-1-1077-16','영어회화 - English Conversation',3,0),('0000-1-1077-17','영어회화 - English Conversation',3,0),('0000-1-1077-18','영어회화 - English Conversation',3,0),('0000-1-1077-19','영어회화 - English Conversation',3,0),('0000-1-1077-20','영어회화 - English Conversation',3,0),('0000-1-1077-21','영어회화 - English Conversation',3,0),('0000-1-1618-01','축구 - Soccer',1,0),('0000-1-1618-02','축구 - Soccer',1,0),('0000-1-1659-01','컴퓨터언어-Computer Language',3,0),('0000-1-1659-02','컴퓨터언어-Computer Language',3,0),('0000-1-1679-01','탁구 - Table Tennis',1,0),('0000-1-1679-02','탁구 - Table Tennis',1,0),('0000-1-1794-01','한국문화사-History of Korean Culture',3,0),('0000-1-1794-02','한국문화사-History of Korean Culture',3,0),('0000-1-2238-01','스케이팅 - Skating',1,0),('0000-1-2238-02','스케이팅 - Skating',1,0),('0000-1-2337-01','언어의이해-The Understanding of Language',3,0),('0000-1-2437-01','연극의이해-Introduction to Theater',3,0),('0000-1-2665-01','기초중국어회화1-Beginning Chinese Conversation 1',3,0),('0000-1-2665-02','기초중국어회화1-Beginning Chinese Conversation 1',3,0),('0000-1-2948-01','읽기와쓰기 - Reading & Writing',3,0),('0000-1-2959-01','인간존재의이해-Introduction on Human Being',3,0),('0000-1-2959-02','인간존재의이해-Introduction on Human Being',3,0),('0000-1-2982-01','인간심리의이해-Introduction of Psychology',3,0),('0000-1-2982-02','인간심리의이해-Introduction of Psychology',3,0),('0000-1-2982-03','인간심리의이해-Introduction of Psychology',3,0),('0000-1-3008-01','조직과리더쉽-Organization and Leadership',3,0),('0000-1-3008-02','조직과리더쉽-Organization and Leadership',3,0),('0000-1-3029-01','인터넷활용-Internet Application',3,0),('0000-1-3029-02','인터넷활용-Internet Application',3,0),('0000-1-3029-03','인터넷활용-Internet Application',3,0),('0000-1-3029-04','인터넷활용-Internet Application',3,0),('0000-1-3029-05','인터넷활용-Internet Application',3,0),('0000-1-3029-06','인터넷활용-Internet Application',3,0),('0000-1-3223-01','골프 - Golf',1,0),('0000-1-3223-02','골프 - Golf',1,0),('0000-1-3293-01','컴퓨터활용-Applications to Computers',3,0),('0000-1-3293-02','컴퓨터활용-Applications to Computers',3,0),('0000-1-3293-03','컴퓨터활용-Applications to Computers',3,0),('0000-1-3589-01','독일어1 - German 1',3,0),('0000-1-3593-01','초급일본어1-Elementary Japanese 1',3,0),('0000-1-3593-02','초급일본어1-Elementary Japanese 1',3,0),('0000-1-3593-03','초급일본어1-Elementary Japanese 1',3,0),('0000-1-3593-04','초급일본어1-Elementary Japanese 1',3,0),('0000-1-3593-05','초급일본어1-Elementary Japanese 1',3,0),('0000-1-3593-06','초급일본어1-Elementary Japanese 1',3,0),('0000-1-3593-07','초급일본어1-Elementary Japanese 1',3,0),('0000-1-3693-01','독일어2 - German 2',3,0),('0000-1-3696-01','초급일본어2-Elementary Japanese 2',3,0),('0000-1-3696-02','초급일본어2-Elementary Japanese 2',3,0),('0000-1-3696-03','초급일본어2-Elementary Japanese 2',3,0),('0000-1-3696-04','초급일본어2-Elementary Japanese 2',3,0),('0000-1-3696-05','초급일본어2-Elementary Japanese 2',3,0),('0000-1-3696-06','초급일본어2-Elementary Japanese 2',3,0),('0000-1-3697-01','프랑스어2 - French 2',3,0),('0000-1-3812-01','스페인어1 - Spanish1',3,0),('0000-1-3814-01','영화의이해-Introduction to Cinema',3,0),('0000-1-3814-02','영화의이해-Introduction to Cinema',3,0),('0000-1-3814-03','영화의이해-Introduction to Cinema',3,0),('0000-1-3919-01','동양의역사-Eastern Asian History',3,0),('0000-1-3925-01','나의역사-Ego-History',3,0),('0000-1-3928-01','성과심리학-Sex and Psychology',3,0),('0000-1-3928-02','성과심리학-Sex and Psychology',3,0),('0000-1-3928-03','성과심리학-Sex and Psychology',3,0),('0000-1-3948-01','사회학의이해-Understanding Sociology',3,0),('0000-1-3948-02','사회학의이해-Understanding Sociology',3,0),('0000-1-4061-01','사회봉사2 - Social Service2',1,0),('0000-1-4081-01','애니메이션의이해-Introduction to Animation',3,0),('0000-1-4081-02','애니메이션의이해-Introduction to Animation',3,0),('0000-1-4282-01','사회과학교양세미나 - Seminar on Social Sciences',3,0),('0000-1-4282-02','사회과학교양세미나 - Seminar on Social Sciences',3,0),('0000-1-4283-01','공학교양세미나 - Seminar on Jeneral Engineering',3,0),('0000-1-4283-02','공학교양세미나 - Seminar on Jeneral Engineering',3,0),('0000-1-4390-01','문학과영화의만남-Connecting Literature and the Cinema',3,0),('0000-1-4638-01','생활속의회계와세무-Accounting and Tax in Life',3,0),('0000-1-4723-01','커뮤니케이션입문-Introduction to Communication',3,0),('0000-1-4723-02','커뮤니케이션입문-Introduction to Communication',3,0),('0000-1-4723-03','커뮤니케이션입문-Introduction to Communication',3,0),('0000-1-4908-01','영어읽기와쓰기 - English Reading & Writing',3,0),('0000-1-4908-02','영어읽기와쓰기 - English Reading & Writing',3,0),('0000-1-4908-03','영어읽기와쓰기 - English Reading & Writing',3,0),('0000-1-4908-04','영어읽기와쓰기 - English Reading & Writing',3,0),('0000-1-4908-05','영어읽기와쓰기 - English Reading & Writing',3,0),('0000-1-4908-06','영어읽기와쓰기 - English Reading & Writing',3,0),('0000-1-4908-07','영어읽기와쓰기 - English Reading & Writing',3,0),('0000-1-4908-08','영어읽기와쓰기 - English Reading & Writing',3,0),('0000-1-4908-09','영어읽기와쓰기 - English Reading & Writing',3,0),('0000-1-4908-10','영어읽기와쓰기 - English Reading & Writing',3,0),('0000-1-4933-01','기초중국어회화2-Beginning Chinese Conversation 2',3,0),('0000-1-4933-02','기초중국어회화2-Beginning Chinese Conversation 2',3,0),('0000-1-4933-03','기초중국어회화2-Beginning Chinese Conversation 2',3,0),('0000-1-4933-04','기초중국어회화2-Beginning Chinese Conversation 2',3,0),('0000-1-4933-05','기초중국어회화2-Beginning Chinese Conversation 2',3,0),('0000-1-4933-06','기초중국어회화2-Beginning Chinese Conversation 2',3,0),('0000-1-5224-01','동서양신화읽기와변용-Reading and Transfiguration of Eastern ',3,0),('0000-1-5225-01','동북아과학기술사 - History of Science and Technology in No',3,0),('0000-1-5226-01','한국전통문화와미학-Korean Traditional Culture and Aesthetic',3,0),('0000-1-5378-01','댄스스포츠 - Dance Sports',1,0),('0000-1-5378-02','댄스스포츠 - Dance Sports',1,0),('0000-1-5655-01','현대사회와윤리-Contemporary Society and Ethics',3,0),('0000-1-5659-01','세계화시대의역사보기-Historical Understanding of Global Era',3,0),('0000-1-5660-01','유럽의과거와현재-European History',3,0),('0000-1-5661-01','인문교양세미나 - Humanities Seminar',3,0),('0000-1-5661-02','인문교양세미나 - Humanities Seminar',3,0),('0000-1-5667-01','정치와현대사회-Politics and Contemporary Society',3,0),('0000-1-5667-02','정치와현대사회-Politics and Contemporary Society',3,0),('0000-1-5667-03','정치와현대사회-Politics and Contemporary Society',3,0),('0000-1-5674-01','생활속의과학-Science and Life',3,0),('0000-1-5674-02','생활속의과학-Science and Life',3,0),('0000-1-5674-03','생활속의과학-Science and Life',3,0),('0000-1-5678-01','대중문화와삶-Popular Culture and Life',3,0),('0000-1-5678-02','대중문화와삶-Popular Culture and Life',3,0),('0000-1-5679-01','생활속의사진-Basic Photography',3,0),('0000-1-5679-02','생활속의사진-Basic Photography',3,0),('0000-1-5683-01','요가 - Yoga',1,0),('0000-1-5683-02','요가 - Yoga',1,0),('0000-1-5684-01','웰니스트레이닝 - Wellness Training',1,0),('0000-1-5684-02','웰니스트레이닝 - Wellness Training',1,0),('0000-1-5687-01','중국어와중국문화입문-Introduction to Chinese language and cu',3,0),('0000-1-5704-01','자연교양세미나 - Seminar on Natural Science',3,0),('0000-1-5704-02','자연교양세미나 - Seminar on Natural Science',3,0),('0000-1-5899-01','과학기술윤리 - Ethics of Science and Technology',3,0),('0000-1-5909-01','공학과디자인-Engineering and Design',3,0),('0000-1-6049-01','교양음악실기1(바이올린) - Music Practice1(Violin)',1,0),('0000-1-6051-01','교양음악실기1(플루트) - Music Practice1(Flute)',1,0),('0000-1-6054-01','교양음악실기1(클래식기타) - Music Practice1(Classic Guitar)',1,0),('0000-1-6135-01','스페인어2 - Spanish2',3,0),('0000-1-6376-01','러시아어2 - Russian 2',3,0),('0000-1-6381-01','마술과셀프마케팅-Magic and Self-Marketing',3,0),('0000-1-6381-02','마술과셀프마케팅-Magic and Self-Marketing',3,0),('0000-1-6518-01','사회속의언어생활-Linguistic life in Life',3,0),('0000-1-6519-01','3D영상콘텐츠의이해및제작 - Introduction to 3D Contents',3,0),('0000-1-6523-01','말하기와소통 - Speaking and Communication',3,0),('0000-1-6523-02','말하기와소통 - Speaking and Communication',3,0),('0000-1-6523-03','말하기와소통 - Speaking and Communication',3,0),('0000-1-6524-01','일본어듣기와쓰기 - Japanese Listening and writing',3,0),('0000-1-6524-02','일본어듣기와쓰기 - Japanese Listening and writing',3,0),('0000-1-6588-01','Jazz Rock Ensemble - Jazz Rock Ensemble',1,0),('0000-1-6590-01','경찰정책론 - Police Policy and Administration',3,0),('0000-1-6993-01','토익필수1 - TOEIC 1',3,0),('0000-1-7000-01','21세기기업의인재상 - Human Capital for the 21st Century Co',2,0),('0000-1-7464-01','힙합/펑크앙상블 -',1,0),('0000-1-7475-01','생활속의계약과협상 - Contract and Negotiation in Daily Life',3,0),('0000-2-0254-01','교육과정 - Educational Curriculum',2,0),('0000-2-0258-01','교육방법및교육공학 - Educational Methodology & Educational ',2,0),('0000-2-0266-01','교육평가 - Teaching Test',2,0),('0000-2-0268-01','교육학개론 - Introduction to Pedagogy',2,0),('0000-2-2038-01','종교와문화-Religion and Culture',3,0),('0000-2-2419-01','HSK연습 - Practicing HSK',3,0),('0000-2-2942-01','미술의이해-Introduction to Art',3,0),('0000-2-2942-02','미술의이해-Introduction to Art',3,0),('0000-2-2968-01','한국의문화유산-Korean Cultural Heritage',3,0),('0000-2-2968-02','한국의문화유산-Korean Cultural Heritage',3,0),('0000-2-2970-01','한국의민속-Korean Folklore',3,0),('0000-2-2970-02','한국의민속-Korean Folklore',3,0),('0000-2-2977-01','생활한문 - Chinese Characters for Everyday Use',3,0),('0000-2-2977-02','생활한문 - Chinese Characters for Everyday Use',3,0),('0000-2-2977-03','생활한문 - Chinese Characters for Everyday Use',3,0),('0000-2-2977-04','생활한문 - Chinese Characters for Everyday Use',3,0),('0000-2-2977-05','생활한문 - Chinese Characters for Everyday Use',3,0),('0000-2-2977-06','생활한문 - Chinese Characters for Everyday Use',3,0),('0000-2-2977-07','생활한문 - Chinese Characters for Everyday Use',3,0),('0000-2-2977-08','생활한문 - Chinese Characters for Everyday Use',3,0),('0000-2-2981-01','국가와행정-The State and Public Administration',3,0),('0000-2-2981-02','국가와행정-The State and Public Administration',3,0),('0000-2-2987-01','기업과경영-Firm in Management',3,0),('0000-2-2988-01','법과경제-Law and Economics',3,0),('0000-2-2991-01','자연과학사-History of Science',3,0),('0000-2-2993-01','정보화와현대사회-Modern Information Technology',3,0),('0000-2-2994-01','환경과생태-Environment and Ecology',3,0),('0000-2-2994-02','환경과생태-Environment and Ecology',3,0),('0000-2-2994-03','환경과생태-Environment and Ecology',3,0),('0000-2-3037-01','대학영문법 - English Grammar',3,0),('0000-2-3037-02','대학영문법 - English Grammar',3,0),('0000-2-3196-01','음악의이해-Introduction to Music',3,0),('0000-2-3196-02','음악의이해-Introduction to Music',3,0),('0000-2-3349-01','스크린일본어-Screen Japanese',3,0),('0000-2-3349-02','스크린일본어-Screen Japanese',3,0),('0000-2-3645-01','법과정치-Law and Policy',3,0),('0000-2-3730-01','매스컴과현대사회-Mass Communication and Modern Society',3,0),('0000-2-3730-02','매스컴과현대사회-Mass Communication and Modern Society',3,0),('0000-2-3932-01','한국근현대사-Korean Modern and Contemporary History',3,0),('0000-2-3932-02','한국근현대사-Korean Modern and Contemporary History',3,0),('0000-2-3933-01','예술철학으로의초대-The Imitation to art Philosophy',3,0),('0000-2-3937-01','동아시아국제관계사-The History of International Relationshi',3,0),('0000-2-3939-01','미국문학과사회-American literature an society',3,0),('0000-2-3944-01','한국명작의이해-Understanding the fine pieces of Korean li',3,0),('0000-2-3950-01','현대사회와인권-Human rights in Contemporary Societies',3,0),('0000-2-4048-01','유럽문학과사회-European Literature and Society',3,0),('0000-2-4050-01','동아시아문학과사회-Literature and Society of East Asia',3,0),('0000-2-4051-01','인간과예술-The Human Mind and Art',3,0),('0000-2-4051-02','인간과예술-The Human Mind and Art',3,0),('0000-2-4052-01','세계문화의이해-Information to world cultures',3,0),('0000-2-4052-02','세계문화의이해-Information to world cultures',3,0),('0000-2-4052-03','세계문화의이해-Information to world cultures',3,0),('0000-2-4054-01','비판적으로사고하기-Critical Thinking',3,0),('0000-2-4054-02','비판적으로사고하기-Critical Thinking',3,0),('0000-2-4054-03','비판적으로사고하기-Critical Thinking',3,0),('0000-2-4054-04','비판적으로사고하기-Critical Thinking',3,0),('0000-2-4067-01','기초영작문 - Basic English Composition',3,0),('0000-2-4067-02','기초영작문 - Basic English Composition',3,0),('0000-2-4263-01','미래사회와표준-Global Standard',3,0),('0000-2-4263-02','미래사회와표준-Global Standard',3,0),('0000-2-4909-01','영어듣기와쓰기 - English Listening and Writing',3,0),('0000-2-4909-02','영어듣기와쓰기 - English Listening and Writing',3,0),('0000-2-4909-03','영어듣기와쓰기 - English Listening and Writing',3,0),('0000-2-4910-01','영어읽기와말하기 - English Reading and Speaking',3,0),('0000-2-4910-02','영어읽기와말하기 - English Reading and Speaking',3,0),('0000-2-4910-03','영어읽기와말하기 - English Reading and Speaking',3,0),('0000-2-4925-01','현대사회와스포츠 - Sports in Contem Porary Society',3,0),('0000-2-4925-02','현대사회와스포츠 - Sports in Contem Porary Society',3,0),('0000-2-5073-01','드라마와영어 - Drama and English',3,0),('0000-2-5073-02','드라마와영어 - Drama and English',3,0),('0000-2-5090-01','창업과경영 - Business and Management',2,0),('0000-2-5090-02','창업과경영 - Business and Management',2,0),('0000-2-5092-01','서비스이론과비즈니스매너 - Service Theory and Business Manners',2,0),('0000-2-5219-01','JPT연습-Practice of JPT',3,0),('0000-2-5367-01','스크린중국어회화 - Screen Chinese',3,0),('0000-2-5368-01','미국의과거와현재-American History',3,0),('0000-2-5662-01','동북아근현대사-Northeast Asian Modern and Contemporary Hi',3,0),('0000-2-5663-01','인물로본한국사-Important Figures in Korean History',3,0),('0000-2-5663-02','인물로본한국사-Important Figures in Korean History',3,0),('0000-2-5668-01','미국과동북아관계-The U.S.and East Asia',3,0),('0000-2-5668-02','미국과동북아관계-The U.S.and East Asia',3,0),('0000-2-5671-01','북한사회의이해-Understanding North Korean Society',3,0),('0000-2-5675-01','디지털영상편집-Digital Video Editing',3,0),('0000-2-5675-02','디지털영상편집-Digital Video Editing',3,0),('0000-2-5675-03','디지털영상편집-Digital Video Editing',3,0),('0000-2-5675-04','디지털영상편집-Digital Video Editing',3,0),('0000-2-5680-01','판타지장르와미래학-Fantasy Art and Futurology',3,0),('0000-2-5681-01','예술사와과학적시선-Consilience of Art and Science',3,0),('0000-2-5685-01','영어회화연습1 - Practice English Conversation 1',3,0),('0000-2-5685-02','영어회화연습1 - Practice English Conversation 1',3,0),('0000-2-5685-03','영어회화연습1 - Practice English Conversation 1',3,0),('0000-2-5688-01','일본문화읽기-Japanese Theme Subjects',3,0),('0000-2-5688-02','일본문화읽기-Japanese Theme Subjects',3,0),('0000-2-5689-01','실용일본어문법-Practical Japanese Grammar',3,0),('0000-2-5763-01','영어회화연습2 - Practice English Conversation 2',3,0),('0000-2-5763-02','영어회화연습2 - Practice English Conversation 2',3,0),('0000-2-5906-01','대중문화영어 - Pop Culture English',3,0),('0000-2-5906-02','대중문화영어 - Pop Culture English',3,0),('0000-2-6018-01','인물로본동양사-Important Figures in Oriental History',3,0),('0000-2-6019-01','공학교양심화세미나 - Advancde Seminar on Jeneral Engineerin',3,0),('0000-2-6020-01','자연교양심화세미나 - Advanced Seminar in Natural Science',3,0),('0000-2-6021-01','한국어표현연습 - Korean Expression Practice',3,0),('0000-2-6022-01','중국어커뮤니케이션-Chinese Communication',3,0),('0000-2-6023-01','실전일본어회화-Practice Japanese Conversation',3,0),('0000-2-6023-02','실전일본어회화-Practice Japanese Conversation',3,0),('0000-2-6056-01','교양음악실기2(바이올린) - Music Practice2(Violin)',1,0),('0000-2-6057-01','교양음악실기2(플루트) - Music Practice2(Flute)',1,0),('0000-2-6058-01','교양음악실기2(클래식기타) - Music Practice2(Classic Guitar)',1,0),('0000-2-6133-01','세계도시문명사-Urban World History',3,0),('0000-2-6137-01','범죄와사회-Crime and Society',3,0),('0000-2-6372-01','실내악앙상블2 - Chamber Music Ensemble 2',1,0),('0000-2-6377-01','사회복지실천론 - Social Work Practice',3,0),('0000-2-6378-01','지역사회복지론 -',3,0),('0000-2-6382-01','경찰연구방법론 - Research Method of Police Science',3,0),('0000-2-6387-01','모바일프로그래밍-Mobile Applications and Programming',3,0),('0000-2-6387-02','모바일프로그래밍-Mobile Applications and Programming',3,0),('0000-2-6397-01','범죄학 - Criminology',3,0),('0000-2-6525-01','중국어듣기와쓰기-Listening and writing Chinese',3,0),('0000-2-6526-01','실용중국어문법-Practical Chinese Grammar',3,0),('0000-2-6580-01','글로벌시대의예술과가치 - Art and Value in Global Times',3,0),('0000-2-6728-01','교양음악실기2(첼로) - Music Practice2(Cello)',1,0),('0000-2-6730-01','교양음악실기2(타악기-난타) - Music Practice 2(Nanta)',1,0),('0000-2-6948-01','대중음악의역사 - History of Pop Music',3,0),('0000-2-6948-02','대중음악의역사 - History of Pop Music',3,0),('0000-2-6957-01','디지털사운드제작 -',3,0),('0000-2-6957-02','디지털사운드제작 -',3,0),('0000-2-6966-01','특수교육론 - Introduction Special Education',2,0),('0000-2-6994-01','토익필수2 - TOEIC 2',3,0),('0000-2-6999-01','색채심리와현대생활 - Color mind & Modern life',3,0),('0000-2-7145-01','교양음악실기2(키보드테크닉) - Music Practice 2(Keyboard Techni',1,0),('0000-2-7471-01','청소년집단상담 - Youth Group Counseling',2,0),('0000-2-7472-01','교육과인간이해 - Education and Understanding the Human Be',2,0),('0000-2-7476-01','전략적의사결정과문제해결 - Strategic Decision Making and Probl',3,0),('0000-2-7595-01','현대재즈의역사 - History of Modern Jazz',3,0),('0000-2-7595-02','현대재즈의역사 - History of Modern Jazz',3,0),('0000-3-0261-01','교육사회 - Educational Sociology',2,0),('0000-3-0265-01','교육철학및교육사 - Educational Philosophy and History',2,0),('0000-3-0910-01','시사영어 - Current English',3,0),('0000-3-0910-02','시사영어 - Current English',3,0),('0000-3-0910-03','시사영어 - Current English',3,0),('0000-3-1879-01','현대사상의이해-Introduction on Modern Thought',3,0),('0000-3-1879-02','현대사상의이해-Introduction on Modern Thought',3,0),('0000-3-2997-01','산업화와환경오염-Industrialization and Environmental Pollu',3,0),('0000-3-3200-01','운동과건강 - Physical Exercise and Health',3,0),('0000-3-3200-02','운동과건강 - Physical Exercise and Health',3,0),('0000-3-3200-03','운동과건강 - Physical Exercise and Health',3,0),('0000-3-3240-01','취업전략 - Job Placement Strategies',2,0),('0000-3-3240-02','취업전략 - Job Placement Strategies',2,0),('0000-3-3924-01','자본주의역사-The History of Capitalism',3,0),('0000-3-3931-01','동아시아고전의이해-Introduction to East Asian Classics',3,0),('0000-3-3934-01','포스트모더니즘의이해-Introduction of Post-Modernism',3,0),('0000-3-3935-01','동아시아의문화유산-Culture-inheritance of Eastern Asia',3,0),('0000-3-3936-01','유럽의문화유산-Culture-inheritance of Europe',3,0),('0000-3-3946-01','여성과남성-Woman and Man',3,0),('0000-3-3946-02','여성과남성-Woman and Man',3,0),('0000-3-3946-03','여성과남성-Woman and Man',3,0),('0000-3-4650-01','영어프리젠테이션 - English Presentation',3,0),('0000-3-4650-02','영어프리젠테이션 - English Presentation',3,0),('0000-3-4650-03','영어프리젠테이션 - English Presentation',3,0),('0000-3-4650-04','영어프리젠테이션 - English Presentation',3,0),('0000-3-4717-01','금융생활의지혜-Financial Intelligence',3,0),('0000-3-4717-02','금융생활의지혜-Financial Intelligence',3,0),('0000-3-4735-01','자산투자및관리-Investment and management',3,0),('0000-3-4930-01','영어토론과인터뷰 - English Discussion and Interview',3,0),('0000-3-4930-02','영어토론과인터뷰 - English Discussion and Interview',3,0),('0000-3-4930-03','영어토론과인터뷰 - English Discussion and Interview',3,0),('0000-3-4930-04','영어토론과인터뷰 - English Discussion and Interview',3,0),('0000-3-5657-01','예술작품속의동양사상-Eastern Thinking by Seeing Work of Art',3,0),('0000-3-5658-01','뉴미디어와이미지의활용-Use of New Media and Image',3,0),('0000-3-5664-01','전쟁사와국제질서-History of Wars and International Order',3,0),('0000-3-5665-01','한국과학기술문명사-History of Korean Science and Technology',3,0),('0000-3-5666-01','역사재해석과영화-Reassessment of History and Film',3,0),('0000-3-5669-01','글로벌시대의국제관계-International relation in Global age',3,0),('0000-3-5672-01','글로벌시대와양극화-Globalization and Socio-economic polariz',3,0),('0000-3-5672-02','글로벌시대와양극화-Globalization and Socio-economic polariz',3,0),('0000-3-5682-01','생활속의디자인 - Introduction to Design',3,0),('0000-3-5765-01','미디어아트-Media Art',3,0),('0000-3-5907-01','비즈니스영어와쓰기 - Business English and Writing',3,0),('0000-3-5907-02','비즈니스영어와쓰기 - Business English and Writing',3,0),('0000-3-6026-01','생활지도와상담 - Guidance and Counseling',2,0);
/*!40000 ALTER TABLE `Sub_Table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User_Review`
--

DROP TABLE IF EXISTS `User_Review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User_Review` (
  `ID` varchar(22) DEFAULT NULL,
  `Lecture_Time` int(11) DEFAULT NULL,
  `Lecture_Quality` int(11) DEFAULT NULL,
  `Lecture_Recommended` int(11) DEFAULT NULL,
  `Homework_Amount` int(11) DEFAULT NULL,
  `Exam_Difficulty` int(11) DEFAULT NULL,
  `Opinion` mediumtext NOT NULL,
  `Time` datetime NOT NULL,
  `IP` varchar(40) NOT NULL,
  KEY `ID` (`ID`),
  CONSTRAINT `User_Review_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `Info_Table` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User_Review`
--

LOCK TABLES `User_Review` WRITE;
/*!40000 ALTER TABLE `User_Review` DISABLE KEYS */;
/*!40000 ALTER TABLE `User_Review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_action_forward`
--

DROP TABLE IF EXISTS `xe_action_forward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_action_forward` (
  `act` varchar(80) NOT NULL,
  `module` varchar(60) NOT NULL,
  `type` varchar(15) NOT NULL,
  UNIQUE KEY `idx_foward` (`act`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_action_forward`
--

LOCK TABLES `xe_action_forward` WRITE;
/*!40000 ALTER TABLE `xe_action_forward` DISABLE KEYS */;
INSERT INTO `xe_action_forward` VALUES ('rss','rss','view'),('atom','rss','view'),('IS','integration_search','view');
/*!40000 ALTER TABLE `xe_action_forward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_addons`
--

DROP TABLE IF EXISTS `xe_addons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_addons` (
  `addon` varchar(250) NOT NULL,
  `is_used` char(1) NOT NULL DEFAULT 'Y',
  `is_used_m` char(1) NOT NULL DEFAULT 'N',
  `is_fixed` char(1) NOT NULL DEFAULT 'N',
  `extra_vars` text,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`addon`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_addons`
--

LOCK TABLES `xe_addons` WRITE;
/*!40000 ALTER TABLE `xe_addons` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_addons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_addons_site`
--

DROP TABLE IF EXISTS `xe_addons_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_addons_site` (
  `site_srl` bigint(11) NOT NULL DEFAULT '0',
  `addon` varchar(250) NOT NULL,
  `is_used` char(1) NOT NULL DEFAULT 'Y',
  `is_used_m` char(1) NOT NULL DEFAULT 'N',
  `extra_vars` text,
  `regdate` varchar(14) DEFAULT NULL,
  UNIQUE KEY `unique_addon_site` (`site_srl`,`addon`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_addons_site`
--

LOCK TABLES `xe_addons_site` WRITE;
/*!40000 ALTER TABLE `xe_addons_site` DISABLE KEYS */;
INSERT INTO `xe_addons_site` VALUES (0,'autolink','Y','N',NULL,'20141029003856'),(0,'blogapi','N','N',NULL,'20141029003856'),(0,'member_communication','Y','N',NULL,'20141029003856'),(0,'member_extra_info','Y','N',NULL,'20141029003856'),(0,'mobile','Y','N',NULL,'20141029003856'),(0,'resize_image','Y','N',NULL,'20141029003856'),(0,'openid_delegation_id','N','N',NULL,'20141029003856'),(0,'point_level_icon','N','N',NULL,'20141029003856'),(0,'adminlogging','N','N',NULL,'20141029011427'),(0,'captcha','N','N',NULL,'20141029011428'),(0,'captcha_member','N','N',NULL,'20141029011428'),(0,'counter','N','N',NULL,'20141029011428'),(0,'oembed','N','N',NULL,'20141029011428');
/*!40000 ALTER TABLE `xe_addons_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_admin_favorite`
--

DROP TABLE IF EXISTS `xe_admin_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_admin_favorite` (
  `admin_favorite_srl` bigint(11) NOT NULL,
  `site_srl` bigint(11) DEFAULT '0',
  `module` varchar(80) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`admin_favorite_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_admin_favorite`
--

LOCK TABLES `xe_admin_favorite` WRITE;
/*!40000 ALTER TABLE `xe_admin_favorite` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_admin_favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_admin_log`
--

DROP TABLE IF EXISTS `xe_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_admin_log` (
  `ipaddress` varchar(100) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `site_srl` bigint(11) DEFAULT '0',
  `module` varchar(100) DEFAULT NULL,
  `act` varchar(100) DEFAULT NULL,
  `request_vars` text,
  KEY `idx_admin_ip` (`ipaddress`),
  KEY `idx_admin_date` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_admin_log`
--

LOCK TABLES `xe_admin_log` WRITE;
/*!40000 ALTER TABLE `xe_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_ai_installed_packages`
--

DROP TABLE IF EXISTS `xe_ai_installed_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_ai_installed_packages` (
  `package_srl` bigint(11) NOT NULL DEFAULT '0',
  `version` varchar(255) DEFAULT NULL,
  `current_version` varchar(255) DEFAULT NULL,
  `need_update` char(1) DEFAULT 'N',
  KEY `idx_package_srl` (`package_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_ai_installed_packages`
--

LOCK TABLES `xe_ai_installed_packages` WRITE;
/*!40000 ALTER TABLE `xe_ai_installed_packages` DISABLE KEYS */;
INSERT INTO `xe_ai_installed_packages` VALUES (18745485,'4.0','4.0','N'),(18378362,'0.2','1.7','N'),(18325662,'1.7.7.2','1.7.7.2','N'),(18324327,'0.1','1.7','N'),(18324266,'0.1','1.7','N'),(18324167,'1.7.1.1','1.7.2','N'),(18910976,'0.1','1.7','N');
/*!40000 ALTER TABLE `xe_ai_installed_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_ai_remote_categories`
--

DROP TABLE IF EXISTS `xe_ai_remote_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_ai_remote_categories` (
  `category_srl` bigint(11) NOT NULL DEFAULT '0',
  `parent_srl` bigint(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL,
  `list_order` bigint(11) NOT NULL,
  PRIMARY KEY (`category_srl`),
  KEY `idx_parent_srl` (`parent_srl`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_ai_remote_categories`
--

LOCK TABLES `xe_ai_remote_categories` WRITE;
/*!40000 ALTER TABLE `xe_ai_remote_categories` DISABLE KEYS */;
INSERT INTO `xe_ai_remote_categories` VALUES (18904838,18322919,'에디터 스타일',14),(18322952,18322919,'위젯 스타일',13),(18322950,18322919,'위젯 스킨',12),(18994170,18322919,'모듈 모바일 스킨',11),(18322943,18322919,'모듈 스킨',10),(18322954,18322919,'레이아웃',8),(18994172,18322919,'모바일 레이아웃',9),(18322919,0,'스킨',7),(18631347,18322917,'단락에디터컴포넌트',6),(18322927,18322917,'위젯',4),(18322929,18322917,'에디터컴포넌트',5),(18322925,18322917,'애드온',3),(18322923,18322917,'모듈',2),(18322907,18322917,'XE 코어',1),(18322917,0,'프로그램',0),(18322977,18322919,'회원레벨 아이콘',15);
/*!40000 ALTER TABLE `xe_ai_remote_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_autoinstall_packages`
--

DROP TABLE IF EXISTS `xe_autoinstall_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_autoinstall_packages` (
  `package_srl` bigint(11) NOT NULL DEFAULT '0',
  `category_srl` bigint(11) DEFAULT '0',
  `path` varchar(250) NOT NULL,
  `have_instance` char(1) NOT NULL DEFAULT 'N',
  `updatedate` varchar(14) DEFAULT NULL,
  `latest_item_srl` bigint(11) NOT NULL DEFAULT '0',
  `version` varchar(255) DEFAULT NULL,
  UNIQUE KEY `unique_path` (`path`),
  KEY `idx_package_srl` (`package_srl`),
  KEY `idx_category_srl` (`category_srl`),
  KEY `idx_regdate` (`updatedate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_autoinstall_packages`
--

LOCK TABLES `xe_autoinstall_packages` WRITE;
/*!40000 ALTER TABLE `xe_autoinstall_packages` DISABLE KEYS */;
INSERT INTO `xe_autoinstall_packages` VALUES (18324167,18322923,'./modules/board','N','20141028202817',21940502,'1.7.1.1'),(18324168,18322923,'./modules/homepage','N','20141028151038',21854391,'1.7.0.1'),(18324169,18322923,'./modules/issuetracker','N','20141027234444',21278648,'1.2.1'),(18324171,18322923,'./modules/livexe','N','20141028175212',19624726,'0.6'),(18324175,18322923,'./modules/material','N','20141028160002',18669818,'1.0'),(18324186,18322923,'./modules/textyle','N','20141027190725',21795348,'1.7.0'),(18324187,18322923,'./modules/textylehub','N','20141028175129',21795365,'1.7.0'),(18324188,18322923,'./modules/planet','N','20141027222021',21015994,'0.1.4'),(18324189,18322923,'./modules/referer_old','N','20141024082010',18325389,'0.15'),(18324191,18322923,'./modules/resource','N','20141028112513',21854259,'1.7.0'),(18324199,18322923,'./modules/tccommentnotify','N','20141027222041',18365815,'1.1.0'),(18324210,18322923,'./modules/wiki','N','20141028125802',21985871,'1.7.0.1'),(18324211,18322943,'./modules/board/skins/xe_board','N','20141028224018',18325569,'0.1'),(18324212,18322943,'./modules/board/skins/xe_default','N','20141028135725',18325513,'0.1'),(18324213,18322943,'./modules/editor/skins/dreditor','N','20141028160036',18865892,'1.3.0'),(18324214,18322943,'./modules/editor/skins/fckeditor','N','20141025021659',18325501,'0.1'),(18324221,18322943,'./modules/editor/skins/xquared','N','20141024215000',18325496,'0.1'),(18324225,18322943,'./modules/textyle/skins/wordPressDefault','N','20141024214949',18325484,'0.1'),(18324226,18322925,'./addons/keyword_link','N','20141026204436',18325653,'0.1'),(18324227,18322925,'./addons/member_join_extend','N','20141028023146',18325647,'0.1'),(18324228,18322925,'./addons/planet_bookmark','N','20141027222336',21017018,'0.1.1'),(18324233,18322925,'./addons/planet_todo','N','20141027222325',21016986,'0.1.1'),(18324241,18322925,'./addons/referer_old','N','20141018234431',18325632,'0.1'),(18324247,18322925,'./addons/tccommentnotify','N','20141027222316',18365845,'1.0'),(18324248,18322925,'./addons/wiki_link','N','20141027222306',21813902,'1.7.0'),(18324261,18322929,'./modules/editor/components/cc_license','N','20141025023030',18325227,'0.1'),(18324266,18322929,'./modules/editor/components/emoticon','N','20141028190354',18325232,'0.1'),(18324271,18322929,'./modules/editor/components/naver_map','N','20141028122449',18325239,'0.1'),(18324273,18322929,'./modules/editor/components/quotation','N','20141028163053',18325248,'0.1'),(18324280,18322929,'./modules/editor/components/photo_editor','N','20141028122438',18325257,'0.1'),(18324292,18322954,'./layouts/cafeXE','N','20141028171421',21802168,'1.7.0'),(18324297,18322954,'./layouts/ideation','N','20141027232747',18325198,'0.1'),(18324299,18322954,'./layouts/xe_official_v2','N','20141029003835',20391868,'1.0'),(18324320,18322927,'./widgets/archive_list','N','20141027095207',18325093,'0.1'),(18324321,18322927,'./widgets/calendar','N','20141029003726',20591626,'0.2'),(18324326,18322927,'./widgets/category','N','20141028155940',18325077,'0.1'),(18324327,18322927,'./widgets/counter_status','N','20141028100256',18325071,'0.1'),(18324328,18322927,'./widgets/DroArc_clock','N','20141027003907',18325065,'0.1'),(18324330,18322927,'./widgets/forum','N','20141028210835',18325054,'0.1'),(18324331,18322927,'./widgets/ideationBanner','N','20141029003640',18325042,'0.1'),(18324332,18322927,'./widgets/ideationPopular','N','20141028214825',18325026,'0.1'),(18324335,18322927,'./widgets/image_counter','N','20141024201238',19099243,'0.2'),(18324336,18322927,'./widgets/logged_members','N','20141028224022',18325004,'0.1'),(18324337,18322927,'./widgets/member_group','N','20141028202141',18324998,'0.1'),(18324338,18322927,'./widgets/navigator','N','20141029003752',21801528,'1.7.0'),(18324343,18322927,'./widgets/newest_comment','N','20141027175845',18324984,'0.1'),(18324344,18322927,'./widgets/newest_document','N','20141028214018',20893807,'1.0'),(18324345,18322927,'./widgets/newest_trackback','N','20141027095158',18324957,'0.1'),(18324346,18322927,'./widgets/planet_document','N','20141028210719',18327255,'0.2'),(18324348,18322927,'./widgets/rank_count','N','20141028202141',18324851,'1.5'),(18324352,18322927,'./widgets/rank_point','N','20141027084312',18324818,'1.0'),(18324353,18322927,'./widgets/rss_reader','N','20141020200753',18324791,'#7'),(18324355,18322927,'./widgets/site_info','N','20141028202141',21801496,'1.7.0'),(18324359,18322927,'./widgets/tab_newest_document','N','20141028211146',18324658,'0.1'),(18324360,18322927,'./widgets/tag_list','N','20141028211429',18324768,'0.1'),(18324361,18322927,'./widgets/webzine','N','20141025125022',18324711,'0.1'),(18324362,18322927,'./widgets/xeBanner','N','20141029003717',18324697,'0.1'),(18324391,18322950,'./widgets/content/skins/xeHome','N','20141028192131',18324681,'0.1'),(18324395,18322950,'./widgets/tab_newest_document/skins/ideationTab','N','20141023204314',18324647,'0.1'),(18324396,18322952,'./widgetstyles/colorbox','N','20141023105045',18324641,'0.1'),(18324398,18322952,'./widgetstyles/memo','N','20141024174551',18324622,'0.1'),(18324401,18322952,'./widgetstyles/postitWire','N','20141022150353',18324610,'0.1'),(18324402,18322952,'./widgetstyles/roundFace','N','20141022033828',18324603,'0.1'),(18324403,18322952,'./widgetstyles/roundWire','N','20141024174702',18324590,'0.1'),(18324404,18322952,'./widgetstyles/simpleRound','N','20141028211338',18324575,'0.1'),(18324405,18322952,'./widgetstyles/simpleSquare','N','20141028105250',18324565,'0.1'),(18324406,18322952,'./widgetstyles/simpleTitle','N','20141028160131',18324546,'0.1'),(18325662,18322907,'.','N','20141029004016',22754364,'1.7.7.2'),(18325755,18322927,'./widgets/popular_planet_document','N','20141028125846',18325772,'0.1'),(18325790,18322923,'./modules/ad','N','20141028201030',19741760,'0.5.2'),(18325791,18322927,'./widgets/lineadWidget','N','20141028200308',19739261,'0.8'),(18325803,18322929,'./modules/editor/components/code_highlighter','N','20141028163225',22272034,'1.3'),(18325813,18322925,'./addons/tag_relation','N','20141028180405',22274979,'1.4.1'),(18325941,18322923,'./modules/sms','N','20141027110019',18745231,'1.3.10'),(18325946,18322923,'./modules/minishop','N','20141121201652',22754398,'1.3.2'),(18325951,18322925,'./addons/sms_alert','N','20141009121141',18326173,'1.0'),(18325952,18322927,'./widgets/sms','N','20141024150328',18326180,'1.1'),(18325989,18322929,'./modules/editor/components/google_translate','N','20141028122444',18777700,'0.2'),(18326005,18322929,'./modules/editor/components/textbox','N','20141025023011',18326938,'0.1'),(18326011,18322925,'./addons/hidden_module','N','20141022105211',18337264,'0.3'),(18326030,18322923,'./modules/statistics','N','20141028194607',18327023,'1.0.1b'),(18326038,18322925,'./addons/statistics','N','20141027160404',18327083,'1.0.1b'),(18326351,18322925,'./addons/remove_id_search','N','20140926205057',18326429,'1.0'),(18326352,18322925,'./addons/guest_name','N','20141028151927',19010744,'1.1.2'),(18326353,18322925,'./addons/write_limit','N','20141014124036',18637861,'1.2.1'),(18326553,18322954,'./layouts/habile_layout','N','20141024212952',18330571,'1.3'),(18327285,18322954,'./layouts/bcptwta','N','20140921083600',18328111,'1.0'),(18327287,18322950,'./widgets/tag_list/skins/tagcloud','N','20141028175147',18328078,'1.0'),(18327419,18322954,'./layouts/mh_simple','N','20141027101912',18327611,'1.1'),(18327455,18322977,'./modules/point/icon/2sis_icon','N','20141024100114',18331756,'1'),(18327462,18322927,'./widgets/gagachat','N','20141028224615',22555267,'3.7'),(18327743,18322954,'./layouts/Treasurej_Heart_Note','N','20141021215908',19334770,'1.6'),(18327995,18322950,'./widgets/counter_status/skins/miznkiz_simple_counter','N','20141028052558',18339071,'0.1'),(18328243,18322927,'./widgets/CoolirisPlayer','N','20141021184257',18332482,'2.0'),(18328356,18322927,'./widgets/newest_medias','N','20141027205911',18330464,'v0.1'),(18328672,18322954,'./layouts/PXE_leaflet_lite','N','20141027171413',18339574,'1.02 Final'),(18328730,18322950,'./widgets/login_info/skins/treasurej_simple150px','N','20141028003220',18953730,'1.5.3'),(18330288,18322950,'./widgets/login_info/skins/sleepless_simple','N','20140817153102',18332123,'1.0.0'),(18330488,18322927,'./widgets/webzine/skins/LILY_GoodStyle','N','20141028213106',18333192,'1.0'),(18330513,18322950,'./widgets/tag_list/skins/treasurej_tagcloud','N','20141027143947',18778301,'1.2'),(18330814,18322954,'./layouts/messenger','N','20140921083551',18331384,'Messenger_v0.1'),(18331803,18322943,'./modules/board/skins/p_board_p','N','20141024152256',18845219,'3.2.0'),(18334573,18322952,'./widgetstyle/webslice','N','20141022150211',18338237,'0.0.1'),(18334938,18322923,'./modules/kin','N','20141027172459',21965762,'1.7.0'),(18334979,18322925,'./addons/popup','N','20141028211842',18335423,'0.1'),(18334980,18322925,'./addons/piclens','N','20141028121729',20168732,'1.5'),(18334989,18322925,'./addons/func_include','N','20141028175234',18336654,'v1.0'),(18334990,18322925,'./addons/entry','N','20141023235950',18685479,'1.2'),(18334996,18322923,'./modules/media','N','20141024012821',18336696,'v0.1'),(18335009,18322927,'./widgets/rnq_newest_document','N','20141028175234',18336745,'1.1.5'),(18335021,18322927,'./widgets/division','N','20141027234341',20582119,'1.1'),(18335028,18322950,'./widgets/login_info/skins/rnq_login','N','20141026161902',18337247,'v0.2'),(18335034,18322950,'./widgets/rnq_newest_document/skins/rnq_newest_integrate','N','20141027095135',18798607,'0.3.1'),(18335040,18322950,'./widgets/rnq_newest_document/skins/rnq_newest_default','N','20141028175233',18798214,'0.3'),(18335043,18322923,'./modules/nms','N','20141027151037',19520872,'0.9.0'),(18335048,18322927,'./widgets/nms_info','N','20141028143140',18349106,'0.1.3'),(18335090,18322943,'./modules/board/skins/xe_naradesign','N','20141028212546',18335100,'1.0'),(18335281,18322923,'./modules/join_extend','N','20141028225743',18988537,'0.5.3.4'),(18335356,18322943,'./modules/textyle/skins/corporate/','N','20141022124849',18335357,'0.9'),(18335369,18322950,'./widgets/content/skins/naradesign/','N','20141028212234',18335372,'0.3'),(18335382,18322950,'./widgets/login_info/skins/webmini','N','20141028194852',18336191,'3.1'),(18337279,18322950,'./widgets/rnq_newest_document/skins/rnq_newest_notice','N','20141028174207',18798196,'0.3'),(18338697,18322925,'./addons/uccup','N','20141027095109',18338747,'v2.3'),(18338699,18322943,'./modules/board/skins/xe_uccup','N','20141027185351',18338792,'v2.3'),(18342939,18322925,'./addons/comment_new','N','20141018232405',18670429,'1.1.1'),(18346921,18322927,'./widgets/randomchat','N','20141027000232',18517236,'1.2'),(18347510,18322954,'./layouts/aginet_official_v2','N','20140924174706',18575161,'2.0.7'),(18351409,18322923,'./modules/zzz_menu_new','N','20141028224527',21832040,'1.7.0'),(18354173,18322952,'./widgetstyles/sz_gradient','N','20141015131541',18354312,'0.1.0'),(18354463,18322977,'./modules/point/icons/lv','N','20141006124448',19013505,'0.2'),(18354979,18322977,'/ modules / point / icons','N','20141023153732',18355002,'SuddenAttack + 확장'),(18357476,18322954,'./layouts/xe_sunooDMLg','N','20141015154405',19462033,'1.0'),(18358588,18322927,'./widgets/newest_memo','N','20141029003607',18363998,'0.1'),(18360610,18322927,'./widgets/flowing_pictures','N','20141028234022',18648791,'1.1.7'),(18362403,18322954,'./layouts/xe_sunooDMRg','N','20141027181318',19462101,'1.0'),(18366133,18322943,'./modules/join_extend/skins/','N','20141027223126',18366143,'1'),(18369597,18322925,'./addons/DisableDrag','N','20141023185136',19229654,'0.3'),(18378357,18322954,'./layouts/xe_cafe','N','20141028202141',21803889,'1.7.0'),(18378362,18322950,'./widgets/login_info/skins/default','N','20141015102658',20168245,'0.2'),(18381054,18322954,'./layouts/naver_photo_style','N','20141027013427',18429470,'1.2.1'),(18383233,18322954,'./layouts/Jungbok_layout_V3.0','N','20141025175145',18389790,'V3.0'),(18386463,18322943,'./modules/textyle/skins','N','20141024152647',19100013,'2.5'),(18388093,18322925,'./addons/bekmeProhibite','N','20141023231252',18388181,'1.0'),(18398352,18322943,'./modules/board/skins/xe_official_planner123','N','20141028221711',22592909,'4.3.0'),(18399622,18322977,'./modules/poin/954','N','20141012212943',18404551,'1.0.1'),(18409541,18322954,'./layouts/hankooktown2','N','20141028124813',19512809,'1.2.2'),(18409634,18322950,'./widgets/login_info/skins/hk','N','20141028124813',18547214,'0.4'),(18410867,18322923,'./modules/pointsend','N','20141028132644',22643085,'1.2.5.1'),(18410868,18322925,'./addons/member_pointsend/','N','20141028133143',21227458,'0.2.2'),(18411910,18322950,'./widgets/newest_comment/skins','N','20140916084342',18413214,'1.0'),(18414428,18322954,'./layouts/Gom-e.net_Hankooktown2_Layout','N','20140924174606',18445386,'1.0.7'),(18419537,18322954,'./layouts/kindguyLayout(joins_200911_1)','N','20141028004151',18422597,'v.200911'),(18424676,18322954,'./layouts/gomenet_xe_official_v2','N','20140921083530',18426534,'1.0.1'),(18432183,18322954,'./layouts/kindguyLayout(munhwa_200911_1)','N','20140921083521',18432699,'v.200911'),(18432187,18322954,'./layouts/kindguyLayout(munhwa_200911_2)','N','20141028004144',18432723,'v.200911'),(18435775,18322927,'./widgets/level_point','N','20141027134504',22595479,'1.0'),(18447927,18322954,'./layouts/Gom2netLayoutEngland','N','20141015090154',18454140,'1.0.1'),(18448761,18322943,'./modules/board/skin/xe_official_hancoma_title_skin','N','20141027095113',18461302,'0.2'),(18454611,18322954,'./layouts/kindguyLayout(khan_200911_1)','N','20141026004103',18454709,'v.200911'),(18454629,18322954,'./layouts/kindguyLayout(khan_200911_2)','N','20141028004121',18454718,'v.200911'),(18459111,18322925,'./addons/addthis','N','20141026095916',18459913,'0.1'),(18510031,18322943,'./modules/textyle/skins/zirho','N','20141024152212',18569108,'0.0.1'),(18511514,18322925,'./addons/adult_keyword','N','20141028233244',22754386,'1.0'),(18512505,18322954,'./layouts/kindguyLayout(BlueN_200912_1)','N','20141028004258',18516495,'200912'),(18512506,18322954,'./layouts/kindguyLayout(BlueN_200912_2)','N','20141028230512',18516511,'200912'),(18527888,18322923,'./modules/oneban','N','20141028203630',18529981,'1.0'),(18536532,18322952,'./widgetstyles/xe_official','N','20141028211324',19432228,'0.2'),(18539546,18322925,'./addons/addvote','N','20141026182910',21244042,'1.0'),(18555205,18322950,'./widgets/counter_status/skins/sworld_counter','N','20141028065135',18591528,'0.1'),(18555654,18322952,'./widgetstyles/tingenara/','N','20141024145147',18557124,'1'),(18561875,18322923,'./modules/smsontextyle','N','20141008193653',18569729,'1.0.1'),(18561895,18322927,'./widgets/sms_textyle','N','20140803060339',18569743,'1.0.1'),(18572882,18322954,'./layouts/xdom_v2','N','20141028231108',19595474,'2.5.2.4'),(18572883,18322950,'./widgets/login_info/skins/xdom_login_v2','N','20141028231109',19051343,'2.3.1'),(18577507,18322927,'./widgets/chat25','N','20141019045959',18587408,'0.0.1'),(18579525,18322950,'./widgets/login_info/skins/tingenaralogin','N','20141027134715',18587232,'1'),(18589320,18322923,'./modules/cashbook','N','20141027154435',19603368,'0.3.7'),(18591523,18322927,'./widgets/module_point','N','20141028224622',19298594,'0.2'),(18595500,18322943,'./modules/integration_search/skins/default_xgenesis','N','20141014182956',18596361,'0.1.1'),(18595504,18322954,'./layouts/xe_official_v2_xgenesis','N','20141027225010',18596408,'0.1.1'),(18595711,18322943,'./modules/communication/skins/name','N','20140928222112',18597241,'0.1'),(18604859,18322927,'./widgets/bannerWidget','N','20141028202055',19739272,'0.3'),(18606308,18322954,'./layouts/elkha_fge','N','20141028003628',18657582,'1.2'),(18606314,18322954,'./layouts/elkha_sky','N','20141027193215',19822870,'1.4.5'),(18606318,18322954,'./layouts/paper_layer','N','20141021180327',18611976,'1.0'),(18607436,18322923,'./modules/wizardxe','N','20141028211712',19150177,'0.0.6'),(18607444,18322927,'./widgets/gallery_frame','N','20141027191758',18619741,'0.0.2'),(18607471,18322927,'./widgets/bangbang_alltogether','N','20141020165646',18645219,'0.0.3'),(18607483,18322954,'./layouts/zirho_layout','N','20141002113731',18645390,'0.0.3'),(18608455,18322923,'./modules/keywordstatistics','N','20141027194632',18631041,'0.3'),(18608457,18322925,'./addons/keywordstatistics','N','20141027194633',18611543,'0.1'),(18610979,18322923,'./modules/msg_admin','N','20141027233758',18614159,'0.1'),(18612951,18322954,'./layouts/elkha_simple','N','20141021184040',18633735,'1.1'),(18617496,18322943,'./modules/textyle/skins/Viewfinder','N','20141020064843',18678663,'0.3'),(18618046,18322950,'./widgets/counter_status/skins/tingenara','N','20141027231634',18620661,'1'),(18621989,18322923,'./modules/gagafilemd5','N','20141027095128',18684166,'1.0'),(18626854,18322927,'./widgets/ota_convert','N','20140927000849',19230465,'0.2'),(18626856,18322943,'./modules/integration_search/skins/default_ota','N','20140926152114',19230489,'0.2'),(18627986,18322927,'./widgets/banner_script','N','20141023004936',18634779,'0.1'),(18631776,18322950,'./widgets/content/skins/official_board_style','N','20141028204417',18638860,'1.2'),(18631835,18322954,'./layouts/kindguyLayout(201001_Kindguy4_1_xe1.3.1.2)','N','20141023135031',18634134,'201001'),(18631838,18322954,'./layouts/kindguyLayout(201001_Kindguy4_2_xe1.3.1.2)','N','20141021220031',18634163,'201001'),(18632016,18322943,'./modules/board/skins/faq','N','20141028192738',18636828,'1.3'),(18634568,18322927,'./widgets/quick_menu','N','20141028115122',18638902,'0.1.0'),(18634632,18322927,'./widgets/lnb_menu','N','20141028210908',20558937,'0.2.0'),(18634838,18322954,'./layouts/vz_clear_blue/','N','20140921083343',18635623,'0.2'),(18635216,18322950,'./widgets/login_info/skins/xgenesis_login','N','20141028003958',18638870,'0.1.0'),(18636930,18322927,'./widgets/MinionInXE','N','20141026112509',18835506,'1.4'),(18637860,18322954,'./layouts/xgenesis_official','N','20141028213639',19516685,'0.2.2'),(18640942,18322923,'./modules/pop_up','N','20141028223744',18646378,'0.0.4'),(18640943,18322925,'./addons/pop_up','N','20141028223846',19149746,'0.0.8'),(18642464,18322954,'./layouts/elkha_graystyle','N','20141028121837',19722768,'2.0.1'),(18642836,18322952,'./widgetstyles/gray_style','N','20141027213902',18826509,'1.2.1'),(18646646,18631347,'./modules/editor/skins/dreditor/drcomponents/iframe','N','20141025213405',18646655,'0.1'),(18647145,18322952,'./widgetstyles/mo_colorline','N','20141028204339',18654291,'0.3'),(18648969,18322925,'./addons/soo_for_muzik_player','N','20141025084114',19687129,'0.3.1'),(18649607,18322929,'./module/editor/components/jowrney_logmap','N','20141025022942',19533339,'0.4.0'),(18649610,18322950,'./widgets/login_info/skins/2010_jowrney_release','N','20141015160201',18654744,'0.1.0'),(18649613,18322954,'./layouts/2010_jowrney','N','20141025175743',19060126,'0.2.5'),(18650492,18322954,'./layouts/elkha_sky2','N','20141028145743',18865308,'1.4.5'),(18650580,18322929,'./modules/editor/components/soo_naver_bookinfo','N','20141025022950',19044122,'0.3.1'),(18652557,18631347,'./modules/editor/skins/dreditor/drcomponents/code','N','20141025022310',18652761,'0.1'),(18655593,18322954,'./xe/layouts','N','20141028143520',18667484,'1.0'),(18662544,18322954,'./layouts/blooz_layout_ver3','N','20141028223046',18701665,'3.2'),(18663182,18322954,'./layouts/shx_chameleon','N','20141020183917',18668568,'0.1.1'),(18664319,18322925,'./addons/P3P','N','20141026195327',18668421,'1.0'),(18666669,18322923,'./modules/stopsmoking','N','20141012124844',19493136,'0.2.2'),(18669571,18322977,'./modules/point/icons/dark','N','20141011230445',18672429,'1.0'),(18673912,18322929,'./modules/editor/components/soo_google_map','N','20141028122403',22231835,'0.9'),(18677776,18322954,'./layouts/lay','N','20141028115135',18682153,'0.1'),(18678675,18322943,'./modules/textyle/skins/Emplode','N','20141017234415',18700716,'0.6'),(18679839,18322929,'./modules/editor/components/soo_naver_image','N','20141025023005',18690439,'1.0.2'),(18681809,18322925,'./addons/age_restrictions','N','20141025190715',18687595,'1.0'),(18682481,18322925,'./addons/soo_js_exif','N','20141028123556',18859459,'0.3.4'),(18682907,18322954,'./layouts/ueo','N','20141028183342',19051858,'0.2'),(18686122,18322943,'./modules/board/skins/elkha_xe_official','N','20141024234035',18687734,'1.0'),(18697182,18322927,'./widgets/calendar_plannerXE123','N','20141028035513',22592897,'4.3.0'),(18700386,18322954,'./layouts/koo','N','20140921083306',18707058,'0.1'),(18703085,18322954,'./layouts/jimseung_nate','N','20141025221927',18705555,'1.0'),(18703356,18322954,'./layouts/how','N','20141028224320',18707091,'0.1'),(18705012,18322954,'./layouts/layoutSkin(kindguy5.1_xe1.4.0.5)','N','20141027165239',18708750,'201002'),(18705013,18322954,'./layouts/layoutSkin(kindguy5.2_xe1.4.0.5)','N','20141020111713',18708767,'201002'),(18706109,18322954,'./layouts/jimseung_biz','N','20140921083200',18711864,'0.1'),(18706113,18322954,'./layouts/jimseung_simplesub','N','20140924174802',18709461,'1'),(18712555,18322954,'./layouts/nom','N','20141023142612',18712759,'0.1'),(18712640,18322927,'./widgets/JW_player','N','20141028234429',18712773,'1.0'),(18714842,18322954,'./layouts/eond_portal_main_2col_right','N','20141028175455',21776053,'0.7'),(18716138,18322954,'./layouts/bom','N','20141025175520',18722236,'0.1'),(18716480,18322954,'./layouts/voo','N','20141028115034',18722243,'0.1'),(18717385,18322925,'./addons/music24/','N','20141025185835',18722175,'1.0.0'),(18722759,18322954,'./layouts/xe_sunooEmLg','N','20141023202355',19462122,'1.0'),(18730576,18322929,'./modules/editor/components/interpark_book_search','N','20141028195756',18740294,'0.1'),(18731809,18322943,'./modules/board/skins/loser_guestbook','N','20141025082333',19235463,'0.1'),(18735942,18322954,'./layouts/mediaOn/','N','20141027165344',18746917,'1.02'),(18739967,18322950,'widgets/content/skins/YGH_line','N','20141029001317',18741565,'0.1'),(18745485,18322923,'./modules/syndication','N','20141028230246',22754349,'4.0'),(18748689,18322954,'./layouts/daerew_v4_layout','N','20141027233339',18926143,'1.2'),(18750254,18322950,'./widgets/login_info/skins/daerew_v4_login','N','20141027233339',18751630,'1.0'),(18766685,18322954,'./layouts/elkha_graystyle2_lite','N','20141027133959',18844159,'1.0.3'),(18766699,18322943,'./modules/board/skins/quiet_board','N','20141027113929',18766890,'2.3'),(18766704,18322925,'./addons/member_layer_config','N','20141028230710',18766875,'1.1'),(18773076,18322923,'./modules/blogshop','N','20141028143101',18920619,'1.1'),(18773077,18631347,'./modules/editor/skins/dreditor/drcomponents/blogshop_writer','N','20141028143102',18920604,'1.1'),(18775186,18322950,'xe/modules/member/skins/default','N','20141022143838',18784334,'0.2'),(18777712,18322925,'./addons/add_document','N','20141028211220',18794485,'0.1.1'),(18779239,18322925,'./addons/daumview_vote','N','20141028224542',18898435,'0.5.5'),(18790298,18322925,'./addon/hellomaster','N','20141017091050',18794783,'1.0.0'),(18790924,18322954,'./layouts/xe_sunooEmRg','N','20141011113505',19462147,'1.0'),(18800584,18322923,'./modules/sboard','N','20141028210456',18878072,'2.1.0'),(18802611,18322950,'./widgets/content/skins/daerew_webzine_notice','N','20141028213727',18810316,'1.0'),(18802619,18322950,'./widgets/counter_status/skins/daerew_counter','N','20141027191641',19433478,'1.1'),(18809955,18322943,'./modules/editor/skins/tinyMCE','N','20141015181543',18810260,'1.4'),(18818977,18322954,'./layouts/blackcity/','N','20141028162346',18832088,'1.0'),(18827207,18322950,'./widgets/login_info/skins/git_login_simple','N','20141026215345',18993961,'2.0'),(18832037,18322954,'./layouts/rom','N','20141024082633',18837238,'0.1'),(18832352,18322923,'/editer/skins/','N','20141003105920',18838645,'1.0'),(18835470,18322927,'./widgets/splanner','N','20141028191131',18878338,'0.3.0'),(18846103,18322954,'./layouts/SORRENT_LAYOUT_RELEASE','N','20141022203428',18851320,'1.0'),(18846109,18322950,'./widgets/login_info/skins/SORRENT_LOGIN','N','20141022203428',18851330,'1.0'),(18849332,18322954,'./layouts/layout_skin(xenara1.1_xe1.4.0.10)','N','20141027164505',18853151,'201004'),(18852198,18322952,'./widgetstyle','N','20141022150120',18853308,'1.0.0'),(18853350,18322925,'./addons/member_join_captcha','N','20141028230842',18855317,'0.1.4'),(18855088,18322954,'./layouts/elkha_dr4','N','20141018153941',19703575,'1.0.2'),(18864982,18322977,'./modules/point/icons/cs_level','N','20141027171450',18866619,'0.1a'),(18866481,18322954,'./layouts/Treasurej_Lifestyle','N','20141028231748',21971882,'1.1'),(18867310,18322923,'./modules/project','N','20141023181340',21278683,'1.3.1'),(18877427,18322954,'./layouts/Treasurej_Craftwork','N','20141021215942',19032188,'1.0.1'),(18882151,18322925,'./addons/facebook_social','N','20141028232750',18882152,'0.1'),(18900548,18322954,'./layouts/layout_skin(kindguy1.0_type1_xe1.4.1.1)','N','20141023134612',18901309,'201005'),(18900551,18322954,'./layouts/layout_skin(kindguy1.1_type2_xe1.4.1.1)','N','20141028130202',18901322,'201005'),(18904767,18322977,'./modules/point/icons/raycity_m','N','20141004035239',18908827,'0.1v'),(18904819,18322977,'./modules/point/icons/raycity_f','N','20141024101238',18908837,'0.1v'),(18905882,18322923,'./modules/loginlog','N','20141121222157',22754408,'0.5.0'),(18910976,18904838,'./modules/editor/styles/dreditor','N','20141023111514',18910977,'0.1'),(18915805,18322943,'./modules/textyle/skins/babyBlack','N','20141015181614',18918781,'v0.1'),(18917848,18322954,'./layouts/xeVector','N','20141017223627',18918526,'0.2'),(18929288,18322954,'./layouts/mcube','N','20141018204357',18957849,'0.2'),(18929292,18322923,'./modules/mcubeimg','N','20141028211226',18983143,'0.3'),(18939397,18322950,'./widgets/login_info/skins/kan_login','N','20141018191543',18948357,'0.1'),(18943118,18322943,'./modules/bodex/skins','N','20141024143350',18953950,'완성버전'),(18953963,18322943,'./modules/board/skins/xe_official_sky','N','20141027095036',18971884,'1.2'),(18956310,18322923,'./modules/iconshop','N','20141027144919',18999633,'0.4'),(18956315,18322925,'./addons/member_icon_print','N','20141027144919',18968140,'0.3'),(18957505,18322927,'./widgets/cu3er','N','20141028211225',18983161,'0.2'),(18959079,18322954,'./layouts/layoutskin_wave_blue','N','20141026124215',21382225,'1.1.0'),(18968288,18322954,'./layouts/kinesis_sitelist','N','20141028004019',19348039,'0.1.1'),(18975451,18322954,'./layouts/layout_skin(kindguy5.0_type2_xe1.4.1.1)','N','20141022145812',18981166,'201006'),(18975452,18322954,'./layouts/layout_skin(kindguy5.1_type2_xe1.4.1.1)','N','20141027191019',18981176,'201006'),(18980346,18322943,'./modules/board/skins/sejin7940_board','N','20141028214223',20120497,'3.7'),(18982154,18322925,'./addons/addfooter/','N','20141027074713',18983942,'0.2'),(18982156,18322925,'./addons/additional_mid/','N','20141023111627',18983989,'0.2'),(18982164,18322925,'./addons/header_editor/','N','20141026011624',18984012,'0.3'),(18982175,18322925,'./addons/id_rejection/','N','20141027210204',18984037,'0.2'),(18982191,18322925,'./addons/meta_add/','N','20141027000119',19814958,'0.2.2'),(18982192,18322925,'./addons/q_emphasis/','N','20141008153026',18984109,'0.1'),(18982195,18322925,'./addons/soo_add_content/','N','20141027092125',22651867,'0.5'),(18982196,18322925,'./addons/soo_autolang/','N','20141028122847',19687115,'1.0.3'),(18982221,18322925,'./addons/soo_parking','N','20141028160134',20367602,'0.2.2'),(18990092,18322925,'./addons/IEblock','N','20141018130616',18993329,'0.1'),(18990133,18322925,'./addons/referercheck','N','20141008171922',19009627,'2.1'),(18990588,18322925,'./addons/soo_mcrblog_link','N','20141028172956',21924371,'1.2.14'),(18994748,18904838,'./modules/editor/components/emoticon/tpl/images/pink','N','20141028194811',18995710,'1.0'),(18995899,18322952,'./widgetstyles/sorrent_simplebox','N','20141026181454',18998803,'0.1'),(18997142,18322977,'./modules/point/icons/300','N','20141011125433',18998204,'1.0'),(18997930,18322923,'./modules/coupon','N','20141028222139',21627586,'1.0'),(18998734,18994172,'./m.layouts/naverstyle','N','20141028235116',19000655,'1.0'),(18999302,18322950,'./widgets/login_info/skins/kan_login_v2','N','20141014184008',19002080,'0.1'),(19009872,18904838,'./modules/editor/components/emoticon/tpl/images/congcon','N','20141023140449',19010544,'1.0'),(19015265,18322977,'./modules/point/icons/cool','N','20140930173051',19026346,'0.1v'),(19015269,18322977,'./modules/point/icons/CA_L_Mark','N','20141023154632',19026360,'0.1v'),(19018202,18322943,'./modules/board/skins/simple_blue','N','20141027095019',19023717,'1.0'),(19020313,18322943,'./modules/board/skins/pastel_light_purple','N','20141026182216',19028626,'1.0'),(19024107,18322923,'./modules/lottery','N','20141028074743',19027139,'0.1'),(19027281,18322950,'./widgets/attendance_check/skins/sky_next_line','N','20141027233403',19029151,'1'),(19030767,18322950,'./widgets/tab_newest_document/skins/tab_sky','N','20141025163621',19039476,'0.2'),(19030768,18322943,'./modules/attendance/skins/sky_at_board','N','20141025005227',19038444,'1'),(19031365,18322954,'./layouts/elkha_neutral','N','20141027221624',20692034,'0.5'),(19032971,18322954,'./layouts/Treasurej_Craftwork_C','N','20141027170305',19038047,'1.0.1'),(19034752,18322954,'./layouts/elkha_x610','N','20141029004025',19072093,'0.1'),(19044000,18322954,'./layouts/kom','N','20141020163611',19050135,'1.0'),(19044001,18322950,'./widgets/login_info/skins/tingenara','N','20141028004117',19050124,'1.0'),(19049200,18322925,'./addons/domain_check','N','20141013151723',19050476,'0.2'),(19050369,18322954,'./layouts/crom_fixy_layout_private','N','20141025180902',19053826,'1.0'),(19051939,18322954,'./layouts/zom','N','20141001075035',19087062,'0.2'),(19056755,18994170,'./modules/board/m.skins/xe_official_planner123','N','20141028210547',22592881,'4.3.0'),(19060125,18994172,'./m.layouts/','N','20141028232237',19063585,'0.1.3'),(19060827,18322954,'./layouts/xom','N','20141028183317',19092257,'0.2'),(19064264,18322977,'./modules/point/icons/getam','N','20141023024651',19064959,'0.1a'),(19064410,18322977,'./modules/point/icons/simple_TS','N','20141024142555',19064414,'1.0'),(19068106,18322977,'./modules/point/icons/nova2','N','20141005125852',19068107,'0.1a'),(19069946,18322925,'./addons/cookie-free_domains','N','20141008153007',19070012,'1.0'),(19071245,18322950,'./widgets/point_status/skins/cloverworld_skin/','N','20140829100700',19071386,'1.0.0.0'),(19073125,18322954,'./layouts/fsfsdas_neutral','N','20141028111721',19848942,'0.4.4'),(19073195,18322923,'./modules/krzip_popup','N','20141024023444',19073196,'0.1'),(19073227,18322943,'./modules/member/skins/default_krzip','N','20141024024001',19073228,'0.1'),(19076083,18322927,'./widgets/sayradio','N','20141024233221',19077336,'1.0.1'),(19077792,18322927,'./widgets/twitter_follow','N','20141027105719',19077793,'1.0'),(19080637,18322950,'./widgets/rank_point/skins/elkha','N','20141026080704',19080640,'0.1'),(19081557,18322950,'./widgets/newest_document/skins/layoutskin_webzine_v2','N','20141028212543',21596748,'1.2'),(19081914,18322925,'./addons/tweet_button','N','20141027164657',19083524,'0.1'),(19088419,18322927,'./widgets/contentextended/','N','20141028122226',20182258,'2.43'),(19088764,18322954,'./layouts/crom_black_box_layout','N','20141028000625',19089573,'1.0'),(19090619,18322954,'./layouts/nabul2_milate_8T','N','20141028124435',19092504,'2.0'),(19097462,18322954,'./layouts/pb','N','20141002164451',19125110,'1.2'),(19098862,18322954,'./layouts/crom_groove_eco_private','N','20141016055812',19099350,'1.0'),(19099015,18322954,'./layouts/ure','N','20141018163704',19099016,'0.1'),(19100570,18322954,'./layouts/PXE_koi','N','20141013133719',19504533,'1.0.1'),(19109313,18322927,'./widgets/content_specificdoc/','N','20141027115809',19109314,'0.2'),(19116278,18322954,'./layouts/modern_line/','N','20141028215246',19135412,'1.0'),(19122280,18322952,'./widgets/widgetstyles/','N','20141028091511',19122812,'1.0.0'),(19125571,18322943,'./moudles/board/skins','N','20141028235614',19128667,'v2'),(19130198,18322923,'./modules/analytics','N','20141028235720',22104216,'0.3.0'),(19130808,18322927,'./widgets/analytics_flash_counter','N','20141021162403',19157494,'0.2'),(19133209,18322927,'./widgets/newest_document_category/','N','20141028122327',19134377,'0.1'),(19133654,18322954,'./layouts/Rebirth_A','N','20141025235852',19224091,'1.1.2'),(19135133,18322954,'./layouts/seven','N','20141028161508',19955250,'1.2'),(19135768,18322950,'./widgets/newest_document/skins/factory_basic_2','N','20141023064843',19135769,'2.0'),(19136412,18322950,'./widgets/newest_comment/skins/factory_basic_2','N','20141020190101',19136413,'2.0'),(19137447,18322925,'./addons/rainbow_link','N','20141027164633',19431548,'0.3'),(19138636,18322954,'./layouts/xenoriter_simple','N','20141028052542',19138637,'1.0'),(19141813,18322950,'./widgets/webzine/skins/','N','20141028220325',19141814,'1.0'),(19145884,18322925,'./addons/always_follower','N','20141025192800',19503998,'1.1'),(19157569,18322925,'./addons/google_analytics','N','20141027164645',19157571,'1.0.0'),(19178969,18322954,'./layouts/xe_official_v2_TmaKing','N','20141028213846',19186638,'0.1.1'),(19182698,18322943,'./modules/board/skins/new_faq','N','20141028181849',20467493,'2.1'),(19187623,18322925,'./addons/refhide','N','20141003115523',19191147,'0.1.1'),(19197538,18322950,'/modules/editor/skins/webhard/','N','20141028195019',19291157,'0.2'),(19197549,18322943,'/modules/board/skins/webhard/','N','20141028195020',19291163,'0.2'),(19201015,18322954,'./layouts/kinesis_cs01f','N','20141027094855',19201021,'1.0.1'),(19201082,18322923,'/modules/mail_m9','N','20141026101014',19201083,'0.1'),(19202124,18322923,'./modules/lunar','N','20141023011943',19213083,'0.1.1'),(19202128,18322943,'./modules/member/skins/default(lunar)','N','20141023011943',19467792,'0.3'),(19202617,18322954,'./layouts/gom2net_layout','N','20140921110825',19204527,'1.0.2'),(19202629,18322925,'./addons/webfontface','N','20141012085029',19206513,'0.1'),(19208301,18322927,'./widgets/coinslider/','N','20141028123534',20182294,'1.5'),(19212262,18322923,'./modules/lucene','N','20141028031104',19315303,'1.2'),(19213125,18322927,'./widgets/solarlunar','N','20141026221529',19213126,'0.1'),(19218468,18322927,'./widgets/birthday','N','20141026093629',19218473,'0.1'),(19219093,18322954,'./layouts/kia','N','20141028182755',19219094,'0.1'),(19219428,18322927,'./widgets/srchat/','N','20141028160600',22754137,'219.45'),(19226818,18322954,'./layouts/gom2net_2nd_layout','N','20141027175755',19273763,'2.0.6'),(21344825,18322977,'./modules/point/icons/level','N','20141023154029',21360732,'2.0'),(19230703,18322954,'./layouts/eond_official','N','20141028175523',21382865,'1.4.1'),(19231437,18322950,'./widgets/planet_document/skins/eond','N','20141028175536',19283934,'1.0'),(19231709,18322952,'./widgetstyles/eond_webzine','N','20141025161518',19231710,'0.2'),(19231756,18322952,'./widgetstyles/eond_doubleline','N','20141028160038',19231762,'0.1'),(19232784,18322954,'./layouts/nabul2_Wishful','N','20141027233027',19232785,'1.0'),(19234197,18322954,'./layouts/eond_mynote','N','20141028175147',21723208,'1.4.4'),(19235403,18322943,'./modules/board/skins/eond_board','N','20141028212132',19235419,'0.2'),(19235552,18322950,'./widgets/login_info/skins/eond_mynote','N','20141028175147',21651021,'0.8'),(19235579,18322950,'./widgets/counter_status/skins/mynote','N','20141028175147',19252856,'0.1'),(19248816,18322954,'./layouts/xe_official_v2_Toyou','N','20141016001709',19258583,'c'),(19260194,18322927,'./widgets/contentslider','N','20141029003132',20199435,'2.1.1'),(19270268,18322950,'./widgets/bgw_menu/skins/naradesign','N','20141028113820',19270269,'0.1'),(19271512,18322950,'./widgets/point_status/skins/eond_official_login','N','20141027223331',19271513,'0.1'),(19274574,18322925,'./addons/a_soo_wikidoc_pointfixer','N','20140825132211',19274579,'1'),(19280154,18322954,'./layouts/kinesis_pl001f','N','20141024212755',19280155,'1.0'),(19283251,18322954,'./layouts/gom2net_3rd_layout','N','20141021215850',19283257,'3.0.1'),(19285120,18322943,'./modules/board/skins/win_guestbook','N','20141025222523',19295125,'0.5'),(19293487,18322925,'./addons/soo_mobile_top','N','20141027154409',20892008,'3'),(19299608,18322977,'./modules/point/icons/ToYou_level','N','20141021202623',19299609,'ToYou_level_icon v1.'),(19302110,18322954,'./layouts/gardenoforchids','N','20141024140440',19302111,'1.0'),(19306395,18322925,'./addons/shortcut','N','20141003102722',19306492,'0.9.9.2'),(19306873,18904838,'./modules/editor/components/emoticon/tpl/images/hicon','N','20141025163220',19310220,'1.0.0'),(19310933,18322927,'./widgets/xclient/','N','20141023114121',19660872,'1.2.0'),(19320728,18322954,'./layouts/darkdream','N','20141001074952',19320733,'1.0'),(19322818,18322954,'./layouts/darkgrid','N','20141018204032',19322819,'1.0'),(19323693,18322923,'./modules/antiaccess','N','20141028190417',20181898,'1.0.3.1'),(19325680,18322923,'./modules/pipingxe','N','20141025010050',19546936,'1.0.5'),(19330518,18322927,'./widget/tocplus','N','20141023112858',19348560,'0.2'),(19330741,18322950,'./widgets/tab_newest_document/skins/colorful','N','20141021185017',19330742,'1.0'),(19336589,18322925,'./addons/soo_block_UA','N','20141015222720',19336590,'0.1'),(19340331,18322954,'./layouts/CN_No1','N','20140921082440',19344956,'1.0.1'),(19344633,18322943,'./modules/member/skins','N','20141025042551',19349355,'1.0.0'),(19346257,18322927,'./widgets/googlesearch','N','20141027231116',19349099,'1.0.1'),(19348911,18322943,'./modules/board/skins/simpleborder_guestbook','N','20141028205006',19356183,'1.2'),(19349000,18322943,'./modules/board/skins/xe_board_extended','N','20141028235536',19349001,'1.0'),(19351727,18322954,'./layouts/crom_eco','N','20141025171733',19351728,'1.0'),(19353209,18322950,'./widgets/tab_newest_document/skins/tab_flash','N','20141026212720',19353210,'1.0'),(19355038,18322950,'./widgets/googlesearch/skin/multi_box','N','20141027231114',19355039,'1.0'),(19355511,18904838,'./modules/editor/skins/simple_editor','N','20141027234021',19355526,'1.0'),(19355521,18322950,'./widgets/googlesearch/skin/translate','N','20141007114254',19355602,'1.0'),(19360170,18322954,'./layouts/layout_skin(xenara_v1.0_type1_xe1.4.4.1)','N','20141002095502',19360187,'201010'),(19360171,18322954,'./layouts/layout_skin(xenara_v1.1_type3_xe1.4.4.1)','N','20141023221202',19360205,'201010'),(19426823,18322943,'./modules/poll/skins/clevis_poll/','N','20141024152733',19440072,'0.3'),(19428586,18322954,'./layouts/fullmetal_by_daramkun','N','20141027165451',19432660,'1.0.0'),(19431275,18322943,'./module/board/skins','N','20141027231018',19432793,'0.2'),(19431519,18322925,'./addons/favicon','N','20141025204902',19434038,'1.1'),(19433415,18322925,'./addons/jquery_external_load','N','20141027190643',22674018,'2.0'),(19440527,18322954,'./layouts/CN_No2','N','20141016144320',19440528,'1.0.1'),(19442769,18322925,'./addons','N','20141025185753',19455388,'0.1.1'),(19988049,18322977,'modules/point/icons','N','20141024142850',19988222,'1.1.1'),(19456969,18322954,'./layouts/impress-06','N','20140921082243',19464583,'1.0.1'),(19458868,18322925,'./addons/soo_feed_delay','N','20141026004118',19458869,'0.1'),(19462008,18322927,'./widgets/login_sunoo','N','20141028225437',19462009,'1.0'),(19462173,18322954,'./layouts/xe_sunooNSLg','N','20141011113458',19462174,'1.0'),(19462195,18322954,'./layouts/xe_sunooWALg','N','20141027233117',19462196,'1.0'),(19473533,18322943,'./modules/integration_search/skins/xgenesis_official','N','20141028003958',19473716,'0.1.0'),(19476930,18322927,'./widgets/stopsmoking_status','N','20141012124844',19476931,'0.1.0'),(19491937,18322954,'./layouts/xe_sunooTALg','N','20141025220451',19491938,'1.0'),(19496679,18904838,'./modules/editor/components/emoticon/tpl/images','N','20141025163148',19504007,'0.1'),(19497436,18322923,'./modules/analysis','N','20141023181543',19528063,'0.1.2'),(19503269,18322925,'./addons/analysis','N','20141023181543',19527972,'0.1.2'),(19506416,18322954,'./layouts/PXE_clio','N','20141028234707',19506418,'1.1'),(19509849,18322954,'./layouts/hankooktown','N','20141024212704',19509864,'1.0'),(19510234,18322954,'./layouts/gallery_layout','N','20141027055956',19532739,'0.2'),(19510889,18322923,'./modules/portalpoint','N','20141027022054',19741258,'1.2'),(19512714,18322954,'./layouts/heaven','N','20141011050417',19514431,'Alpha'),(19513447,18322925,'./addons/sns_linker_lite','N','20141028212553',22754189,'1.1'),(19513978,18322954,'./layouts/layout_photoGalleryA_Free','N','20141024153230',19514630,'1.0'),(19514473,18322943,'./modules/issuetracker/','N','20141021114304',19539420,'1.1.0'),(19514688,18322927,'./widgets/xgenesis_login','N','20141028155558',19539957,'0.1.1'),(19515289,18322927,'./widgets/minion4','N','20141028100209',19635737,'2.0.1'),(19515672,18322954,'./layouts/xe_sunooBCLg','N','20141027233057',19515673,'1.0'),(19518187,18322923,'./modules/socialxe','N','20141028224550',22123379,'1.0.11'),(19518188,18322923,'./modules/socialxeserver','N','20141028164035',22120897,'1.0.11'),(19518196,18322925,'./addons/socialxe_helper','N','20141028212259',20361435,'1.0.6'),(19518201,18322927,'./widgets/socialxe_comment','N','20141028212259',20361452,'1.0.8'),(19518204,18322927,'./widgets/socialxe_info','N','20141028141810',19679127,'1.0.6'),(19519171,18322950,'/widgets/tab_newest_document/skins/xe_official','N','20141024140236',19519369,'xe_official'),(19519182,18322923,'./modules/aroundmap','N','20141028145329',19519377,'0.2'),(19519186,18322925,'./addons/qrcode','N','20141028141151',19528193,'0.2'),(19519188,18322923,'./modules/rssboard','N','20141028141124',19539111,'0.3'),(19519235,18322923,'./modules/sphinx','N','20141021104142',19519336,'0.2'),(19522899,18322923,'./modules/bannermgm','N','20141028193230',19523059,'0.1'),(19522900,18322927,'./widgets/bannermgm_widget','N','20141028193230',19525794,'0.2'),(19524346,18322950,'./widgets/login_info/skins/gallery_layout_login','N','20141025132946',19527566,'0.2'),(19524575,18322923,'./modules/alliance','N','20141012124818',19652232,'1.2.2'),(19524576,18322923,'./modules/alliancehub','N','20141007180127',19542992,'1.0.1'),(19524772,18322925,'./addons/alliancexe_grab_comment','N','20140806151636',19586763,'0.3'),(19525249,18322943,'./modules/textyle/skins/fotowallXE','N','20141027193512',19526784,'0.4.0'),(19526505,18322927,'./widgets/bible_read','N','20141024215123',19585818,'1.1'),(19526573,18322943,'./modules/board/skins/lune_board/','N','20141028235454',20290780,'1.04'),(19527443,18322925,'./addons/event_board','N','20141022141219',19527447,'0.1'),(19527550,18322927,'./widgets/gallery_layout_widget','N','20141028211138',19532746,'1.0.0'),(19527787,18322927,'./widgets/sitemap','N','20141028003600',19527788,'0.1.0'),(19529399,18322954,'./layouts/crom_iXE/','N','20141028211327',19600243,'1.0.3'),(19529916,18322943,'./modules/editor/skins/jowrneyEditor','N','20141026221934',19533373,'0.1.0'),(19529917,18322977,'해당사항없음','N','20141024120430',19533355,'0.1.0'),(19530900,18322950,'./widgets/content/skins/church_skin','N','20141027233500',19532808,'0.2'),(19530901,18322950,'./widgets/login_info/skins/church_layout_login','N','20140930223328',19530913,'0.1'),(19532317,18322954,'./layouts/portal_layout','N','20141028160933',19533824,'0.3'),(19532779,18322954,'./layouts/church_layout','N','20141015170049',19532784,'0.2'),(19533731,18322977,'./modules/point/icons/xeicon_coa','N','20141027120043',19736559,'3.0'),(19534671,18322927,'./widgets/sejin7940_calendar','N','20141028175217',19534672,'1.0'),(19548524,18322927,'./widgets/newest_document_tab','N','20141029000858',19548663,'0.2'),(19549401,18322925,'./addons/source_marking','N','20141021054106',19549402,'0.3'),(19550402,18322925,'./addons/socialxe_mid_forwarder','N','20141024073340',20361446,'1.0.3'),(19551431,18322977,'./modules/document/tpl/icons','N','20141004034557',19551432,'0.1'),(19555797,18322925,'./addons/prettyphoto','N','20141028224534',21336236,'1.1.3.0'),(19555878,18322925,'./addons/webfont','N','20141028140201',21378394,'1.1.3.1'),(19555887,18322925,'./addons/css3pie','N','20141029002752',20878725,'1.3.3.0'),(19555890,18322954,'./layouts/sketchbook5','N','20141028193057',21336191,'1.6.3.6'),(19555903,18322943,'./modules/board/skins/sketchbook5','N','20141029003319',22754336,'1.7.0'),(19555926,18322925,'./addons/tag_relation/skins/default','N','20141028184229',19915132,'0.9.5'),(19555927,18322950,'./widgets/socialxe_comment/skins/sketchbook5','N','20141028212259',22509535,'1.7.0'),(19560898,18322943,'./modules/member/skins/photoGalleryA','N','20141027112332',19560902,'1.0'),(19565911,18322925,'./addons/soo_body_content','N','20141023105307',19565912,'0.2'),(19566135,18322977,'./modules/point/icons/NetCabin_Lvic','N','20141009044152',19576465,'0.1'),(19570840,18322954,'./layouts/CN_No3','N','20141023125251',19582438,'1.0.4'),(19574799,18322925,'./addons/','N','20141028202840',19576713,'0.1'),(19583417,18322925,'./addons/wiki-link','N','20141017223820',19600787,'0.2'),(19594292,18994172,'./m.layouts/sketchbook5Mobile','N','20141028232204',20557098,'1.2.2.3'),(19594435,18994170,'./modules/board/m.skins/sketchbook5Mobile','N','20141028233104',20973906,'1.2.3.3'),(19600206,18322925,'./addons/autowww','N','20141023213610',19604227,'1.0'),(19608490,18322925,'./addons/del-www','N','20141017135141',19608585,'0.1'),(19618448,18322954,'./layouts/Tony/','N','20141028210827',19620083,'0.1.1'),(19618480,18322950,'./widgets/login_info/skins/Tony/','N','20141021171833',19618481,'0.1.0'),(19623053,18322950,'./widgets/login_info/skins/neutral','N','20141028091020',20803425,'0.2'),(19623082,18322950,'./widgets/login_info/skins/graystyle','N','20141028121916',20702518,'0.21'),(19623904,18322954,'./layouts/layout_photoGalleyA_sub','N','20141027112332',19623910,'1.2'),(19623994,18322954,'./layouts/layout_newsMagazine_free','N','20141028000049',19623995,'1.0'),(19624853,18322950,'./widgets/login_info/skins/Quad','N','20141022141024',19624859,'1.0.0'),(19624858,18322954,'./layouts/Quad/','N','20141018021705',19630832,'1.0.3'),(19630138,18322954,'./layouts/SimpleDropDown','N','20141019221521',20467486,'2.0.0'),(19637507,18322943,'./modules/board/skins/JB_erebus_board','N','20141028222935',22450338,'1.3.1'),(19639463,18322952,'./widgetstyles/lineBox','N','20141028211350',19639464,'0.1'),(19655120,18322954,'./layouts/NetCabin_X3','N','20141027174717',20430977,'0.2.2'),(19657758,18322954,'./layouts/NetCabin_X2','N','20141008165801',19657761,'0.1'),(19657941,18322929,'./modules/editor/components/chess','N','20141025022937',19688815,'1.1.2'),(19673444,18904838,'./modules/editor/styles/NomarginPTag','N','20141024223856',19675462,'0.0.1.1'),(19674194,18904838,'./modules/editor/styles/misol','N','20141027234009',19674198,'0.1'),(19674471,18322927,'./widgets/twitter','N','20141028100121',19676523,'0.2'),(19684891,18322954,'./layouts/elkha_pieces','N','20141025203109',19788968,'0.2'),(19684961,18322950,'./widgets/login_info/skins/eond_gateway','N','20141021010249',19684962,'0.5'),(19692489,18322927,'./xe/widgets/music24_kr_clock','N','20141027174450',19692490,'1.0.0'),(19692912,18322925,'./xe/addons/music24/','N','20141028234906',19692913,'1.0.0'),(19700913,18322954,'./layouts/elkha_monochrome','N','20141027105522',19803893,'0.12'),(19702417,18322950,'./widgets/login_info/skins/monochrome','N','20141027205733',20803243,'0.2'),(19702419,18322950,'./widgets/language_select/skins/monochrome','N','20141027105523',19702444,'0.1'),(19705472,18322927,'./widgets/content/skins/XEgrid_content','N','20141021123738',19705666,'1.1'),(19707673,18322950,'./widgets/newest_document/skins/CN_No_series','N','20141024194530',19707678,'1.1'),(19707730,18322952,'./widgetstyles/CN_No_series','N','20141028155950',19707731,'1.1'),(19707750,18322954,'./layouts/CN_No4','N','20140930154441',19708324,'1.1'),(19708869,18322927,'./widgets/navigation','N','20141028112307',19712189,'0.4'),(19711536,18322954,'./layouts/people blue','N','20141021134020',19744693,'0.5'),(19712183,18322954,'./layouts/smart','N','20141028210411',20902184,'0.9.9'),(19712751,18322954,'./layouts/layout_skin(xenara_v1.2_type2_xe1.4.5.2)','N','20141024140011',19712752,'1.2'),(19723352,18322927,'./widgets/facebook','N','20141028233059',19723353,'0.1'),(19740666,18322943,'./modules/textyle/skins/PHOTOGRAPHER','N','20141026195721',19757669,'0.2'),(19740680,18322943,'./modules/textyle/skins/DESIGNER','N','20141024153304',19757652,'0.2'),(19740711,18322943,'./modules/textyle/skins/designspiration','N','20141027190917',19757610,'0.2'),(19744664,18322943,'./modules/textyle/skins/BlueMood','N','20141026180034',19757584,'0.2'),(19749792,18322954,'./layouts/layoutwotc_text','N','20141028004554',22596494,'1.0.7'),(19754728,18322977,'./modules/point/icons/ca_ladder_60','N','20141023154010',19755182,'0.1'),(19759864,18322927,'./widgets/google_map','N','20141028194057',19759892,'0.2'),(19765252,18322954,'./layouts/XEgrid_Free','N','20141025235803',19765321,'1.0.3'),(19767397,18322954,'./layouts/columnist','N','20141027054518',20270404,'1.5.1'),(19775760,18322950,'./widgets/content/skins/Photographer','N','20141026195721',19775761,'0.1'),(19775788,18322950,'./widgets/content/skins/default_new','N','20141024194514',19775789,'0.1'),(19775816,18322950,'./widgets/content/skins/default2','N','20141026180034',19775820,'0.1'),(19775829,18322950,'./widgets/tag_list/skins/default1','N','20141026195721',19775830,'0.1'),(19775849,18322950,'./widgets/tag_list/skins/default2','N','20141015033002',19775850,'0.1'),(19775878,18322950,'./widgets/tag_list/skins/default3','N','20141026180034',19775879,'0.1'),(19775899,18322950,'./widgets/counter_status/skins/Designspiration','N','20141004185614',19775901,'0.1'),(19775908,18322950,'./widgets/counter_status/skins/default_new','N','20141024153306',19775909,'0.1'),(19775924,18322950,'./widgets/counter_status/skins/default2','N','20141026180034',19775928,'0.1'),(19775942,18322950,'./widgets/category/skins/Designspiration','N','20141023204409',19775943,'0.1'),(19775958,18322950,'./widgets/category/skins/BlogskinDesigner','N','20141026180034',19775962,'0.1'),(19775971,18322950,'./widgets/category/skins/default_new','N','20141024153306',19775972,'0.1'),(19804189,18322925,'./addons/elkha_www','N','20141028113307',20702493,'0.11'),(19806836,18322977,'modules/point/icons/elkha_poporina_zerostar50','N','20141025013948',19806837,'0.1'),(19807569,18322950,'./widgets/login_info/skins/webengine_black','N','20141028193335',19827659,'1.2'),(19816429,18322954,'./layouts/CN_No5','N','20141023145821',19816430,'1.0'),(19816467,18322950,'./widgets/newest_document/skins/CN_No5','N','20141024194445',19816468,'1.0'),(19816486,18322950,'./widgets/newest_comment/skins/CN_No5','N','20141028185846',19816487,'1.0'),(19817434,18322929,'modules/editor/components/eh_player','N','20141028213108',19828441,'0.3'),(19818901,18322954,'./layouts/ikarusv1simple','N','20141028235725',19829113,'1.1.0'),(19831182,18322954,'./layouts/layout_skin(xenara_v3.0_type2_xe1.4.4.4)','N','20141027154926',19831183,'3.0'),(19831194,18322954,'./layouts/layout_skin(xenara_v3.1_type2_xe1.4.4.4)','N','20141001192648',19831195,'3.1'),(19833041,18322952,'./widgetstyles/sctb','N','20141028220657',20213631,'6.0'),(19834157,18322954,'xe/layouts','N','20141022153825',19834158,'0.2.1'),(19849619,18322952,'./widgetstyles/SteelblueRound','N','20141026161228',19849620,'0.1'),(19854096,18322927,'./widgets/qrcode_creator','N','20141028234302',19854097,'0.1'),(19859881,18322925,'./addons/googleplus/','N','20141019171215',19864516,'0.1.1'),(19862381,18322925,'./addons/soo_googleplus','N','20141027164552',19890691,'0.2.1'),(19875631,18322925,'./addons/gosite','N','20141014103817',19875632,'0.1'),(19885185,18322943,'./modules/board/skins/sr_memo','N','20141028235824',20959847,'0.9.1'),(19891355,18322954,'./layouts/steelblue4_Basic','N','20141024071402',19891356,'4'),(19894029,18322943,'./modules/bodex/skins/sw_contact','N','20141027145318',19902554,'0.9'),(19901434,18994170,'./modules/board/m.skins','N','20141028233204',19902394,'0.1'),(19906026,18322925,'./addons/mypeople','N','20141018030505',19906139,'0.0.1'),(19918081,18322943,'./modules/board/skins/CNboard','N','20141028222129',19918082,'1.0'),(19918823,18322954,'./layouts/cimple_plus','N','20141028185332',19921280,'1.3c'),(19923002,18322925,'./addons/sejin7940_write_limit/','N','20141028175054',22687892,'1.5.2'),(19960240,18322925,'./addons/sejin7940_readed_count','N','20141028175208',19960243,'1.6'),(19962621,18322943,'./modules/member/skins/noangel_black','N','20141024153055',19962952,'1.0a'),(19964934,18322950,'/widgets/login_info/skins/cronos_free','N','20141028233606',20187569,'1.1'),(19974913,18322954,'./layouts/Dynamic','N','20141028194539',20429124,'3.0'),(19976643,18322950,'./modules/message/skins/naruCD','N','20141022164033',19984421,'0.1.2'),(19983564,18322977,'./modules/editor/components/emoticon/tpl/images/','N','20141027204551',19984752,'1.4.5.10'),(20003560,18322977,'레이아웃에서 직접 업로드','N','20141022130556',20003621,'1.0'),(20006558,18322950,'./widgets/counter_status/skins/mamgood_counter','N','20141024155932',20017755,'1.0'),(20048768,18322954,'/layouts/store_style','N','20141026000359',20048864,'1.2'),(20048812,18322950,'./widgets/point_status/skins/save_style','N','20141021215630',20048823,'1.0'),(20070033,18322927,'./widgets/widget_kgcalendar','N','20141027190339',20117642,'1.1'),(20070206,18322927,'./widgets/widget_kgcontent','N','20141028222655',20315271,'1.2'),(20072467,18322954,'/layouts/store_style2','N','20141026171032',20072471,'2.0'),(20074878,18322954,'./layouts/elkha_tskorea','N','20141028225246',20691958,'0.2'),(20075809,18322950,'./widgets/language_select/skins/tskorea','N','20141028225159',20092424,'0.1'),(20075810,18322927,'./widgets/layout_info','N','20141013184015',20092486,'0.1'),(20078903,18322950,'./widgets/counter_status/skins/mamgood_counter2','N','20141024212416',20078904,'1.0'),(20078936,18322950,'./widgets/content/skins/mamgood_content','N','20141023111820',20078937,'1.0'),(20078965,18322950,'./widgets/login_info/skins/mamgood_login','N','20141021111501',20078966,'1.0'),(20079057,18322954,'./layouts/minipaper_style','N','20141025234950',20634505,'1.5'),(20079764,18322950,'./widgets/counter_status/skins/flash','N','20141027150205',20079797,'0.1'),(20091784,18322927,'./widgets/shopxeslider','N','20141028194013',20123108,'V1.1'),(20092690,18322950,'widgets/content/skins/elkha_nivo','N','20141024053849',20092697,'0.1'),(20092760,18322925,'./addons/elkha_packer','N','20141027160447',20702463,'0.11'),(20117694,18322943,'.modules/board/skins/','N','20141028225212',20117695,'1.0'),(20118343,18322925,'./addon/cufon','N','20141003204450',20190605,'0.1.0'),(20120961,18322927,'./widgets/treasurej_popular','N','20141028224025',22550390,'1.0.5'),(20122381,18322950,'./widgets/treasurej_popular/skins/treasurej_popular_tabr','N','20141028182123',21972737,'1.1'),(20155119,18322943,'./modules/member/skins/XET_member','N','20141027152111',21197586,'1.2'),(20155171,18322943,'./modules/communication/skins/XET_communication','N','20141027152111',21344485,'1.2.1'),(20168220,18322954,'./layouts/xe_cafe_site','N','20141028175707',21803913,'1.7.0'),(20168286,18322950,'./widgets/login_info/skins/cafe_site','N','20141028175707',21802090,'1.7.0'),(20168297,18322950,'./widgets/language_select/skins/cafe_site','N','20141028175708',21802140,'1.7.0'),(20176065,18322954,'/layouts/store_style25','N','20141022220730',20176066,'2.5'),(20185969,18322927,'./widgets/autoredirect/','N','20141022172953',20185972,'1.0'),(20186750,18322950,'./widgets/treasurej_popular/skins/treasurej_popular_tabs','N','20141028150038',21972593,'1.1'),(20187337,18322923,'./modules/guestbook','N','20141028221313',21962590,'1.7.0.1'),(20187411,18322923,'./modules/faq','N','20141028221430',21854296,'1.7.0.1'),(20187450,18322923,'./modules/contact','N','20141028221242',21968983,'1.7.0.2'),(20191860,18322925,'./addons/me2plugin','N','20141003102717',20191861,'1.0'),(20236415,18322943,'./modules/attendance/skins/sr_at_skin','N','20141024214842',20236418,'0.1'),(20242228,18322943,'/modules/editor/skins/','N','20141026230727',20624981,'0.1'),(20259612,18322954,'./layouts/HappyTravel_v1','N','20141022234445',20261781,'1.0'),(20276661,18322950,'./widgets/login_info/skins/cafe_official','N','20141028202141',21801927,'1.7.0'),(20276676,18322950,'./widgets/language_select/skins/xe_cafe_language','N','20141028151047',20276677,'0.1'),(20276726,18322954,'./layouts/xe_cafe_hub','N','20141028171437',21803871,'1.7.0'),(20277082,18322943,'./modules/homepage/skins/xe_cafe_v2','N','20141024143212',20309227,'0.1.1'),(20277901,18322925,'./addons/controlbox','N','20141029002752',20632434,'1.0.1'),(20279228,18322923,'./modules/boardauction','N','20141027211652',20295567,'0.1.1'),(20279332,18322943,'./modules/board/skins/xe_auction','N','20141027211651',20369078,'0.1.1'),(20290703,18994170,'./modules/board/m.skins/m_sr_memo','N','20141029002109',20300033,'0.3.6'),(20324298,18322923,'./modules/textmessage','N','20141121202538',22754396,'2.3'),(20324311,18322923,'./modules/notification','N','20141121155016',22754426,'2.3.3'),(20330088,18322954,'./layouts/layoutwotc_portal','N','20141029003939',20691619,'1.0.2'),(20340640,18322925,'./addons/iphone_checkbox','N','20141023213541',20409821,'1.0a'),(20350195,18322927,'./widgets/contentslist','N','20141029003155',20350196,'0.5'),(20393822,18322923,'./modules/newposts','N','20141028211405',22754134,'2.1.5'),(20401336,18322954,'./layouts/style_a_lite','N','20141029002305',22754384,'2.1.3'),(20415487,18322943,'./modules/lottery/skins/simple/','N','20141028074737',20451828,'1.1'),(20427455,18322952,'./widgetstyles/admin_ws','N','20141028160006',20454155,'0.2'),(20453531,18322925,'./addons/bodyfade/','N','20141023214640',21401825,'0.2.3'),(20458826,18322954,'./layouts/we_home','N','20141028183940',20980624,'1.8'),(20464644,18322927,'./widgets/vanner','N','20141028210919',20464663,'0.2'),(20466120,18322925,'./addons/elfinder','N','20141028214742',20480086,'0.1.1'),(20472943,18322954,'./layouts/xe_solid_enterprise_LeCiel_v1','N','20141027203420',20613484,'1.7'),(20473328,18904838,'./modules/editor/skins/fckplus','N','20141027191504',20487316,'1.1a'),(20473753,18322943,'./modules/editor/skins/xeed','N','20140808003445',20473754,'0.2'),(20476783,18904838,'./modules/editor/skins/fckplus_SimpleWhite','N','20141028144552',20487172,'1.1a'),(20476937,18322943,'./modules/contact/skins/cameron','N','20141028220603',21970579,'1.5'),(20493834,18322950,'./widgets/content/skins/updatenews','N','20141028183940',21134264,'1.8'),(20495669,18322943,'./modules/member/skins/simple','N','20141024185057',20507441,'0.1'),(20502461,18322943,'./modules/message/skins/cmd_message','N','20140925140928',20502462,'0.1'),(20509760,18322943,'./modules/page/skins/sejin7940_page','N','20141028172834',22572810,'1.4.1'),(20514706,18322943,'./modules/board/skins/simple_board','N','20141028223653',22754093,'1.3'),(20519604,18322943,'./modules/member/skins/simple_for_14','N','20141027153005',21193099,'0.2'),(20520233,18322952,'./widgetstyles/sketchbook5_wincomi','N','20141028211314',20798858,'3.0'),(20520855,18322925,'./addons/color_message','N','20140924042735',20520858,'1.1'),(20522778,18322925,'./addons/color_message_for_14','N','20141003102938',20522789,'1.0'),(20522820,18322950,'./widgets/bible_read/skins/KnDol','N','20141024215123',20590447,'1.1.0'),(20525058,18322925,'./addons/popup_menu_like_1_4/','N','20141011063237',20798880,'1.0'),(20526823,18322925,'./addons/me2plugin_for_14','N','20141003103107',20526828,'1.0'),(20531619,18322954,'./layouts/white_square_layout','N','20141027165733',20882875,'1.3'),(20533710,18322950,'./widgets/xeBanner/skins','N','20141026152957',20533711,'1.0.0'),(20547035,18322925,'./addons/exif','N','20141027141021',21378417,'0.9.2.2'),(20557173,18322950,'./widgets/content/skins/xe2011_contributor_present','N','20141028233634',20557174,'0.1'),(20558958,18322950,'./widgets/lnb_menu/skins/','N','20141028003358',20558964,'0.1.0'),(20564368,18322925,'./addons/sejin7940_align/','N','20141028175152',20564370,'1.0'),(20579823,18322954,'./layouts/SilverCloud','N','20141028113528',20579824,'1.0'),(20605745,18322927,'./widgets/widget_kgmedia','N','20141028234207',20695833,'1.1'),(20612563,18322927,'./widgets/contentsmedia','N','20141027200036',20696865,'0.7'),(20644220,18322925,'./addons/prohibit_monologue','N','20141023225657',20644221,'0.1'),(20649732,18322925,'./addons/commentwritedownload/','N','20141026102855',20681999,'0.3'),(20670102,18322923,'./modules/lisense','N','20141021231531',20692149,'0.1'),(20673638,18322925,'./addons/html5audio_flash','N','20141025185504',22541039,'1.5.1'),(20673640,18322925,'./addons/falling_snow','N','20141019213928',20697610,'1.5.0'),(20673970,18322923,'./modules/referer','N','20141028205000',22754289,'3.7.1'),(20673999,18322925,'./addons/referer','N','20141028204928',22754264,'3.3.3'),(20687933,18322954,'./layouts/xdt_offical_2','N','20141027141509',20949015,'1.4'),(20707031,18322943,'/modules/contact/skins','N','20141028220611',20707032,'1.0'),(20710471,18322923,'./modules/checkip','N','20141028233136',20765854,'0.2.2'),(20789735,18322943,'./modules/socialxe/skins/bootstrap.single','N','20141028055001',20789736,'1.0'),(20792413,18322950,'./widgets/content/skins/sticky_note','N','20141026030147',20792414,'1.0.3.0'),(20796792,18322927,'./widgets/notice','N','20141029003435',22754121,'1.0.4'),(20806148,18322925,'./addons/nonebutton/','N','20141012165007',20823285,'0.1.1'),(20832909,18322927,'./widgets/user_finder','N','20141027212651',20836373,'0.1'),(20832931,18322923,'./modules/user_finder','N','20141027212650',20836347,'0.1'),(20882492,18322923,'./modules/purplebook','N','20141027105147',22754128,'3.0'),(20908270,18322923,'./modules/detail_search','N','20141028224332',20949711,'0.1.1'),(20908354,18322950,'./widgets/dswidget','N','20141028224440',20950044,'0.1.1'),(20927819,18322950,'./widgets/login_info/skins/photo15','N','20141027133524',22253694,'1.1'),(20936395,18322923,'./modules/umessage','N','20141028224553',20943903,'1.1'),(20949728,18322925,'./addons/CssOutPlus','N','20141003102551',20952200,'1.1'),(20951206,18322925,'./addons/css3pie_js','N','20141022115609',20966650,'1.1.1'),(20954749,18322925,'./addons/message_alarm','N','20141028224636',21041089,'2.0'),(20957609,18322925,'./addons/wating_message','N','20141003102114',20957612,'0.1.1'),(20959091,18322925,'./addons/doc_viewer','N','20141027191819',20959094,'0.1.2'),(20966755,18322954,'./layouts/xdt_community','N','20141019223736',21002067,'1.1'),(20971314,18322925,'./addons/kakao_link','N','20141028133054',20975200,'0.2'),(20972639,18322943,'/modules/editor/skins/xpresseditor/','N','20141028175138',20973083,'1.1'),(20989209,18322954,'./layouts/Chemistry_lite','N','20141028160501',20989210,'1.0'),(20999893,18322927,'./widgets/sys_status','N','20141027224500',21005314,'3.1.1'),(21003996,18322927,'./widgets/xestream','N','20141028234140',21014531,'0.2'),(21004386,18322950,'./widgets/sys_status/skin/simple','N','20141002031743',21004387,'0.1'),(21009029,18322954,'./layouts/xdt_community_2','N','20141025191231',21009030,'1.0'),(21014822,18322929,'./modules/editor/components/soo_youtube','N','20141028220443',21039496,'0.5.1'),(21015635,18322925,'./addons/appoint_view_user','N','20141028234811',21409930,'1.0'),(21038796,18322950,'./widgets/point_status/skins/bootstrap','N','20141029001555',21146775,'1.2'),(21038825,18322950,'./widgets/content/skins/sketchbook5_style','N','20141027205306',21648135,'1.0'),(21090780,18322954,'./layouts/pleasure','N','20141026000159',21092056,'1.2'),(21092346,18322925,'./addons/xdt_button','N','20141028175926',21739119,'2.1'),(21124707,18322925,'./addons/soo_add_ssl','N','20141028022937',21124708,'0.1'),(21127741,18322927,'./widgets/contentextended','N','20141028234124',21189359,'2.45'),(21146815,18322950,'./widgets/sys_status/skins/tb','N','20141019212527',21146816,'1.0'),(21154641,18322925,'/addons/iframe_resize','N','20141028010727',21189969,'0.2'),(21189057,18322925,'./addons/settitle','N','20141026200505',21383555,'1.3'),(21189175,18322925,'./addons/texteffect','N','20141011130605',21197591,'0.2 beta'),(21190663,18322925,'./addons/report_addon','N','20141028032612',21194703,'0.2'),(21194822,18322929,'./modules/editor/components/simple_jw/','N','20141026234436',21364752,'0.3.0'),(21194850,18322925,'./addons/bootstrap_btn','N','20141028142849',21194883,'1.0'),(21195053,18322925,'./addons/bootstrap_icon','N','20141021220939',21202617,'1.1a'),(21195185,18322923,'./modules/authentication','N','20141028205950',22754169,'3.1.2'),(21196855,18322927,'./widgets/camtv','N','20141028234112',21228634,'0.1'),(21204144,18322925,'./addons/number_display','N','20141023225613',21204145,'0.1'),(21211103,18322923,'./modules/sejin7940_comment','N','20141028123350',22724164,'1.5'),(21220010,18322925,'./addons/limit_message','N','20141028115813',21229637,'0.2'),(21221462,18322925,'./addons/naver_analytics','N','20141028162712',22658323,'1.2'),(21231044,18322923,'./modules/reset_password','N','20141028123356',22728311,'1.2'),(21245296,18322954,'./layouts/xe_official_v2.2','N','20141027120301',21295736,'v.2.2'),(21262112,18322925,'./addons/scrollbar','N','20141026203737',21262114,'0.9.1'),(21267409,18322954,'./layouts/qookrabbit','N','20141029002750',21363221,'1.9.8'),(21289114,18322925,'./addons/division','N','20141025132752',21294767,'1.0'),(21290615,18994170,'./modules/board/m.skins/xenon_m_board','N','20141028233116',21393065,'1.5'),(21290617,18994170,'./modules/page/m.skins/xenon_m_page','N','20141028232955',21295253,'1.1'),(21290626,18994170,'./modules/member/m.skins/xenon_m_member','N','20141028232916',21429905,'1.0'),(21290627,18994172,'./m.layouts/XenonMoblie','N','20141029001430',21815540,'1.9.1'),(21298003,18322925,'./addons/layerpopup','N','20141029002339',22754287,'1.10'),(21302525,18322954,'./layouts/xdt_pure','N','20141028224125',22403094,'1.3'),(21305288,18322952,'./widgetstyles/nico/','N','20141028173202',21532773,'2.0'),(21305881,18322923,'./modules/xewall','N','20141028225347',22754355,'0.3.1'),(21352246,18322950,'./widgets/counter_status/skins/qookrabbit_status','N','20141029002752',21352247,'0.1'),(21352623,18322925,'./addons/mresizer','N','20141027140339',21526220,'1.5'),(21354730,18322925,'./addons/msg_point','N','20141019152931',21354731,'1.0'),(21354767,18322925,'./addons/change_nickname','N','20141029003332',22754182,'2.0'),(21364832,18322950,'./widgets/newest_comment/skins/mynote','N','20141028175514',21364833,'0.1'),(21367559,18322954,'/layout','N','20141028113408',21389903,'0.0.2'),(21369594,18322925,'./addons/my_comment_addon','N','20141028140704',21394119,'1.1.2'),(21369690,18322950,'./widgets/newest_comment/skins/xenon_m_com','N','20141021110736',21370425,'1.1'),(21369691,18322950,'./widgets/newest_document/skins/xenon_m_doc','N','20141026151350',21369734,'1.0'),(21369692,18322950,'./widgets/newest_document/skins/xenon_m_gel','N','20141028163745',21369738,'1.0'),(21370287,18322923,'./modules/smartux','N','20141027202147',21370289,'1.0.2'),(21373345,18322925,'/addons/searchhighlight','N','20141027094822',21394152,'0.2'),(21374711,18322923,'./modules/ncenterlite','N','20141029001301',22754378,'2.1'),(21378491,18994170,'./modules/board/m.skins/sketchbook5','N','20141029003319',22754337,'1.7.0'),(21382622,18322925,'./addon/jquery_snow','N','20141018235034',21391227,'0.0.1'),(21384926,18322925,'./addons/dragcolor','N','20141027184539',21385023,'1.0'),(21388442,18322923,'./modules/mobileex','N','20141028224342',22107721,'0.6.1'),(21391263,18322925,'./addons/mbanner','N','20141027011622',21391414,'1.0'),(21393465,18322927,'./widgets/uchat','N','20141028235608',22634229,'1.1.6'),(21396254,18322950,'./widgets/content/skins/tb_sb','N','20141029001555',21396255,'1.0'),(21398290,18322925,'./addons/pagechange','N','20141024033358',21432465,'3.0'),(21400134,18322925,'./addons/securityPlus','N','20141023213347',21400135,'1.0.0'),(21410063,18322952,'./widgetstyle/xdt_windless','N','20141028200838',21410071,'1.0'),(21411060,18322943,'./modules/socialxe/skins/tb','N','20141027210025',22122003,'1.1'),(21411087,18322943,'./modules/socialxeserver/skins/tb','N','20141024145428',21411095,'1.0'),(21411172,18322943,'./modules/contact/skins/tb','N','20141027014321',21411184,'1.0'),(21412475,18322923,'./modules/recruit','N','20141025235735',21412476,'1.0'),(21413017,18322927,'./widgets/xegallery','N','20141028225940',21433519,'0.3'),(21415137,18322925,'./addons/setitle2','N','20141026092446',21415140,'2.1.0'),(21428178,18322954,'./layouts/xdt_simple_home','N','20141028093030',22403086,'1.4'),(21439563,18322925,'./addons/kru_sslhelper','N','20141023213339',21711242,'3.0.1'),(21526323,18322925,'./addons/noclick','N','20141028224335',22754294,'1.3'),(21535219,18322954,'./layouts/live_login/','N','20141025232308',21768603,'1.5.0.0'),(21591779,18322925,'./addons/radarURL','N','20141028162150',21594258,'1.2'),(21606824,18322925,'./addons/href_fixed1','N','20141028042422',21606841,'1.0'),(21620531,18322925,'./addons/opengraph/','N','20141028173934',21620532,'0.0.2'),(21626541,18322925,'./addons/Redirection','N','20141026204527',21626542,'1.0a'),(21643081,18322950,'./widgets/content/skins/mynote','N','20141028212536',21643082,'0.1'),(21643233,18322954,'./layouts/Express999','N','20141028220313',21838875,'2.1'),(21648251,18322950,'./widgets/content/skins/tb_cw','N','20141027011915',21978061,'2.2'),(21651786,18322925,'./addons/scmplayer','N','20141028212622',22299133,'1.5'),(21715889,18322943,'./modules/message/skins/eond','N','20141021204836',21715890,'0.1'),(21717275,18322923,'./modules/okname','N','20141027211602',21726208,'0.2.0'),(21717279,18322925,'./addons/okname','N','20141027211602',21726233,'0.2.0'),(21736776,18322925,'./addons/kru_dab/','N','20141024034103',21748943,'1.1'),(21749191,18322954,'./layouts/Simple_Style_S','N','20141028233610',21852443,'1.3.1'),(21749702,18994170,'./modules/textyle/m.skins','N','20141027130305',21762837,'0.9'),(21752944,18322925,'./addons/layerAlert','N','20141024033200',21753018,'1.1'),(21761048,18322954,'./layouts/eond_rosso','N','20141025185341',21791719,'0.3.3'),(21776217,18322954,'./layouts/eond_starter','N','20141028175440',21776218,'0.1'),(21782412,18322954,'./layouts/eond_compact','N','20141028183753',21782413,'0.8.4'),(21788706,18322925,'./addons/AntiProxy','N','20141027225200',21788708,'1.0.0'),(21798677,18322943,'./modules/ncenterlite/skins/playerplace/','N','20141027193653',21798682,'1.0.1'),(21802016,18322943,'./modules/board/skins/xe_official_planner123/colorset/','N','20141028184924',22754341,'0.4'),(21805731,18322925,'./addons/activescrollbar','N','20141024214932',21805732,'1.0'),(21807603,18322927,'./widgets/xehoverdir','N','20141027145757',21807604,'0.1'),(21810388,18322954,'./layouts/xdt_black_time','N','20141028102933',22403080,'1.2'),(21813965,18322943,'./modules/board/skins/xe_v3_gallery_haan','N','20141028221620',21814028,'0.3.1'),(21838367,18322927,'./widgets/foodin','N','20141015101306',21845017,'0.0.3'),(21838368,18322950,'./widgets/foodin/skin/simple','N','20141015101246',21845033,'0.0.1'),(21842038,18322925,'./addons/scrolltopcontrol','N','20141028234755',21845072,'1.1.0'),(21854312,18322923,'./modules/forum','N','20141028204051',21956789,'1.7.0.1'),(21855754,18322927,'./widgets/server_status','N','20141028164527',21885905,'0.2'),(21861240,18322943,'./modules/member/skins/tb','N','20141028200033',21861246,'1.0'),(21861251,18322943,'./modules/communication/skins/tb','N','20141028200036',21861263,'1.0'),(21861272,18322943,'./modules/message/skins/tb','N','20141028200037',21861277,'1.0'),(21861282,18322943,'./modules/integration_search/skins/tb','N','20141028200039',21861307,'1.0'),(21862798,18322954,'./layouts/brownwhite','N','20141024121206',21863022,'1.1'),(21876980,18322923,'./modules/multidomain','N','20141027163158',22504901,'1.3.1'),(21876999,18322925,'./addons/multidomain','N','20141027163225',22280906,'1.2'),(21883072,18322954,'./layouts/the_bootstrap','N','20141029001554',22456562,'3.5.1.2'),(21889835,18322925,'./addons/counter_ex','N','20141028021246',22754120,'1.0.3'),(21901097,18322925,'./addons/today_fortune','N','20141028224630',21908882,'1.0.1'),(21933112,18322925,'./addons/to_sns','N','20141028172924',22104881,'0.2.1'),(21933295,18322923,'./modules/realnotice','N','20141027192815',21933310,'0.5'),(21950613,18322925,'./addons/sejin7940_mustlogin','N','20141028175129',21959492,'0.2'),(21961680,18322925,'./addons/stoptrackback','N','20141028191915',21973651,'0.3'),(21978106,18322925,'./addons/sejin7940_autotrash','N','20141028175119',21978124,'1.1.1'),(22017658,18322954,'./layouts/xecenter','N','20141028234455',22633469,'1.3.7'),(22069845,18322927,'./widgets/traffic_status/','N','20141020212859',22069874,'1.0'),(22073155,18322923,'./modules/cash','N','20141025172929',22074809,'0.1'),(22115065,18322954,'./layouts/awake','N','20141028182028',22754348,'1.0.4'),(22115651,18322923,'./modules/pa','N','20141028044346',22121058,'1.0'),(22141994,18322977,'./modules/point/icons/level_icon','N','20141028222916',22734341,'SSS'),(22160991,18322977,'./modules/point/icons/wf_lv','N','20141025154510',22161011,'0.2'),(22186881,18322977,'./modules/point/icons/star','N','20141023153454',22186890,'S'),(22194465,18322925,'./addons/member_join_ex','N','20141028110454',22194483,'0.2.3'),(22208650,18322923,'/messageTalk','N','20141028140355',22208679,'1.1'),(22208653,18322925,'/addons/font-awesome','N','20141026180020',22226740,'1.0'),(22223413,18322925,'./addons/wiki_extend','N','20141023204429',22223443,'0.1'),(22228663,18322925,'./addons/addon_insert_sticker/','N','20141027094732',22228684,'1.0'),(22235916,18322925,'./addons/zipped_xe/','N','20141027173042',22278143,'0.3'),(22243421,18322977,'./modules/point/icons/xecenter_icon','N','20141027141305',22243436,'1.0.0'),(22245450,18322943,'./widgets/logged_members/skin/w_redtokbox/','N','20141024101428',22277385,'1.2'),(22245529,18322977,'./modules/point/icons/2sis_icon','N','20141028234309',22245577,'1'),(22260417,18322925,'./addons/301moved','N','20141023213256',22265949,'0.2'),(22263678,18322927,'./widgets/eh_whcarousel','N','20141029002034',22754205,'0.4'),(22266089,18322977,'./modules/point/icons/default_J','N','20141024142207',22266113,'PK_CP'),(22280542,18322954,'./layouts/simplestrap','N','20141029003037',22754303,'1.4.2'),(22282486,18322950,'./widgets/content/skins/simplestrap_sb','N','20141029001206',22417886,'1.0.1'),(22283649,18322943,'./modules/member/skins/simplestrap','N','20141029001206',22537469,'1.2'),(22283657,18322943,'./modules/communication/skins/simplestrap','N','20141029001206',22537484,'1.3'),(22288778,18322927,'./widgets/talkbox','N','20141028185541',22460980,'1.1'),(22301990,18322943,'./modules/board/skins/sketchbook5_youtube','N','20141028010715',22304077,'0.3'),(22303618,18322977,'./modules/point/icons/donek','N','20141004031745',22303661,'Acc'),(22305559,18322977,'./modules/point/icons/KJA_Love','N','20141026023237',22305588,'R'),(22332211,18322954,'./layouts/xdt_cool','N','20141028234213',22595789,'1.2.2'),(22337183,18322977,'./modules/point/icons/typical-t','N','20141016143630',22337194,'Timeless'),(22337993,18322954,'/layouts/','N','20141028042244',22337998,'1.0.0'),(22348667,18322954,'./layouts/blue','N','20141027152504',22348685,'1.0'),(22351328,18322943,'./modules/forum/skins/flat_forum_lite','N','20141027193924',22374400,'1.0.4'),(22356670,18322943,'./modules/integration_search/skins/yjsoft_ggcse','N','20141027130259',22379825,'0.1.0'),(22356680,18322977,'./modules/point/icons/zanazana','N','20141017015013',22356690,'I don\'t no'),(22359020,18322925,'addons/elkha_simple_spam/','N','20141027221714',22359071,'0.1'),(22377937,18322954,'./layouts/hestia','N','20141128234341',22754444,'2.1.4'),(22393789,18322950,'widgets/counter_status/skins/hestia_status','N','20141029001109',22393813,'1.0'),(22396862,18322954,'./layouts/xdt_style_b','N','20141028174006',22658304,'1.2.4'),(22402420,18322977,'./modules/point/icons/500-983','N','20141023153358',22402437,'fort'),(22440981,18322950,'./widgets/content/skins/eond_ygh','N','20141028105416',22440999,'0.3'),(22443686,18322925,'./addons/stalk','N','20141027001449',22443696,'0.1'),(22446815,18322923,'./modules/upgletyle/','N','20141028155748',22692647,'0.1.4'),(22450636,18322943,'./modules/editor/skins/xpresseditor_axupload5/','N','20141028095754',22504927,'1.1'),(22452877,18904838,'./modules/editor/styles/simplestrap','N','20141028222736',22452885,'0.1'),(22454021,18322923,'./messageTalk','N','20141028223303',22460914,'2.0.1'),(22455366,18322925,'./addons/block_document','N','20141028042538',22456090,'0.1'),(22456939,18322925,'./addons/xdt_scrollbar','N','20141028222340',22456955,'1.0'),(22466915,18322923,'./modules/plusad','N','20141028234008',22530791,'0.5'),(22466916,18322927,'./widgets/plusadWidget','N','20141028234008',22530804,'0.6'),(22467273,18322943,'./modules/board/skins/phiz_A_zine2','N','20141028192458',22471747,'1.0'),(22470148,18322954,'./layouts/layout_intermission','N','20141028234117',22545808,'0.9.6'),(22473723,18322954,'./layouts/daol_official','N','20141028100546',22715041,'1.1'),(22481310,18322977,'./modules/point/icons/Dandy_TJ','N','20141018154334',22481349,'I\'m very Dandy'),(22488105,18322954,'./layouts/webengine_white','N','20141029003903',22602278,'1.2'),(22495339,18322927,'./widgets/ddayWidget','N','20141028234020',22530866,'0.4'),(22497371,18322925,'./addons/url_shortener','N','20141023163844',22574332,'1.12'),(22501400,18322950,'./widgets/newest_document/skins/luke_doc','N','20141027152424',22502529,'v1.0'),(22501977,18322954,'./layouts/dark_white','N','20141028153750',22508454,'v1.1'),(22505945,18322950,'./widgets/newest_comment/skins/luke_doc','N','20141028035129',22505955,'v1.0'),(22508537,18322923,'./modules/rockgame','N','20141028193043',22541336,'0.4'),(22511691,18322925,'./addons/member_activity_check','N','20141028122926',22754292,'0.4'),(22514693,18994172,'./m.layouts/xenon_nx','N','20141028232415',22754354,'0.9.3'),(22516532,18322925,'./addons/langfilter','N','20141028161820',22553932,'0.5'),(22526030,18322927,'./widgets/simple_clock','N','20141027191500',22537348,'1.1'),(22526528,18322925,'./addons/falling_snow2','N','20141028162035',22528351,'1.0.0'),(22526756,18322925,'./addons/falling_snow3','N','20141019150044',22528554,'1.0.0'),(22529553,18322927,'./widgets/browserWidget','N','20141028224812',22532112,'0.2'),(22529686,18322927,'./widgets/simple_calendar','N','20141028052455',22579892,'1.3'),(22529898,18322950,'./widgets/browserWidget/skins/simplestrap','N','20141027154858',22529948,'0.1'),(22530581,18322943,'./modules/board/skins/contact_write','N','20141028211433',22719362,'1.11'),(22531811,18322950,'./widgets/treasurej_popular/skins/neat_popular_tabs','N','20141028224024',22754295,'1.1'),(22535332,18322954,'./layouts/xeschool_red','N','20141027013628',22535344,'red'),(22535350,18322977,'./modules/point/icons/redskiicons','N','20141028143201',22535354,'redski'),(22535360,18322977,'./modules/point/icons/plusalpine','N','20141024175608',22535364,'plusAlpha'),(22537451,18322925,'./addons/bootstrap3_css','N','20141028235205',22537493,'1.0'),(22539420,18322927,'./widgets/cute_clock','N','20141027003651',22539425,'1.0'),(22540074,18322954,'./layouts/s4us_1.0/','N','20141029002400',22540131,'1.0'),(22540502,18322943,'./modules/contact/skins/phizContact','N','20141028181126',22540527,'1.0'),(22540996,18322929,'./modules/editor/components/multimedia_link','N','20141029003427',22616932,'1.2.0'),(22542092,18322925,'./addons/ajaxboard','N','20141028210145',22588760,'1.5.6'),(22542943,18322954,'./layouts/Fresh','N','20141028211729',22543307,'1.0'),(22544858,18322925,'./addons/html5video_flash','N','20141028234748',22544908,'1.0.0'),(22547855,18994170,'./modules/member/m.skins/Blouse','N','20141028232842',22568070,'1.3'),(22549104,18322925,'./addons/recommend','N','20141028214540',22549119,'1.0'),(22553944,18322925,'./addons/xdt_css','N','20141026003612',22553960,'1.0'),(22556480,18322927,'./widgets/moonchat','N','20141027195712',22556507,'0.0.1'),(22562932,18322950,'./widgets/sitemap/skins/select','N','20141028003600',22585636,'0.3.3'),(22563110,18322950,'/widgets/content/skins/s4utabview','N','20141028055135',22563143,'1.0'),(22563158,18322954,'./layouts/nextep/','N','20141028055135',22581078,'nextep v1.2'),(22566102,18322943,'/modules/board/skins/wmboard','N','20141027094742',22573020,'2.1'),(22568598,18322954,'./layouts/xdt_simple_home2','N','20141028230911',22715996,'1.0.1'),(22572346,18322927,'./widgets/hindole','N','20141028230434',22572496,'1.0'),(22572358,18322950,'./widgets/newest_comment/skins/hindole_v1_com','N','20141027175845',22572455,'1.0'),(22572362,18322954,'./layout/book_layout','N','20141028222212',22572485,'1.0'),(22572365,18322952,'./widgetstyles/hindole_box','N','20141028200315',22600170,'1.1'),(22572369,18322952,'./widgetstyles/simple-style','N','20141028212759',22572466,'1.0'),(22572375,18322950,'./widgets/newest_document/skins/hindole_v1_doc','N','20141028221807',22572430,'1.0'),(22577184,18322923,'./modules/sejin7940_copy','N','20141029000835',22618584,'1.2'),(22579388,18322923,'./modules/money','N','20141028181046',22702291,'0.1.3'),(22580059,18322925,'./addons/xesticky','N','20141027025100',22580144,'0.1'),(22581369,18322927,'./widgets/towc_new_docu','N','20141028201010',22638452,'1.9'),(22583905,18322950,'./widgets/content/skins/phiz_rwd_images','N','20141028185529',22583963,'1.0'),(22583972,18322954,'./layouts/phizRWDThemes','N','20141028222130',22714063,'1.2'),(22585779,18322943,'./modules/board/skins/sm','N','20141029000824',22696276,'1.9.7'),(22587055,18994170,'./modules/board/m.skins/sm','N','20141029000829',22693248,'0.5.8'),(22589792,18994172,'./m.layouts/webbuilder/','N','20141028232217',22589821,'1.0.1'),(22590268,18322923,'./modules/quizgame','N','20141028192628',22590276,'0.1'),(22590697,18322943,'./modules/editor/skins/ckeditor','N','20141027185124',22590711,'1.0.0'),(22591861,18322954,'./layouts/webbuilder_layout','N','20141028212827',22610423,'1.0.3'),(22594541,18322923,'./modules/nproduct','N','20141121221215',22754412,'1.6.2'),(22594546,18322923,'./modules/nstore','N','20141121221213',22754425,'2.1'),(22594548,18322923,'./modules/store_review','N','20141028165928',22596375,'0.2'),(22594549,18322923,'./modules/ncart','N','20141121221214',22754416,'1.6.1'),(22594556,18322923,'./modules/epay','N','20141121221250',22754420,'2.1'),(22594557,18322923,'./modules/nmileage','N','20141028185409',22754160,'1.5.4'),(22594571,18322927,'./widgets/frontdisplay','N','20141028184251',22596682,'1.0'),(22594576,18322927,'./widgets/category_menu','N','20141028184330',22596721,'1.2'),(22594585,18322925,'./addons/trolley','N','20141028160154',22596756,'0.3'),(22596809,18322923,'./modules/nstore_digital','N','20141028211411',22754165,'1.9'),(22596810,18322923,'./modules/nstore_digital_contents','N','20141028181222',22597002,'1.1'),(22597020,18322923,'./modules/license','N','20141028160348',22635123,'1.2.1'),(22597112,18322923,'./modules/cympusadmin','N','20141028185355',22754161,'1.3'),(22597120,18322923,'./modules/store_search','N','20141028160200',22597129,'0.9'),(22597227,18322923,'./modules/paynoty','N','20141028212729',22597244,'1.1'),(22597855,18322943,'./modules/board/skins/rest_default','N','20141029003431',22723910,'1.2.1'),(22605220,18322925,'./addons/cameron_plugin','N','20141028183156',22754325,'1.1'),(22607524,18322925,'./addons/dsori_submanager_free','N','20141028143233',22608742,'0.1740.2'),(22610153,18322943,'./modules/editor/skins/dsori_ckeditor','N','20141027114840',22624901,'0.1740.3'),(22610154,18322925,'./addons/dsori_ckeditor_setting','N','20141027114936',22624891,'0.1740.3'),(22610502,18322954,'./layouts/webbuilder_layout2','N','20141028221044',22614812,'1.0.3'),(22611192,18322925,'./addons/dsori_facebook_comment','N','20141026161140',22613855,'0.1740.2'),(22616427,18322925,'./addons/pushwing','N','20141028184816',22663888,'0.5'),(22616439,18322925,'./addons/pushwing_comment','N','20141025205115',22639689,'0.5'),(22617898,18322923,'./modules/automail','N','20141028211411',22617911,'0.1'),(22618830,18322925,'./addons/checklen','N','20141028122841',22754320,'1.3'),(22622633,18322925,'./addons/blockact','N','20141017093904',22622739,'1.0'),(22627992,18322954,'./layouts/orange_simple','N','20141028180721',22670250,'1.4'),(22631178,18322925,'./addons/mobile_redirect','N','20141026205506',22631183,'1.0'),(22631822,18322954,'./layouts/xeview_layout','N','20141027160412',22631830,'1.2'),(22631837,18322954,'./layouts/five_start','N','20141023115400',22631859,'1.0'),(22634250,18322925,'./addons/browser_helper','N','20141027162348',22634254,'1.0'),(22634927,18322952,'./widgetstyles/m_cronos_ws','N','20141028212710',22647143,'1.1'),(22634955,18322950,'./widgets/mcontent/skins/m_cronos_w','N','20141028233458',22647135,'1.1'),(22640952,18322925,'./addons/twoc_memo_del','N','20141019153209',22640972,'1.1'),(22641332,18322954,'./layouts/Imagemonster','N','20141028231141',22754363,'2.1'),(22641961,18322927,'./widgets/photoslider','N','20141028231141',22641966,'1.0'),(22643750,18322925,'./addons/bot_title_control','N','20141027094541',22643845,'0.2'),(22644317,18322925,'./addons/member_control','N','20141027084230',22644546,'0.1'),(22646356,18322943,'./modules/member/skins/sketchbook5_member_skin','N','20141028184201',22718814,'0.4.5'),(22646443,18322943,'./modules/communication/skins/sketchbook5_communication_skin','N','20141028103848',22718953,'0.4.5'),(22646468,18994170,'./modules/member/m.skins/sketchbook5_member_m.skin','N','20141028224059',22719660,'0.4.6'),(22646488,18994170,'./modules/communication/m.skins/sketchbook5_communication_m.skin','N','20141028053005',22719741,'0.4.6'),(22648755,18322923,'./modules/upgletyle_plugin_daumview','N','20141023125249',22648765,'0.1.0.b1'),(22648862,18322925,'./addons/robotcontrol','N','20141028173355',22649123,'1.1.0'),(22649096,18322925,'./addons/fa_loader','N','20141028212346',22754343,'4.2.0'),(22649328,18322925,'./addons/checkkorean','N','20141026234351',22651662,'1.1'),(22650410,18322925,'./addons/login_defencer','N','20141024011501',22650418,'1.0'),(22651543,18322954,'./layouts/flat_series','N','20141028233313',22754140,'1.0.22'),(22651552,18322950,'./widgets/login_info/skins/flat_series','N','20141029003038',22744104,'1.0.1'),(22652987,18322925,'./addons/confirm_declare','N','20141028035832',22652996,'1.0'),(22653121,18322923,'./modules/xpay/','N','20141028103307',22681674,'1.0'),(22653449,18322925,'./addons/block_ip','N','20141028201016',22754110,'2.0'),(22654408,18322925,'./addons/authentication_change','N','20141028205252',22654433,'0.1'),(22655078,18322954,'./layouts/emergence','N','20141028103620',22660012,'1.0.0'),(22657234,18322925,'./addons/xdt_google_analytics','N','20141028224927',22754376,'1.1'),(22657836,18322923,'./modules/ezmember/','N','20141028200702',22716032,'0.0.2'),(22657837,18322925,'./addons/ezmember/','N','20141028200759',22716021,'0.0.2'),(22658404,18994170,'./modules/board/m.skins/sosifam_memo','N','20141028215811',22738369,'0.5'),(22658524,18322927,'./widgets/hb_bank','N','20141028200618',22658668,'0.1'),(22660923,18994172,'./m.layouts/phizMobileThemes','N','20141029001548',22754202,'1.2'),(22660940,18994170,'./modules/member/m.skin/phizMobile','N','20141028232746',22660950,'1.0'),(22660953,18994170,'./modules/communication/m.skin/phizMobile','N','20141028145352',22660956,'1.0'),(22664861,18322927,'./widgets/cameronSlider','N','20141029002600',22754328,'1.0.1'),(22664862,18322950,'./widgets/cameronSlider/skins/cameraSlider','N','20141028224011',22754329,'1.0.1'),(22665526,18322950,'./widgets/cameronSlider/skins/bxSlider','N','20141029003828',22754330,'1.0.1'),(22665670,18322950,'./widgets/cameronSlider/skins/FlexSlider2','N','20141029003823',22754331,'1.0.1'),(22668729,18322927,'./widgets/webcon_newswidget','N','20141029000140',22700746,'1.0.1'),(22669010,18322954,'./layouts/css3_simple','N','20141028204932',22687348,'1.1'),(22672196,18322925,'./addons/new_document_notify','N','20141028150944',22754335,'2.0'),(22673162,18322925,'./addons/fileicon','N','20141027143213',22716823,'1.1'),(22673736,18322925,'./addons/jqueryuicdn','N','20141028113206',22673740,'1.0'),(22677441,18322925,'./addons/jquerycdn','N','20141028113158',22714080,'1.0.3'),(22678118,18322927,'./widgets/webcon_smartTab','N','20141029003427',22680181,'1.0'),(22678527,18322954,'./layouts/awake2','N','20141027202501',22678540,'1.6'),(22678880,18322925,'./addons/jsecure_xe','N','20141027095616',22684436,'0.2'),(22690074,18322923,'./modules/sejin7940_vote','N','20141028235606',22754377,'1.3'),(22692696,18322927,'./widgets/webcon_mosaicGallery','N','20141028160508',22692724,'1.0'),(22692901,18322927,'./widgets/srchat_count/','N','20141027161303',22692906,'1.0'),(22694914,18322954,'./layouts/xdt_sewol','N','20141022130959',22715019,'1.1'),(22699529,18322927,'./widgets/webcon_carousel','N','20141028211717',22699542,'1.0'),(22699627,18322925,'./addons/textarea_resizer/','N','20141028222347',22744685,'0.3'),(22703498,18322927,'./widgets/webcon_N_newsSearch','N','20141028122109',22703507,'1.0'),(22703903,18322927,'./widgets/webcon_verticalTab','N','20141029000150',22708158,'1.0.1'),(22703904,18322925,'./addons/xetrace','N','20141024163805',22703936,'1.0.0'),(22705169,18322923,'./modules/moneysend','N','20141027082654',22705176,'0.0.1'),(22706212,18322925,'./addons/hide_mid','N','20141028153114',22706230,'0.1.0'),(22711628,18322954,'./layouts/xelab_ll1/','N','20141028155322',22712736,'1.0.2'),(22715595,18322925,'./addons/captbha','N','20141028184346',22715603,'0.5'),(22716306,18322954,'./layouts/firstkenta','N','20141028233912',22754179,'1.1'),(22716407,18322925,'./addons/autodeny','N','20141025075340',22716411,'0.7'),(22718180,18322927,'./widgets/webcon_effectSlider','N','20141028170859',22718196,'1.0'),(22718477,18322925,'./addons/fa_fileicon','N','20141028163627',22754338,'1.1.2'),(22720710,18322943,'./modules/upgletyle/skins/emplode','N','20141027133954',22720745,'0.7'),(22723913,18322925,'./addons/dyform_no_spam','N','20141029002055',22754375,'0.3.3'),(22725377,18322925,'./addons/addon_linker/','N','20141028222230',22744678,'1.0'),(22726124,18322923,'./modules/ggmailing','N','20141128232233',22754443,'0.3.6'),(22730394,18322943,'/modules/sejin7940_comment/skins/sketchbook5_mycomment_skin','N','20141028123406',22730414,'1.0.0'),(22730395,18994170,'/modules/sejin7940_comment/m.skins/sketchbook5_mycomment_mskin','N','20141028222830',22730475,'1.0.0'),(22730560,18322954,'./layouts/purexe','N','20141028224039',22754180,'1.1'),(22753344,18322954,'./layouts/simple_is_best','N','20141028231232',22754147,'1.0'),(22734419,18322929,'./modules/editor/components/youtube_link_lite/','N','20141028210712',22735828,'1.0'),(22735026,18322925,'./addons/sejin7940_mobile_resize/','N','20141028222541',22735066,'1.0'),(22735793,18322927,'./widgets/webcon_mosaicContents','N','20141028145838',22735808,'1.0'),(22736227,18322925,'./addons/memberinfo','N','20141028224046',22754342,'1.10'),(22736353,18322925,'./addons/bbCode','N','20141026195810',22736356,'1.0'),(22736372,18322925,'./addons/placeHolders','N','20141021120324',22736378,'1.0'),(22737353,18322977,'./modules/point/icons/semo','N','20141018154101',22737363,'Volkswagen'),(22737874,18322925,'./addons/ggstopspam','N','20141028211001',22737893,'0.1'),(22739163,18322927,'./widgets/webcon_wslider','N','20141029003514',22739183,'1.0'),(22743728,18322925,'./addons/aa_insert_ad','N','20141029001411',22743740,'1.0.0'),(22744282,18322925,'./addons/stop_spambot_xe','N','20141028210943',22754146,'0.6'),(22745646,18322925,'./addons/point_pang_pang','N','20141028201201',22745649,'0.1.1'),(22750559,18322925,'./addons/beforecheckwrite/','N','20141028201238',22750562,'0.1'),(22752234,18322925,'./addons/block_country','N','20141028201057',22752338,'0.2'),(22753306,18322927,'./widgets/ggboardmailing_widget','N','20141121205706',22754403,'0.2'),(22753312,18322925,'./addons/new_document_notify2','N','20141022092131',22754204,'1.0.12'),(22753313,18322923,'./modules/attendance','N','20141029000333',22754314,'5.0.5'),(22753314,18322923,'./modules/privilege','N','20141028223947',22754353,'0.1.2'),(22753315,18322925,'./addons/sns_card','N','20141027154606',22754099,'1.1'),(22753316,18322954,'./layouts/doorweb_v4','N','20141029000359',22754256,'1.7'),(22753317,18322950,'./widgets/content/skins/doorweb','N','20141028235710',22754080,'1.7'),(22753322,18322954,'./layouts/rkt001','N','20141028230243',22754084,'1.0.0'),(22753326,18322923,'./modules/ajaxboard','N','20141028230633',22754290,'1.7.3'),(22753327,18322950,'./widgets/content/skins/funnyxeGallery','N','20141028162730',22754107,'1.1'),(22753328,18322954,'./layouts/verti','N','20141026174539',22754108,'0.1.0'),(22753334,18322943,'./modules/editor/skins/xpresseditor_datauri','N','20141025022335',22754114,'1.7'),(22753336,18322925,'./addons/font_awesome_new','N','20141025090404',22754115,'1.0'),(22753339,18322954,'./layouts/rkt002','N','20141028004211',22754118,'1.0.0'),(22753340,18322925,'./addons/font_nanum_gothic','N','20141028141124',22754124,'1.0'),(22753341,18322954,'./layouts/The_Simple_Classic_Lite','N','20141028175522',22754141,'1.1'),(22753342,18322927,'./widgets/slideimg','N','20141029002703',22754339,'1.1.2'),(22753343,18322925,'./addons/multimedia_thumbnail','N','20141028161221',22754311,'2.0.0'),(22753345,18994172,'./m.layouts/simple_is_best_mobile','N','20141028232423',22754148,'1.0'),(22753346,18322923,'./modules/currency','N','20141028180907',22754151,'1.0'),(22753348,18322923,'./modules/inipay','N','20141121221236',22754422,'1.2'),(22753349,18322923,'./modules/inipaymobile','N','20141028152517',22754154,'1.0'),(22753350,18322923,'./modules/kcp','N','20141121221227',22754421,'1.1'),(22753351,18322923,'./modules/cashpay','N','20141028170032',22754156,'1.0'),(22753352,18322923,'./modules/paypal','N','20141028152429',22754157,'1.0'),(22753353,18322954,'./layouts/kbfree','N','20141028234713',22754197,'1.2'),(22753428,18322927,'./widgets/sejin7940_navermap','N','20141028212558',22754347,'0.2.1'),(22753356,18322923,'./modules/gdata','N','20141028182049',22754185,'0.5'),(22753358,18322927,'./widgets/picasa_recent_images','N','20141028182049',22754183,'0.1'),(22753359,18322925,'./addons/my_reading','N','20141028143126',22754203,'1.1'),(22753373,18322925,'./addons/updatecategory','N','20141028214355',22754190,'1.0'),(22753374,18322927,'./widgets/xelayout_weather','N','20141027115135',22754198,'0.2'),(22753381,18322925,'./addons/sxe_block_write','N','20141028123032',22754220,'0.2'),(22753382,18322925,'./addons/sxe_now_connected','N','20141028123025',22754221,'0.2'),(22753383,18322943,'./modules/ajaxboard/skins/sketchbook5','N','20141028231017',22754296,'1.0.2'),(22753386,18322954,'./layouts/b_black','N','20141028223724',22754231,'1.0'),(22753387,18322925,'./addons/sxe_bbcode_lite','N','20141023151909',22754233,'0.1'),(22753388,18322923,'./modules/sejin7940_nick','N','20141028204125',22754240,'1.0.2'),(22753390,18322925,'./addons/sxe_writing_format','N','20141028215337',22754246,'0.2'),(22753393,18322925,'./addons/sxe_ncenter_plus','N','20141028200748',22754245,'0.2'),(22753396,18322923,'./modules/ximember','N','20141028224928',22754333,'1.4'),(22753397,18322923,'./modules/board_extend','N','20141028224410',22754254,'1.1'),(22753399,18322943,'./modules/ncenterlite/skins/wild_ones','N','20141028131428',22754258,'0.1'),(22753403,18322927,'./widgets/opageWidget','N','20141028210616',22754271,'1.0'),(22753406,18322929,'./modules/editor/components/map_components','N','20141028180241',22754298,'1.1'),(22753408,18322925,'./addons/member_extra_vars_check','N','20141028122933',22754283,'1.0'),(22753411,18322925,'./addons/block_control','N','20141028122949',22754291,'1.0'),(22753413,18322927,'./widgets/sitemap_selectbox','N','20141028182000',22754293,'0.1.0'),(22753415,18994170,'./modules/ajaxboard/m.skins/flatBoard','N','20141027180621',22754306,'1.0'),(22753416,18322925,'./addons/auto_multimedia','N','20141028161350',22754308,'1.0'),(22753417,18322925,'./addons/addon_write_insert_media','N','20141028195142',22754309,'0.1'),(22753419,18322925,'./addons/addon_insert_video','N','20141028161140',22754313,'0.1'),(22753420,18322923,'./modules/pointhistory','N','20141028222630',22754374,'0.1.4'),(22753422,18322950,'./widgets/treasurej_popular/skins/smart_popular_tabs','N','20141028224033',22754327,'1.1'),(22753429,18322943,'./modules/board/skins/sosi_memo','N','20141028185919',22754360,'0.1'),(22753430,18322943,'./modules/ajaxboard/skins/memo','N','20141028231022',22754361,'1.0'),(22753437,18322950,'./widgets/calendar/skins/UXF_CALENDER_TYPE_01','N','20141028233345',22754368,'1.1.0'),(22753439,18322927,'./widgets/tocplus','N','20141028163236',22754373,'0.3'),(22753449,18322925,'./addons/xss_session_protector','N','20141028233309',22754383,'0.1'),(22753457,18322954,'./layouts/simplicity','N','20141121183743',22754391,'1.0'),(22753458,18322925,'./addons/youtube_control','N','20141121214622',22754401,'1.2'),(22753459,18322923,'./modules/maps','N','20141121221154',22754414,'1.1.1'),(22753460,18322927,'./widgets/maps_widget','N','20141121221154',22754427,'1.1.1'),(22753462,18322925,'./addons/download_wanna_reply','N','20141121192240',22754407,'1.0'),(22753463,18322925,'./addons/image_preview','N','20141121192224',22754413,'1.0'),(22753464,18322943,'./modules/boards/skins/mixitup','N','20141121155259',22754418,'1.0'),(22753466,18322925,'./addons/auto_nick','N','20141121221205',22754428,'1.0'),(22753474,18322943,'./modules/lottery/skins/eond_v1','N','20141128180701',22754439,'0.1'),(22753476,18322954,'./layouts/CustomStrap','N','20141129012500',22754447,'1.0');
/*!40000 ALTER TABLE `xe_autoinstall_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_comment_declared`
--

DROP TABLE IF EXISTS `xe_comment_declared`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_comment_declared` (
  `comment_srl` bigint(11) NOT NULL,
  `declared_count` bigint(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_srl`),
  KEY `idx_declared_count` (`declared_count`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_comment_declared`
--

LOCK TABLES `xe_comment_declared` WRITE;
/*!40000 ALTER TABLE `xe_comment_declared` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_comment_declared` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_comment_declared_log`
--

DROP TABLE IF EXISTS `xe_comment_declared_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_comment_declared_log` (
  `comment_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `ipaddress` varchar(128) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  KEY `idx_comment_srl` (`comment_srl`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_comment_declared_log`
--

LOCK TABLES `xe_comment_declared_log` WRITE;
/*!40000 ALTER TABLE `xe_comment_declared_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_comment_declared_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_comment_voted_log`
--

DROP TABLE IF EXISTS `xe_comment_voted_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_comment_voted_log` (
  `comment_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `ipaddress` varchar(128) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `point` bigint(11) NOT NULL,
  KEY `idx_comment_srl` (`comment_srl`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_comment_voted_log`
--

LOCK TABLES `xe_comment_voted_log` WRITE;
/*!40000 ALTER TABLE `xe_comment_voted_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_comment_voted_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_comments`
--

DROP TABLE IF EXISTS `xe_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_comments` (
  `comment_srl` bigint(11) NOT NULL,
  `module_srl` bigint(11) NOT NULL DEFAULT '0',
  `document_srl` bigint(11) NOT NULL DEFAULT '0',
  `parent_srl` bigint(11) NOT NULL DEFAULT '0',
  `is_secret` char(1) NOT NULL DEFAULT 'N',
  `content` longtext NOT NULL,
  `voted_count` bigint(11) NOT NULL DEFAULT '0',
  `blamed_count` bigint(11) NOT NULL DEFAULT '0',
  `notify_message` char(1) NOT NULL DEFAULT 'N',
  `password` varchar(60) DEFAULT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `user_name` varchar(80) NOT NULL,
  `nick_name` varchar(80) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `email_address` varchar(250) NOT NULL,
  `homepage` varchar(250) NOT NULL,
  `uploaded_count` bigint(11) NOT NULL DEFAULT '0',
  `regdate` varchar(14) DEFAULT NULL,
  `last_update` varchar(14) DEFAULT NULL,
  `ipaddress` varchar(128) NOT NULL,
  `list_order` bigint(11) NOT NULL,
  `status` bigint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`comment_srl`),
  UNIQUE KEY `idx_module_list_order` (`module_srl`,`list_order`),
  KEY `idx_module_srl` (`module_srl`),
  KEY `idx_document_srl` (`document_srl`),
  KEY `idx_voted_count` (`voted_count`),
  KEY `idx_blamed_count` (`blamed_count`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_uploaded_count` (`uploaded_count`),
  KEY `idx_regdate` (`regdate`),
  KEY `idx_last_update` (`last_update`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_list_order` (`list_order`),
  KEY `idx_status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_comments`
--

LOCK TABLES `xe_comments` WRITE;
/*!40000 ALTER TABLE `xe_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_comments_list`
--

DROP TABLE IF EXISTS `xe_comments_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_comments_list` (
  `comment_srl` bigint(11) NOT NULL,
  `document_srl` bigint(11) NOT NULL DEFAULT '0',
  `head` bigint(11) NOT NULL DEFAULT '0',
  `arrange` bigint(11) NOT NULL DEFAULT '0',
  `module_srl` bigint(11) NOT NULL DEFAULT '0',
  `regdate` varchar(14) DEFAULT NULL,
  `depth` bigint(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_srl`),
  KEY `idx_list` (`document_srl`,`head`,`arrange`),
  KEY `idx_date` (`module_srl`,`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_comments_list`
--

LOCK TABLES `xe_comments_list` WRITE;
/*!40000 ALTER TABLE `xe_comments_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_comments_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_counter_log`
--

DROP TABLE IF EXISTS `xe_counter_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_counter_log` (
  `site_srl` bigint(11) NOT NULL DEFAULT '0',
  `ipaddress` varchar(250) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `user_agent` varchar(250) DEFAULT NULL,
  KEY `idx_site_counter_log` (`site_srl`,`ipaddress`),
  KEY `idx_counter_log` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_counter_log`
--

LOCK TABLES `xe_counter_log` WRITE;
/*!40000 ALTER TABLE `xe_counter_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_counter_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_counter_site_status`
--

DROP TABLE IF EXISTS `xe_counter_site_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_counter_site_status` (
  `site_srl` bigint(11) NOT NULL,
  `regdate` bigint(11) NOT NULL,
  `unique_visitor` bigint(11) DEFAULT '0',
  `pageview` bigint(11) DEFAULT '0',
  UNIQUE KEY `site_status` (`site_srl`,`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_counter_site_status`
--

LOCK TABLES `xe_counter_site_status` WRITE;
/*!40000 ALTER TABLE `xe_counter_site_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_counter_site_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_counter_status`
--

DROP TABLE IF EXISTS `xe_counter_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_counter_status` (
  `regdate` bigint(11) NOT NULL,
  `unique_visitor` bigint(11) DEFAULT '0',
  `pageview` bigint(11) DEFAULT '0',
  PRIMARY KEY (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_counter_status`
--

LOCK TABLES `xe_counter_status` WRITE;
/*!40000 ALTER TABLE `xe_counter_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_counter_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_document_aliases`
--

DROP TABLE IF EXISTS `xe_document_aliases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_document_aliases` (
  `alias_srl` bigint(11) NOT NULL DEFAULT '0',
  `module_srl` bigint(11) NOT NULL DEFAULT '0',
  `document_srl` bigint(11) NOT NULL DEFAULT '0',
  `alias_title` varchar(250) NOT NULL,
  PRIMARY KEY (`alias_srl`),
  UNIQUE KEY `idx_module_title` (`module_srl`,`alias_title`),
  KEY `idx_module_srl` (`module_srl`),
  KEY `idx_document_srl` (`document_srl`),
  KEY `idx_alias_title` (`alias_title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_document_aliases`
--

LOCK TABLES `xe_document_aliases` WRITE;
/*!40000 ALTER TABLE `xe_document_aliases` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_document_aliases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_document_categories`
--

DROP TABLE IF EXISTS `xe_document_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_document_categories` (
  `category_srl` bigint(11) NOT NULL DEFAULT '0',
  `module_srl` bigint(11) NOT NULL DEFAULT '0',
  `parent_srl` bigint(12) NOT NULL DEFAULT '0',
  `title` varchar(250) DEFAULT NULL,
  `expand` char(1) DEFAULT 'N',
  `document_count` bigint(11) NOT NULL DEFAULT '0',
  `regdate` varchar(14) DEFAULT NULL,
  `last_update` varchar(14) DEFAULT NULL,
  `list_order` bigint(11) NOT NULL,
  `group_srls` text,
  `color` varchar(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`category_srl`),
  KEY `idx_module_srl` (`module_srl`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_document_categories`
--

LOCK TABLES `xe_document_categories` WRITE;
/*!40000 ALTER TABLE `xe_document_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_document_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_document_declared`
--

DROP TABLE IF EXISTS `xe_document_declared`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_document_declared` (
  `document_srl` bigint(11) NOT NULL,
  `declared_count` bigint(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`document_srl`),
  KEY `idx_declared_count` (`declared_count`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_document_declared`
--

LOCK TABLES `xe_document_declared` WRITE;
/*!40000 ALTER TABLE `xe_document_declared` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_document_declared` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_document_declared_log`
--

DROP TABLE IF EXISTS `xe_document_declared_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_document_declared_log` (
  `document_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `ipaddress` varchar(128) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  KEY `idx_document_srl` (`document_srl`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_document_declared_log`
--

LOCK TABLES `xe_document_declared_log` WRITE;
/*!40000 ALTER TABLE `xe_document_declared_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_document_declared_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_document_extra_keys`
--

DROP TABLE IF EXISTS `xe_document_extra_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_document_extra_keys` (
  `module_srl` bigint(11) NOT NULL,
  `var_idx` bigint(11) NOT NULL,
  `var_name` varchar(250) NOT NULL,
  `var_type` varchar(50) NOT NULL,
  `var_is_required` char(1) NOT NULL DEFAULT 'N',
  `var_search` char(1) NOT NULL DEFAULT 'N',
  `var_default` text,
  `var_desc` text,
  `eid` varchar(40) DEFAULT NULL,
  UNIQUE KEY `unique_module_keys` (`module_srl`,`var_idx`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_document_extra_keys`
--

LOCK TABLES `xe_document_extra_keys` WRITE;
/*!40000 ALTER TABLE `xe_document_extra_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_document_extra_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_document_extra_vars`
--

DROP TABLE IF EXISTS `xe_document_extra_vars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_document_extra_vars` (
  `module_srl` bigint(11) NOT NULL,
  `document_srl` bigint(11) NOT NULL,
  `var_idx` bigint(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `value` longtext,
  `eid` varchar(40) DEFAULT NULL,
  UNIQUE KEY `unique_extra_vars` (`module_srl`,`document_srl`,`var_idx`,`lang_code`),
  KEY `idx_document_list_order` (`document_srl`,`module_srl`,`var_idx`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_document_extra_vars`
--

LOCK TABLES `xe_document_extra_vars` WRITE;
/*!40000 ALTER TABLE `xe_document_extra_vars` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_document_extra_vars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_document_histories`
--

DROP TABLE IF EXISTS `xe_document_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_document_histories` (
  `history_srl` bigint(11) NOT NULL DEFAULT '0',
  `module_srl` bigint(11) NOT NULL DEFAULT '0',
  `document_srl` bigint(11) NOT NULL DEFAULT '0',
  `content` longtext,
  `nick_name` varchar(80) NOT NULL,
  `member_srl` bigint(11) DEFAULT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `ipaddress` varchar(128) NOT NULL,
  PRIMARY KEY (`history_srl`),
  KEY `idx_module_srl` (`module_srl`),
  KEY `idx_document_srl` (`document_srl`),
  KEY `idx_regdate` (`regdate`),
  KEY `idx_ipaddress` (`ipaddress`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_document_histories`
--

LOCK TABLES `xe_document_histories` WRITE;
/*!40000 ALTER TABLE `xe_document_histories` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_document_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_document_readed_log`
--

DROP TABLE IF EXISTS `xe_document_readed_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_document_readed_log` (
  `document_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `ipaddress` varchar(128) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  KEY `idx_document_srl` (`document_srl`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_document_readed_log`
--

LOCK TABLES `xe_document_readed_log` WRITE;
/*!40000 ALTER TABLE `xe_document_readed_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_document_readed_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_document_trash`
--

DROP TABLE IF EXISTS `xe_document_trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_document_trash` (
  `trash_srl` bigint(11) NOT NULL DEFAULT '0',
  `document_srl` bigint(11) NOT NULL DEFAULT '0',
  `module_srl` bigint(11) NOT NULL DEFAULT '0',
  `trash_date` varchar(14) DEFAULT NULL,
  `description` text,
  `ipaddress` varchar(128) NOT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `user_name` varchar(80) NOT NULL,
  `nick_name` varchar(80) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  PRIMARY KEY (`trash_srl`),
  KEY `idx_document_srl` (`document_srl`),
  KEY `idx_module_srl` (`module_srl`),
  KEY `idx_trash_date` (`trash_date`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_member_srl` (`member_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_document_trash`
--

LOCK TABLES `xe_document_trash` WRITE;
/*!40000 ALTER TABLE `xe_document_trash` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_document_trash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_document_voted_log`
--

DROP TABLE IF EXISTS `xe_document_voted_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_document_voted_log` (
  `document_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `ipaddress` varchar(128) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `point` bigint(11) NOT NULL,
  KEY `idx_document_srl` (`document_srl`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_document_voted_log`
--

LOCK TABLES `xe_document_voted_log` WRITE;
/*!40000 ALTER TABLE `xe_document_voted_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_document_voted_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_documents`
--

DROP TABLE IF EXISTS `xe_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_documents` (
  `document_srl` bigint(11) NOT NULL,
  `module_srl` bigint(11) NOT NULL DEFAULT '0',
  `category_srl` bigint(11) NOT NULL DEFAULT '0',
  `lang_code` varchar(10) NOT NULL DEFAULT '',
  `is_notice` char(1) NOT NULL DEFAULT 'N',
  `title` varchar(250) DEFAULT NULL,
  `title_bold` char(1) NOT NULL DEFAULT 'N',
  `title_color` varchar(7) DEFAULT NULL,
  `content` longtext NOT NULL,
  `readed_count` bigint(11) NOT NULL DEFAULT '0',
  `voted_count` bigint(11) NOT NULL DEFAULT '0',
  `blamed_count` bigint(11) NOT NULL DEFAULT '0',
  `comment_count` bigint(11) NOT NULL DEFAULT '0',
  `trackback_count` bigint(11) NOT NULL DEFAULT '0',
  `uploaded_count` bigint(11) NOT NULL DEFAULT '0',
  `password` varchar(60) DEFAULT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `user_name` varchar(80) NOT NULL,
  `nick_name` varchar(80) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `email_address` varchar(250) NOT NULL,
  `homepage` varchar(250) NOT NULL,
  `tags` text,
  `extra_vars` text,
  `regdate` varchar(14) DEFAULT NULL,
  `last_update` varchar(14) DEFAULT NULL,
  `last_updater` varchar(80) DEFAULT NULL,
  `ipaddress` varchar(128) NOT NULL,
  `list_order` bigint(11) NOT NULL,
  `update_order` bigint(11) NOT NULL,
  `allow_trackback` char(1) NOT NULL DEFAULT 'Y',
  `notify_message` char(1) NOT NULL DEFAULT 'N',
  `status` varchar(20) DEFAULT 'PUBLIC',
  `comment_status` varchar(20) DEFAULT 'ALLOW',
  PRIMARY KEY (`document_srl`),
  KEY `idx_module_srl` (`module_srl`),
  KEY `idx_category_srl` (`category_srl`),
  KEY `idx_is_notice` (`is_notice`),
  KEY `idx_readed_count` (`readed_count`),
  KEY `idx_voted_count` (`voted_count`),
  KEY `idx_blamed_count` (`blamed_count`),
  KEY `idx_comment_count` (`comment_count`),
  KEY `idx_trackback_count` (`trackback_count`),
  KEY `idx_uploaded_count` (`uploaded_count`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_regdate` (`regdate`),
  KEY `idx_last_update` (`last_update`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_list_order` (`list_order`),
  KEY `idx_update_order` (`update_order`),
  KEY `idx_module_list_order` (`module_srl`,`list_order`),
  KEY `idx_module_update_order` (`module_srl`,`update_order`),
  KEY `idx_module_readed_count` (`module_srl`,`readed_count`),
  KEY `idx_module_voted_count` (`module_srl`,`voted_count`),
  KEY `idx_module_notice` (`module_srl`,`is_notice`),
  KEY `idx_module_document_srl` (`module_srl`,`document_srl`),
  KEY `idx_module_blamed_count` (`module_srl`,`blamed_count`),
  KEY `idx_module_status` (`module_srl`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_documents`
--

LOCK TABLES `xe_documents` WRITE;
/*!40000 ALTER TABLE `xe_documents` DISABLE KEYS */;
INSERT INTO `xe_documents` VALUES (150,120,0,'ko','N','Welcom','N','N','<p style=\"text-align: center; \">Thie is Database Project Site.</p>',0,0,0,0,0,0,NULL,'andante624','admin','Andante624',4,'minchae624@gmail.com','','','N;','20141029012732','20141029015009',NULL,'119.192.238.165',-150,-167,'N','N','PUBLIC','DENY');
/*!40000 ALTER TABLE `xe_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_editor_autosave`
--

DROP TABLE IF EXISTS `xe_editor_autosave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_editor_autosave` (
  `member_srl` bigint(11) DEFAULT '0',
  `ipaddress` varchar(128) DEFAULT NULL,
  `module_srl` bigint(11) DEFAULT NULL,
  `document_srl` bigint(11) NOT NULL DEFAULT '0',
  `title` varchar(250) DEFAULT NULL,
  `content` longtext NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_module_srl` (`module_srl`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_editor_autosave`
--

LOCK TABLES `xe_editor_autosave` WRITE;
/*!40000 ALTER TABLE `xe_editor_autosave` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_editor_autosave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_editor_components`
--

DROP TABLE IF EXISTS `xe_editor_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_editor_components` (
  `component_name` varchar(250) NOT NULL,
  `enabled` char(1) NOT NULL DEFAULT 'N',
  `extra_vars` text,
  `list_order` bigint(11) NOT NULL,
  PRIMARY KEY (`component_name`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_editor_components`
--

LOCK TABLES `xe_editor_components` WRITE;
/*!40000 ALTER TABLE `xe_editor_components` DISABLE KEYS */;
INSERT INTO `xe_editor_components` VALUES ('colorpicker_text','Y',NULL,41),('colorpicker_bg','Y',NULL,43),('emoticon','Y',NULL,45),('url_link','Y',NULL,47),('image_link','Y',NULL,49),('multimedia_link','Y',NULL,51),('quotation','Y',NULL,53),('table_maker','Y',NULL,55),('poll_maker','Y',NULL,57),('image_gallery','Y',NULL,59);
/*!40000 ALTER TABLE `xe_editor_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_editor_components_site`
--

DROP TABLE IF EXISTS `xe_editor_components_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_editor_components_site` (
  `site_srl` bigint(11) NOT NULL DEFAULT '0',
  `component_name` varchar(250) NOT NULL,
  `enabled` char(1) NOT NULL DEFAULT 'N',
  `extra_vars` text,
  `list_order` bigint(11) NOT NULL,
  UNIQUE KEY `unique_component_site` (`site_srl`,`component_name`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_editor_components_site`
--

LOCK TABLES `xe_editor_components_site` WRITE;
/*!40000 ALTER TABLE `xe_editor_components_site` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_editor_components_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_files`
--

DROP TABLE IF EXISTS `xe_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_files` (
  `file_srl` bigint(11) NOT NULL,
  `upload_target_srl` bigint(11) NOT NULL DEFAULT '0',
  `upload_target_type` char(3) DEFAULT NULL,
  `sid` varchar(60) DEFAULT NULL,
  `module_srl` bigint(11) NOT NULL DEFAULT '0',
  `member_srl` bigint(11) NOT NULL,
  `download_count` bigint(11) NOT NULL DEFAULT '0',
  `direct_download` char(1) NOT NULL DEFAULT 'N',
  `source_filename` varchar(250) DEFAULT NULL,
  `uploaded_filename` varchar(250) DEFAULT NULL,
  `file_size` bigint(11) NOT NULL DEFAULT '0',
  `comment` varchar(250) DEFAULT NULL,
  `isvalid` char(1) DEFAULT 'N',
  `regdate` varchar(14) DEFAULT NULL,
  `ipaddress` varchar(128) NOT NULL,
  PRIMARY KEY (`file_srl`),
  KEY `idx_upload_target_srl` (`upload_target_srl`),
  KEY `idx_upload_target_type` (`upload_target_type`),
  KEY `idx_module_srl` (`module_srl`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_download_count` (`download_count`),
  KEY `idx_file_size` (`file_size`),
  KEY `idx_is_valid` (`isvalid`),
  KEY `idx_regdate` (`regdate`),
  KEY `idx_ipaddress` (`ipaddress`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_files`
--

LOCK TABLES `xe_files` WRITE;
/*!40000 ALTER TABLE `xe_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_lang`
--

DROP TABLE IF EXISTS `xe_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_lang` (
  `site_srl` bigint(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `value` text,
  KEY `idx_lang` (`site_srl`,`name`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_lang`
--

LOCK TABLES `xe_lang` WRITE;
/*!40000 ALTER TABLE `xe_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_layouts`
--

DROP TABLE IF EXISTS `xe_layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_layouts` (
  `layout_srl` bigint(12) NOT NULL,
  `site_srl` bigint(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `extra_vars` text,
  `layout_path` varchar(250) DEFAULT NULL,
  `module_srl` bigint(12) DEFAULT '0',
  `regdate` varchar(14) DEFAULT NULL,
  `layout_type` char(1) DEFAULT 'P',
  PRIMARY KEY (`layout_srl`),
  KEY `menu_site_srl` (`site_srl`),
  KEY `idx_module_srl` (`module_srl`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_layouts`
--

LOCK TABLES `xe_layouts` WRITE;
/*!40000 ALTER TABLE `xe_layouts` DISABLE KEYS */;
INSERT INTO `xe_layouts` VALUES (67,0,'default','default','O:8:\"stdClass\":9:{s:16:\"error_return_url\";s:1:\"/\";s:7:\"ruleset\";s:12:\"updateLayout\";s:12:\"_layout_type\";s:1:\"P\";s:15:\"xe_validator_id\";s:37:\"modules/layout/tpl/lyaout_info_view/1\";s:8:\"WEB_FONT\";s:2:\"NO\";s:11:\"LAYOUT_TYPE\";s:9:\"MAIN_PAGE\";s:10:\"VISUAL_USE\";s:3:\"YES\";s:3:\"GNB\";s:2:\"64\";s:14:\"menu_name_list\";a:1:{i:64;s:12:\"Welcome menu\";}}',NULL,0,'20141029003858','P'),(68,0,'default','welcome_mobile_layout','O:8:\"stdClass\":4:{s:3:\"GNB\";i:64;s:11:\"LAYOUT_TYPE\";s:9:\"MAIN_PAGE\";s:10:\"VISUAL_USE\";s:3:\"YES\";s:14:\"menu_name_list\";a:1:{i:64;s:12:\"Welcome menu\";}}',NULL,0,'20141029003858','M'),(106,0,'xe_official','XE 공식 사이트 레이아웃',NULL,NULL,0,'20141029004420','P'),(107,0,'user_layout','테스트 레이아웃',NULL,NULL,0,'20141029004420','P'),(108,0,'simplestrap','Simplestrap',NULL,NULL,0,'20141029004607','P'),(122,0,'rkt001','RKT-001',NULL,NULL,0,'20141029010630','P'),(123,0,'common','common',NULL,NULL,0,'20141029011106','P'),(124,0,'layout_intermission','Intermission 레이아웃',NULL,NULL,0,'20141029011106','P'),(125,0,'psds','psds',NULL,NULL,0,'20141029011106','P'),(126,0,'widgets','widgets',NULL,NULL,0,'20141029011106','P'),(127,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029011106','P'),(128,0,'colorCode','XE Color Code 모바일 레이아웃',NULL,NULL,0,'20141029011115','M'),(129,0,'simpleGray','XE 심플 그레이 레이아웃',NULL,NULL,0,'20141029011115','M'),(134,0,'common','common',NULL,NULL,0,'20141029012319','P'),(135,0,'psds','psds',NULL,NULL,0,'20141029012319','P'),(136,0,'widgets','widgets',NULL,NULL,0,'20141029012319','P'),(137,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029012319','P'),(138,0,'common','common',NULL,NULL,0,'20141029012421','P'),(139,0,'psds','psds',NULL,NULL,0,'20141029012421','P'),(140,0,'widgets','widgets',NULL,NULL,0,'20141029012421','P'),(141,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029012421','P'),(142,0,'common','common',NULL,NULL,0,'20141029012511','P'),(143,0,'psds','psds',NULL,NULL,0,'20141029012511','P'),(144,0,'widgets','widgets',NULL,NULL,0,'20141029012511','P'),(145,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029012511','P'),(146,0,'common','common',NULL,NULL,0,'20141029012531','P'),(147,0,'psds','psds',NULL,NULL,0,'20141029012531','P'),(148,0,'widgets','widgets',NULL,NULL,0,'20141029012531','P'),(149,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029012531','P'),(152,0,'common','common',NULL,NULL,0,'20141029013319','P'),(153,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029013319','P'),(154,0,'kwlayout','기본 레이아웃',NULL,NULL,0,'20141029013319','P'),(155,0,'common','common',NULL,NULL,0,'20141029013622','P'),(156,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029013622','P'),(157,0,'common','common',NULL,NULL,0,'20141029013705','P'),(158,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029013705','P'),(159,0,'common','common',NULL,NULL,0,'20141029013707','P'),(160,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029013707','P'),(161,0,'common','common',NULL,NULL,0,'20141029014051','P'),(162,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029014051','P'),(163,0,'common','common',NULL,NULL,0,'20141029014409','P'),(164,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029014409','P'),(165,0,'common','common',NULL,NULL,0,'20141029014500','P'),(166,0,'widgetstyles','widgetstyles',NULL,NULL,0,'20141029014500','P'),(210,0,'default','default','O:8:\"stdClass\":4:{s:3:\"GNB\";i:207;s:11:\"LAYOUT_TYPE\";s:9:\"MAIN_PAGE\";s:10:\"VISUAL_USE\";s:3:\"YES\";s:14:\"menu_name_list\";a:1:{i:207;s:12:\"Welcome menu\";}}',NULL,0,'20141120160335','P'),(211,0,'default','welcome_mobile_layout','O:8:\"stdClass\":4:{s:3:\"GNB\";i:207;s:11:\"LAYOUT_TYPE\";s:9:\"MAIN_PAGE\";s:10:\"VISUAL_USE\";s:3:\"YES\";s:14:\"menu_name_list\";a:1:{i:207;s:12:\"Welcome menu\";}}',NULL,0,'20141120160335','M'),(217,0,'common','common',NULL,NULL,0,'20141120162015','P'),(218,0,'common','common',NULL,NULL,0,'20141120162038','P'),(219,0,'common','common',NULL,NULL,0,'20141120162106','P');
/*!40000 ALTER TABLE `xe_layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member`
--

DROP TABLE IF EXISTS `xe_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member` (
  `member_srl` bigint(11) NOT NULL,
  `user_id` varchar(80) NOT NULL,
  `email_address` varchar(250) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email_id` varchar(80) NOT NULL,
  `email_host` varchar(160) DEFAULT NULL,
  `user_name` varchar(40) NOT NULL,
  `nick_name` varchar(40) NOT NULL,
  `find_account_question` bigint(11) DEFAULT NULL,
  `find_account_answer` varchar(250) DEFAULT NULL,
  `homepage` varchar(250) DEFAULT NULL,
  `blog` varchar(250) DEFAULT NULL,
  `birthday` char(8) DEFAULT NULL,
  `allow_mailing` char(1) NOT NULL DEFAULT 'Y',
  `allow_message` char(1) NOT NULL DEFAULT 'Y',
  `denied` char(1) DEFAULT 'N',
  `limit_date` varchar(14) DEFAULT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `last_login` varchar(14) DEFAULT NULL,
  `change_password_date` varchar(14) DEFAULT NULL,
  `is_admin` char(1) DEFAULT 'N',
  `description` text,
  `extra_vars` text,
  `list_order` bigint(11) NOT NULL,
  PRIMARY KEY (`member_srl`),
  UNIQUE KEY `unique_user_id` (`user_id`),
  UNIQUE KEY `unique_email_address` (`email_address`),
  UNIQUE KEY `unique_nick_name` (`nick_name`),
  KEY `idx_email_host` (`email_host`),
  KEY `idx_allow_mailing` (`allow_mailing`),
  KEY `idx_is_denied` (`denied`),
  KEY `idx_regdate` (`regdate`),
  KEY `idx_last_login` (`last_login`),
  KEY `idx_is_admin` (`is_admin`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member`
--

LOCK TABLES `xe_member` WRITE;
/*!40000 ALTER TABLE `xe_member` DISABLE KEYS */;
INSERT INTO `xe_member` VALUES (4,'andante624','minchae624@gmail.com','a34cd6b4c5150988c86ecb2094237b6a','minchae624','gmail.com','admin','Andante624',NULL,NULL,'','',NULL,'N','Y','N',NULL,'20141029003854','20141130002922','20141029003854','Y',NULL,NULL,-4),(104,'veronicatarina','veronicatarina@gmail.com','81dc9bdb52d04dc20036dbd8313ed055','veronicatarina','gmail.com','이유진','veronicatarina',5,'예쁜남자','','','','N','Y','N','','20141029004159','20141120201436','20141029004159','Y','','O:8:\"stdClass\":1:{s:15:\"xe_validator_id\";s:20:\"modules/member/tpl/1\";}',-104),(105,'dokrsky','dokrsky@gmail.com','81dc9bdb52d04dc20036dbd8313ed055','dokrsky','gmail.com','문성인','dokrsky',5,'깨끗한피부','','','','N','Y','N','','20141029004238','20141029020902','20141029004238','Y','','O:8:\"stdClass\":1:{s:15:\"xe_validator_id\";s:20:\"modules/member/tpl/1\";}',-105);
/*!40000 ALTER TABLE `xe_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_auth_mail`
--

DROP TABLE IF EXISTS `xe_member_auth_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_auth_mail` (
  `auth_key` varchar(60) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `user_id` varchar(80) NOT NULL,
  `new_password` varchar(80) NOT NULL,
  `is_register` char(1) DEFAULT 'N',
  `regdate` varchar(14) DEFAULT NULL,
  UNIQUE KEY `unique_key` (`auth_key`,`member_srl`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_auth_mail`
--

LOCK TABLES `xe_member_auth_mail` WRITE;
/*!40000 ALTER TABLE `xe_member_auth_mail` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_member_auth_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_autologin`
--

DROP TABLE IF EXISTS `xe_member_autologin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_autologin` (
  `autologin_key` varchar(80) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  UNIQUE KEY `unique_key` (`autologin_key`,`member_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_autologin`
--

LOCK TABLES `xe_member_autologin` WRITE;
/*!40000 ALTER TABLE `xe_member_autologin` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_member_autologin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_count_history`
--

DROP TABLE IF EXISTS `xe_member_count_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_count_history` (
  `member_srl` bigint(11) NOT NULL,
  `content` longtext NOT NULL,
  `last_update` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`member_srl`),
  KEY `idx_last_update` (`last_update`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_count_history`
--

LOCK TABLES `xe_member_count_history` WRITE;
/*!40000 ALTER TABLE `xe_member_count_history` DISABLE KEYS */;
INSERT INTO `xe_member_count_history` VALUES (105,'a:2:{i:0;a:3:{i:0;s:13:\"1.231.100.204\";i:1;s:32:\"잘못된 비밀번호입니다.\";i:2;i:1414516108;}i:1;a:3:{i:0;s:15:\"119.192.238.165\";i:1;s:32:\"잘못된 비밀번호입니다.\";i:2;i:1416481894;}}','20141120201134'),(4,'a:1:{i:0;a:3:{i:0;s:15:\"119.192.238.165\";i:1;s:32:\"잘못된 비밀번호입니다.\";i:2;i:1416481564;}}','20141120200604'),(104,'a:3:{i:0;a:3:{i:0;s:15:\"119.192.238.165\";i:1;s:32:\"잘못된 비밀번호입니다.\";i:2;i:1416481656;}i:1;a:3:{i:0;s:15:\"119.192.238.165\";i:1;s:32:\"잘못된 비밀번호입니다.\";i:2;i:1416481772;}i:2;a:3:{i:0;s:15:\"119.192.238.165\";i:1;s:32:\"잘못된 비밀번호입니다.\";i:2;i:1416481875;}}','20141120201115');
/*!40000 ALTER TABLE `xe_member_count_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_denied_nick_name`
--

DROP TABLE IF EXISTS `xe_member_denied_nick_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_denied_nick_name` (
  `nick_name` varchar(80) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`nick_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_denied_nick_name`
--

LOCK TABLES `xe_member_denied_nick_name` WRITE;
/*!40000 ALTER TABLE `xe_member_denied_nick_name` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_member_denied_nick_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_denied_user_id`
--

DROP TABLE IF EXISTS `xe_member_denied_user_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_denied_user_id` (
  `user_id` varchar(80) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `description` text,
  `list_order` bigint(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_denied_user_id`
--

LOCK TABLES `xe_member_denied_user_id` WRITE;
/*!40000 ALTER TABLE `xe_member_denied_user_id` DISABLE KEYS */;
INSERT INTO `xe_member_denied_user_id` VALUES ('addon','20141029003856','',-5),('admin','20141029003856','',-6),('adminlogging','20141029003856','',-7),('autoinstall','20141029003856','',-8),('board','20141029003856','',-9),('comment','20141029003856','',-10),('communication','20141029003856','',-11),('counter','20141029003856','',-12),('document','20141029003856','',-13),('editor','20141029003856','',-14),('file','20141029003856','',-15),('importer','20141029003856','',-16),('install','20141029003856','',-17),('integration_search','20141029003856','',-18),('krzip','20141029003856','',-19),('layout','20141029003856','',-20),('member','20141029003856','',-21),('menu','20141029003856','',-22),('message','20141029003856','',-23),('module','20141029003856','',-24),('page','20141029003856','',-25),('point','20141029003856','',-26),('poll','20141029003856','',-27),('rss','20141029003856','',-28),('session','20141029003856','',-29),('spamfilter','20141029003856','',-30),('syndication','20141029003856','',-31),('tag','20141029003856','',-32),('trash','20141029003856','',-33),('widget','20141029003856','',-34),('www','20141029003856','',-35),('root','20141029003856','',-36),('administrator','20141029003856','',-37),('telnet','20141029003856','',-38),('ftp','20141029003856','',-39),('http','20141029003856','',-40);
/*!40000 ALTER TABLE `xe_member_denied_user_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_friend`
--

DROP TABLE IF EXISTS `xe_member_friend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_friend` (
  `friend_srl` bigint(11) NOT NULL,
  `friend_group_srl` bigint(11) NOT NULL DEFAULT '0',
  `member_srl` bigint(11) NOT NULL,
  `target_srl` bigint(11) NOT NULL,
  `list_order` bigint(11) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`friend_srl`),
  KEY `idx_friend_group_srl` (`friend_group_srl`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_target_srl` (`target_srl`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_friend`
--

LOCK TABLES `xe_member_friend` WRITE;
/*!40000 ALTER TABLE `xe_member_friend` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_member_friend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_friend_group`
--

DROP TABLE IF EXISTS `xe_member_friend_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_friend_group` (
  `friend_group_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`friend_group_srl`),
  KEY `index_owner_member_srl` (`member_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_friend_group`
--

LOCK TABLES `xe_member_friend_group` WRITE;
/*!40000 ALTER TABLE `xe_member_friend_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_member_friend_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_group`
--

DROP TABLE IF EXISTS `xe_member_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_group` (
  `site_srl` bigint(11) NOT NULL DEFAULT '0',
  `group_srl` bigint(11) NOT NULL,
  `list_order` bigint(11) NOT NULL,
  `title` varchar(80) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `is_default` char(1) DEFAULT 'N',
  `is_admin` char(1) DEFAULT 'N',
  `image_mark` text,
  `description` text,
  PRIMARY KEY (`group_srl`),
  UNIQUE KEY `idx_site_title` (`site_srl`,`title`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_group`
--

LOCK TABLES `xe_member_group` WRITE;
/*!40000 ALTER TABLE `xe_member_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_member_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_group_member`
--

DROP TABLE IF EXISTS `xe_member_group_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_group_member` (
  `site_srl` bigint(11) NOT NULL DEFAULT '0',
  `group_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  KEY `idx_site_srl` (`site_srl`),
  KEY `idx_group_member` (`group_srl`,`member_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_group_member`
--

LOCK TABLES `xe_member_group_member` WRITE;
/*!40000 ALTER TABLE `xe_member_group_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_member_group_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_join_form`
--

DROP TABLE IF EXISTS `xe_member_join_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_join_form` (
  `member_join_form_srl` bigint(11) NOT NULL,
  `column_type` varchar(60) NOT NULL,
  `column_name` varchar(60) NOT NULL,
  `column_title` varchar(60) NOT NULL,
  `required` char(1) NOT NULL DEFAULT 'N',
  `default_value` text,
  `is_active` char(1) DEFAULT 'Y',
  `description` text,
  `list_order` bigint(11) NOT NULL DEFAULT '1',
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`member_join_form_srl`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_join_form`
--

LOCK TABLES `xe_member_join_form` WRITE;
/*!40000 ALTER TABLE `xe_member_join_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_member_join_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_login_count`
--

DROP TABLE IF EXISTS `xe_member_login_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_login_count` (
  `ipaddress` varchar(128) NOT NULL,
  `count` bigint(11) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `last_update` varchar(14) DEFAULT NULL,
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_regdate` (`regdate`),
  KEY `idx_last_update` (`last_update`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_login_count`
--

LOCK TABLES `xe_member_login_count` WRITE;
/*!40000 ALTER TABLE `xe_member_login_count` DISABLE KEYS */;
INSERT INTO `xe_member_login_count` VALUES ('1.231.100.204',1,'20141029020828','20141029020828'),('119.192.238.165',5,'20141030154139','20141120201134');
/*!40000 ALTER TABLE `xe_member_login_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_message`
--

DROP TABLE IF EXISTS `xe_member_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_message` (
  `message_srl` bigint(11) NOT NULL,
  `related_srl` bigint(11) NOT NULL,
  `sender_srl` bigint(11) NOT NULL,
  `receiver_srl` bigint(11) NOT NULL,
  `message_type` char(1) NOT NULL DEFAULT 'S',
  `title` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `readed` char(1) NOT NULL DEFAULT 'N',
  `list_order` bigint(11) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `readed_date` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`message_srl`),
  KEY `idx_related_srl` (`related_srl`),
  KEY `idx_sender_srl` (`sender_srl`),
  KEY `idx_receiver_srl` (`receiver_srl`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_message`
--

LOCK TABLES `xe_member_message` WRITE;
/*!40000 ALTER TABLE `xe_member_message` DISABLE KEYS */;
INSERT INTO `xe_member_message` VALUES (214,215,4,4,'S','로그인 실패 기록 보고 입니다.','<h2>로그인 실패 기록을 알려드립니다.</h2><ul><li>등록일: 2014-10-30 03:41:39pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-10-30 03:41:47pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-10-30 03:42:00pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-10-30 03:42:04pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-11-20 04:03:56pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-11-20 04:04:11pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-11-20 04:04:20pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-11-20 04:13:41pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-11-20 04:13:45pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-11-20 04:13:48pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-11-20 04:13:51pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-11-20 04:13:54pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li><li>등록일: 2014-11-20 04:13:59pm<ul><li>IP 주소: 119.192.238.165</li><li>message: 잘못된 비밀번호입니다.</li></ul></li></ul><hr /><p>* 비밀번호를 틀리는 등의 일이 없었는데 이 메시지를 보신다면, 계정 관리에 유의 바랍니다.<br />* 이 메시지는 로그인이 성공한 순간 누적 로그인 실패 기록이 많을 경우, 로그인 성공 이전 실패 기록을 모아서 발송합니다.<br />발송 시각: 2014-11-20 04:14:36pm</p>','N',-214,'20141120161437',NULL);
/*!40000 ALTER TABLE `xe_member_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_member_scrap`
--

DROP TABLE IF EXISTS `xe_member_scrap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_member_scrap` (
  `member_srl` bigint(11) NOT NULL,
  `document_srl` bigint(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `user_name` varchar(80) NOT NULL,
  `nick_name` varchar(80) NOT NULL,
  `target_member_srl` bigint(11) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `list_order` bigint(11) NOT NULL,
  UNIQUE KEY `unique_scrap` (`member_srl`,`document_srl`),
  KEY `idx_regdate` (`regdate`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_member_scrap`
--

LOCK TABLES `xe_member_scrap` WRITE;
/*!40000 ALTER TABLE `xe_member_scrap` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_member_scrap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_menu`
--

DROP TABLE IF EXISTS `xe_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_menu` (
  `menu_srl` bigint(12) NOT NULL,
  `site_srl` bigint(11) NOT NULL DEFAULT '0',
  `title` varchar(250) DEFAULT NULL,
  `listorder` bigint(11) DEFAULT '0',
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`menu_srl`),
  KEY `menu_site_srl` (`site_srl`),
  KEY `idx_title` (`title`),
  KEY `idx_listorder` (`listorder`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_menu`
--

LOCK TABLES `xe_menu` WRITE;
/*!40000 ALTER TABLE `xe_menu` DISABLE KEYS */;
INSERT INTO `xe_menu` VALUES (64,0,'Welcome menu',-64,'20141029003857'),(71,0,'__ADMINMENU_V17__',-71,'20141029003902'),(226,0,'unlinked',-226,'20141121225641');
/*!40000 ALTER TABLE `xe_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_menu_item`
--

DROP TABLE IF EXISTS `xe_menu_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_menu_item` (
  `menu_item_srl` bigint(12) NOT NULL,
  `parent_srl` bigint(12) NOT NULL DEFAULT '0',
  `menu_srl` bigint(12) NOT NULL,
  `name` text,
  `url` varchar(250) DEFAULT NULL,
  `is_shortcut` char(1) DEFAULT 'N',
  `open_window` char(1) DEFAULT 'N',
  `expand` char(1) DEFAULT 'N',
  `normal_btn` varchar(255) DEFAULT NULL,
  `hover_btn` varchar(255) DEFAULT NULL,
  `active_btn` varchar(255) DEFAULT NULL,
  `group_srls` text,
  `listorder` bigint(11) DEFAULT '0',
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`menu_item_srl`),
  KEY `idx_menu_srl` (`menu_srl`),
  KEY `idx_listorder` (`listorder`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_menu_item`
--

LOCK TABLES `xe_menu_item` WRITE;
/*!40000 ALTER TABLE `xe_menu_item` DISABLE KEYS */;
INSERT INTO `xe_menu_item` VALUES (121,0,64,'Welcome','page_TLSQ15','N','N','N',NULL,NULL,NULL,NULL,-122,'20141029010335'),(72,0,71,'{$lang->menu_gnb[\'dashboard\']}','index.php?module=admin','N','N','N',NULL,NULL,NULL,NULL,-72,'20141029003902'),(73,0,71,'{$lang->menu_gnb[\'menu\']}','#','N','N','N',NULL,NULL,NULL,NULL,-73,'20141029003902'),(74,0,71,'{$lang->menu_gnb[\'user\']}','#','N','N','N',NULL,NULL,NULL,NULL,-74,'20141029003902'),(75,0,71,'{$lang->menu_gnb[\'content\']}','#','N','N','N',NULL,NULL,NULL,NULL,-75,'20141029003902'),(76,0,71,'{$lang->menu_gnb[\'configuration\']}','#','N','N','N',NULL,NULL,NULL,NULL,-76,'20141029003902'),(77,0,71,'{$lang->menu_gnb[\'advanced\']}','#','N','N','N',NULL,NULL,NULL,NULL,-77,'20141029003902'),(78,73,71,'{$lang->menu_gnb_sub[\'siteMap\']}','index.php?module=admin&act=dispMenuAdminSiteMap','N','N','N','','','',NULL,-78,'20141029003902'),(79,73,71,'{$lang->menu_gnb_sub[\'siteDesign\']}','index.php?module=admin&act=dispMenuAdminSiteDesign','N','N','N','','','',NULL,-79,'20141029003902'),(80,74,71,'{$lang->menu_gnb_sub[\'userList\']}','index.php?module=admin&act=dispMemberAdminList','N','N','N','','','',NULL,-80,'20141029003902'),(81,74,71,'{$lang->menu_gnb_sub[\'userSetting\']}','index.php?module=admin&act=dispMemberAdminConfig','N','N','N','','','',NULL,-81,'20141029003902'),(82,74,71,'{$lang->menu_gnb_sub[\'userGroup\']}','index.php?module=admin&act=dispMemberAdminGroupList','N','N','N','','','',NULL,-82,'20141029003902'),(83,75,71,'{$lang->menu_gnb_sub[\'document\']}','index.php?module=admin&act=dispDocumentAdminList','N','N','N','','','',NULL,-83,'20141029003902'),(84,75,71,'{$lang->menu_gnb_sub[\'comment\']}','index.php?module=admin&act=dispCommentAdminList','N','N','N','','','',NULL,-84,'20141029003902'),(85,75,71,'{$lang->menu_gnb_sub[\'file\']}','index.php?module=admin&act=dispFileAdminList','N','N','N','','','',NULL,-85,'20141029003902'),(86,75,71,'{$lang->menu_gnb_sub[\'poll\']}','index.php?module=admin&act=dispPollAdminList','N','N','N','','','',NULL,-86,'20141029003902'),(87,75,71,'{$lang->menu_gnb_sub[\'rss\']}','index.php?module=admin&act=dispRssAdminIndex','N','N','N','','','',NULL,-87,'20141029003902'),(88,75,71,'{$lang->menu_gnb_sub[\'multilingual\']}','index.php?module=admin&act=dispModuleAdminLangcode','N','N','N','','','',NULL,-88,'20141029003902'),(89,75,71,'{$lang->menu_gnb_sub[\'importer\']}','index.php?module=admin&act=dispImporterAdminImportForm','N','N','N','','','',NULL,-89,'20141029003902'),(90,75,71,'{$lang->menu_gnb_sub[\'trash\']}','index.php?module=admin&act=dispTrashAdminList','N','N','N','','','',NULL,-90,'20141029003902'),(91,77,71,'{$lang->menu_gnb_sub[\'easyInstall\']}','index.php?module=admin&act=dispAutoinstallAdminIndex','N','N','N','','','',NULL,-91,'20141029003902'),(92,77,71,'{$lang->menu_gnb_sub[\'installedLayout\']}','index.php?module=admin&act=dispLayoutAdminInstalledList','N','N','N','','','',NULL,-92,'20141029003902'),(93,77,71,'{$lang->menu_gnb_sub[\'installedModule\']}','index.php?module=admin&act=dispModuleAdminContent','N','N','N','','','',NULL,-93,'20141029003902'),(94,77,71,'{$lang->menu_gnb_sub[\'installedWidget\']}','index.php?module=admin&act=dispWidgetAdminDownloadedList','N','N','N','','','',NULL,-94,'20141029003902'),(95,77,71,'{$lang->menu_gnb_sub[\'installedAddon\']}','index.php?module=admin&act=dispAddonAdminIndex','N','N','N','','','',NULL,-95,'20141029003902'),(96,77,71,'{$lang->menu_gnb_sub[\'editor\']}','index.php?module=admin&act=dispEditorAdminIndex','N','N','N','','','',NULL,-96,'20141029003902'),(97,75,71,'{$lang->menu_gnb_sub[\'spamFilter\']}','index.php?module=admin&act=dispSpamfilterAdminDeniedIPList','N','N','N','','','',NULL,-97,'20141029003902'),(98,76,71,'{$lang->menu_gnb_sub[\'adminConfigurationGeneral\']}','index.php?module=admin&act=dispAdminConfigGeneral','N','N','N','','','',NULL,-98,'20141029003902'),(99,76,71,'{$lang->menu_gnb_sub[\'adminConfigurationFtp\']}','index.php?module=admin&act=dispAdminConfigFtp','N','N','N','','','',NULL,-99,'20141029003902'),(100,76,71,'{$lang->menu_gnb_sub[\'adminMenuSetup\']}','index.php?module=admin&act=dispAdminSetup','N','N','N','','','',NULL,-100,'20141029003902'),(101,76,71,'{$lang->menu_gnb_sub[\'fileUpload\']}','index.php?module=admin&act=dispFileAdminConfig','N','N','N','','','',NULL,-101,'20141029003902'),(102,76,71,'{$lang->menu_gnb_sub[\'filebox\']}','index.php?module=admin&act=dispModuleAdminFileBox','N','N','N','','','',NULL,-102,'20141029003902'),(103,74,71,'{$lang->menu_gnb_sub[\'point\']}','index.php?module=admin&act=dispPointAdminConfig','N','N','N','','','',NULL,-103,'20141029003902'),(110,0,64,'강의 계획서 조회','page_CVSB05','N','N','N',NULL,NULL,NULL,NULL,-123,'20141029004825'),(114,115,64,'교수 랭킹','page_KpVk92','N','N','N',NULL,NULL,NULL,NULL,0,'20141029004953'),(115,0,64,'검색 랭킹','page_KpVk92','Y','N','N',NULL,NULL,NULL,NULL,-124,'20141029004825'),(117,115,64,'강의 랭킹','page_XPTn07','N','N','N',NULL,NULL,NULL,NULL,-117,'20141029005147'),(229,0,226,'강의 계획서 상세 조회','detail_list','N','N','N',NULL,NULL,NULL,NULL,0,'20141121225641'),(227,0,226,'강의 계획서 간편 조회','simple_list','N','N','N',NULL,NULL,NULL,NULL,0,'20141121225641'),(233,0,226,'review','review','N','N','N',NULL,NULL,NULL,NULL,0,'20141129190118'),(241,0,226,'radar','radar','N','N','N',NULL,NULL,NULL,NULL,0,'20141130014013'),(243,0,226,'radarview','radarview','N','N','N',NULL,NULL,NULL,NULL,0,'20141130014143');
/*!40000 ALTER TABLE `xe_menu_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_menu_layout`
--

DROP TABLE IF EXISTS `xe_menu_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_menu_layout` (
  `menu_srl` bigint(12) NOT NULL,
  `layout_srl` bigint(12) NOT NULL,
  PRIMARY KEY (`menu_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_menu_layout`
--

LOCK TABLES `xe_menu_layout` WRITE;
/*!40000 ALTER TABLE `xe_menu_layout` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_menu_layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_admins`
--

DROP TABLE IF EXISTS `xe_module_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_admins` (
  `module_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  UNIQUE KEY `unique_module_admin` (`module_srl`,`member_srl`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_admins`
--

LOCK TABLES `xe_module_admins` WRITE;
/*!40000 ALTER TABLE `xe_module_admins` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_module_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_categories`
--

DROP TABLE IF EXISTS `xe_module_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_categories` (
  `module_category_srl` bigint(11) NOT NULL DEFAULT '0',
  `title` varchar(250) DEFAULT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`module_category_srl`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_categories`
--

LOCK TABLES `xe_module_categories` WRITE;
/*!40000 ALTER TABLE `xe_module_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_module_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_config`
--

DROP TABLE IF EXISTS `xe_module_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_config` (
  `module` varchar(250) NOT NULL,
  `site_srl` bigint(11) NOT NULL,
  `config` text,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`module`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_config`
--

LOCK TABLES `xe_module_config` WRITE;
/*!40000 ALTER TABLE `xe_module_config` DISABLE KEYS */;
INSERT INTO `xe_module_config` VALUES ('comment',0,'N;','20141029003852'),('editor',0,'N;','20141029003852'),('layout',0,'N;','20141029003852'),('rss',0,'N;','20141029003852'),('menu',0,'O:8:\"stdClass\":1:{s:17:\"unlinked_menu_srl\";i:226;}','20141121225641'),('autoinstall',0,'O:8:\"stdClass\":1:{s:14:\"downloadServer\";s:33:\"http://download.xpressengine.com/\";}','20141120160330'),('file',0,'O:8:\"stdClass\":3:{s:16:\"allowed_filesize\";s:1:\"2\";s:19:\"allowed_attach_size\";s:1:\"2\";s:17:\"allowed_filetypes\";s:3:\"*.*\";}','20141120160330'),('poll',0,'O:8:\"stdClass\":2:{s:4:\"skin\";s:7:\"default\";s:8:\"colorset\";s:6:\"normal\";}','20141120160330'),('member',0,'O:8:\"stdClass\":16:{s:11:\"enable_join\";s:1:\"Y\";s:13:\"enable_openid\";s:1:\"N\";s:16:\"enable_auth_mail\";s:1:\"N\";s:10:\"image_name\";s:1:\"Y\";s:10:\"image_mark\";s:1:\"Y\";s:13:\"profile_image\";s:1:\"Y\";s:20:\"image_name_max_width\";s:2:\"90\";s:21:\"image_name_max_height\";s:2:\"20\";s:20:\"image_mark_max_width\";s:2:\"20\";s:21:\"image_mark_max_height\";s:2:\"20\";s:23:\"profile_image_max_width\";s:2:\"80\";s:24:\"profile_image_max_height\";s:2:\"80\";s:16:\"group_image_mark\";s:1:\"N\";s:17:\"password_strength\";s:6:\"normal\";s:10:\"signupForm\";a:13:{i:0;O:8:\"stdClass\":9:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:13:\"email_address\";s:5:\"title\";s:13:\"email_address\";s:12:\"mustRequired\";b:1;s:9:\"imageType\";b:0;s:8:\"required\";b:1;s:5:\"isUse\";b:1;s:8:\"isPublic\";s:1:\"Y\";s:12:\"isIdentifier\";b:1;}i:1;O:8:\"stdClass\":9:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:7:\"user_id\";s:5:\"title\";s:7:\"user_id\";s:12:\"mustRequired\";b:0;s:9:\"imageType\";b:0;s:8:\"required\";b:1;s:5:\"isUse\";b:1;s:8:\"isPublic\";s:1:\"Y\";s:12:\"isIdentifier\";b:0;}i:2;O:8:\"stdClass\":9:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:8:\"password\";s:5:\"title\";s:8:\"password\";s:12:\"mustRequired\";b:1;s:9:\"imageType\";b:0;s:8:\"required\";b:1;s:5:\"isUse\";b:1;s:8:\"isPublic\";s:1:\"N\";s:12:\"isIdentifier\";b:0;}i:3;O:8:\"stdClass\":9:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:9:\"user_name\";s:5:\"title\";s:9:\"user_name\";s:12:\"mustRequired\";b:0;s:9:\"imageType\";b:0;s:8:\"required\";b:1;s:5:\"isUse\";b:1;s:8:\"isPublic\";s:1:\"Y\";s:12:\"isIdentifier\";b:0;}i:4;O:8:\"stdClass\":9:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:9:\"nick_name\";s:5:\"title\";s:9:\"nick_name\";s:12:\"mustRequired\";b:1;s:9:\"imageType\";b:0;s:8:\"required\";b:1;s:5:\"isUse\";b:1;s:8:\"isPublic\";s:1:\"Y\";s:12:\"isIdentifier\";b:0;}i:5;O:8:\"stdClass\":9:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:21:\"find_account_question\";s:5:\"title\";s:21:\"find_account_question\";s:12:\"mustRequired\";b:1;s:9:\"imageType\";b:0;s:8:\"required\";b:1;s:5:\"isUse\";b:1;s:8:\"isPublic\";s:1:\"N\";s:12:\"isIdentifier\";b:0;}i:6;O:8:\"stdClass\":9:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:8:\"homepage\";s:5:\"title\";s:8:\"homepage\";s:12:\"mustRequired\";b:0;s:9:\"imageType\";b:0;s:8:\"required\";b:0;s:5:\"isUse\";b:1;s:8:\"isPublic\";s:1:\"Y\";s:12:\"isIdentifier\";b:0;}i:7;O:8:\"stdClass\":9:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:4:\"blog\";s:5:\"title\";s:4:\"blog\";s:12:\"mustRequired\";b:0;s:9:\"imageType\";b:0;s:8:\"required\";b:0;s:5:\"isUse\";b:1;s:8:\"isPublic\";s:1:\"Y\";s:12:\"isIdentifier\";b:0;}i:8;O:8:\"stdClass\":9:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:8:\"birthday\";s:5:\"title\";s:8:\"birthday\";s:12:\"mustRequired\";b:0;s:9:\"imageType\";b:0;s:8:\"required\";b:0;s:5:\"isUse\";b:1;s:8:\"isPublic\";s:1:\"Y\";s:12:\"isIdentifier\";b:0;}i:9;O:8:\"stdClass\":9:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:9:\"signature\";s:5:\"title\";s:9:\"signature\";s:12:\"mustRequired\";b:0;s:9:\"imageType\";b:0;s:8:\"required\";b:0;s:5:\"isUse\";b:0;s:8:\"isPublic\";s:1:\"N\";s:12:\"isIdentifier\";b:0;}i:10;O:8:\"stdClass\":11:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:13:\"profile_image\";s:5:\"title\";s:13:\"profile_image\";s:12:\"mustRequired\";b:0;s:9:\"imageType\";b:1;s:8:\"required\";b:0;s:5:\"isUse\";b:0;s:8:\"isPublic\";s:1:\"N\";s:12:\"isIdentifier\";b:0;s:9:\"max_width\";N;s:10:\"max_height\";N;}i:11;O:8:\"stdClass\":11:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:10:\"image_name\";s:5:\"title\";s:10:\"image_name\";s:12:\"mustRequired\";b:0;s:9:\"imageType\";b:1;s:8:\"required\";b:0;s:5:\"isUse\";b:0;s:8:\"isPublic\";s:1:\"N\";s:12:\"isIdentifier\";b:0;s:9:\"max_width\";N;s:10:\"max_height\";N;}i:12;O:8:\"stdClass\":11:{s:13:\"isDefaultForm\";b:1;s:4:\"name\";s:10:\"image_mark\";s:5:\"title\";s:10:\"image_mark\";s:12:\"mustRequired\";b:0;s:9:\"imageType\";b:1;s:8:\"required\";b:0;s:5:\"isUse\";b:0;s:8:\"isPublic\";s:1:\"N\";s:12:\"isIdentifier\";b:0;s:9:\"max_width\";N;s:10:\"max_height\";N;}}s:10:\"identifier\";s:13:\"email_address\";}','20141120160331'),('point',0,'O:8:\"stdClass\":21:{s:9:\"max_level\";i:30;s:10:\"level_step\";a:30:{i:1;i:90;i:2;i:360;i:3;i:810;i:4;i:1440;i:5;i:2250;i:6;i:3240;i:7;i:4410;i:8;i:5760;i:9;i:7290;i:10;i:9000;i:11;i:10890;i:12;i:12960;i:13;i:15210;i:14;i:17640;i:15;i:20250;i:16;i:23040;i:17;i:26010;i:18;i:29160;i:19;i:32490;i:20;i:36000;i:21;i:39690;i:22;i:43560;i:23;i:47610;i:24;i:51840;i:25;i:56250;i:26;i:60840;i:27;i:65610;i:28;i:70560;i:29;i:75690;i:30;i:81000;}s:12:\"signup_point\";i:10;s:11:\"login_point\";i:5;s:10:\"point_name\";s:5:\"point\";s:10:\"level_icon\";s:7:\"default\";s:16:\"disable_download\";b:0;s:15:\"insert_document\";i:10;s:19:\"insert_document_act\";s:23:\"procBoardInsertDocument\";s:19:\"delete_document_act\";s:23:\"procBoardDeleteDocument\";s:14:\"insert_comment\";i:5;s:18:\"insert_comment_act\";s:44:\"procBoardInsertComment,procBlogInsertComment\";s:18:\"delete_comment_act\";s:44:\"procBoardDeleteComment,procBlogDeleteComment\";s:11:\"upload_file\";i:5;s:15:\"upload_file_act\";s:14:\"procFileUpload\";s:15:\"delete_file_act\";s:14:\"procFileDelete\";s:13:\"download_file\";i:-5;s:17:\"download_file_act\";s:16:\"procFileDownload\";s:13:\"read_document\";i:0;s:5:\"voted\";i:0;s:6:\"blamed\";i:0;}','20141120160333'),('document',0,'O:8:\"stdClass\":1:{s:14:\"thumbnail_type\";s:4:\"crop\";}','20141029014856'),('module',0,'O:8:\"stdClass\":3:{s:18:\"isUpdateFixedValue\";b:1;s:10:\"htmlFooter\";N;s:9:\"siteTitle\";s:19:\"KW Syllabus Service\";}','20141029014856');
/*!40000 ALTER TABLE `xe_module_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_extend`
--

DROP TABLE IF EXISTS `xe_module_extend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_extend` (
  `parent_module` varchar(80) NOT NULL,
  `extend_module` varchar(80) NOT NULL,
  `type` varchar(15) NOT NULL,
  `kind` varchar(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_extend`
--

LOCK TABLES `xe_module_extend` WRITE;
/*!40000 ALTER TABLE `xe_module_extend` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_module_extend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_extra_vars`
--

DROP TABLE IF EXISTS `xe_module_extra_vars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_extra_vars` (
  `module_srl` bigint(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `value` text,
  UNIQUE KEY `unique_module_vars` (`module_srl`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_extra_vars`
--

LOCK TABLES `xe_module_extra_vars` WRITE;
/*!40000 ALTER TABLE `xe_module_extra_vars` DISABLE KEYS */;
INSERT INTO `xe_module_extra_vars` VALUES (120,'module_type','controller'),(120,'page_type','ARTICLE'),(109,'xe_js_callback','top.opener.top.fullSetupDone();top.window.close();var dummy='),(113,'regdate','20141029004953'),(113,'page_type','OUTSIDE'),(116,'page_type','OUTSIDE'),(120,'xe_js_callback','top.opener.top.fullSetupDone();top.window.close();var dummy='),(120,'regdate','20141029010335'),(109,'page_type','OUTSIDE'),(109,'path','./layouts/default/custompage/search_view.html'),(113,'path','./layouts/default/custompage/prof_rank.html'),(113,'xe_js_callback','top.opener.top.fullSetupDone();top.window.close();var dummy='),(116,'path','./layouts/default/custompage/sub_rank.html'),(116,'xe_js_callback','top.opener.top.fullSetupDone();top.window.close();var dummy='),(120,'document_srl','150'),(225,'path','./layouts/default/custompage/list_view.html'),(225,'xe_js_callback','top.opener.top.fullSetupDone();top.window.close();var dummy='),(225,'page_type','OUTSIDE'),(228,'path','./layouts/default/custompage/detail_view.html'),(228,'xe_js_callback','top.opener.top.fullSetupDone();top.window.close();var dummy='),(228,'page_type','OUTSIDE'),(232,'page_type','OUTSIDE'),(232,'path','./layouts/default/custompage/review.php'),(232,'xe_js_callback','top.opener.top.fullSetupDone();top.window.close();var dummy='),(242,'xe_js_callback','top.opener.top.fullSetupDone();top.window.close();var dummy='),(242,'page_type','OUTSIDE'),(242,'path','./layouts/default/custompage/radar/radarchart.html'),(240,'page_type','OUTSIDE'),(240,'path','./layouts/default/custompage/radar/radarchart.html'),(240,'regdate','20141029004953'),(240,'xe_js_callback','top.opener.top.fullSetupDone();top.window.close();var dummy=');
/*!40000 ALTER TABLE `xe_module_extra_vars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_filebox`
--

DROP TABLE IF EXISTS `xe_module_filebox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_filebox` (
  `module_filebox_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `filename` varchar(250) NOT NULL,
  `fileextension` varchar(4) NOT NULL,
  `filesize` bigint(11) NOT NULL DEFAULT '0',
  `comment` varchar(250) DEFAULT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`module_filebox_srl`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_fileextension` (`fileextension`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_filebox`
--

LOCK TABLES `xe_module_filebox` WRITE;
/*!40000 ALTER TABLE `xe_module_filebox` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_module_filebox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_grants`
--

DROP TABLE IF EXISTS `xe_module_grants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_grants` (
  `module_srl` bigint(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `group_srl` bigint(11) NOT NULL,
  UNIQUE KEY `unique_module` (`module_srl`,`name`,`group_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_grants`
--

LOCK TABLES `xe_module_grants` WRITE;
/*!40000 ALTER TABLE `xe_module_grants` DISABLE KEYS */;
INSERT INTO `xe_module_grants` VALUES (109,'access',0),(109,'manager',-3),(225,'manager',-3),(228,'manager',-3);
/*!40000 ALTER TABLE `xe_module_grants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_locks`
--

DROP TABLE IF EXISTS `xe_module_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_locks` (
  `lock_name` varchar(40) NOT NULL,
  `deadline` varchar(14) DEFAULT NULL,
  `member_srl` bigint(11) DEFAULT NULL,
  UNIQUE KEY `unique_lock_name` (`lock_name`),
  KEY `idx_deadline` (`deadline`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_locks`
--

LOCK TABLES `xe_module_locks` WRITE;
/*!40000 ALTER TABLE `xe_module_locks` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_module_locks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_mobile_skins`
--

DROP TABLE IF EXISTS `xe_module_mobile_skins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_mobile_skins` (
  `module_srl` bigint(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `value` text,
  UNIQUE KEY `unique_module_mobile_skins` (`module_srl`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_mobile_skins`
--

LOCK TABLES `xe_module_mobile_skins` WRITE;
/*!40000 ALTER TABLE `xe_module_mobile_skins` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_module_mobile_skins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_part_config`
--

DROP TABLE IF EXISTS `xe_module_part_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_part_config` (
  `module` varchar(250) NOT NULL,
  `module_srl` bigint(11) NOT NULL,
  `config` text,
  `regdate` varchar(14) DEFAULT NULL,
  KEY `idx_module_part_config` (`module`,`module_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_part_config`
--

LOCK TABLES `xe_module_part_config` WRITE;
/*!40000 ALTER TABLE `xe_module_part_config` DISABLE KEYS */;
INSERT INTO `xe_module_part_config` VALUES ('layout',67,'O:8:\"stdClass\":1:{s:13:\"header_script\";N;}','20141120162038'),('comment',225,'N;','20141121225641'),('comment',228,'N;','20141121225641'),('document',225,'N;','20141121225641'),('document',228,'N;','20141121225641'),('editor',225,'N;','20141121225641'),('editor',228,'N;','20141121225641'),('file',225,'N;','20141121225641'),('file',228,'N;','20141121225641'),('point',225,'N;','20141121225641'),('point',228,'N;','20141121225641'),('rss',225,'N;','20141121225641'),('rss',228,'N;','20141121225641'),('comment',230,'N;','20141129043442'),('document',230,'N;','20141129043442'),('editor',230,'N;','20141129043442'),('file',230,'N;','20141129043442'),('point',230,'N;','20141129043442'),('rss',230,'N;','20141129043442'),('comment',232,'N;','20141129190118'),('document',232,'N;','20141129190118'),('editor',232,'N;','20141129190118'),('file',232,'N;','20141129190118'),('point',232,'N;','20141129190118'),('rss',232,'N;','20141129190118'),('comment',234,'N;','20141129210415'),('document',234,'N;','20141129210416'),('editor',234,'N;','20141129210416'),('file',234,'N;','20141129210416'),('point',234,'N;','20141129210416'),('rss',234,'N;','20141129210416'),('comment',236,'N;','20141129213824'),('document',236,'N;','20141129213824'),('editor',236,'N;','20141129213824'),('file',236,'N;','20141129213824'),('point',236,'N;','20141129213825'),('rss',236,'N;','20141129213825'),('comment',238,'N;','20141129215209'),('document',238,'N;','20141129215209'),('editor',238,'N;','20141129215209'),('file',238,'N;','20141129215209'),('point',238,'N;','20141129215209'),('rss',238,'N;','20141129215209'),('comment',240,'N;','20141130014013'),('document',240,'N;','20141130014013'),('editor',240,'N;','20141130014013'),('file',240,'N;','20141130014013'),('point',240,'N;','20141130014013'),('rss',240,'N;','20141130014013'),('comment',242,'N;','20141130014143'),('document',242,'N;','20141130014144'),('editor',242,'N;','20141130014144'),('file',242,'N;','20141130014144'),('point',242,'N;','20141130014144'),('rss',242,'N;','20141130014144');
/*!40000 ALTER TABLE `xe_module_part_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_skins`
--

DROP TABLE IF EXISTS `xe_module_skins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_skins` (
  `module_srl` bigint(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `value` text,
  UNIQUE KEY `unique_module_skins` (`module_srl`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_skins`
--

LOCK TABLES `xe_module_skins` WRITE;
/*!40000 ALTER TABLE `xe_module_skins` DISABLE KEYS */;
INSERT INTO `xe_module_skins` VALUES (120,'display_title','hide'),(120,'display_popupmenu','hide');
/*!40000 ALTER TABLE `xe_module_skins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_module_trigger`
--

DROP TABLE IF EXISTS `xe_module_trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_module_trigger` (
  `trigger_name` varchar(80) NOT NULL,
  `called_position` varchar(15) NOT NULL,
  `module` varchar(80) NOT NULL,
  `type` varchar(15) NOT NULL,
  `called_method` varchar(80) NOT NULL,
  UNIQUE KEY `idx_trigger` (`trigger_name`,`called_position`,`module`,`type`,`called_method`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_module_trigger`
--

LOCK TABLES `xe_module_trigger` WRITE;
/*!40000 ALTER TABLE `xe_module_trigger` DISABLE KEYS */;
INSERT INTO `xe_module_trigger` VALUES ('comment.deleteComment','after','file','controller','triggerCommentDeleteAttached'),('comment.deleteComment','after','point','controller','triggerDeleteComment'),('comment.deleteComment','after','poll','controller','triggerDeleteCommentPoll'),('comment.getCommentMenu','after','member','controller','triggerGetCommentMenu'),('comment.insertComment','after','file','controller','triggerCommentAttachFiles'),('comment.insertComment','after','point','controller','triggerInsertComment'),('comment.insertComment','after','poll','controller','triggerInsertCommentPoll'),('comment.insertComment','before','file','controller','triggerCommentCheckAttached'),('comment.insertComment','before','spamfilter','controller','triggerInsertComment'),('comment.updateComment','after','file','controller','triggerCommentAttachFiles'),('comment.updateComment','after','poll','controller','triggerUpdateCommentPoll'),('comment.updateComment','before','file','controller','triggerCommentCheckAttached'),('comment.updateComment','before','spamfilter','controller','triggerInsertComment'),('communication.sendMessage','before','spamfilter','controller','triggerSendMessage'),('display','before','editor','controller','triggerEditorComponentCompile'),('display','before','widget','controller','triggerWidgetCompile'),('document.deleteDocument','after','comment','controller','triggerDeleteDocumentComments'),('document.deleteDocument','after','file','controller','triggerDeleteAttached'),('document.deleteDocument','after','point','controller','triggerDeleteDocument'),('document.deleteDocument','after','poll','controller','triggerDeleteDocumentPoll'),('document.deleteDocument','after','syndication','controller','triggerDeleteDocument'),('document.deleteDocument','after','tag','controller','triggerDeleteTag'),('document.deleteDocument','before','point','controller','triggerBeforeDeleteDocument'),('document.getDocumentMenu','after','member','controller','triggerGetDocumentMenu'),('document.insertDocument','after','editor','controller','triggerDeleteSavedDoc'),('document.insertDocument','after','file','controller','triggerAttachFiles'),('document.insertDocument','after','point','controller','triggerInsertDocument'),('document.insertDocument','after','poll','controller','triggerInsertDocumentPoll'),('document.insertDocument','after','syndication','controller','triggerInsertDocument'),('document.insertDocument','after','tag','controller','triggerInsertTag'),('document.insertDocument','before','file','controller','triggerCheckAttached'),('document.insertDocument','before','spamfilter','controller','triggerInsertDocument'),('document.insertDocument','before','tag','controller','triggerArrangeTag'),('document.moveDocumentModule','after','syndication','controller','triggerMoveDocumentModule'),('document.moveDocumentToTrash','after','syndication','controller','triggerMoveDocumentToTrash'),('document.restoreTrash','after','syndication','controller','triggerRestoreTrash'),('document.updateDocument','after','editor','controller','triggerDeleteSavedDoc'),('document.updateDocument','after','file','controller','triggerAttachFiles'),('document.updateDocument','after','poll','controller','triggerUpdateDocumentPoll'),('document.updateDocument','after','syndication','controller','triggerUpdateDocument'),('document.updateDocument','after','tag','controller','triggerInsertTag'),('document.updateDocument','before','file','controller','triggerCheckAttached'),('document.updateDocument','before','point','controller','triggerUpdateDocument'),('document.updateDocument','before','spamfilter','controller','triggerInsertDocument'),('document.updateDocument','before','tag','controller','triggerArrangeTag'),('document.updateReadedCount','after','point','controller','triggerUpdateReadedCount'),('document.updateVotedCount','after','point','controller','triggerUpdateVotedCount'),('editor.deleteSavedDoc','after','file','controller','triggerDeleteAttached'),('file.deleteFile','after','point','controller','triggerDeleteFile'),('file.downloadFile','after','point','controller','triggerDownloadFile'),('file.downloadFile','before','point','controller','triggerBeforeDownloadFile'),('file.insertFile','after','point','controller','triggerInsertFile'),('member.doLogin','after','point','controller','triggerAfterLogin'),('member.getMemberMenu','after','board','controller','triggerMemberMenu'),('member.insertMember','after','point','controller','triggerInsertMember'),('menu.getModuleListInSitemap','after','board','model','triggerModuleListInSitemap'),('module.deleteModule','after','comment','controller','triggerDeleteModuleComments'),('module.deleteModule','after','document','controller','triggerDeleteModuleDocuments'),('module.deleteModule','after','file','controller','triggerDeleteModuleFiles'),('module.deleteModule','after','syndication','controller','triggerDeleteModule'),('module.deleteModule','after','tag','controller','triggerDeleteModuleTags'),('module.dispAdditionSetup','after','point','view','triggerDispPointAdditionSetup'),('module.dispAdditionSetup','before','comment','view','triggerDispCommentAdditionSetup'),('module.dispAdditionSetup','before','document','view','triggerDispDocumentAdditionSetup'),('module.dispAdditionSetup','before','editor','view','triggerDispEditorAdditionSetup'),('module.dispAdditionSetup','before','file','view','triggerDispFileAdditionSetup'),('module.dispAdditionSetup','before','rss','view','triggerDispRssAdditionSetup'),('module.procModuleAdminCopyModule','after','comment','controller','triggerCopyModule'),('module.procModuleAdminCopyModule','after','document','controller','triggerCopyModule'),('module.procModuleAdminCopyModule','after','document','controller','triggerCopyModuleExtraKeys'),('module.procModuleAdminCopyModule','after','editor','controller','triggerCopyModule'),('module.procModuleAdminCopyModule','after','file','controller','triggerCopyModule'),('module.procModuleAdminCopyModule','after','point','controller','triggerCopyModule'),('module.procModuleAdminCopyModule','after','rss','controller','triggerCopyModule'),('moduleHandler.proc','after','rss','controller','triggerRssUrlInsert'),('trackback.insertTrackback','before','spamfilter','controller','triggerInsertTrackback');
/*!40000 ALTER TABLE `xe_module_trigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_modules`
--

DROP TABLE IF EXISTS `xe_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_modules` (
  `module_srl` bigint(11) NOT NULL,
  `module` varchar(80) NOT NULL,
  `module_category_srl` bigint(11) DEFAULT '0',
  `layout_srl` bigint(11) DEFAULT '0',
  `use_mobile` char(1) DEFAULT 'N',
  `mlayout_srl` bigint(11) DEFAULT '0',
  `menu_srl` bigint(11) DEFAULT '0',
  `site_srl` bigint(11) NOT NULL DEFAULT '0',
  `mid` varchar(40) NOT NULL,
  `is_skin_fix` char(1) NOT NULL DEFAULT 'Y',
  `skin` varchar(250) DEFAULT NULL,
  `is_mskin_fix` char(1) NOT NULL DEFAULT 'Y',
  `mskin` varchar(250) DEFAULT NULL,
  `browser_title` varchar(250) NOT NULL,
  `description` text,
  `is_default` char(1) NOT NULL DEFAULT 'N',
  `content` longtext,
  `mcontent` longtext,
  `open_rss` char(1) NOT NULL DEFAULT 'Y',
  `header_text` text,
  `footer_text` text,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`module_srl`),
  UNIQUE KEY `idx_site_mid` (`site_srl`,`mid`),
  KEY `idx_module` (`module`),
  KEY `idx_module_category` (`module_category_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_modules`
--

LOCK TABLES `xe_modules` WRITE;
/*!40000 ALTER TABLE `xe_modules` DISABLE KEYS */;
INSERT INTO `xe_modules` VALUES (240,'page',0,67,'Y',-1,226,0,'radar','Y','/USE_DEFAULT/','Y','/USE_DEFAULT/','radar','','N',NULL,NULL,'Y','','','20141130014013'),(242,'page',0,0,'Y',0,226,0,'radarview','Y',NULL,'Y',NULL,'radarview','','N',NULL,NULL,'Y','','','20141130014143'),(232,'page',0,67,'Y',-1,226,0,'review','Y',NULL,'Y',NULL,'review','','N',NULL,NULL,'Y','','','20141129190118'),(225,'page',0,67,'Y',-1,226,0,'simple_list','Y',NULL,'Y',NULL,'강의 계획서 간편 조회','','N',NULL,NULL,'Y','','','20141121225641'),(228,'page',0,67,'Y',-1,226,0,'detail_list','Y',NULL,'Y',NULL,'강의 계획서 상세 조회','','N',NULL,NULL,'Y','','','20141121225641'),(109,'page',0,67,'Y',-1,64,0,'page_CVSB05','Y',NULL,'Y',NULL,'강의 계획서 조회','','N',NULL,NULL,'Y','','','20141029004825'),(113,'page',0,67,'Y',-1,64,0,'page_KpVk92','Y','/USE_DEFAULT/','Y','/USE_DEFAULT/','교수 랭킹','','N',NULL,NULL,'Y','','','20141029004953'),(116,'page',0,67,'Y',-1,64,0,'page_XPTn07','Y',NULL,'Y',NULL,'강의 랭킹','','N',NULL,NULL,'Y','','','20141029005147'),(120,'page',0,67,'Y',-1,64,0,'page_TLSQ15','N','/USE_DEFAULT/','N','/USE_DEFAULT/','Welcome','','N',NULL,NULL,'Y','','','20141029010335');
/*!40000 ALTER TABLE `xe_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_point`
--

DROP TABLE IF EXISTS `xe_point`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_point` (
  `member_srl` bigint(11) NOT NULL,
  `point` bigint(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_srl`),
  KEY `idx_point` (`point`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_point`
--

LOCK TABLES `xe_point` WRITE;
/*!40000 ALTER TABLE `xe_point` DISABLE KEYS */;
INSERT INTO `xe_point` VALUES (4,99964),(104,10005),(105,10000);
/*!40000 ALTER TABLE `xe_point` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_poll`
--

DROP TABLE IF EXISTS `xe_poll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_poll` (
  `poll_srl` bigint(11) NOT NULL,
  `stop_date` varchar(14) DEFAULT NULL,
  `upload_target_srl` bigint(11) NOT NULL,
  `poll_count` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `ipaddress` varchar(128) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `list_order` bigint(11) NOT NULL,
  PRIMARY KEY (`poll_srl`),
  KEY `idx_upload_target_srl` (`upload_target_srl`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_regdate` (`regdate`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_poll`
--

LOCK TABLES `xe_poll` WRITE;
/*!40000 ALTER TABLE `xe_poll` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_poll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_poll_item`
--

DROP TABLE IF EXISTS `xe_poll_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_poll_item` (
  `poll_item_srl` bigint(11) NOT NULL,
  `poll_srl` bigint(11) NOT NULL,
  `poll_index_srl` bigint(11) NOT NULL,
  `upload_target_srl` bigint(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `poll_count` bigint(11) NOT NULL,
  PRIMARY KEY (`poll_item_srl`),
  KEY `index_poll_srl` (`poll_srl`),
  KEY `idx_poll_index_srl` (`poll_index_srl`),
  KEY `idx_upload_target_srl` (`upload_target_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_poll_item`
--

LOCK TABLES `xe_poll_item` WRITE;
/*!40000 ALTER TABLE `xe_poll_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_poll_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_poll_log`
--

DROP TABLE IF EXISTS `xe_poll_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_poll_log` (
  `poll_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `ipaddress` varchar(128) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  KEY `idx_poll_srl` (`poll_srl`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_poll_log`
--

LOCK TABLES `xe_poll_log` WRITE;
/*!40000 ALTER TABLE `xe_poll_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_poll_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_poll_title`
--

DROP TABLE IF EXISTS `xe_poll_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_poll_title` (
  `poll_srl` bigint(11) NOT NULL,
  `poll_index_srl` bigint(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `checkcount` bigint(11) NOT NULL DEFAULT '1',
  `poll_count` bigint(11) NOT NULL,
  `upload_target_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `ipaddress` varchar(128) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  `list_order` bigint(11) NOT NULL,
  KEY `idx_poll_srl` (`poll_srl`,`poll_index_srl`),
  KEY `idx_upload_target_srl` (`upload_target_srl`),
  KEY `idx_member_srl` (`member_srl`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_regdate` (`regdate`),
  KEY `idx_list_order` (`list_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_poll_title`
--

LOCK TABLES `xe_poll_title` WRITE;
/*!40000 ALTER TABLE `xe_poll_title` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_poll_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_sequence`
--

DROP TABLE IF EXISTS `xe_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_sequence` (
  `seq` bigint(64) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`seq`)
) ENGINE=MyISAM AUTO_INCREMENT=244 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_sequence`
--

LOCK TABLES `xe_sequence` WRITE;
/*!40000 ALTER TABLE `xe_sequence` DISABLE KEYS */;
INSERT INTO `xe_sequence` VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26),(27),(28),(29),(30),(31),(32),(33),(34),(35),(36),(37),(38),(39),(40),(41),(42),(43),(44),(45),(46),(47),(48),(49),(50),(51),(52),(53),(54),(55),(56),(57),(58),(59),(60),(61),(62),(63),(64),(65),(66),(67),(68),(69),(70),(71),(72),(73),(74),(75),(76),(77),(78),(79),(80),(81),(82),(83),(84),(85),(86),(87),(88),(89),(90),(91),(92),(93),(94),(95),(96),(97),(98),(99),(100),(101),(102),(103),(104),(105),(106),(107),(108),(109),(110),(111),(112),(113),(114),(115),(116),(117),(118),(119),(120),(121),(122),(123),(124),(125),(126),(127),(128),(129),(130),(131),(132),(133),(134),(135),(136),(137),(138),(139),(140),(141),(142),(143),(144),(145),(146),(147),(148),(149),(150),(151),(152),(153),(154),(155),(156),(157),(158),(159),(160),(161),(162),(163),(164),(165),(166),(167),(168),(169),(170),(171),(172),(173),(174),(175),(176),(177),(178),(179),(180),(181),(182),(183),(184),(185),(186),(187),(188),(189),(190),(191),(192),(193),(194),(195),(196),(197),(198),(199),(200),(201),(202),(203),(204),(205),(206),(207),(208),(209),(210),(211),(212),(213),(214),(215),(216),(217),(218),(219),(220),(221),(222),(223),(224),(225),(226),(227),(228),(229),(230),(231),(232),(233),(234),(235),(236),(237),(238),(239),(240),(241),(242),(243);
/*!40000 ALTER TABLE `xe_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_session`
--

DROP TABLE IF EXISTS `xe_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_session` (
  `session_key` varchar(255) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `expired` varchar(14) DEFAULT NULL,
  `val` longtext,
  `ipaddress` varchar(128) NOT NULL,
  `last_update` varchar(14) DEFAULT NULL,
  `cur_mid` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`session_key`),
  KEY `idx_session_member_srl` (`member_srl`),
  KEY `idx_session_expired` (`expired`),
  KEY `idx_session_update` (`last_update`),
  KEY `idx_session_cur_mid` (`cur_mid`),
  KEY `idx_session_update_mid` (`member_srl`,`last_update`,`cur_mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_session`
--

LOCK TABLES `xe_session` WRITE;
/*!40000 ALTER TABLE `xe_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_site_admin`
--

DROP TABLE IF EXISTS `xe_site_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_site_admin` (
  `site_srl` bigint(11) NOT NULL,
  `member_srl` bigint(11) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  UNIQUE KEY `idx_site_admin` (`site_srl`,`member_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_site_admin`
--

LOCK TABLES `xe_site_admin` WRITE;
/*!40000 ALTER TABLE `xe_site_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_site_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_sites`
--

DROP TABLE IF EXISTS `xe_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_sites` (
  `site_srl` bigint(11) NOT NULL,
  `index_module_srl` bigint(11) DEFAULT '0',
  `domain` varchar(255) NOT NULL,
  `default_language` varchar(255) DEFAULT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`site_srl`),
  UNIQUE KEY `unique_domain` (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_sites`
--

LOCK TABLES `xe_sites` WRITE;
/*!40000 ALTER TABLE `xe_sites` DISABLE KEYS */;
INSERT INTO `xe_sites` VALUES (0,120,'http://dbproject.kmkyoung.com','ko','20141029003849');
/*!40000 ALTER TABLE `xe_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_spamfilter_denied_ip`
--

DROP TABLE IF EXISTS `xe_spamfilter_denied_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_spamfilter_denied_ip` (
  `ipaddress` varchar(250) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`ipaddress`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_spamfilter_denied_ip`
--

LOCK TABLES `xe_spamfilter_denied_ip` WRITE;
/*!40000 ALTER TABLE `xe_spamfilter_denied_ip` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_spamfilter_denied_ip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_spamfilter_denied_word`
--

DROP TABLE IF EXISTS `xe_spamfilter_denied_word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_spamfilter_denied_word` (
  `word` varchar(250) NOT NULL,
  `hit` bigint(20) NOT NULL DEFAULT '0',
  `latest_hit` varchar(14) DEFAULT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`word`),
  KEY `idx_hit` (`hit`),
  KEY `idx_latest_hit` (`latest_hit`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_spamfilter_denied_word`
--

LOCK TABLES `xe_spamfilter_denied_word` WRITE;
/*!40000 ALTER TABLE `xe_spamfilter_denied_word` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_spamfilter_denied_word` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_spamfilter_log`
--

DROP TABLE IF EXISTS `xe_spamfilter_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_spamfilter_log` (
  `spamfilter_log_srl` bigint(11) NOT NULL,
  `ipaddress` varchar(250) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`spamfilter_log_srl`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_spamfilter_log`
--

LOCK TABLES `xe_spamfilter_log` WRITE;
/*!40000 ALTER TABLE `xe_spamfilter_log` DISABLE KEYS */;
INSERT INTO `xe_spamfilter_log` VALUES (212,'119.192.238.165','20141120160335'),(216,'119.192.238.165','20141120161437');
/*!40000 ALTER TABLE `xe_spamfilter_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_syndication_except_modules`
--

DROP TABLE IF EXISTS `xe_syndication_except_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_syndication_except_modules` (
  `module_srl` bigint(11) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`module_srl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_syndication_except_modules`
--

LOCK TABLES `xe_syndication_except_modules` WRITE;
/*!40000 ALTER TABLE `xe_syndication_except_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_syndication_except_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_syndication_logs`
--

DROP TABLE IF EXISTS `xe_syndication_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_syndication_logs` (
  `log_srl` bigint(11) NOT NULL,
  `module_srl` bigint(11) DEFAULT '0',
  `document_srl` bigint(11) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  UNIQUE KEY `primary_key` (`log_srl`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_syndication_logs`
--

LOCK TABLES `xe_syndication_logs` WRITE;
/*!40000 ALTER TABLE `xe_syndication_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_syndication_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_tags`
--

DROP TABLE IF EXISTS `xe_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_tags` (
  `tag_srl` bigint(11) NOT NULL,
  `module_srl` bigint(11) NOT NULL DEFAULT '0',
  `document_srl` bigint(11) NOT NULL DEFAULT '0',
  `tag` varchar(240) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`tag_srl`),
  KEY `idx_module_srl` (`module_srl`),
  KEY `idx_document_srl` (`document_srl`),
  KEY `idx_regdate` (`regdate`),
  KEY `idx_tag` (`document_srl`,`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_tags`
--

LOCK TABLES `xe_tags` WRITE;
/*!40000 ALTER TABLE `xe_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe_trash`
--

DROP TABLE IF EXISTS `xe_trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe_trash` (
  `trash_srl` bigint(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `origin_module` varchar(250) NOT NULL DEFAULT 'document',
  `serialized_object` longtext NOT NULL,
  `description` text,
  `ipaddress` varchar(128) NOT NULL,
  `remover_srl` bigint(11) NOT NULL,
  `regdate` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`trash_srl`),
  KEY `idx_regdate` (`regdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe_trash`
--

LOCK TABLES `xe_trash` WRITE;
/*!40000 ALTER TABLE `xe_trash` DISABLE KEYS */;
/*!40000 ALTER TABLE `xe_trash` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-02  2:59:52
