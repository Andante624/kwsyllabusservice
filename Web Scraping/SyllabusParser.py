# -*- coding: utf-8-*

import re
import sys
import requests
import urllib2
import urllib


if len(sys.argv) < 3:
		sys.stderr.write("put the 2 argment (ex) 2014 2\n");
		sys.exit(1)

url = "http://info.kw.ac.kr/webnote/lecture/h_lecture.php?"

values = {'layout_opt' : 'N', 'mode': 'view', 'this_year' : sys.argv[1], 'hakgi' : sys.argv[2], 'fsel1':'_','fsel2' : '_', 'fsel3' :'' , 'hh' :'' ,'fsel1_code':'' , 'fsel1_str' :'' , 'fsel2_code' :'' , 'fsel2_str':'', 'sugang_opt':'all','refresh':'Y'}

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/7.0.6 Safari/537.78.2'

headers = { 'User-Agent' : user_agent, 'charset' : 'euc-kr'}

try:
	data = urllib.urlencode(values)
	req = urllib2.Request(url,data,headers)
	response = urllib2.urlopen(req)
	list_page = response.read()
except urllib2.HTTPError, error:
	print error.read()
	sys.exit(1)

output = open(sys.argv[1]+"-"+sys.argv[2]+".txt",'w')
result=re.findall(r'h_lecture01+\S+tmp__stu',list_page)


print "output : " + str(len(result))
for line in result:
		output.write("http://info.kw.ac.kr/webnote/lecture/"+line+"\n")

response.close()
output.close()
