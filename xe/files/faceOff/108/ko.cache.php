<?php if(!defined("__XE__")) exit(); $layout_info = new stdClass;
$layout_info->site_srl = "0";
$layout_info->layout = "simplestrap";
$layout_info->type = "";
$layout_info->path = "./layouts/simplestrap/";
$layout_info->title = "Simplestrap";
$layout_info->description = "";
$layout_info->version = "1.4.2";
$layout_info->date = "20140817";
$layout_info->homepage = "";
$layout_info->layout_srl = $layout_srl;
$layout_info->layout_title = $layout_title;
$layout_info->license = "";
$layout_info->license_link = "";
$layout_info->layout_type = "P";
$layout_info->author = array();
$layout_info->author[0] = new stdClass;
$layout_info->author[0]->name = "윈컴이";
$layout_info->author[0]->email_address = "wincomi@me.com";
$layout_info->author[0]->homepage = "http://www.wincomi.com/";
$layout_info->extra_var = new stdClass;
$layout_info->extra_var->premium = new stdClass;
$layout_info->extra_var->premium->group = "일반";
$layout_info->extra_var->premium->title = "Premium Pack";
$layout_info->extra_var->premium->type = "select";
$layout_info->extra_var->premium->value = $vars->premium;
$layout_info->extra_var->premium->description = "Simplestrap Premium Pack을 사용하시나요?";
$layout_info->extra_var->premium->options = array();
$layout_info->extra_var->premium->options["N"] = new stdClass;
$layout_info->extra_var->premium->options["N"]->val = "(기본) 아니요, 무료 버전을 사용합니다.";
$layout_info->extra_var->premium->options["Y"] = new stdClass;
$layout_info->extra_var->premium->options["Y"]->val = "예, Premium Pack을 사용하고, 라이센스 'Layout by Wincomi'를 제거하고 싶습니다.";
$layout_info->extra_var->premium_desk = new stdClass;
$layout_info->extra_var->premium_desk->group = "일반";
$layout_info->extra_var->premium_desk->title = "";
$layout_info->extra_var->premium_desk->type = "";
$layout_info->extra_var->premium_desk->value = $vars->premium_desk;
$layout_info->extra_var->premium_desk->description = "라이센스 'Layout by Wincomi'를 제거하기 위해, Simplestrap Premium Pack 파일에서 premium.html 파일을 ./layouts/simplestrap/custom 폴더에 업로드 해주세요. (custom 폴더가 없을 경우, 생성하시면 됩니다.) 자세히 : http://www.wincomi.com/store_manual/41796";
$layout_info->extra_var->logo_title = new stdClass;
$layout_info->extra_var->logo_title->group = "일반";
$layout_info->extra_var->logo_title->title = "홈페이지 이름";
$layout_info->extra_var->logo_title->type = "text";
$layout_info->extra_var->logo_title->value = $vars->logo_title;
$layout_info->extra_var->logo_title->description = "상단바에 표시될 홈페이지 이름을 입력하세요.";
$layout_info->extra_var->index_url = new stdClass;
$layout_info->extra_var->index_url->group = "일반";
$layout_info->extra_var->index_url->title = "홈페이지 주소";
$layout_info->extra_var->index_url->type = "text";
$layout_info->extra_var->index_url->value = $vars->index_url;
$layout_info->extra_var->index_url->description = "홈페이지 이름을 클릭 시 이동 할 URL을 입력하세요.";
$layout_info->extra_var->logo_img = new stdClass;
$layout_info->extra_var->logo_img->group = "일반";
$layout_info->extra_var->logo_img->title = "홈페이지 이미지";
$layout_info->extra_var->logo_img->type = "image";
$layout_info->extra_var->logo_img->value = $vars->logo_img;
$layout_info->extra_var->logo_img->description = "(옵션) 홈페이지 이름 대신 표시될 이미지를 올려주세요. (최대 높이 50px)";
$layout_info->extra_var->colorset = new stdClass;
$layout_info->extra_var->colorset->group = "디자인";
$layout_info->extra_var->colorset->title = "컬러셋";
$layout_info->extra_var->colorset->type = "select";
$layout_info->extra_var->colorset->value = $vars->colorset;
$layout_info->extra_var->colorset->description = "메인 점보트론, 버튼 등의 색상을 정할 수 있습니다.";
$layout_info->extra_var->colorset->options = array();
$layout_info->extra_var->colorset->options["info"] = new stdClass;
$layout_info->extra_var->colorset->options["info"]->val = "(기본) 하늘색";
$layout_info->extra_var->colorset->options["primary"] = new stdClass;
$layout_info->extra_var->colorset->options["primary"]->val = "파랑색";
$layout_info->extra_var->colorset->options["success"] = new stdClass;
$layout_info->extra_var->colorset->options["success"]->val = "초록색";
$layout_info->extra_var->colorset->options["warning"] = new stdClass;
$layout_info->extra_var->colorset->options["warning"]->val = "노란색";
$layout_info->extra_var->colorset->options["danger"] = new stdClass;
$layout_info->extra_var->colorset->options["danger"]->val = "빨간색";
$layout_info->extra_var->border_radius = new stdClass;
$layout_info->extra_var->border_radius->group = "디자인";
$layout_info->extra_var->border_radius->title = "둥근 모서리";
$layout_info->extra_var->border_radius->type = "select";
$layout_info->extra_var->border_radius->value = $vars->border_radius;
$layout_info->extra_var->border_radius->description = "버튼 등에 둥근 모서리를 사용합니다.";
$layout_info->extra_var->border_radius->options = array();
$layout_info->extra_var->border_radius->options["Y"] = new stdClass;
$layout_info->extra_var->border_radius->options["Y"]->val = "(기본) 예";
$layout_info->extra_var->border_radius->options["N"] = new stdClass;
$layout_info->extra_var->border_radius->options["N"]->val = "아니요";
$layout_info->extra_var->box_shadow = new stdClass;
$layout_info->extra_var->box_shadow->group = "디자인";
$layout_info->extra_var->box_shadow->title = "그림자";
$layout_info->extra_var->box_shadow->type = "select";
$layout_info->extra_var->box_shadow->value = $vars->box_shadow;
$layout_info->extra_var->box_shadow->description = "사이드바 패널 등에 그림자를 사용합니다.";
$layout_info->extra_var->box_shadow->options = array();
$layout_info->extra_var->box_shadow->options["Y"] = new stdClass;
$layout_info->extra_var->box_shadow->options["Y"]->val = "(기본) 예";
$layout_info->extra_var->box_shadow->options["N"] = new stdClass;
$layout_info->extra_var->box_shadow->options["N"]->val = "아니요";
$layout_info->extra_var->bootstrap2_design = new stdClass;
$layout_info->extra_var->bootstrap2_design->group = "디자인";
$layout_info->extra_var->bootstrap2_design->title = "Bootstrap 2";
$layout_info->extra_var->bootstrap2_design->type = "select";
$layout_info->extra_var->bootstrap2_design->value = $vars->bootstrap2_design;
$layout_info->extra_var->bootstrap2_design->description = "The Bootstrap 레이아웃과 비슷한 디자인을 적용합니다.";
$layout_info->extra_var->bootstrap2_design->options = array();
$layout_info->extra_var->bootstrap2_design->options["N"] = new stdClass;
$layout_info->extra_var->bootstrap2_design->options["N"]->val = "(기본) 아니요";
$layout_info->extra_var->bootstrap2_design->options["Y"] = new stdClass;
$layout_info->extra_var->bootstrap2_design->options["Y"]->val = "예";
$layout_info->extra_var->site_frame = new stdClass;
$layout_info->extra_var->site_frame->group = "형태";
$layout_info->extra_var->site_frame->title = "프레임 형태";
$layout_info->extra_var->site_frame->type = "select";
$layout_info->extra_var->site_frame->value = $vars->site_frame;
$layout_info->extra_var->site_frame->description = "사이드바 위치를 정하세요.";
$layout_info->extra_var->site_frame->options = array();
$layout_info->extra_var->site_frame->options["sidebar_content"] = new stdClass;
$layout_info->extra_var->site_frame->options["sidebar_content"]->val = "(기본) 사이드바 + 컨텐츠";
$layout_info->extra_var->site_frame->options["content_sidebar"] = new stdClass;
$layout_info->extra_var->site_frame->options["content_sidebar"]->val = "컨텐츠 + 사이드바";
$layout_info->extra_var->site_frame->options["content"] = new stdClass;
$layout_info->extra_var->site_frame->options["content"]->val = "컨텐츠";
$layout_info->extra_var->site_frame_content = new stdClass;
$layout_info->extra_var->site_frame_content->group = "형태";
$layout_info->extra_var->site_frame_content->title = "'컨텐츠' 프레임 고정";
$layout_info->extra_var->site_frame_content->type = "text";
$layout_info->extra_var->site_frame_content->value = $vars->site_frame_content;
$layout_info->extra_var->site_frame_content->description = "'컨텐츠' 프래임으로 고정할 모듈을 입력하세요. 보통 메인 모듈을 사용합니다. 쉼표로 구분합니다. 예) home,main,content";
$layout_info->extra_var->navbar_fixed = new stdClass;
$layout_info->extra_var->navbar_fixed->group = "상단바";
$layout_info->extra_var->navbar_fixed->title = "상단바 고정";
$layout_info->extra_var->navbar_fixed->type = "select";
$layout_info->extra_var->navbar_fixed->value = $vars->navbar_fixed;
$layout_info->extra_var->navbar_fixed->description = "상단바를 스크롤을 내려도 고정 시킵니다.";
$layout_info->extra_var->navbar_fixed->options = array();
$layout_info->extra_var->navbar_fixed->options["Y"] = new stdClass;
$layout_info->extra_var->navbar_fixed->options["Y"]->val = "(기본) 예";
$layout_info->extra_var->navbar_fixed->options["N"] = new stdClass;
$layout_info->extra_var->navbar_fixed->options["N"]->val = "아니요";
$layout_info->extra_var->navbar_color = new stdClass;
$layout_info->extra_var->navbar_color->group = "상단바";
$layout_info->extra_var->navbar_color->title = "상단바 색상";
$layout_info->extra_var->navbar_color->type = "select";
$layout_info->extra_var->navbar_color->value = $vars->navbar_color;
$layout_info->extra_var->navbar_color->description = "'검정'을 선택 할 시 상단 바에 navbar-inverse를 추가합니다.";
$layout_info->extra_var->navbar_color->options = array();
$layout_info->extra_var->navbar_color->options[""] = new stdClass;
$layout_info->extra_var->navbar_color->options[""]->val = "흰색";
$layout_info->extra_var->navbar_color->options["inverse"] = new stdClass;
$layout_info->extra_var->navbar_color->options["inverse"]->val = "검정 (반전)";
$layout_info->extra_var->navbar_search = new stdClass;
$layout_info->extra_var->navbar_search->group = "상단바";
$layout_info->extra_var->navbar_search->title = "검색";
$layout_info->extra_var->navbar_search->type = "select";
$layout_info->extra_var->navbar_search->value = $vars->navbar_search;
$layout_info->extra_var->navbar_search->description = "상단바에 통합 검색을 추가합니다.";
$layout_info->extra_var->navbar_search->options = array();
$layout_info->extra_var->navbar_search->options["Y"] = new stdClass;
$layout_info->extra_var->navbar_search->options["Y"]->val = "예";
$layout_info->extra_var->navbar_search->options["N"] = new stdClass;
$layout_info->extra_var->navbar_search->options["N"]->val = "아니요";
$layout_info->extra_var->socialxe_login = new stdClass;
$layout_info->extra_var->socialxe_login->group = "상단바";
$layout_info->extra_var->socialxe_login->title = "소셜 로그인";
$layout_info->extra_var->socialxe_login->type = "select";
$layout_info->extra_var->socialxe_login->value = $vars->socialxe_login;
$layout_info->extra_var->socialxe_login->description = "메뉴에 소셜 로그인을 추가합니다. * 난다날아님의 SocialXE 모듈이 필요합니다.";
$layout_info->extra_var->socialxe_login->options = array();
$layout_info->extra_var->socialxe_login->options["N"] = new stdClass;
$layout_info->extra_var->socialxe_login->options["N"]->val = "아니요";
$layout_info->extra_var->socialxe_login->options["Y"] = new stdClass;
$layout_info->extra_var->socialxe_login->options["Y"]->val = "예";
$layout_info->extra_var->navbar_member_point = new stdClass;
$layout_info->extra_var->navbar_member_point->group = "상단바";
$layout_info->extra_var->navbar_member_point->title = "회원 포인트";
$layout_info->extra_var->navbar_member_point->type = "select";
$layout_info->extra_var->navbar_member_point->value = $vars->navbar_member_point;
$layout_info->extra_var->navbar_member_point->description = "회원 메뉴에 회원 포인트, 캐쉬를 표시합니다. 캐쉬를 표시하기 위해 캐쉬 시스템 모듈이 필요합니다.";
$layout_info->extra_var->navbar_member_point->options = array();
$layout_info->extra_var->navbar_member_point->options["Y"] = new stdClass;
$layout_info->extra_var->navbar_member_point->options["Y"]->val = "포인트만 표시";
$layout_info->extra_var->navbar_member_point->options["Y2"] = new stdClass;
$layout_info->extra_var->navbar_member_point->options["Y2"]->val = "포인트, 캐쉬 표시";
$layout_info->extra_var->navbar_member_point->options["N"] = new stdClass;
$layout_info->extra_var->navbar_member_point->options["N"]->val = "표시 안함";
$layout_info->extra_var->navbar_sm_dropdown = new stdClass;
$layout_info->extra_var->navbar_sm_dropdown->group = "상단바";
$layout_info->extra_var->navbar_sm_dropdown->title = "[모바일] 모든 메뉴 표시";
$layout_info->extra_var->navbar_sm_dropdown->type = "select";
$layout_info->extra_var->navbar_sm_dropdown->value = $vars->navbar_sm_dropdown;
$layout_info->extra_var->navbar_sm_dropdown->description = "[BETA] 모바일에서 상단바의 드랍다운 메뉴를 모두 보여줍니다.";
$layout_info->extra_var->navbar_sm_dropdown->options = array();
$layout_info->extra_var->navbar_sm_dropdown->options["N"] = new stdClass;
$layout_info->extra_var->navbar_sm_dropdown->options["N"]->val = "아니요";
$layout_info->extra_var->navbar_sm_dropdown->options["Y"] = new stdClass;
$layout_info->extra_var->navbar_sm_dropdown->options["Y"]->val = "예";
$layout_info->extra_var->sb_desc = new stdClass;
$layout_info->extra_var->sb_desc->group = "사이드바";
$layout_info->extra_var->sb_desc->title = "사이드바";
$layout_info->extra_var->sb_desc->type = "";
$layout_info->extra_var->sb_desc->value = $vars->sb_desc;
$layout_info->extra_var->sb_desc->description = "형태를 '사이드바 + 컨텐트' 혹은 '컨텐트 + 사이드바'로  설정해야 사용이 가능합니다.";
$layout_info->extra_var->sb_col = new stdClass;
$layout_info->extra_var->sb_col->group = "사이드바";
$layout_info->extra_var->sb_col->title = "사이드바 너비";
$layout_info->extra_var->sb_col->type = "select";
$layout_info->extra_var->sb_col->value = $vars->sb_col;
$layout_info->extra_var->sb_col->description = "사이드바의 너비를 조정합니다.";
$layout_info->extra_var->sb_col->options = array();
$layout_info->extra_var->sb_col->options["2"] = new stdClass;
$layout_info->extra_var->sb_col->options["2"]->val = "(사이드바) 2 : (컨텐트) 10";
$layout_info->extra_var->sb_col->options["3"] = new stdClass;
$layout_info->extra_var->sb_col->options["3"]->val = "(사이드바) 3 : (컨텐트) 9";
$layout_info->extra_var->sb_submenu = new stdClass;
$layout_info->extra_var->sb_submenu->group = "사이드바";
$layout_info->extra_var->sb_submenu->title = "메뉴 출력";
$layout_info->extra_var->sb_submenu->type = "select";
$layout_info->extra_var->sb_submenu->value = $vars->sb_submenu;
$layout_info->extra_var->sb_submenu->description = "현재 위치의 메뉴를 표시합니다.";
$layout_info->extra_var->sb_submenu->options = array();
$layout_info->extra_var->sb_submenu->options["Y"] = new stdClass;
$layout_info->extra_var->sb_submenu->options["Y"]->val = "(기본) 예";
$layout_info->extra_var->sb_submenu->options["N"] = new stdClass;
$layout_info->extra_var->sb_submenu->options["N"]->val = "아니요";
$layout_info->extra_var->sb_post = new stdClass;
$layout_info->extra_var->sb_post->group = "사이드바";
$layout_info->extra_var->sb_post->title = "최신 글 출력";
$layout_info->extra_var->sb_post->type = "select";
$layout_info->extra_var->sb_post->value = $vars->sb_post;
$layout_info->extra_var->sb_post->description = "사이드바에 최신 글을 출력합니다.";
$layout_info->extra_var->sb_post->options = array();
$layout_info->extra_var->sb_post->options["Y"] = new stdClass;
$layout_info->extra_var->sb_post->options["Y"]->val = "예";
$layout_info->extra_var->sb_post->options["N"] = new stdClass;
$layout_info->extra_var->sb_post->options["N"]->val = "아니요";
$layout_info->extra_var->sb_post_count = new stdClass;
$layout_info->extra_var->sb_post_count->group = "사이드바";
$layout_info->extra_var->sb_post_count->title = "최신 글 출력 갯수";
$layout_info->extra_var->sb_post_count->type = "text";
$layout_info->extra_var->sb_post_count->value = $vars->sb_post_count;
$layout_info->extra_var->sb_post_count->description = "최신 글 리스트를 출력할 갯수를 정해주세요. (기본 5개)";
$layout_info->extra_var->sb_post_module = new stdClass;
$layout_info->extra_var->sb_post_module->group = "사이드바";
$layout_info->extra_var->sb_post_module->title = "최신 글 출력 모듈 번호";
$layout_info->extra_var->sb_post_module->type = "text";
$layout_info->extra_var->sb_post_module->value = $vars->sb_post_module;
$layout_info->extra_var->sb_post_module->description = "최신 글을 출력할 모듈 번호를 입력합니다. 비워둘 경우 모든 게시물이 출력됩니다. (쉼표로 구분합니다.)";
$layout_info->extra_var->sb_comm = new stdClass;
$layout_info->extra_var->sb_comm->group = "사이드바";
$layout_info->extra_var->sb_comm->title = "최신 댓글 출력";
$layout_info->extra_var->sb_comm->type = "select";
$layout_info->extra_var->sb_comm->value = $vars->sb_comm;
$layout_info->extra_var->sb_comm->description = "사이드바에 최신 댓글을 출력합니다.";
$layout_info->extra_var->sb_comm->options = array();
$layout_info->extra_var->sb_comm->options["Y"] = new stdClass;
$layout_info->extra_var->sb_comm->options["Y"]->val = "예";
$layout_info->extra_var->sb_comm->options["N"] = new stdClass;
$layout_info->extra_var->sb_comm->options["N"]->val = "아니요";
$layout_info->extra_var->sb_comm_count = new stdClass;
$layout_info->extra_var->sb_comm_count->group = "사이드바";
$layout_info->extra_var->sb_comm_count->title = "최신 댓글 출력 갯수";
$layout_info->extra_var->sb_comm_count->type = "text";
$layout_info->extra_var->sb_comm_count->value = $vars->sb_comm_count;
$layout_info->extra_var->sb_comm_count->description = "최신 댓글 리스트를 출력할 갯수를 정해주세요. (기본 5개)";
$layout_info->extra_var->sb_comm_module = new stdClass;
$layout_info->extra_var->sb_comm_module->group = "사이드바";
$layout_info->extra_var->sb_comm_module->title = "최신 댓글 출력 모듈 번호";
$layout_info->extra_var->sb_comm_module->type = "text";
$layout_info->extra_var->sb_comm_module->value = $vars->sb_comm_module;
$layout_info->extra_var->sb_comm_module->description = "최신 댓글을 출력할 모듈 번호를 입력합니다. 비워둘 경우 모든 댓글이 출력됩니다. (쉼표로 구분합니다.)";
$layout_info->extra_var->sb_title_icon = new stdClass;
$layout_info->extra_var->sb_title_icon->group = "사이드바";
$layout_info->extra_var->sb_title_icon->title = "제목 아이콘";
$layout_info->extra_var->sb_title_icon->type = "select";
$layout_info->extra_var->sb_title_icon->value = $vars->sb_title_icon;
$layout_info->extra_var->sb_title_icon->description = "Recent Post 등 앞에 아이콘을 표시합니다.";
$layout_info->extra_var->sb_title_icon->options = array();
$layout_info->extra_var->sb_title_icon->options["Y"] = new stdClass;
$layout_info->extra_var->sb_title_icon->options["Y"]->val = "예";
$layout_info->extra_var->sb_title_icon->options["N"] = new stdClass;
$layout_info->extra_var->sb_title_icon->options["N"]->val = "아니요";
$layout_info->extra_var->sb_widget1 = new stdClass;
$layout_info->extra_var->sb_widget1->group = "사이드바";
$layout_info->extra_var->sb_widget1->title = "사용자 지정 위젯 1";
$layout_info->extra_var->sb_widget1->type = "textarea";
$layout_info->extra_var->sb_widget1->value = $vars->sb_widget1;
$layout_info->extra_var->sb_widget1->description = "사이드바 나올 사용자 지정 위젯을 넣으세요. (HTML 사용 가능) (더 많은 위젯을 이용하고 싶으실 경우, custom_sidebar_top.html 혹은 custom_sidebar_bottom.html 를 사용하세요.)";
$layout_info->extra_var->sb_widget2 = new stdClass;
$layout_info->extra_var->sb_widget2->group = "사이드바";
$layout_info->extra_var->sb_widget2->title = "사용자 지정 위젯 2";
$layout_info->extra_var->sb_widget2->type = "textarea";
$layout_info->extra_var->sb_widget2->value = $vars->sb_widget2;
$layout_info->extra_var->sb_widget2->description = "";
$layout_info->extra_var->sb_widget3 = new stdClass;
$layout_info->extra_var->sb_widget3->group = "사이드바";
$layout_info->extra_var->sb_widget3->title = "사용자 지정 위젯 3";
$layout_info->extra_var->sb_widget3->type = "textarea";
$layout_info->extra_var->sb_widget3->value = $vars->sb_widget3;
$layout_info->extra_var->sb_widget3->description = "";
$layout_info->extra_var->jumbotron = new stdClass;
$layout_info->extra_var->jumbotron->group = "점보트론";
$layout_info->extra_var->jumbotron->title = "점보트론 사용";
$layout_info->extra_var->jumbotron->type = "select";
$layout_info->extra_var->jumbotron->value = $vars->jumbotron;
$layout_info->extra_var->jumbotron->description = "";
$layout_info->extra_var->jumbotron->options = array();
$layout_info->extra_var->jumbotron->options["Y"] = new stdClass;
$layout_info->extra_var->jumbotron->options["Y"]->val = "예";
$layout_info->extra_var->jumbotron->options["N"] = new stdClass;
$layout_info->extra_var->jumbotron->options["N"]->val = "아니요";
$layout_info->extra_var->jumbotron_hide_mid = new stdClass;
$layout_info->extra_var->jumbotron_hide_mid->group = "점보트론";
$layout_info->extra_var->jumbotron_hide_mid->title = "점보트론 숨기기";
$layout_info->extra_var->jumbotron_hide_mid->type = "text";
$layout_info->extra_var->jumbotron_hide_mid->value = $vars->jumbotron_hide_mid;
$layout_info->extra_var->jumbotron_hide_mid->description = "점보트론을 특정 페이지 (모듈)에서 숨길 경우 사용하는 기능입니다. 모듈 ID를 입력해주세요. (쉼표로 구분합니다.)";
$layout_info->extra_var->jumbotron_align = new stdClass;
$layout_info->extra_var->jumbotron_align->group = "점보트론";
$layout_info->extra_var->jumbotron_align->title = "점보트론 글자 정렬";
$layout_info->extra_var->jumbotron_align->type = "select";
$layout_info->extra_var->jumbotron_align->value = $vars->jumbotron_align;
$layout_info->extra_var->jumbotron_align->description = "";
$layout_info->extra_var->jumbotron_align->options = array();
$layout_info->extra_var->jumbotron_align->options["center"] = new stdClass;
$layout_info->extra_var->jumbotron_align->options["center"]->val = "가운데 정렬";
$layout_info->extra_var->jumbotron_align->options["left"] = new stdClass;
$layout_info->extra_var->jumbotron_align->options["left"]->val = "왼쪽 정렬";
$layout_info->extra_var->jumbotron_align->options["right"] = new stdClass;
$layout_info->extra_var->jumbotron_align->options["right"]->val = "오른쪽 정렬";
$layout_info->extra_var->footer_copyright = new stdClass;
$layout_info->extra_var->footer_copyright->group = "푸터";
$layout_info->extra_var->footer_copyright->title = "Coypright";
$layout_info->extra_var->footer_copyright->type = "textarea";
$layout_info->extra_var->footer_copyright->value = $vars->footer_copyright;
$layout_info->extra_var->footer_copyright->description = "하단 메뉴 아래에 나올 저작권 내용 등을 적어주세요. (HTML 사용 가능)";
$layout_info->extra_var->footer_bottom_menu = new stdClass;
$layout_info->extra_var->footer_bottom_menu->group = "푸터";
$layout_info->extra_var->footer_bottom_menu->title = "푸터 메뉴";
$layout_info->extra_var->footer_bottom_menu->type = "select";
$layout_info->extra_var->footer_bottom_menu->value = $vars->footer_bottom_menu;
$layout_info->extra_var->footer_bottom_menu->description = "푸터 메뉴를 사용합니다. 아래 '푸터 메뉴'에서 사이트맵을 선택하세요.";
$layout_info->extra_var->footer_bottom_menu->options = array();
$layout_info->extra_var->footer_bottom_menu->options["N"] = new stdClass;
$layout_info->extra_var->footer_bottom_menu->options["N"]->val = "아니요";
$layout_info->extra_var->footer_bottom_menu->options["Y"] = new stdClass;
$layout_info->extra_var->footer_bottom_menu->options["Y"]->val = "예";
$layout_info->extra_var->footer_lang = new stdClass;
$layout_info->extra_var->footer_lang->group = "푸터";
$layout_info->extra_var->footer_lang->title = "언어 변경 표시";
$layout_info->extra_var->footer_lang->type = "select";
$layout_info->extra_var->footer_lang->value = $vars->footer_lang;
$layout_info->extra_var->footer_lang->description = "푸터에 언어 변경 버튼을 보여줍니다.";
$layout_info->extra_var->footer_lang->options = array();
$layout_info->extra_var->footer_lang->options["N"] = new stdClass;
$layout_info->extra_var->footer_lang->options["N"]->val = "아니요";
$layout_info->extra_var->footer_lang->options["Y"] = new stdClass;
$layout_info->extra_var->footer_lang->options["Y"]->val = "예";
$layout_info->extra_var->footer_bottom = new stdClass;
$layout_info->extra_var->footer_bottom->group = "푸터";
$layout_info->extra_var->footer_bottom->title = "푸터 하단";
$layout_info->extra_var->footer_bottom->type = "textarea";
$layout_info->extra_var->footer_bottom->value = $vars->footer_bottom;
$layout_info->extra_var->footer_bottom->description = "푸터 최하단에 나올 내용을 적어주세요. (HTML 사용 가능)";
$layout_info->extra_var->fontawesome = new stdClass;
$layout_info->extra_var->fontawesome->group = "기타";
$layout_info->extra_var->fontawesome->title = "Font Awesome";
$layout_info->extra_var->fontawesome->type = "select";
$layout_info->extra_var->fontawesome->value = $vars->fontawesome;
$layout_info->extra_var->fontawesome->description = "Font Awesome 버전을 선택하세요.";
$layout_info->extra_var->fontawesome->options = array();
$layout_info->extra_var->fontawesome->options["f3_only"] = new stdClass;
$layout_info->extra_var->fontawesome->options["f3_only"]->val = "3.2.1";
$layout_info->extra_var->fontawesome->options["f4_only"] = new stdClass;
$layout_info->extra_var->fontawesome->options["f4_only"]->val = "4";
$layout_info->extra_var->fontawesome->options["f3_f4"] = new stdClass;
$layout_info->extra_var->fontawesome->options["f3_f4"]->val = "3.2.1 + 4 (권장 안 함)";
$layout_info->extra_var->fontface = new stdClass;
$layout_info->extra_var->fontface->group = "기타";
$layout_info->extra_var->fontface->title = "웹 폰트";
$layout_info->extra_var->fontface->type = "select";
$layout_info->extra_var->fontface->value = $vars->fontface;
$layout_info->extra_var->fontface->description = "fontface.kr에서 지원하는 웹 폰트를 사용합니다.";
$layout_info->extra_var->fontface->options = array();
$layout_info->extra_var->fontface->options["N"] = new stdClass;
$layout_info->extra_var->fontface->options["N"]->val = "사용 안함";
$layout_info->extra_var->fontface->options["NanumGothic"] = new stdClass;
$layout_info->extra_var->fontface->options["NanumGothic"]->val = "나눔고딕 (추천)";
$layout_info->extra_var->fontface->options["NanumGothicBold"] = new stdClass;
$layout_info->extra_var->fontface->options["NanumGothicBold"]->val = "나눔고딕 (굵게)";
$layout_info->extra_var->fontface->options["NanumMyeongjo"] = new stdClass;
$layout_info->extra_var->fontface->options["NanumMyeongjo"]->val = "나눔명조";
$layout_info->extra_var->fontface->options["NanumMyeongjoBold"] = new stdClass;
$layout_info->extra_var->fontface->options["NanumMyeongjoBold"]->val = "나눔명조 (굵게)";
$layout_info->extra_var->custom = new stdClass;
$layout_info->extra_var->custom->group = "커스텀 / 고급";
$layout_info->extra_var->custom->title = "커스텀";
$layout_info->extra_var->custom->type = "checkbox";
$layout_info->extra_var->custom->value = $vars->custom;
$layout_info->extra_var->custom->description = "커스텀 내용을 추가하시려면 원하는 것을 체크 한 후 'layouts/simplestrap/custom/???' 경로에 파일을 넣으시면 됩니다.";
$layout_info->extra_var->custom->options = array();
$layout_info->extra_var->custom->options["custom_style"] = new stdClass;
$layout_info->extra_var->custom->options["custom_style"]->val = "custom_style.css";
$layout_info->extra_var->custom->options["custom_js"] = new stdClass;
$layout_info->extra_var->custom->options["custom_js"]->val = "custom_js.js";
$layout_info->extra_var->custom->options["custom_top"] = new stdClass;
$layout_info->extra_var->custom->options["custom_top"]->val = "custom_top.html (페이지 상단)";
$layout_info->extra_var->custom->options["custom_bottom"] = new stdClass;
$layout_info->extra_var->custom->options["custom_bottom"]->val = "custom_bottom.html (페이지 하단)";
$layout_info->extra_var->custom->options["custom_content_top"] = new stdClass;
$layout_info->extra_var->custom->options["custom_content_top"]->val = "custom_content_top.html (컨텐츠 상단)";
$layout_info->extra_var->custom->options["custom_content_bottom"] = new stdClass;
$layout_info->extra_var->custom->options["custom_content_bottom"]->val = "custom_content_bottom.html (컨텐츠 하단)";
$layout_info->extra_var->custom->options["custom_sidebar_top"] = new stdClass;
$layout_info->extra_var->custom->options["custom_sidebar_top"]->val = "custom_sidebar_top.html (사이드바 상단/더 많은 위젯 추가)";
$layout_info->extra_var->custom->options["custom_sidebar_bottom"] = new stdClass;
$layout_info->extra_var->custom->options["custom_sidebar_bottom"]->val = "custom_sidebar_bottom.html (사이드바 하단/더 많은 위젯 추가)";
$layout_info->extra_var->custom->options["custom_jumbotron"] = new stdClass;
$layout_info->extra_var->custom->options["custom_jumbotron"]->val = "custom_jumbotron.html (점보트론 변경)";
$layout_info->extra_var->custom->options["custom_setting"] = new stdClass;
$layout_info->extra_var->custom->options["custom_setting"]->val = "custom_setting.html";
$layout_info->extra_var->css3pie = new stdClass;
$layout_info->extra_var->css3pie->group = "커스텀 / 고급";
$layout_info->extra_var->css3pie->title = "CSS3PIE";
$layout_info->extra_var->css3pie->type = "select";
$layout_info->extra_var->css3pie->value = $vars->css3pie;
$layout_info->extra_var->css3pie->description = "IE에서 둥근 모서리를 사용하게 해주는 CSS3PIE (2.0 beta1)를 사용합니다.";
$layout_info->extra_var->css3pie->options = array();
$layout_info->extra_var->css3pie->options["Y"] = new stdClass;
$layout_info->extra_var->css3pie->options["Y"]->val = "예";
$layout_info->extra_var->css3pie->options["N"] = new stdClass;
$layout_info->extra_var->css3pie->options["N"]->val = "아니요";
$layout_info->extra_var->code_prettify = new stdClass;
$layout_info->extra_var->code_prettify->group = "커스텀 / 고급";
$layout_info->extra_var->code_prettify->title = "Code prettify";
$layout_info->extra_var->code_prettify->type = "select";
$layout_info->extra_var->code_prettify->value = $vars->code_prettify;
$layout_info->extra_var->code_prettify->description = "Code Prettify (코드 강조) 기능을 사용합니다.";
$layout_info->extra_var->code_prettify->options = array();
$layout_info->extra_var->code_prettify->options["N"] = new stdClass;
$layout_info->extra_var->code_prettify->options["N"]->val = "아니요";
$layout_info->extra_var->code_prettify->options["Y"] = new stdClass;
$layout_info->extra_var->code_prettify->options["Y"]->val = "예";
$layout_info->extra_var->xe_css_remove = new stdClass;
$layout_info->extra_var->xe_css_remove->group = "커스텀 / 고급";
$layout_info->extra_var->xe_css_remove->title = "XE CSS 제거";
$layout_info->extra_var->xe_css_remove->type = "select";
$layout_info->extra_var->xe_css_remove->value = $vars->xe_css_remove;
$layout_info->extra_var->xe_css_remove->description = "
					xe.min.css 혹은 xe.css를 제거하는 방법을 선택하세요. (선택하실 때 주의가 필요합니다.)
					
				";
$layout_info->extra_var->xe_css_remove->options = array();
$layout_info->extra_var->xe_css_remove->options["script"] = new stdClass;
$layout_info->extra_var->xe_css_remove->options["script"]->val = "(기본) 스크립트";
$layout_info->extra_var->xe_css_remove->options["N"] = new stdClass;
$layout_info->extra_var->xe_css_remove->options["N"]->val = "제거 안 함 (주의 - 디자인 문제 발생)";
$layout_info->extra_var_count = "43";
$layout_info->menu_count = "2";
$layout_info->menu = new stdClass;
$layout_info->default_menu = "main_menu";
$layout_info->menu->main_menu = new stdClass;
$layout_info->menu->main_menu->name = "main_menu";
$layout_info->menu->main_menu->title = "메인 메뉴";
$layout_info->menu->main_menu->maxdepth = "3";
$layout_info->menu->main_menu->menu_srl = $vars->main_menu;
$layout_info->menu->main_menu->xml_file = "./files/cache/menu/".$vars->main_menu.".xml.php";
$layout_info->menu->main_menu->php_file = "./files/cache/menu/".$vars->main_menu.".php";
$layout_info->menu->footer_menu = new stdClass;
$layout_info->menu->footer_menu->name = "footer_menu";
$layout_info->menu->footer_menu->title = "푸터 메뉴";
$layout_info->menu->footer_menu->maxdepth = "1";
$layout_info->menu->footer_menu->menu_srl = $vars->footer_menu;
$layout_info->menu->footer_menu->xml_file = "./files/cache/menu/".$vars->footer_menu.".xml.php";
$layout_info->menu->footer_menu->php_file = "./files/cache/menu/".$vars->footer_menu.".php";