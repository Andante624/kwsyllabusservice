var w = 400,
	h = 400;

var colorscale = d3.scale.category10();
var Lecture_Time = document.getElementById("Lecture_Time").value;
var Lecture_Quality = document.getElementById("Lecture_Quality").value;
var Lecture_Recommended = document.getElementById("Lecture_Recommended").value;
var Homework_Amount = document.getElementById("Homework_Amount").value;
var Exam_Difficulty = document.getElementById("Exam_Difficulty").value;
var d = [[
			{axis:"강의 시간",value:Lecture_Time},
			{axis:"강의 품질",value:Lecture_Quality},
			{axis:"강의 추천",value:Lecture_Recommended},
			{axis:"과제 횟수",value:Homework_Amount},
			{axis:"시험 난이도",value:Exam_Difficulty}]
		];

var mycfg = {
  w: w,
  h: h,
  maxValue: 5,
  levels: 5,
  ExtraWidthX: 400
}

RadarChart.draw("#chart", d, mycfg);

var svg = d3.select('#body').append("center")
	.selectAll('svg')
	.append('svg')
	.attr("width", h)
	.attr("height", h)
