function changeColor(object, oldColor, newColor) 
{
	object.style.backgroundColor = newColor;
	object.style.cursor = "hand";
	object.onmouseout = function()
	{
		object.style.backgroundColor = oldColor;
		object.style.cursor = "pointer";
	}
}

function clickRowEvent(object)
{
	document.location.href = "index.php/?mid=detail_list&id="+object.id;
}

