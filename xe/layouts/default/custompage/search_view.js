var group7000 = new Array('7000:전정대 공통','7060:전자 공학과','7070:전기 통신 공학과','7420:전자 융합 공학과','7220:컴퓨터 공학과','7260:컴퓨터 소프트웨어학과','7320:전기 공학과','7340:전자 재료 공학과','7410:로봇 학부');
var group1000 = new Array('1000:공과대 공통','1140:화학 공학과','1160:환경 공학과','1170:건축 공학과','1270:건축 학과');
var group6000 = new Array('6000:자연대 공통','6030:수학과','6050:화학과','6100:전자 바이오 물리 학과','6060:전자 물리 학과','6090:생활 체육 학과','6110:사이버 정보 보안 학과');
var group4000 = new Array('4050:법학부','4090:부동산 법무 학과');
var group5000 = new Array('5080:경영 학부');
var group9000 = new Array('9030:행정 학과','9050:산업 심리 학과','9060:미디어 영상 학부');
var groupE000 = new Array('E020:국어 국문 학과','E030:영어 영문 학과');
var groupD000 = new Array('D000:동북아 공통','D020:동북아 통상 학부','D060:국제 통상 학과','D030:동북아 문화 산업 학부','D161:국제학부 국제 지역 전공','D040:국제 협력 학부','D162:국제학부 글로벌 코리아 전공');

function submit_check()
{
		if(document.getElementById("Day1").value != '' && document.getElementById("Time1").value=='')
		{
				alert("강의 시간은 요일만 선택할 수 없습니다.");
				return false;
		}
		if(document.getElementById("Day2").value != '' && document.getElementById("Time2").value=='')
		{
				alert("강의 시간은 요일만 선택할 수 없습니다.");
				return false;
		}
	
	return true;
}

function changeDay1()
{
	var time1 = document.getElementById("Time1");
	time1.selectedIndex=0;
}

function changeDay2()
{
	var time2 = document.getElementById("Time2");
	time2.selectedIndex=0;
}

function changeCollege()
{
	var collegeSelector = document.getElementById("College");
	var selectedCollege = collegeSelector.value;
	var majorSelector = document.getElementById("Major");

	majorSelector.options.length=0;
	majorSelector.options[0]=new Option('전체','',true,true);
	
	switch(selectedCollege)
	{
			case '':
				setAllList();
			case '7':
				setList(majorSelector,group7000);
				break;
			case '1':
				setList(majorSelector,group1000);
				break;
			case '6':
				setList(majorSelector,group6000);
				break;
			case '4':
				setList(majorSelector,group4000);
				break;
			case '5':
				setList(majorSelector,group5000);
				break;
			case '9':
				setList(majorSelector,group9000);
				break;
			case 'E':
				setList(majorSelector,groupE000);
				break;
			case 'D':
				setList(majorSelector,groupD000);
				break;
	}

}

function setAllList(majorSelector)
{
	var majorSelector = document.getElementById("Major");
	majorSelector.options[0]=new Option('전체','',true,true);
	
	setList(majorSelector,group7000);
	setList(majorSelector,group1000);
	setList(majorSelector,group6000);
	setList(majorSelector,group4000);
	setList(majorSelector,group5000);
	setList(majorSelector,group9000);
	setList(majorSelector,groupE000);
	setList(majorSelector,groupD000);
}


function setList(majorSelector,Items)
{
	var tmp = [];
	var current_length = majorSelector.options.length;

	for (var i=0;i<Items.length;i++)
	{
		if (Items[i])
		{
			   tmp = Items[i].split(':');
			   majorSelector.options[majorSelector.options.length]=new Option(tmp[1],tmp[0],true,true);
		}
	}
	majorSelector.selectedIndex=0;
}

function init()
{
	var grade = document.getElementById("Grade");
	var completion = document.getElementById("Completion");
	var day1 = document.getElementById("Day1");
	var day2 = document.getElementById("Day2");
	 var time1 = document.getElementById("Time1");
	 var time2 = document.getElementById("Time2");
	 var college = document.getElementById("College");
	 var name = document.getElementById("subject_name");
	 var prof = document.getElementById("Professor");

	 grade.selectedIndex=0;
	 completion.selectedIndex=0;
	 day1.selectedIndex=0;
	 day2.selectedIndex=0;
	 time1.selectedIndex=0;
	 time2.selectedIndex=0;
	 college.selectedIndex=0;
	 changeCollege();
	 name.value="";
	 prof.value="";

}
