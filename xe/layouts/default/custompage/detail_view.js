
$(document).ready(function() {
	
	$("#insert_review").click(function() {

		var action = $("#ReviewForm").attr('action');
		var form_data = {
				Id: $("#Id").val(),
				Lecture_Time: $("#Lecture_Time").val(),
				Lecture_Quality: $("#Lecture_Quality").val(),
				Lecture_Recommended: $("#Lecture_Recommended").val(),
				Homework_Amount: $("#Homework_Amount").val(),
				Exam_Difficulty: $("#Exam_Difficulty").val(),
				Opinion: $("#Opinion").val(),
				is_ajax: 1
		};
		$.ajax({
				type: "POST",
				url: action,
				data: form_data,
				success: function(response){
						alert("평가가 등록되었습니다");
						document.getElementById("radar_frame").contentDocument.location.reload(true);
						document.getElementById("review_list").contentDocument.location.reload(true);
						$("#Opinion").val('');
						$("#Lecture_Time").val(0);
						$("#Lecture_Quality").val(0);
						$("#Lecture_Recommended").val(0);
						$("#Homework_Amount").val(0);
						$("#Exam_Difficulty").val(0);
				}
		});
		return false;
	});
});
